angular.module('app.controllers')
   .controller('approvalDetailCtrl', ['$scope', '$stateParams', 'Constant', 'Requester', 'DownloadFile', function ($scope, $stateParams, Constant, Requester, DownloadFile) {
      $scope.$on("$ionicView.enter", function (event, data) {
         $scope.approvalId = $stateParams.approvalId;
         //提交审批
         $scope.select = {
            detail: {}
         };
         $scope.imgCount = 0;
         $scope.getApprovalManageDetail();
      });

      $scope.getNameWidth = function (nameStr) {
         var width = textWidth(nameStr, 16) + 30 + 'px';
         return width;
      };
      //查看文件
      $scope.openFile = function (url) {
         //  url = 'https://test17.xgenban.com/wmdp/comup/201805/18/19603499049049307.pdf';
         DownloadFile.readFile(url);
      };

      $scope.currentApprovalStatus = function (progress) {
         var result;
         result = progress == 'PASS' ? '通过' : progress == 'APPROVAL' ? '审批中' : progress == 'FAIL' ? '不通过' : '';
         return result;
      };


      //request--detail
      $scope.getApprovalManageDetail = function () {
         $scope.picUrls = [];
         $scope.docs = [];
         $scope.controls = [];
         $scope.inputs = [];//单行文本控件数组
         $scope.fulltexts = [];//多行文本控件数组
         $scope.dates = [];//日期控件数组
         $scope.dateRanges = [];//日期区间数组
         $scope.radios = [];//单选框控件数组
         $scope.checkboxes = [];//多选框控件数组
         $scope.timeList = [];//时间控件数组

         Requester.getApprovalManageDetail($scope.approvalId).then(function (data) {
            if (data.result) {
               $scope.select.detail = data.data;
               $scope.inputs = data.data.controls.inputs;
               $scope.fulltexts =  data.data.controls.fulltexts;
               $scope.dates = data.data.controls.dates;
               $scope.dateRanges = data.data.controls.dateRanges?data.data.controls.dateRanges:[];
               $scope.radios =  data.data.controls.radios;
               $scope.checkboxes =  data.data.controls.checkboxes;

               $scope.inputs.forEach(function(item){
                  $scope.controls.push({
                     name:item.name,
                     text:item.text,
                     type:1,
                     index:item.idx?item.idx:1
                  });
               });
               
               $scope.fulltexts.forEach(function(item){
                  $scope.controls.push({
                     name:item.name,
                     fulltext:item.fulltext,
                     type:2,
                     index:item.idx?item.idx:2
                  });
               });

               $scope.dates.forEach(function(item){
                  $scope.controls.push({
                     name:item.name,
                      date:item.date,
                     type:3,
                     index:item.idx?item.idx:3
                  });
               });

               $scope.dateRanges.forEach(function(item){
                  $scope.controls.push({
                     name:item.name,
                     startDate:item.startDate,
                     endDate:item.endDate,
                     type:4,
                     index:item.idx?item.idx:4
                  });
               });
               $scope.radios.forEach(function(item){
                  $scope.controls.push({
                     name:item.name,
                     checkOption:item.checkOption,
                     type:5,
                     index:item.idx?item.idx:5
                  });
               });
               $scope.checkboxes.forEach(function(item){
                  $scope.controls.push({
                     name:item.name,
                     checkOptions:item.checkOptions,
                     type:6,
                     index:item.idx?item.idx:6
                  });
               });
               
               $scope.timeList.forEach(function(item){
                  $scope.controls.push({
                     name:item.name,
                     time:item.time,
                     type:7,
                     index:item.idx?item.idx:7
                  });
               });



               for (var i = 0; i < data.data.attaches.length; i++) {
                  var url = data.data.attaches[i].attachUrl;
                  var fileType = url.slice(url.lastIndexOf('.') + 1);
                  if (fileType === 'png' || fileType === 'jpg' || fileType === 'jpeg' || fileType === 'bmp'|| fileType.length>10) {
                     $scope.picUrls.push({
                        src: url,
                        thumb: url
                     });
                  } else {
                     $scope.docs.push({
                        attachUrl: url,
                        attachName: data.data.attaches[i].attachName
                     });
                  }
               }
            } else {
               console.log('detail failed');
            }


         });
      };
   }]);