angular.module('app.controllers')
  .controller('classAppraisalDetailCtrl', ['$scope', '$ionicPopup', 'Constant', '$ionicLoading', '$stateParams', 'Requester', 'UserPreference', '$timeout', function ($scope, $ionicPopup, Constant, $ionicLoading, $stateParams, Requester, UserPreference, $timeout) {

    $scope.$on("$ionicView.beforeEnter", function (event, data) {

      $scope.isIos = ionic.Platform.isIOS();
      $scope.grades = UserPreference.getArray('VoteGrades');
      $scope.isIOS = ionic.Platform.isIOS()?true:false;
      $scope.gradeName = $scope.grades.length > 0 ? $scope.grades[0].name : '';
      console.log("name" + $scope.gradeName);
      $scope.allList = [];
      $scope.hasPassArr = [];
      $scope.unPassArr = [];
      $scope.hasSetVote = true;
      $scope.getGradeList();

      //获取评比维度

      $scope.gradeSelected = 0;
      $scope.voteTaskId = $stateParams.voteTaskId;
      $scope.title = $stateParams.title;
      $scope.page = 1;
      $scope.getVoteItem(); //评比维度   
      $scope.getGradeVoteResultList(); //刷新列表

    });

    //进入视图时
    $scope.$on("$ionicView.enter", function (event, data) {


    });

    //选择年级 
    $scope.switchGrade = function (t, index) {
      $scope.gradeSelected = index;
      $scope.gradeName = t.name;
      $scope.hasPassArr = [];
      $scope.unPassArr = [];
      $scope.getGradeVoteResultList();

    };


    //***** vote result request */

    //获取年级列表
    $scope.getGradeList = function () {

      Requester.getGradeList().then(function (resp) {
        if (resp.result) {
          $scope.grades = resp.data;
         
          $scope.gradeName = $scope.grades[0].name;
          UserPreference.setObject('VoteGrades', $scope.grades);
        } else {
          $scope.grades = [];
        }
      });
    };

    //获取评比结果列表
    $scope.getGradeVoteResultList = function () {
      $scope.hasPassArr = [];
      $scope.unPassArr = [];
      Requester.getClassVoteReultList($scope.voteTaskId, $scope.gradeName, $scope.page).then(function (resp) {
        if (resp.result) {
         
          $scope.allList = resp.data;
          if (resp.data) {
            resp.data.forEach(function (item) {
              if (item.voteFinished) {
                $scope.hasPassArr.push(item);
              } else {
                $scope.unPassArr.push(item);
              }
            });
          }

        }

      }).finally(function () {

      });
    };

    //查询评比维度 
    $scope.getVoteItem = function () {
      $scope.voteItems = []; //评比维度
      Requester.selectVoteItem().then(function (resp) {

        $scope.hasSetVote = resp.result;
        $scope.voteItems = resp.data;
        return resp.data;
      }).then(function (result) {
        if (result) {
          $scope.voteItems.forEach(function (res) {
            res.value = null;
          });
        } else {
          //toaster.warning({ title: "温馨提示", body: "当前还未设置评比维度，请登录管理后台处理!" });
        }
      });
    };
    //修改或新增评分
    $scope.fixOrAddMarkRequest = function (tag, classId, classvoteDetailId, classVoteItem) {
      if (tag == 1) {
        Requester.fixClassScoreResult(classId, classvoteDetailId, classVoteItem).then(function (resp) {
          console.log(resp.data);
          if (resp.result) {
            
            $scope.getGradeVoteResultList(); //刷新列表
          }else{
          }
        });
      } else {
        Requester.addClassScoreResult(classId, classvoteDetailId, classVoteItem).then(function (resp) {
          if (resp.result) {
           
            console.log(resp.data);
            $scope.getGradeVoteResultList(); //刷新列表
          }
        });
      }

    };


    //***** request end */


    //添加平分或者修改平分
    $scope.addGradedOrFix = function (tag, item) {
      console.log(item);
      // $scope.MyFunction = function() {
      //   return true;
      // };

      if (!$scope.data)
        $scope.data = {
          health: null,
          discipline: null,
          attendance: null,
          example1: null,
          example2: null
        };
      var arrLength = item.voteItemScore == null ? 0 : item.voteItemScore.length;
      //$scope.voteItems = [];
      if (item.voteItemScore != null) {
        $scope.voteItems = item.voteItemScore;
      } else {

      }

     $scope.btnStatus = '';
     var UIPath = '';
     if (Constant.debugMode) UIPath = 'page/';
      var myPopup = $ionicPopup.show({
        templateUrl:UIPath+ 'home/classAppraisal/classAppraisalAlert.html',
       //template: '<div ng-repeat="voteItem in voteItems"><label class="item-input" style="padding-left:0px !important;"> <span class="input-label" style="width:70px;text-align:right;">' + '{{voteItem.name}}' + ':</span><input type="number"  pattern="[0-9]*" oninput="if(value.length>3)value=value.slice(0,3)" style="border:1px solid #CFCFCF;padding-left:4px;padding-right:5px;height:30px;"  placeholder="1-100" ng-model="voteItem.value" autofocus><span style="margin-left:10px;margin-right:40px;">分</span></label></div>',
        title: '<div> <img src="img/classAppraisal/left.png" /><span>' + item.className + '</span><img src="img/classAppraisal/right.png" /></div>',
        scope: $scope,
        cssClass: 'dateSelectAlert',
        buttons: [{
            text: '取消',
            type: 'button-cancel'
          },
          {
            text: '<b>确定</b>',
            type: 'button-xgreen',
            onTap: function (e) {
              console.log($scope.voteItems);
              for (var i = 0; i < $scope.voteItems.length; i++) {
                if (  $scope.voteItems[i].value == null || $scope.voteItems[i].value == undefined) {
                  $ionicLoading.show({
                    template: '评分不能有空值',
                    duration: 1500
                  });
                  e.preventDefault();
                  break;
                }
                if (!judgeIsInteger($scope.voteItems[i].value)) {
                  $ionicLoading.show({
                    template: '请保持评分在1-100的整数',
                    duration: 1500
                  });
                  e.preventDefault();
                  break;
                }
              }
              var classVoteItem = [];
              for (var j = 0; j < $scope.voteItems.length; j++) {
                classVoteItem.push({
                  id: $scope.voteItems[j].id,
                  score: $scope.voteItems[j].value
                });
              }

              return classVoteItem;

            }
          }
        ]
      });
    
      myPopup.then(function (classVoteItem) {
        //请求
        console.log(classVoteItem);
        if (classVoteItem)
          $scope.fixOrAddMarkRequest(tag, item.classId, item.id, classVoteItem);
      });

    };

    $scope.getDeviceType = function () {
      var result = 'android';
      $scope.isIos = ionic.Platform.isIOS() ;
      if ($scope.isIos) {
          if (window.screen.width >= 375 && window.screen.height >= 812) {
              result = 'iphoneX';
          } else {
              result = 'iphone';
          }

      } else {
       result = 'android';
      }
      return result;
  };

  }]);