angular.module('app.controllers')
.controller('repairProgressCtrl',['$scope', 'Requester', '$stateParams', '$ionicHistory', 'toaster', function($scope,Requester,$stateParams, $ionicHistory,toaster){
    $scope.$on("$ionicView.beforeEnter", function (event, data){
       
        $scope.repairRecordId = $stateParams.repairRecordId;
        $scope.isNotice = $stateParams.manufacturersContactId?true:false;//是否已经通知过售后了
        console.log('$scope.isNotice:'+$scope.isNotice+'manufacturersContactId:'+ $stateParams.manufacturersContactId);
        $scope.isAssociated  = $stateParams.isAssociated;//关联的厂家id
        //进度状态
        $scope.progressStatues = [
            {key:1,value:'已经联系售后了'},
            {key:2,value:'今天会解决'},
            {key:3,value:'马上过来修'},
            {key:4,value:'设备没坏不用修'},
            {key:5,value:'设备修好了'},
            {key:6,value:'设备报废了'},
        ];
        $scope.closedNum =  Math.pow(2,30);
        //提交进度信息
        $scope.select = {
          key:0,
          status :$scope.isNotice===true?8:4,
          content:'',
          manufacturersContactId: $stateParams.manufacturersId ?$stateParams.manufacturersId :'',
          saleMerberId: $scope.isAssociated?$scope.isAssociated:$stateParams.manufacturersContactId,
        };
        // console.log($scope.select);
        $scope.saleMerbers = [];
        if($scope.select.manufacturersContactId){
            //获取售后人员
            $scope.getManufacturesMermberList();
        }      
        $scope.getManufacturesList();
    
       //$scope.select.vendorId = $scope.vendorList[1].id;
       
     });

     $scope.changeSelectStatus = function(status){
        $scope.select.status = status;
     };
     $scope.loadData = function(){
        //  console.log('--load data');
        $scope.saleMerbers = [];
        $scope.select.saleMerberId = '';
        if($scope.select.manufacturersContactId)
        $scope.getManufacturesMermberList();
     };
     $scope.loadData2 = function(){
       
     };
     //确定
     $scope.confirm = function(){
        
        if($scope.select.status!==8)$scope.select.saleMerberId = null;
         Requester.addRepairProgress($scope.repairRecordId, $scope.select.status,$scope.select.content, $scope.select.saleMerberId).then(function(res){
            if(res.result){ 
                if($scope.select.status== Math.pow(2,30)){
                    toaster.success({title: "关闭成功", body: "该报修记录成功关闭",timeout:3000});
                }else{
                    toaster.success({title: "更新成功", body: "该报修记录更新进度成功",timeout:3000});
                }
                $ionicHistory.goBack();
                $scope.select = {
                    key:0,
                    status :4,
                    content:''
                  };
            }else{

            }
         });
     };

     //获取厂家列表
     $scope.getManufacturesList  = function(){
        Requester.getManufacturesList().then(function(resp){
            if(resp.result){
                //$scope.manufacturersList = resp.data;
                if(resp.data&&resp.data.length>0){
                    $scope.manufacturersList =  resp.data;
                    
                }
            }else{
                toaster.warning({
                    title: "温馨提示",
                    body: resp.message
                });
            }

        }) ;
     };

     //获取售后人员列表
     $scope.getManufacturesMermberList = function(){
        Requester.getManufacturesMermberList($scope.select.manufacturersContactId).then(function(resp){
            if(resp.result){
                console.log(resp.data);
                $scope.saleMerbers = resp.data;
            }else{
                toaster.warning({
                    title: "温馨提示",
                    body: resp.message
                });
            }
        });
     };
     


   
}]);