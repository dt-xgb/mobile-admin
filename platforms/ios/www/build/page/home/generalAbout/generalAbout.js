angular.module('app.controllers')
.controller('generalAboutCtrl', ['$scope', 'Constant', 'UserPreference', '$state', '$ionicHistory', function ($scope, Constant, UserPreference, $state, $ionicHistory) {

    $scope.userRole = UserPreference.getObject('user').role;
    if (String($scope.userRole) === Constant.USER_ROLES.SCHOOL_ADMIN) {
        $scope.schoolAdmin = true;
    }

    $scope.goBack = function () {
        if ($ionicHistory.backView())
            $ionicHistory.goBack();
        else
            $state.go('tabsController.mainPage');
    };

}]);