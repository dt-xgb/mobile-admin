angular.module('app.controllers')
.controller('editProfileCtrl', ['$scope', 'Constant', '$ionicHistory', '$stateParams', 'UserPreference', 'SettingService', 'BroadCast', 'toaster', 'MESSAGES', '$ionicLoading', '$state', 'Requester', 'CameraHelper', function ($scope, Constant, $ionicHistory, $stateParams, UserPreference, SettingService, BroadCast, toaster, MESSAGES, $ionicLoading, $state, Requester, CameraHelper) {
    $scope.visibleArea = $stateParams.content;
    $scope.user = UserPreference.getObject("user");
    $scope.defaultChildID = UserPreference.get('DefaultChildID');
    switch ($scope.visibleArea) {
        case 'name':
            $scope.pageTitle = "修改姓名";
            break;
        case 'password':
            $scope.pageTitle = "修改密码";
            break;
        case 'children':
            $scope.pageTitle = "子女信息";
            break;
    }

    $scope.pwd = {
        _old: '',
        _new: '',
        _confirm: ''
    };
    $scope.save = function () {
        switch ($scope.visibleArea) {
            case 'name':
                SettingService.editInfo('name', $scope.user.name);
                break;
            case 'password':
                var reg = /^[a-zA-Z0-9]+$/;
                if (!$scope.pwd._old.match(reg) || !$scope.pwd._new.match(reg)) {
                    toaster.warning({title: MESSAGES.REMIND, body: MESSAGES.LOGIN_PASSWORD_PATTERN});
                    return;
                }
                if ($scope.pwd._new != $scope.pwd._confirm) {
                    toaster.warning({title: MESSAGES.REMIND, body: MESSAGES.PASSWORD_CONFIRM_ERROR});
                    return;
                }
                SettingService.changePwd($scope.pwd._old, $scope.pwd._new);
                break;
        }
        $ionicLoading.show({
            noBackdrop: true,
            template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
        });
    };

    $scope.setDefaultChild = function (child) {
        $scope.defaultChildID = child.id;
        UserPreference.set('DefaultChildID', child.id);
        UserPreference.set('DefaultClassID', child.classno.id);
        UserPreference.set('DefaultChildName', child.student_name);
        UserPreference.set('DefaultSchoolName', child.school.schoolName);
        UserPreference.set('DefaultSchoolID', child.school.id);
    };

    $scope.goBack = function () {
        $ionicHistory.nextViewOptions({
            disableBack: true
        });
        if ($scope.visibleArea === 'children') {
            $state.go('tabsController.settingPage');
        }
        else {
            $state.go('user_profile');
        }
    };

    $scope.selectImage = function (child) {
        $scope.toChangeChild = child;
        CameraHelper.selectImage('av', {allowEdit: true});
    };

    $scope.$on(BroadCast.IMAGE_SELECTED, function (a, rst) {
        if (rst && rst.which === 'av') {
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
            Requester.modifyStuInfo($scope.toChangeChild.id, 'logo', rst.source.substr(rst.source.indexOf('base64,') + 7)).then(function (resp) {
                if (resp.result) {
                    toaster.success({title: '修改成功'});
                    $scope.toChangeChild.logo = rst.source;
                    for (var i = 0; i < $scope.user.student; i++) {
                        if ($scope.user.student[i].id == $scope.toChangeChild.id) {
                            $scope.user.student[i] = $scope.toChangeChild;
                            UserPreference.setObject('user', user);
                            break;
                        }
                    }
                }
            }).finally(function () {
                $ionicLoading.hide();
            });
        }
    });

    $scope.$on(BroadCast.EDIT_INFO, function (a, rst) {
        if (rst && rst.result) {
            toaster.success({title: MESSAGES.SAVE_SUCCESS, body: MESSAGES.INFO_UPDATED});
            $scope.goBack();
        }
        else
            toaster.error({title: MESSAGES.REMIND, body: rst.message});
        $ionicLoading.hide();
    });

    $scope.$on(BroadCast.PASSWORD_CHANGE, function (a, rst) {
        if (rst && rst.result) {
            toaster.success({title: MESSAGES.REMIND, body: MESSAGES.PWD_UPDATED});
            $scope.goBack();
        }
        else
            toaster.error({title: MESSAGES.REMIND, body: rst.message});
        $ionicLoading.hide();
    });

    $scope.$on(BroadCast.CONNECT_ERROR, function (arg, data) {
        $ionicLoading.hide();
    });
}]);