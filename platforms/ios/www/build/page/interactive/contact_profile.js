angular.module('app.controllers')
.controller('contactProfileCtrl', ['$scope', '$ionicHistory', '$stateParams', '$state', 'ChatService', 'Constant', function ($scope, $ionicHistory, $stateParams, $state, ChatService, Constant) {
    $scope.goBack = function () {
        if ($ionicHistory.backView())
            $ionicHistory.goBack();
        else
            $state.go('tabsController.mainPage');
    };
    function getUserRoleCN(role) {
        var arr = {};
        arr.school_admin = '学校管理员';
        arr['3'] = '老师';
        arr['4'] = '家长';
        arr['2'] = '学生';
        return arr[role];
    }

    $scope.$on('$ionicView.enter', function () {
        $scope.SessionId = $stateParams.obj.SessionId;
        $scope.showSendBtn = !$stateParams.obj.self;
        ChatService.getUserDetail($scope.SessionId).then(
            function (data) {
                var user = data.data;
                $scope.SessionNick = user.name;
                $scope.SessionImage = user.logo;
                if (!$scope.SessionImage)
                    $scope.SessionImage = Constant.IM_USER_AVATAR;
                $scope.phone = user.phoneNumber;
                $scope.role = user.rolename;
                $scope.rolename = getUserRoleCN($scope.role);
                if (user.sex) {
                    if (user.sex.toUpperCase() === 'MALE')
                        $scope.sex = '男';
                    else if (user.sex.toUpperCase() === 'FEMALE')
                        $scope.sex = '女';
                }
                else
                    $scope.sex = '';
                $scope.children = user.student;
                $scope.subjects = user.subjects;
                $scope.classes = user.classNoList;
                $scope.families = user.familys;
                console.log(user);
            }
        );

    });

    $scope.goChat = function () {
        var opt = {
            SessionId: $scope.SessionId,
            SessionNick: $scope.SessionNick,
            SessionImage: $scope.SessionImage,
            SessionType: webim.SESSION_TYPE.C2C
        };
        $state.go('communicate_detail', {obj: opt});
    };

}]);
