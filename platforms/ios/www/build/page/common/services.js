angular.module('app.services', [])
    .factory('UserPreference', ['$window', function ($window) {
        return {
            set: function (key, value) {
                $window.localStorage[key] = value;
            },
            get: function (key, defaultValue) {
                return $window.localStorage[key] || defaultValue;
            },
            setObject: function (key, value) {
                $window.localStorage[key] = JSON.stringify(value);
            },
            getObject: function (key) {
                return JSON.parse($window.localStorage[key] || '{}');
            },
            getArray: function (key) {
                return JSON.parse($window.localStorage[key] || '[]');
            },
            getBoolean: function (key) {
                return $window.localStorage[key] === true || $window.localStorage[key] === 'true';
            },
            clear: function () {
                $window.localStorage.clear();
            }
        };
    }])
    .service('scrollDelegateFix', ['$ionicScrollDelegate', function ($ionicScrollDelegate) {
        //fix a $ionicScrollDelegate bug in ionic v1,which exists in modals
        return {
            $getByHandle: function (name) {
                var instances = $ionicScrollDelegate.$getByHandle(name)._instances;
                return instances.filter(function (element) {
                    //return (element['$$delegateHandle'] === name);
                    return element.$$delegateHandle === name;
                })[0];
            }
        };
    }])
    .factory('CameraHelper', ['$q', '$ionicPopup', '$ionicActionSheet', '$rootScope', 'BroadCast', '$cordovaCamera', '$jrCrop', 'Constant', function ($q, $ionicPopup, $ionicActionSheet, $rootScope, BroadCast, $cordovaCamera, $jrCrop, Constant) {
        var helper = {};
        helper.cropImage = function (imageURI, width, height, cropTitle) {
            $jrCrop.crop({
                url: imageURI,
                width: width,
                height: height,
                title: cropTitle,
                cancelText: '取消',
                chooseText: '确定'
            }).then(function (canvas) {
                // success!
                $rootScope.$broadcast(BroadCast.IMAGE_CROP, canvas.toDataURL());
            }, function () {
                // User canceled or couldn't load image.
                $rootScope.$broadcast(BroadCast.IMAGE_CROP, undefined);
            });
        };
        helper.selectImage = function (which, imgOpt) {
            if (!navigator.camera) {
                alert("Camera API not supported");
                return;
            }
            $ionicActionSheet.show({
                buttons: [{
                    text: '<i class="icon ion-camera"></i>相机'
                },
                {
                    text: '<i class="icon ion-ios-photos"></i>相册'
                }
                ],
                titleText: '请选择图片来源(单张图片大小不得超过10M)',
                cancelText: '取消',
                cancel: function () {
                    // add cancel code..
                },
                buttonClicked: function (index) {
                    var srcType = Camera.PictureSourceType.SAVEDPHOTOALBUM;
                    if (index === 0) {
                        srcType = Camera.PictureSourceType.CAMERA;
                    }
                    var destinationType = Camera.DestinationType.DATA_URL;
                    if ((imgOpt && imgOpt.allowEdit) || which == 'chat') {
                        destinationType = Camera.DestinationType.FILE_URI;
                    }
                    if (index === 0 || index === 1) {
                        var defaultHeight = 300;
                        var defaultWidth = 300;
                        var cropTitle = '截取高亮区域';
                        var options = {
                            quality: which === 'portrait' ? 95 : 85,
                            destinationType: destinationType,
                            sourceType: srcType,
                            allowEdit: false,
                            encodingType: Camera.EncodingType.JPEG,
                            targetWidth: Constant.CAPTURE_IMAGE_RANGE,
                            targetHeight: Constant.CAPTURE_IMAGE_RANGE,
                            correctOrientation: true,
                            popoverOptions: CameraPopoverOptions,
                            saveToPhotoAlbum: false
                        };
                        if (imgOpt) {
                            if (imgOpt.width && imgOpt.height) {
                                defaultHeight = imgOpt.height;
                                defaultWidth = imgOpt.width;
                            } else {
                                options.targetWidth = defaultWidth * 2;
                                options.targetHeight = defaultHeight * 2;
                            }
                            if (imgOpt.title)
                                cropTitle = imgOpt.title;
                        }
                        console.log(imgOpt);
                        $cordovaCamera.getPicture(options).then(function (imageURI) {

                            if (imgOpt && imgOpt.allowEdit) {
                                $jrCrop.crop({
                                    url: imageURI,
                                    width: defaultWidth,
                                    height: defaultHeight,
                                    title: cropTitle,
                                    cancelText: '取消',
                                    chooseText: '确定'
                                }).then(function (canvas) {
                                    // success!
                                    var data = {
                                        which: which,
                                        source: canvas.toDataURL()
                                    };
                                    $rootScope.$broadcast(BroadCast.IMAGE_SELECTED, data);
                                }, function () {
                                    // User canceled or couldn't load image.
                                    $rootScope.$broadcast(BroadCast.IMAGE_SELECTED, undefined);
                                });
                            } else {
                                if (which == 'chat') {
                                    window.resolveLocalFileSystemURL(imageURI, function (fileEntry) {
                                        fileEntry.file(function (fileObj) {
                                            var data = {
                                                which: which,
                                                source: fileObj
                                            };
                                            $rootScope.$broadcast(BroadCast.IMAGE_SELECTED, data);
                                        });
                                    });
                                } else {
                                    var data = {
                                        which: which,
                                        source: "data:image/jpeg;base64," + imageURI
                                    };
                                    $rootScope.$broadcast(BroadCast.IMAGE_SELECTED, data);
                                }
                            }

                        }, function (err) {
                            $rootScope.$broadcast(BroadCast.IMAGE_SELECTED, undefined);
                            //alert("Read Photo Error: " + err);
                        });
                    }
                    return true;
                }
            });
        };
        helper.selectMultiImage = function (callback, limit) {
            $ionicActionSheet.show({
                buttons: [{
                    text: '<i class="icon ion-camera"></i>相机'
                },
                {
                    text: '<i class="icon ion-ios-photos"></i>相册'
                }
                ],
                titleText: '请选择图片来源(单张图片大小不得超过10M)',
                cancelText: '取消',
                cancel: function () {
                    // add cancel code..
                },
                buttonClicked: function (index) {
                    if (index === 0) {
                        var options = {
                            quality: 85,
                            destinationType: Camera.DestinationType.DATA_URL,
                            sourceType: Camera.PictureSourceType.CAMERA,
                            allowEdit: false,
                            encodingType: Camera.EncodingType.JPEG,
                            targetWidth: Constant.CAPTURE_IMAGE_RANGE,
                            targetHeight: Constant.CAPTURE_IMAGE_RANGE,
                            correctOrientation: true,
                            saveToPhotoAlbum: false
                        };
                        $cordovaCamera.getPicture(options).then(function (imageURI) {
                            callback(imageURI);
                        }, function (err) {
                            console.log(err);
                        });
                    } else if (index === 1) {
                        window.imagePicker.getPictures(
                            function (results) {
                                callback(results);
                            },
                            function (error) {
                                console.log('Error: ' + error);
                            }, {
                            maximumImagesCount: limit,
                            width: Constant.CAPTURE_IMAGE_RANGE
                        }
                        );
                    }
                    return true;
                }
            });
        };
        return helper;
    }])
    .factory('httpInterceptor', ['$q', '$injector', function ($q, $injector) {
        var regex = new RegExp('\.(html|js|css)$', 'i');
        var isAsset = function (url) {
            return regex.test(url);
        };
        var doLogin = function () {
            var state = $injector.get('$state');
            var user = $injector.get('UserPreference');
            var auth = $injector.get('AuthorizeService');
            if (!auth.logining && state.current.url !== '/login') {
                var loginModel = {
                    remember: true,
                    username: user.get('username', ''),
                    password: user.get('password', '')
                };
                if (loginModel.username !== '' && loginModel.password !== '') {
                    console.error('timeout, try auto login.');
                    auth.login(loginModel).then(function (res) {
                        if (res && res.result)
                            state.go('tabsController.mainPage');
                    });
                } else {
                    console.error('username or password not found');
                    state.go('login');
                }
            }
        };
        return {
            // optional method
            'request': function (config) {
                // do something on success
                //if(!isAsset(config.url)){            //if the call is not for an asset file
                //	config.url+= "?ts=" +  Date.now();     //append the timestamp
                //}

                if (!isAsset(config.url)) {
                    var Constant = $injector.get('Constant');
                    if (config.url.indexOf('login') > 0 || config.url.indexOf('logout') > 0)
                        config.timeout = Constant.serverTimeout;
                    else
                        config.timeout = Constant.heavyServerTimeout;
                }
                return config;
            },
            // optional method
            'requestError': function (rejection) {
                // do something on error

                return $q.reject(rejection);
            },
            // optional method
            'response': function (response) {
                // do something on success
                if (!isAsset(response.config.url)) {
                    if (response.data.code === -100) {
                        doLogin();
                    }
                }
                return response;
            },
            // optional method
            'responseError': function (rejection) {
                if (!isAsset(rejection.config.url)) {
                    var toaster = $injector.get('toaster');
                    var MESSAGES = $injector.get('MESSAGES');
                    var rootScope = $injector.get('$rootScope');
                    var BroadCast = $injector.get('BroadCast');
                    // console.log(' response ---:' + rejection.status);
                    rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    switch (rejection.status) {
                        case -1:
                        case 408:
                            toaster.warning({
                                title: MESSAGES.REQUEST_ERROR,
                                body: MESSAGES.CONNECT_TIMEOUT_MSG
                            });
                            break;
                        case 401:
                            doLogin();
                            break;
                        case 404:
                            toaster.error({
                                title: MESSAGES.CONNECT_ERROR,
                                body: rejection.data.message
                            });
                            break;
                        case 500:
                            toaster.error({
                                title: MESSAGES.CONNECT_ERROR,
                                body: MESSAGES.SERVER_ERROR
                            });
                            break;
                        case 503:
                            toaster.error({
                                title: rejection.data.message,
                                body: ''
                            });
                            break;
                        default:
                            toaster.error({
                                title: MESSAGES.CONNECT_ERROR,
                                body: MESSAGES.CONNECT_ERROR_MSG
                            });
                            break;
                    }
                }
                return $q.reject(rejection);
            }
        };
    }])
    .service('closePopupService', [
        function () {
            var currentPopup;
            var htmlEl = angular.element(document.querySelector('html'));
            htmlEl.on('click', function (event) {
                if (event.target.nodeName === 'HTML') {
                    if (currentPopup) {
                        currentPopup.close();
                    }
                }
            });

            this.register = function (popup) {
                currentPopup = popup;
            };
        }
    ])
    .factory('PushService', ['$http', '$window', 'Constant', '$state', '$ionicPopup', '$rootScope', 'BroadCast', function ($http, $window, Constant, $state, $ionicPopup, $rootScope, BroadCast) {
        var push = {};
        //启动极光推送
        push.init = function (config) {
            var setTagsWithAliasCallback = function (event) {
                console.log(event);
                console.log("Broadcast Rev: Jpush tag and alias settled");
            };
            var receiveNotificationCallback = function (event) {
                console.log(event);
                $rootScope.$broadcast(BroadCast.NEW_PUSH_REV, undefined);
            };
            var openNotificationCallback = function (event) {
                var notice;

                if (device.platform === "Android") {
                    notice = event.extras;

                } else {
                    notice = event;
                    push.setBadgeNumber();
                }
                //    console.log('event notice');
                //    console.log(notice);
                push.clearAllNotification();
                switch (String(notice.messageType)) {
                    case '1':
                    case '2':
                        notice.imageUrls = notice.imageUrls.split(',');
                        $state.go('news_detail', {
                            notice: notice,
                            index: notice.id
                        });
                        break;
                    case '10000':
                        $state.go('tabsController.communicatePage');
                        break;
                    case '100101':
                        //用户反馈
                        $state.go('suggest');
                        break;
                    case '100104':
                        //异常处理 
                        $state.go('abnormalHandle', {
                            notice: notice
                        });
                        break;
                    case '100105':
                        //报修记录 管理员接收
                        $state.go('repairDetail', {
                            repairRecordId: notice.orderId,
                            readStatus: false
                        });
                        break;

                    case '100107':
                        //审批通过 通知
                        $state.go('assetsCheckDetail', {
                            assetsId: notice.orderId,
                            progressStatus: 1
                        });
                        break;

                    case '100111':
                        $state.go('approvalDetail', {
                            archiveId: notice.archiveId
                        });
                        break;


                    case '373373':
                        $state.go('covidDailySummery');
                        break;



                    default:
                        //$state.go('abnormalHandle',{notice: {extras:{createTime:'2018-07-17 10:47',text:'哈哈哈nishi66',title:'标题'}}});  

                        break;
                }
            };

            //  $window.plugins.jPushPlugin.init();
            $window.plugins.jPushPlugin.init();
            push.clearAllNotification();
            //设置tag和Alias触发事件处理
            document.addEventListener('jpush.setTagsWithAlias', setTagsWithAliasCallback, false);
            //打开推送消息事件处理
            document.addEventListener("jpush.openNotification", openNotificationCallback, false);
            document.addEventListener("jpush.receiveNotification", receiveNotificationCallback, false);
            if (ionic.Platform.isIOS()) {
                document.addEventListener("jpush.backgroundNotification", receiveNotificationCallback, false);
                push.setBadgeNumber();
            } else {
                $window.plugins.jPushPlugin.setLatestNotificationNum(2);
            }
            document.addEventListener("pause", function () {
                console.log('app pause');
                push.appBackground = true;
                push.clearAllNotification();
            }, false);
            document.addEventListener("resume", function () {
                console.log('app resume');
                push.appBackground = false;
                push.clearAllNotification();
            }, false);
            $window.plugins.jPushPlugin.setDebugMode(Constant.debugMode);
            $window.plugins.jPushPlugin.setSilenceTime(22, 0, 7, 0);
            push.getRegID();
        };

        //获取状态
        push.isPushStopped = function (fun) {
            $window.plugins.jPushPlugin.isPushStopped(fun);
        };
        //停止极光推送
        push.stopPush = function () {
            $window.plugins.jPushPlugin.stopPush();
        };

        //清除通知
        push.clearAllNotification = function () {
            if (ionic.Platform.isIOS())
                $window.plugins.jPushPlugin.clearAllLocalNotifications();
            else
                $window.plugins.jPushPlugin.clearAllNotification();
        };

        push.getRegID = function () {
            setTimeout(function () {
                $window.plugins.jPushPlugin.getRegistrationID(function (data) {
                    console.log("JPushPlugin:registrationID is " + data);
                    if (data) {
                        push.regID = data;
                    } else {
                        push.getRegID();
                    }
                });
            }, 2000);
        };

        //重启极光推送
        push.resumePush = function () {
            $window.plugins.jPushPlugin.resumePush();
        };

        //设置标签和别名
        push.setTagsWithAlias = function (tags, alias) {

            // $window.plugins.jPushPlugin.setTagsWithAlias(tags, alias, function () {
            //     console.log("Jpush Tag and Alias settled");
            // }, function (msg) {
            //     console.log("Jpush Tag and Alias set failed " + msg);
            //     setTimeout(function () {
            //         push.setTagsWithAlias(tags, alias);
            //     }, 5000);
            // });
            push.setAlias(alias);
            push.setTags(tags);
        };

        //设置标签
        push.setTags = function (tags) {
            //   $window.plugins.jPushPlugin.setTags(tags);
            console.log('is set tags');
            $window.plugins.jPushPlugin.setTags({
                sequence: 1,
                tags: tags
            }, function (res) {
                console.log('set tags succeed');
            }, function (err) {
                console.log('set tags failure');
            });

        };

        //设置别名
        push.setAlias = function (alias) {
            console.log('is set alias ');
            $window.plugins.jPushPlugin.setAlias({
                sequence: 1,
                alias: alias
            }, function (res) {
                console.log('set alias succeed');
            }, function (err) {
                console.log('set alias faliure');
                console.log(err);
            });
            console.log('is set alias 222');
        };

        //删除别名
        push.deleteAlias = function () {
            $window.plugins.jPushPlugin.deleteAlias({
                sequence: 1
            },
                function (result) {
                    var sequence = result.sequence;
                },
                function (error) {
                    var sequence = error.sequence;
                    var errorCode = error.code;
                });
        };

        push.setAndroidPushTime = function (days, startHour, endHour) {
            $window.plugins.jPushPlugin.setPushTime(days, startHour, endHour);
        };

        push.setAndroidSilenceTime = function (startHour, startMinute, endHour, endMinute) {
            $window.plugins.jPushPlugin.setSilenceTime(startHour, startMinute, endHour, endMinute);
        };

        push.getUserNotificationSettings = function (fun) {
            $window.plugins.jPushPlugin.getUserNotificationSettings(fun);
        };

        push.setBasicNotification = function (type) {
            $window.plugins.jPushPlugin.setBasicPushNotificationBuilder(type);
        };

        push.setBadgeNumber = function () {
            $window.plugins.jPushPlugin.getApplicationIconBadgeNumber(function (data) {
                if (data > 0)
                    $window.plugins.jPushPlugin.setApplicationIconBadgeNumber(0);
            });
        };

        return push;
    }])

    //结构待优化


    .factory('AuthorizeService', ['$http', 'Constant', '$rootScope', 'BroadCast', 'UserPreference', 'PushService', '$ionicPopup', '$state', '$q', '$timeout', '$injector', function ($http, Constant, $rootScope, BroadCast, UserPreference, PushService, $ionicPopup, $state, $q, $timeout, $injector) {
        var auth = {};


        auth.login = function (user,success,failure) {
            if (auth.logining) {
                console.log('request is processing.');
                return;
            }
            auth.logining = true;
            if (iOSDevice()) {
                var defer = $q.defer();
                var tool = $injector.get('SavePhotoTool');
                cordova.plugin.http.clearCookies();
                cordova.plugin.http.setRequestTimeout(15.0);
                // UserPreference.set("IOSCookies",cookies);
                //  cordova.plugin.http.setCookie(Constant.ServerUrl + "/login/manage",cookies);
                cordova.plugin.http.setDataSerializer('json');
                var options = {
                    loginname: user.username.trim(),
                    password: md5(user.password).toUpperCase()
                };
                console.log('====login model start===');
                console.log(options);
                console.log('====login model end===');
                var header = { "Content-Type": 'application/json' };

                cordova.plugin.http.post(Constant.ServerUrl + "/login/manage",
                    options, header, function (response) {
                        var cookies = cordova.plugin.http.getCookieString(Constant.ServerUrl + "/login/manage");
                        if (cookies) {
                            UserPreference.set("IOSCookies", cookies);
                            cordova.plugin.http.setCookie(Constant.ServerUrl + "/login/manage", cookies);
                        }
                        try {
                            auth.logining = false;
                            var data = JSON.parse(response.data);
                           // console.log(data);
                            if (data.result) {
                                var tags = [];
                                if (data.data.sex && data.data.sex !== '')
                                    tags.push(data.data.sex);
                                tags.push("JS_" + data.data.rolename.toUpperCase());
                                if (data.data.school) {
                                    tags.push("XX" + data.data.school.id);

                                    UserPreference.set('DefaultSchoolID', data.data.school.id);
                                    UserPreference.set('DefaultSchoolName', data.data.school.schoolName);
                                }
                                if (data.data.area) {
                                    var area = data.data.area.id;
                                    tags.push("SF" + area.substr(0, 2) + "0000");
                                    tags.push("CS" + area.substr(0, 4) + "00");
                                    tags.push("DQ" + area.substr(0, 6));
                                }
                                UserPreference.setObject("user", data.data);
                                auth.isLogin = true;
                                setTimeout(function () {
                                    if (window.cordova) {
                                        MobclickAgent.onEvent('app_login');
                                        console.log('to login ');
                                        PushService.setTagsWithAlias(tags, user.username.toLowerCase());
                                    }
                                    if (Constant.debugMode) console.log(tags);
                                }, 5000);
                            } else {
                                UserPreference.set("password", '');
                                if ($state.current.url !== '/login')
                                    $state.go('login');
                            }
                            // $timeout(function () {
                               
                            // });
                            success(data);
                            return defer.resolve(data);

                        } catch (error) {
                            auth.logining = false;
                            console.error("JSON parsing error");
                        }


                    }, function (error) {
                        auth.logining = false;
                        if (error.status) {
                            tool.interceptors(error.status);
                        }
                        failure(error);
                        return  defer.reject(error);
                    });
              
                return defer.promise;
            } else {
                console.log('login start');
                return $http.post(Constant.ServerUrl + "/login/manage", {
                    loginname: user.username.trim(),
                    password: md5(user.password).toUpperCase()
                }).then(function (response) {
                    //cache user
                    console.log('login result');
                    var data = response.data;
                    if (data.result) {
                        var tags = [];
                        if (data.data.sex && data.data.sex !== '')
                            tags.push(data.data.sex);
                        tags.push("JS_" + data.data.rolename.toUpperCase());
                        if (data.data.school) {
                            tags.push("XX" + data.data.school.id);

                            UserPreference.set('DefaultSchoolID', data.data.school.id);
                            UserPreference.set('DefaultSchoolName', data.data.school.schoolName);
                        }
                        if (data.data.area) {
                            var area = data.data.area.id;
                            tags.push("SF" + area.substr(0, 2) + "0000");
                            tags.push("CS" + area.substr(0, 4) + "00");
                            tags.push("DQ" + area.substr(0, 6));
                        }
                        UserPreference.setObject("user", data.data);
                        auth.isLogin = true;
                        setTimeout(function () {
                            if (window.cordova) {
                                MobclickAgent.onEvent('app_login');
                                console.log('to login ');
                                PushService.setTagsWithAlias(tags, user.username.toLowerCase());
                            }
                            if (Constant.debugMode) console.log(tags);
                        }, 5000);
                    } else {
                        UserPreference.set("password", '');
                        if ($state.current.url !== '/login')
                            $state.go('login');
                    }
                    return data;
                }, function (error) {
                    return $q.reject(error);
                }).finally(function () {
                    setTimeout(function () {
                        auth.logining = false;
                    }, 5000);
                });
            }

        };

        auth.logout = function () {
            auth.isLogin = false;
            var defer = $q.defer();
            if (window.cordova) {
                PushService.setTagsWithAlias([], "");
                MobclickAgent.onEvent('app_logout');
            }
            if(iOSDevice()){
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/logout", {}, header, function (response) { 
                    var data = JSON.parse(response);
                    $timeout(function () {
                        defer.resolve(data);
                   });   
                }, function (response) {
                    $timeout(function () {
                        defer.reject(error);
                    });
                });
                return defer.promise;
            }else{
                return $http.get(Constant.ServerUrl + "/logout");
            }
           
        };

        return auth;
    }])
    .factory('SchoolService', ['$http', 'Constant', '$rootScope', 'BroadCast', 'UserPreference', function ($http, Constant, $rootScope, BroadCast, UserPreference) {
        var school = {};
        school.newNews = function (reqData) {
            var role = String(UserPreference.getObject('user').rolename);
            if (role === Constant.USER_ROLES.PARENT)
                reqData.stuId = UserPreference.get('DefaultChildID');
            if (iOSDevice()) {
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.post(Constant.ServerUrl + "/campusview/issue", reqData, header, function (response) {
                    var data = JSON.parse(response);
                    try { 
                        $rootScope.$broadcast(BroadCast.NEW_NEWS, data);
                        if (data.result)
                            school.getNewsList(1);
                        if (role == Constant.USER_ROLES.PARENT || role == Constant.USER_ROLES.STUDENT) {
                            if (window.cordova) MobclickAgent.onEvent('app_new_news');
                        } else {
                            if (window.cordova) MobclickAgent.onEvent('app_new_news_teacher');
                        }
                    } catch (e) {
                        console.error('JSON parsing error');
                    }
                }, function (response) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });

            } else {
                $http.post(Constant.ServerUrl + "/campusview/issue", reqData)
                    .success(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.NEW_NEWS, data);
                        if (data.result)
                            school.getNewsList(1);
                        if (role == Constant.USER_ROLES.PARENT || role == Constant.USER_ROLES.STUDENT) {
                            if (window.cordova) MobclickAgent.onEvent('app_new_news');
                        } else {
                            if (window.cordova) MobclickAgent.onEvent('app_new_news_teacher');
                        }
                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    });
            }

        };

        school.stickNews = function (id) {
            if (iOSDevice()) {
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.put(Constant.ServerUrl + "/campusview/topByFocus/" + id, {}, header, function (response) {
                    // prints 200
                    var data = JSON.parse(response);
                    try {
                        data.request = {
                            type: BroadCast.SET_FOCUS_RST_REV,
                            id: id
                        };
                        $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                    } catch (e) {
                        console.error('JSON parsing error');
                    }
                }, function (response) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });

            } else {
                $http.put(Constant.ServerUrl + "/campusview/topByFocus/" + id)
                    .success(function (data, header, config, status) {
                        data.request = {
                            type: BroadCast.SET_FOCUS_RST_REV,
                            id: id
                        };
                        $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);

                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    });
            }

        };

        school.undoStickNews = function (id) {
            if (iOSDevice()) {
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.put(Constant.ServerUrl + "/campusview/focuscancel/" + id, {}, header, function (response) {
                    var data = JSON.parse(response);
                    try {     
                        data.request = {
                            type: BroadCast.UNDO_STICK_RST_REV,
                            id: id
                        };
                        $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                    } catch (e) {
                        console.error('JSON parsing error');
                    }
                }, function (response) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            } else {
                $http.put(Constant.ServerUrl + "/campusview/focuscancel/" + id)
                    .success(function (data, header, config, status) {
                        data.request = {
                            type: BroadCast.UNDO_STICK_RST_REV,
                            id: id
                        };
                        $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);

                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    });
            }

        };

        school.allowPublish = function (reqData) {
            if (reqData && reqData.id) {
                if (iOSDevice()) {
                    cordova.plugin.http.setDataSerializer('json');
                    var header = { "Content-Type": 'application/json' };
                    cordova.plugin.http.post(Constant.ServerUrl + "/campusview/pass", reqData, header, function (response) {
                        var data = JSON.parse(response);
                        try { 
                            data.request = {
                                type: BroadCast.ALLOW_PUBLISH_RST_REV,
                                id: reqData.id
                            };
                            $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                            $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                        } catch (e) {
                            console.error('JSON parsing error');
                        }
                    }, function (response) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    });
                } else {
                    $http.post(Constant.ServerUrl + "/campusview/pass", reqData)
                        .success(function (data, header, config, status) {
                            data.request = {
                                type: BroadCast.ALLOW_PUBLISH_RST_REV,
                                id: reqData.id
                            };
                            $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                        })
                        .error(function (data, header, config, status) {
                            $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                        });
                }
            }
        };

        school.ignorePublish = function (id) {
            if (iOSDevice()) {
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.put(Constant.ServerUrl + "/campusview/ignore/" + id, {}, header, function (response) {
                    var data = JSON.parse(response);
                    try {
                        data.request = {
                            type: BroadCast.IGNORE_RST_REV,
                            id: id
                        };
                        $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                    } catch (e) {
                        console.error('JSON parsing error');
                    }
                }, function (response) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            } else {
                $http.put(Constant.ServerUrl + "/campusview/ignore/" + id)
                    .success(function (data, header, config, status) {
                        data.request = {
                            type: BroadCast.IGNORE_RST_REV,
                            id: id
                        };
                        $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);

                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    });
            }

        };

        school.deleteNews = function (id) {
            if (iOSDevice()) {
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.delete(Constant.ServerUrl + "/campusview/delete?id=" + id, {}, header, function (response) {
                    var data = JSON.parse(response);
                    try {  
                        data.request = {
                            type: BroadCast.DELETE_NEWS_REV,
                            id: id
                        };
                        $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                    } catch (e) {
                        console.error('JSON parsing error');
                    }
                }, function (response) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            } else {
                $http.delete(Constant.ServerUrl + "/campusview/delete?id=" + id)
                    .success(function (data, header, config, status) {
                        data.request = {
                            type: BroadCast.DELETE_NEWS_REV,
                            id: id
                        };
                        $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    });
            }

        };

        school.cancelPublish = function (id) {
            if (iOSDevice()) {
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/campusview/cancelIssue/" + id, {}, header, function (response) {
                    var data = JSON.parse(response);
                    try {
                        data.request = {
                            type: BroadCast.CANCEL_PUBLISH_REV,
                            id: id
                        };
                        $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                    } catch (e) {
                        console.error('JSON parsing error');
                    }
                }, function (response) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            } else {
                $http.get(Constant.ServerUrl + "/campusview/cancelIssue/" + id)
                    .success(function (data, header, config, status) {
                        data.request = {
                            type: BroadCast.CANCEL_PUBLISH_REV,
                            id: id
                        };
                        $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    });
            }

        };

        school.getNewsList = function (page, key, reqId) {
            var req = {
                page: page,
                rows: Constant.reqLimit
            };
            angular.extend(req, key);
            console.log(req);
            if (iOSDevice()) {
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/campusview/list", req, header, function (response) {
                    var data = JSON.parse(response);
                    try { 
                        if (data.result) {
                            var arr = data.data;
                            for (var i = 0; i < arr.length; i++) {
                                arr[i].ispublish = arr[i].state != Constant.NEWS_STATUS.ADMIN_REVIEW.key;
                            }
                            if (page && page > 1)
                                Array.prototype.push.apply(school.list, arr);
                            else {
                                school.list = arr;
                                school.list.forEach(function (item) {
                                    item.picUrls = [];
                                    item.imageUrls.forEach(function (url) {
                                        var urlItem = {
                                            thumb: '',
                                            src: ''
                                        };
                                        urlItem.thumb = url;
                                        urlItem.src = url;
                                        item.picUrls.push(urlItem);
                                    });
                                });
                                if (!key)
                                    UserPreference.setObject("news_list_cache", school.list);
                            }
                            school.listHasMore = arr.length >= Constant.reqLimit / 2;
                            if (Constant.debugMode) {
                                console.log('news data rev, page:' + page);
                                console.log(arr);
                            }
                            data.reqId = reqId;
                        }

                        $rootScope.$broadcast(BroadCast.NEWS_LIST_REV, data);
                    } catch (e) {
                        console.error('JSON parsing error');
                    }
                }, function (response) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            } else {

                $http.get(Constant.ServerUrl + "/campusview/list", {
                    params: req
                })
                    .success(function (data, header, config, status) {

                        if (data.result) {
                            var arr = data.data;
                            for (var i = 0; i < arr.length; i++) {
                                arr[i].ispublish = arr[i].state != Constant.NEWS_STATUS.ADMIN_REVIEW.key;
                            }
                            if (page && page > 1)
                                Array.prototype.push.apply(school.list, arr);
                            else {
                                school.list = arr;
                                school.list.forEach(function (item) {
                                    item.picUrls = [];
                                    item.imageUrls.forEach(function (url) {
                                        var urlItem = {
                                            thumb: '',
                                            src: ''
                                        };
                                        urlItem.thumb = url;
                                        urlItem.src = url;
                                        item.picUrls.push(urlItem);
                                    });
                                });
                                if (!key)
                                    UserPreference.setObject("news_list_cache", school.list);
                            }
                            school.listHasMore = arr.length >= Constant.reqLimit / 2;
                            if (Constant.debugMode) {
                                console.log('news data rev, page:' + page);
                                console.log(arr);
                            }
                            data.reqId = reqId;
                        }

                        $rootScope.$broadcast(BroadCast.NEWS_LIST_REV, data);
                    })
                    .error(function (data, header, config, status) {
                        school.listHasMore = false;
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                        //toaster.error({title: MESSAGES.CONNECT_ERROR, body: MESSAGES.CONNECT_ERROR_MSG});
                    });
            }

        };

        school.getBannerList = function () {
            if (iOSDevice()) {
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/campusview/focuslist", {}, header, function (response) {
                    var data = JSON.parse(response);
                    try {
                        
                        if (data.result) {
                            school.bannerList = data.data;
                            if (Constant.debugMode) {
                                console.log('banner data rev');
                                console.log(school.bannerList);
                            }
                            UserPreference.setObject("news_banner_cache", school.bannerList);
                        }
                        $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                    } catch (e) {
                        console.error('JSON parsing error');
                    }
                }, function (response) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            } else {
                $http.get(Constant.ServerUrl + "/campusview/focuslist")
                    .success(function (data, header, config, status) {
                        if (data.result) {

                            school.bannerList = data.data;
                            if (Constant.debugMode) {
                                console.log('banner data rev');
                                console.log(school.bannerList);
                            }
                            UserPreference.setObject("news_banner_cache", school.bannerList);
                        }
                        $rootScope.$broadcast(BroadCast.BANNER_LIST_REV, data);
                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                        //toaster.error({title: MESSAGES.CONNECT_ERROR, body: MESSAGES.CONNECT_ERROR_MSG});
                    });
            }

        };

        school.getNewsType = function () {

            if (iOSDevice()) {
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/getdic/show-type/new", {}, header, function (response) {
                    var data = JSON.parse(response);
                    try {
                       
                        if (data.result) {
                            var list = data.data;
                            var selectList = [];
                            for (var i = 0; i < list.length; i++) {
                                if (list[i].parentKey === 2) {
                                    selectList.push(list[i]);
                                }
                            }
                            UserPreference.setObject("news_type_selectable", selectList);
                            UserPreference.setObject("news_type", list);
                            $rootScope.$broadcast(BroadCast.NEWS_TYPE_REV, data);
                        }
                    } catch (e) {
                        console.error('JSON parsing error');
                    }
                }, function (response) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            } else {
                $http.get(Constant.ServerUrl + "/getdic/show-type/new")
                    .success(function (data, header, config, status) {
                        if (data.result) {
                            var list = data.data;
                            var selectList = [];
                            for (var i = 0; i < list.length; i++) {
                                if (list[i].parentKey === 2) {
                                    selectList.push(list[i]);
                                }
                            }
                            UserPreference.setObject("news_type_selectable", selectList);
                            UserPreference.setObject("news_type", list);
                            $rootScope.$broadcast(BroadCast.NEWS_TYPE_REV, data);
                        }
                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    });
            }

        };

        return school;
    }])
    .factory('SettingService', ['$http', 'Constant', '$rootScope', 'BroadCast', 'UserPreference', 'toaster', 'MESSAGES', '$ionicLoading', '$ionicPopup', '$timeout', 'ChatService', function ($http, Constant, $rootScope, BroadCast, UserPreference, toaster, MESSAGES, $ionicLoading, $ionicPopup, $timeout, ChatService) {
        var setting = {};

        setting.sendFeedback = function (text) {
            if (iOSDevice()) {
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.post(Constant.ServerUrl + "/opnionup", text, header, function (response) {
                    var data = JSON.parse(response);
                    try { 
                        $rootScope.$broadcast(BroadCast.FEEDBACK, data);
                        if (window.cordova) MobclickAgent.onEvent('app_suggest');

                    } catch (e) {
                        console.error('JSON parsing error');
                    }
                }, function (response) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            } else {
                $http.post(Constant.ServerUrl + "/opnionup", text)
                    .success(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.FEEDBACK, data);
                        if (window.cordova) MobclickAgent.onEvent('app_suggest');
                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                        //toaster.error({title: MESSAGES.CONNECT_ERROR, body:MESSAGES.CONNECT_ERROR_MSG});
                    });
            }

        };

        setting.changePwd = function (oldPass, newPass) {
            if (iOSDevice()) {
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.post(Constant.ServerUrl + "/changepassword", {
                    curpassword: md5(oldPass).toUpperCase(),
                    newpassword: md5(newPass).toUpperCase()
                }, header, function (response) {
                    // prints 200
                    var data = JSON.parse(response);
                    try {   
                        $rootScope.$broadcast(BroadCast.PASSWORD_CHANGE, data);

                    } catch (e) {
                        console.error('JSON parsing error');
                    }
                }, function (response) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            } else {
                $http.post(Constant.ServerUrl + "/changepassword", {
                    curpassword: md5(oldPass).toUpperCase(),
                    newpassword: md5(newPass).toUpperCase()
                })
                    .success(function (data, header, config, status) {

                        $rootScope.$broadcast(BroadCast.PASSWORD_CHANGE, data);
                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                        //toaster.error({title: MESSAGES.CONNECT_ERROR, body:MESSAGES.CONNECT_ERROR_MSG});
                    });
            }

        };

        setting.editInfo = function (key, value) {

            var param = {};
            if (!key || !value)
                return;
            param.key = key;
            param.value = value;
            if (iOSDevice()) {
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.post(Constant.ServerUrl + "/infoedit", param, header, function (response) {
                    // prints 200
                    try {
                        var data = JSON.parse(response);
                        if (data.result) {
                            var user = UserPreference.getObject('user');
                            if (key === 'name') {
                                user.name = value;
                                if (window.cordova) MobclickAgent.onEvent('app_set_nick');
                            } else if (key === 'logo') {
                                user.logo = 'data:image/jpeg;base64,' + value;
                                if (ChatService.loginInfo)
                                    ChatService.loginInfo.image = user.logo;
                                if (window.cordova) MobclickAgent.onEvent('app_set_avata');
                            } else if (key === 'sex') {
                                user.sex = value;
                                if (window.cordova) MobclickAgent.onEvent('app_set_sex');
                            }
                            UserPreference.setObject('user', user);
                        }

                        $rootScope.$broadcast(BroadCast.EDIT_INFO, data);

                    } catch (e) {
                        console.error('JSON parsing error');
                    }
                }, function (response) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            } else {
                $http.post(Constant.ServerUrl + "/infoedit", param)
                    .success(function (data, header, config, status) {

                        if (data.result) {
                            var user = UserPreference.getObject('user');
                            if (key === 'name') {
                                user.name = value;
                                if (window.cordova) MobclickAgent.onEvent('app_set_nick');
                            } else if (key === 'logo') {
                                user.logo = 'data:image/jpeg;base64,' + value;
                                if (ChatService.loginInfo)
                                    ChatService.loginInfo.image = user.logo;
                                if (window.cordova) MobclickAgent.onEvent('app_set_avata');
                            } else if (key === 'sex') {
                                user.sex = value;
                                if (window.cordova) MobclickAgent.onEvent('app_set_sex');
                            }
                            UserPreference.setObject('user', user);
                        }

                        $rootScope.$broadcast(BroadCast.EDIT_INFO, data);
                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                        //toaster.error({title: MESSAGES.CONNECT_ERROR, body:MESSAGES.CONNECT_ERROR_MSG});
                    });
            }


        };

        setting.checkUpdate = function (silent) {
            var os = ionic.Platform.isIOS() ? 'ios_admin' : 'android_admin';

            function hasNewVersion(v) {
                var vr = v.split('.');
                var vl = Constant.version.split('.');
                if (vr && vr.length === vl.length) {
                    for (var i = 0; i < vr.length; i++) {
                        if (Number(vr[i]) > Number(vl[i]))
                            return true;
                        else if (Number(vr[i]) < Number(vl[i]))
                            return false;
                    }
                }
                return false;
            }
            if (iOSDevice()) {
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/checkversion", {
                    ostype: os,
                    version: Constant.version
                }, header, function (response) {
                    var data = JSON.parse(response);
                    if (data.data && data.data.version) {
                        if (!hasNewVersion(data.data.version)) {
                            if (!silent) $ionicLoading.show({
                                template: '已是最新版应用',
                                noBackdrop: true,
                                duration: 1000
                            });
                        } else {
                            if (Constant.debugMode) console.log(data.data);
                            var confirmPopup = $ionicPopup.confirm({
                                title: '更新提示',
                                template: data.data.content,
                                cancelText: '取消',
                                okText: '升级',
                                okType: 'button-balanced'
                            });
                            if (ionic.Platform.isIOS()) {
                                confirmPopup.then(function (res) {
                                    if (res) {
                                        window.open(data.data.url, '_system');
                                    }
                                });
                            } else if (ionic.Platform.isAndroid()) {

                                confirmPopup.then(function (res) {
                                    if (res) {
                                        window.open('market://details?id=com.sct.xgenban.admin', '_system');
                                    }
                                });
                            }
                        }

                    } else if (!silent) $ionicLoading.show({
                        template: '已是最新版应用',
                        noBackdrop: true,
                        duration: 1000
                    });
                }, function (response) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            } else {
                $http.get(Constant.ServerUrl + "/checkversion", {
                    params: {
                        ostype: os,
                        version: Constant.version
                    }
                })
                    .success(function (data, header, config, status) {
                        if (data.data && data.data.version) {
                            if (!hasNewVersion(data.data.version)) {
                                if (!silent) $ionicLoading.show({
                                    template: '已是最新版应用',
                                    noBackdrop: true,
                                    duration: 1000
                                });
                            } else {
                                if (Constant.debugMode) console.log(data.data);
                                var confirmPopup = $ionicPopup.confirm({
                                    title: '更新提示',
                                    template: data.data.content,
                                    cancelText: '取消',
                                    okText: '升级',
                                    okType: 'button-balanced'
                                });
                                if (ionic.Platform.isIOS()) {
                                    confirmPopup.then(function (res) {
                                        if (res) {
                                            window.open(data.data.url, '_system');
                                        }
                                    });
                                } else if (ionic.Platform.isAndroid()) {

                                    confirmPopup.then(function (res) {
                                        if (res) {
                                            window.open('market://details?id=com.sct.xgenban.admin', '_system');
                                        }
                                    });
                                }
                            }

                        } else if (!silent) $ionicLoading.show({
                            template: '已是最新版应用',
                            noBackdrop: true,
                            duration: 1000
                        });
                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    });
            }


        };

        return setting;
    }])
    .factory('NoticeService', ['$http', 'Constant', '$rootScope', 'BroadCast', 'UserPreference', function ($http, Constant, $rootScope, BroadCast, UserPreference) {
        var notice = {};

        notice.newNotice = function (reqData) {
            if (iOSDevice()) {
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.post(Constant.ServerUrl + "/notice/issue", reqData, header, function (response) {
                   
                    try { 
                        var data = JSON.parse(response);
                        $rootScope.$broadcast(BroadCast.NEW_NOTICE, data);
                        if (window.cordova) MobclickAgent.onEvent('app_new_notice');

                    } catch (e) {
                        console.error('JSON parsing error');
                    }
                }, function (response) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            } else {
                $http.post(Constant.ServerUrl + "/notice/issue", reqData)
                    .success(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.NEW_NOTICE, data);
                        if (window.cordova) MobclickAgent.onEvent('app_new_notice');
                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    });
            }

        };

        notice.deleteNotice = function (id) {
            if (iOSDevice()) {
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.delete(Constant.ServerUrl + "/notice/delete/" + id, {}, header, function (response) {
                    // prints 200
                    var data = JSON.parse(response);
                    try {     
                        data.request = {
                            type: BroadCast.DELETE_NOTICE_REV,
                            id: id
                        };
                        $rootScope.$broadcast(BroadCast.DELETE_NOTICE_REV, data);

                    } catch (e) {
                        console.error('JSON parsing error');
                    }
                }, function (response) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            } else {
                $http.delete(Constant.ServerUrl + "/notice/delete/" + id)
                    .success(function (data, header, config, status) {
                        data.request = {
                            type: BroadCast.DELETE_NOTICE_REV,
                            id: id
                        };
                        $rootScope.$broadcast(BroadCast.DELETE_NOTICE_REV, data);
                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    });
            }

        };

        notice.getNoticeList = function (page, type) {
            var req = {
                page: page,
                rows: Constant.reqLimit
            };
            if (type)
                req.noticeKey = type;
            if (iOSDevice()) {
                req = {
                    page:'1',
                    rows:'10',
                    noticeKey:type? type:''
                };
                console.log('======req:');
                console.log(req);
                console.log('======end:');
                cordova.plugin.http.setRequestTimeout(15.0);
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                // cordova.plugin.http.get(Constant.ServerUrl + "/notice/list", req, header, function (response) {
                //     // prints 200
                //     try {
                //         var data = JSON.parse(response);
                //         if (data.result) {
                //             console.log('====news list is :');
                //             console.log(data);
                //             console.log('====end :');
                //             var arr = data.data;
                //             for (var j = 0; j < arr.length; j++) {
                //                 arr[j].time = getWeekday(arr[j].createTime);
                //             }

                //             if (page && page > 1)
                //                 Array.prototype.push.apply(notice.list, arr);
                //             else {
                //                 notice.list = arr;
                //                 UserPreference.setObject("notice_list_cache" + type, notice.list);
                //             }
                //             if (arr.length < Constant.reqLimit)
                //                 notice.listHasMore = false;
                //             else
                //                 notice.listHasMore = true;
                //             if (Constant.debugMode) {
                //                 console.log('notice data rev, page:' + page);
                //                 console.log(arr);
                //             }
                //         }
                //         $rootScope.$broadcast(BroadCast.NOTICE_LIST_REV, data);

                //     } catch (e) {
                //         console.error('JSON parsing error');
                //     }
                // }, function (response) {
                //     notice.listHasMore = false;
                //     console.log('========news errror');
                //     console.log(response);
                //     $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                // });
            } else {
                $http.get(Constant.ServerUrl + "/notice/list", {
                    params: req
                })
                    .success(function (data, header, config, status) {
                        if (data.result) {
                            var arr = data.data;
                            for (var j = 0; j < arr.length; j++) {
                                arr[j].time = getWeekday(arr[j].createTime);
                            }

                            if (page && page > 1)
                                Array.prototype.push.apply(notice.list, arr);
                            else {
                                notice.list = arr;
                                UserPreference.setObject("notice_list_cache" + type, notice.list);
                            }
                            if (arr.length < Constant.reqLimit)
                                notice.listHasMore = false;
                            else
                                notice.listHasMore = true;
                            if (Constant.debugMode) {
                                console.log('notice data rev, page:' + page);
                                console.log(arr);
                            }
                        }
                        $rootScope.$broadcast(BroadCast.NOTICE_LIST_REV, data);
                    })
                    .error(function (data, header, config, status) {
                        notice.listHasMore = false;
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                        //toaster.error({title: MESSAGES.CONNECT_ERROR, body:MESSAGES.CONNECT_ERROR_MSG});
                    });
            }

        };

        return notice;
    }])
    .factory('ChatService', ['Constant', 'UserPreference', '$rootScope', 'BroadCast', '$http', '$q', '$ionicPopup', '$window', '$state', '$injector', '$timeout', function (Constant, UserPreference, $rootScope, BroadCast, $http, $q, $ionicPopup, $window, $state, $injector, $timeout) {
        var chat = {};
        /**
         * 联系人缓存
         */
        chat.infoMap = UserPreference.getObject('ChatUserInfoMap');
        /**
         * 通讯录缓存
         */
        chat.friendsMap = UserPreference.getObject('ChatContactListMap');
        chat.retryCount = 0;
        chat.getTabUnreadCount = function () {
            var count = 0;
            for (var i = 0; i < chat.conversations.length; i++) {
                count += chat.conversations[i].UnreadMsgCount;
            }
            $rootScope.$broadcast(BroadCast.BADGE_UPDATE, {
                type: 'im',
                count: count
            });
        };

        chat.sendPush = function (to, title, message) {
            if (iOSDevice()) {
                var defer = $q.defer();
                var tool = $injector.get('SavePhotoTool');

                cordova.plugin.http.setDataSerializer('json');
                var options = {
                    to: to,
                    title: title,
                    message: message
                };
                var header = { "Content-Type": 'application/json' };

                cordova.plugin.http.post(Constant.ServerUrl + "/im/push",
                    options, header, function (response) {
                        var data = JSON.parse(response.data);
                        try {
                            $timeout(function () {
                                return defer.resolve(data);
                            });

                        } catch (error) {

                        }

                    }, function (error) {
                        if (error.status) {
                            tool.interceptors(error.status);
                        }
                        $timeout(function () {
                            return defer.reject(error);
                        });
                    });

                return defer.promise;
            } else {
                return $http.post(Constant.ServerUrl + "/im/push", {
                    to: to,
                    title: title,
                    message: message
                })
                    .then(function (response) {
                        return response.data;
                    }, function (error) {
                        return $q.reject(error);
                    });
            }

        };

        chat.init = function () {
            var user = UserPreference.getObject("user");
            if (chat.loginInfo || !user.usersig) {
                return;
            }
            var loginInfo = {
                sdkAppID: Constant.IMAppID,
                accountType: Constant.IMAccountType,
                identifier: user.id + '',
                userSig: user.usersig,
                image: user.logo
            };

            var onKickedEventCall = function () {
                var confirmPopup = $ionicPopup.confirm({
                    title: '温馨提示',
                    template: '账号在其它地方登录，您已下线。',
                    cancelText: '取消',
                    okText: '重新登录',
                    okType: 'button-balanced'
                });
                confirmPopup.then(function (res) {
                    if (res) {
                        $state.go('tabsController.mainPage');
                        chat.logout();
                    }
                });
            };

            var onGroupSystemNotifys = function () {
                chat.getContacts();
            };

            var listeners = {
                "onConnNotify": function (resp) {
                    var info;
                    switch (resp.ErrorCode) {
                        case webim.CONNECTION_STATUS.ON:
                            webim.Log.warn('建立连接成功: ' + resp.ErrorInfo);
                            break;
                        case webim.CONNECTION_STATUS.OFF:
                            info = '连接已断开，无法收到新消息，请检查下你的网络是否正常: ' + resp.ErrorInfo;
                            webim.Log.warn(info);
                            break;
                        case webim.CONNECTION_STATUS.RECONNECT:
                            info = '连接状态恢复正常: ' + resp.ErrorInfo;
                            webim.Log.warn(info);
                            break;
                        default:
                            webim.Log.error('未知连接状态: =' + resp.ErrorInfo);
                            break;
                    }
                }, //监听连接状态回调变化事件,必填
                "onMsgNotify": function onMsgNotify(newMsgList) {
                    console.warn(newMsgList);
                    for (var j in newMsgList) { //遍历新消息
                        var newMsg = newMsgList[j];
                        if (newMsg.fromAccount === '@TIM#SYSTEM') {
                            chat.getContacts();
                            continue;
                        }
                        var selSess = newMsg.getSession();
                        var selSessID = selSess.id();
                        var selType = selSess.type();
                        var headUrl = Constant.IM_GROUP_AVATAR,
                            nickName = selSess.name();
                        if (selType == webim.SESSION_TYPE.C2C) {
                            var c2cInfo = chat.getMemberInfoFromMap(selType, selSessID);
                            if (c2cInfo && c2cInfo.name) {
                                nickName = c2cInfo.name;
                            } else {
                                nickName = selSessID;
                            }
                            if (c2cInfo && c2cInfo.image) {
                                headUrl = c2cInfo.image;
                            } else {
                                headUrl = Constant.IM_USER_AVATAR;
                            }
                        }
                        var con = {
                            SessionId: selSessID,
                            SessionImage: headUrl,
                            SessionType: selType,
                            SessionNick: nickName,
                            SessionTime: selSess.time(),
                            MsgTimeStamp: getChatTimeLabel(selSess.time()),
                            UnreadMsgCount: selSess.unread(),
                            MsgShow: delHtmlTag(convertMsgtoPushStr(newMsg))
                        };
                        var k = 0;
                        for (; chat.conversations && k < chat.conversations.length; k++) {
                            if (chat.conversations[k].SessionId == selSess.id()) {
                                chat.conversations[k] = con;
                                break;
                            }
                        }
                        if (!chat.conversations)
                            chat.conversations = [];
                        if (k === chat.conversations.length) {
                            chat.conversations.push(con);
                        }
                    }
                    chat.getTabUnreadCount();
                    $rootScope.$broadcast(BroadCast.IM_NEW_MESSAGE, newMsgList);
                }, //监听新消息(私聊，普通群(非直播聊天室)消息，全员推送消息)事件，必填
                "onGroupSystemNotifys": onGroupSystemNotifys,
                "onKickedEventCall": onKickedEventCall
            };
            var opts = {
                isLogOn: false
            };

            function showLoginError() {
                var confirmPopup = $ionicPopup.confirm({
                    template: '通讯服务器连接失败，请重试！',
                    cancelText: '取消',
                    okText: '重试',
                    okType: 'button-balanced'
                });
                confirmPopup.then(function (res) {
                    if (res) {
                        chat.init();
                    }
                });
            }

            webim.login(loginInfo, listeners, opts,
                function (resp) {
                    chat.loginInfo = loginInfo;
                    chat.retryCount = 0;
                    chat.getRecentContacts();
                    chat.getContacts();
                },
                function (err) {
                    if (chat.retryCount > 2) {
                        chat.retryCount = 0;
                        showLoginError();
                    } else {
                        setTimeout(function () {
                            chat.retryCount++;
                            chat.init();
                        }, 1000);
                    }
                });
        };

        chat.getRecentContacts = function (classid) {
            if (!chat.loginInfo) {
                chat.init();
                console.log("try login firstly..");
                return;
            }
            webim.getRecentContactList({
                'Count': 100
            }, function (resp) {
                //console.log(resp);
                var data = [];
                var tempSess, tempSessMap = {}; //临时会话变量
                if (resp.SessionItem && resp.SessionItem.length > 0) {
                    for (var i in resp.SessionItem) {
                        var item = resp.SessionItem[i];
                        var type = item.Type; //接口返回的会话类型
                        var sessType, typeZh, sessionId, sessionNick = '',
                            sessionImage = '',
                            senderId = '',
                            senderNick = '';
                        if (type == webim.RECENT_CONTACT_TYPE.C2C) { //私聊
                            typeZh = '私聊';
                            sessType = webim.SESSION_TYPE.C2C; //设置会话类型
                            sessionId = item.To_Account; //会话id，私聊时为好友ID或者系统账号（值为@TIM#SYSTEM，业务可以自己决定是否需要展示），注意：从To_Account获取,

                            if (sessionId === '@TIM#SYSTEM') { //先过滤系统消息，，
                                webim.Log.warn('过滤好友系统消息,sessionId=' + sessionId);
                                continue;
                            }
                            var c2cInfo = chat.getMemberInfoFromMap(sessType, sessionId, classid);
                            if (c2cInfo && c2cInfo.name) { //从infoMap获取c2c昵称
                                sessionNick = c2cInfo.name; //会话昵称，私聊时为好友昵称，接口暂不支持返回，需要业务自己获取（前提是用户设置过自己的昵称，通过拉取好友资料接口（支持批量拉取）得到）
                            } else { //没有找到或者没有设置过
                                sessionNick = sessionId; //会话昵称，如果昵称为空，默认将其设成会话id
                            }
                            if (c2cInfo && c2cInfo.image) { //从infoMap获取c2c头像
                                sessionImage = c2cInfo.image; //会话头像，私聊时为好友头像，接口暂不支持返回，需要业务自己获取（前提是用户设置过自己的昵称，通过拉取好友资料接口（支持批量拉取）得到）
                            } else { //没有找到或者没有设置过
                                sessionImage = Constant.IM_USER_AVATAR; //会话头像，默认
                            }
                            senderId = senderNick = ''; //私聊时，这些字段用不到，直接设置为空

                        } else if (type == webim.RECENT_CONTACT_TYPE.GROUP) { //群聊
                            typeZh = '群聊';
                            sessType = webim.SESSION_TYPE.GROUP; //设置会话类型
                            sessionId = item.ToAccount; //会话id，群聊时为群ID，注意：从ToAccount获取
                            sessionNick = item.GroupNick; //会话昵称，群聊时，为群名称，接口一定会返回

                            if (item.GroupImage) {
                                sessionImage = item.GroupImage;
                            } else
                                sessionImage = Constant.IM_GROUP_AVATAR;
                            senderId = item.MsgGroupFrom_Account; //群消息的发送者id

                            if (!senderId) { //发送者id为空
                                webim.Log.warn('群消息发送者id为空,senderId=' + senderId + ",groupid=" + sessionId);
                                continue;
                            }
                            // if (senderId == '@TIM#SYSTEM') {//先过滤群系统消息，因为接口暂时区分不了是进群还是退群等提示消息，
                            // 	webim.Log.warn('过滤群系统消息,senderId=' + senderId + ",groupid=" + sessionId);
                            // 	continue;
                            // }
                        } else {
                            typeZh = '未知类型';
                            sessionId = item.ToAccount;
                        }

                        if (!sessionId) { //会话id为空
                            webim.Log.warn('会话id为空,sessionId=' + sessionId);
                            continue;
                        }

                        if (sessionId === '@TLS#NOT_FOUND') { //会话id不存在，可能是已经被删除了
                            webim.Log.warn('会话id不存在,sessionId=' + sessionId);
                            continue;
                        }

                        if (sessionNick.length > 10) { //帐号或昵称过长，截取一部分
                            sessionNick = sessionNick.substr(0, 10) + "...";
                        }

                        tempSess = tempSessMap[sessType + "_" + sessionId];
                        if (!tempSess) { //先判断是否存在（用于去重），不存在增加一个
                            tempSessMap[sessType + "_" + sessionId] = true;
                            data.push({
                                SessionType: sessType, //会话类型
                                SessionTypeZh: typeZh, //会话类型中文
                                SessionId: webim.Tool.formatText2Html(sessionId), //会话id
                                SessionNick: webim.Tool.formatText2Html(sessionNick), //会话昵称
                                SessionImage: sessionImage, //会话头像
                                C2cAccount: webim.Tool.formatText2Html(senderId), //发送者id
                                C2cNick: webim.Tool.formatText2Html(senderNick), //发送者昵称
                                UnreadMsgCount: item.UnreadMsgCount, //未读消息数
                                MsgSeq: item.MsgSeq, //消息seq
                                MsgRandom: item.MsgRandom, //消息随机数
                                MsgTimeStamp: getChatTimeLabel(item.MsgTimeStamp), //消息时间戳
                                MsgShow: item.MsgShow //消息内容
                            });
                        }
                    }
                }
                chat.conversations = data;

                function initUnreadMsgCount() {
                    var sess;
                    var sessMap = webim.MsgStore.sessMap();
                    for (var i in sessMap) {
                        sess = sessMap[i];
                        for (var j = 0; j < data.length; j++) {
                            if (sess.id() == data[j].SessionId) {
                                data[j].UnreadMsgCount = sess.unread();
                                break;
                            }
                        }
                    }
                    chat.getTabUnreadCount();
                }

                webim.syncMsgs(initUnreadMsgCount);
                $rootScope.$broadcast(BroadCast.IM_RECENT_CONTACTS, data);
            }, function (error) {
                $rootScope.$broadcast(BroadCast.IM_RECENT_CONTACTS, undefined);
                console.error(error);
            });
        };

        chat.getContacts = function () {
            var options = {
                'Member_Account': chat.loginInfo.identifier,
                'Offset': 0,
                'GroupBaseInfoFilter': [
                    'Type',
                    'Name',
                    'Introduction',
                    'Notification',
                    'FaceUrl',
                    'CreateTime',
                    'Owner_Account',
                    'LastInfoTime',
                    'LastMsgTime',
                    'NextMsgSeq',
                    'MemberNum',
                    'MaxMemberNum',
                    'ApplyJoinOption'
                ],
                'SelfInfoFilter': [
                    'Role',
                    'JoinTime',
                    'MsgFlag',
                    'UnreadMsgNum'
                ]
            };
            webim.getJoinedGroupListHigh(
                options,
                function (resp) {
                    chat.retryCount = 0;
                    if (!resp.GroupIdList || resp.GroupIdList.length === 0) {
                        console.log('你目前还没有加入任何群组');
                        return;
                    }
                    var data = [];
                    for (var i = 0; i < resp.GroupIdList.length; i++) {
                        var item = {
                            'SortField': webim.Tool.groupTypeEn2Ch(resp.GroupIdList[i].Type),
                            'GroupId': resp.GroupIdList[i].GroupId,
                            'Name': webim.Tool.formatText2Html(resp.GroupIdList[i].Name),
                            'TypeEn': resp.GroupIdList[i].Type,
                            'Type': webim.Tool.groupTypeEn2Ch(resp.GroupIdList[i].Type),
                            'RoleEn': resp.GroupIdList[i].SelfInfo.Role,
                            'Role': webim.Tool.groupRoleEn2Ch(resp.GroupIdList[i].SelfInfo.Role),
                            'MsgFlagEn': webim.Tool.groupMsgFlagEn2Ch(resp.GroupIdList[i].SelfInfo.MsgFlag),
                            'MsgFlag': webim.Tool.groupMsgFlagEn2Ch(resp.GroupIdList[i].SelfInfo.MsgFlag),
                            'MemberNum': resp.GroupIdList[i].MemberNum,
                            'Notification': webim.Tool.formatText2Html(resp.GroupIdList[i].Notification),
                            'Introduction': webim.Tool.formatText2Html(resp.GroupIdList[i].Introduction),
                            'JoinTime': webim.Tool.formatTimeStamp(resp.GroupIdList[i].SelfInfo.JoinTime),
                            'NextMsgSeq': resp.GroupIdList[i].NextMsgSeq,
                            'FaceUrl': resp.GroupIdList[i].FaceUrl
                        };

                        var classid, needCache = false;
                        var arr = item.GroupId.split('_');
                        if (arr && arr.length > 1) {
                            classid = item.ClassID = arr[arr.length - 1];
                            if (item.TypeEn === 'Public' && (arr[0] === 'all' || arr[0] === 'school')) {
                                needCache = true;
                                if (arr[0] === 'school') {
                                    classid = item.ClassID = 'schoolTeacher';
                                    data.push(item);
                                }
                            } else
                                data.push(item);
                            chat.getGroupMemberInfo(item, needCache, classid);
                        }
                    }
                    chat.groups = data;
                },
                function (err) {
                    console.log(err.ErrorInfo);
                    if (chat.retryCount > 2) {
                        chat.retryCount = 0;
                        $rootScope.$broadcast(BroadCast.IM_REV_CONTACTS, undefined);
                    } else {
                        setTimeout(function () {
                            chat.retryCount++;
                            chat.getContacts();
                        }, 5000);
                    }
                }
            );
        };

        chat.getMemberInfoFromMap = function (sessType, sessionId, classid) {
            var key = sessType + "_" + sessionId;
            if (sessType == webim.SESSION_TYPE.C2C) {
                var ckey = sessType + "_" + sessionId + "_" + classid;
                if (chat.infoMap[ckey])
                    return chat.infoMap[ckey];
                else
                    return chat.infoMap[key];
            }
            return chat.infoMap[key];
        };

        chat.getGroupMemberInfo = function (group, needCache, classid) {
            var options = {
                'GroupId': group.GroupId,
                'Offset': 0,
                'MemberInfoFilter': [
                    'Account',
                    'Role',
                    'NameCard',
                    'JoinTime',
                    'LastSendMsgTime',
                    'ShutUpUntil',
                    'AppMemberDefinedData'
                ]
            };
            webim.getGroupMemberInfo(
                options,
                function (resp) {
                    console.log(resp);
                    chat.retryCount = 0;
                    if (resp.MemberNum <= 0) {
                        console.log(group.Name + '目前没有成员');
                        return;
                    }
                    //console.log(resp);
                    var data = [];
                    for (var i in resp.MemberList) {
                        var account = resp.MemberList[i].Member_Account;
                        var role = webim.Tool.groupRoleEn2Ch(resp.MemberList[i].Role);
                        var join_time = webim.Tool.formatTimeStamp(
                            resp.MemberList[i].JoinTime);
                        var shut_up_until = webim.Tool.formatTimeStamp(
                            resp.MemberList[i].ShutUpUntil);
                        if (shut_up_until === 0) {
                            shut_up_until = '-';
                        }
                        var nick, icon, category;
                        var extra = resp.MemberList[i].AppMemberDefinedData;
                        for (var j = 0; j < extra.length; j++) {
                            if (extra[j].Key === 'Category')
                                category = extra[j].Value;
                            else if (extra[j].Key === 'Nick')
                                nick = extra[j].Value;
                            else if (extra[j].Key === 'Icon')
                                icon = extra[j].Value;
                        }

                        var key = webim.SESSION_TYPE.C2C + "_" + account;
                        var pre = chat.infoMap[key];
                        if (!pre || (pre && nick !== ""))
                            chat.infoMap[key] = {
                                name: nick,
                                image: icon
                            };
                        if (needCache) {
                            chat.infoMap[key + "_" + classid] = chat.infoMap[key];
                            data.push({
                                SortField: nick,
                                GroupId: group.GroupId,
                                Member_Account: account,
                                Role: role,
                                JoinTime: join_time,
                                ShutUpUntil: shut_up_until,
                                Icon: icon,
                                Name: nick,
                                Category: category,
                                ClassID: classid
                            });
                        }
                    }
                    if (needCache) {
                        chat.friendsMap[classid] = data;
                        UserPreference.setObject('ChatUserInfoMap', chat.infoMap);
                        UserPreference.setObject('ChatContactListMap', chat.friendsMap);
                        $rootScope.$broadcast(BroadCast.IM_REV_CONTACTS, data);
                    }
                },
                function (err) {
                    if (needCache) {
                        $rootScope.$broadcast(BroadCast.IM_REV_CONTACTS, undefined);
                        if (chat.retryCount > 2) {
                            chat.retryCount = 0;
                        } else {
                            setTimeout(function () {
                                chat.retryCount++;
                                chat.getContacts();
                            }, 5000);
                        }
                    }
                    console.log(err.ErrorInfo);
                }
            );
        };

        chat.getC2CHistoryMsgs = function (id, lastMsgTime, msgKey) {
            var options = {
                'Peer_Account': id, //好友帐号
                'MaxCnt': 10, //拉取消息条数
                'LastMsgTime': lastMsgTime, //最近的消息时间，即从这个时间点向前拉取历史消息
                'MsgKey': msgKey
            };
            webim.getC2CHistoryMsgs(
                options,
                function (resp) {
                    $rootScope.$broadcast(BroadCast.IM_C2C_HISTORY_MESSAGE, resp);
                },
                function (error) {
                    $rootScope.$broadcast(BroadCast.IM_C2C_HISTORY_MESSAGE, error);
                }
            );
        };

        chat.getGroupHistoryMsgs = function (id, ReqMsgSeq) {
            if (ReqMsgSeq === 0)
                ReqMsgSeq = undefined;
            var options = {
                'GroupId': id,
                'ReqMsgNumber': 10,
                'ReqMsgSeq': ReqMsgSeq
            };
            webim.syncGroupMsgs(
                options,
                function (resp) {
                    $rootScope.$broadcast(BroadCast.IM_GROUP_HISTORY_MESSAGE, resp);
                },
                function (error) {
                    $rootScope.$broadcast(BroadCast.IM_GROUP_HISTORY_MESSAGE, error);
                }
            );
        };

        chat.quitGroup = function (group_id) {
            var defer = $q.defer();
            var options = null;
            if (group_id) {
                options = {
                    'GroupId': group_id
                };
            }
            webim.quitGroup(
                options,
                function (resp) {
                    defer.resolve(resp);
                },
                function (err) {
                    defer.reject(err);
                }
            );
            return defer.promise;
        };

        chat.getGroupInfo = function (id) {
            var defer = $q.defer();
            var options = {
                'GroupIdList': [
                    id
                ],
                'GroupBaseInfoFilter': [
                    'Type',
                    'Name',
                    'Introduction',
                    'Notification',
                    'FaceUrl',
                    'CreateTime',
                    'Owner_Account',
                    'LastInfoTime',
                    'LastMsgTime',
                    'NextMsgSeq',
                    'MemberNum',
                    'MaxMemberNum',
                    'ApplyJoinOption'
                ],
                'MemberInfoFilter': [
                    'Account',
                    'Role',
                    'NameCard',
                    'JoinTime',
                    'LastSendMsgTime',
                    'ShutUpUntil',
                    'AppMemberDefinedData'
                ]
            };
            webim.getGroupInfo(
                options,
                function (resp) {
                    defer.resolve(resp);
                },
                function (err) {
                    defer.reject(err);
                }
            );
            return defer.promise;
        };

        chat.modifyGroupInfo = function (gid, name) {
            var defer = $q.defer();
            var options = {
                'GroupId': gid,
                'Name': name
            };
            webim.modifyGroupBaseInfo(
                options,
                function (resp) {
                    defer.resolve(resp);
                },
                function (err) {
                    defer.reject(err);
                }
            );
            return defer.promise;
        };

        chat.modifyGroupMember = function (gid, members, deleteMode) {
            var defer = $q.defer();
            if (deleteMode) {
                var options = {
                    'GroupId': gid,
                    'MemberToDel_Account': members
                };
                webim.deleteGroupMember(
                    options,
                    function (resp) {
                        defer.resolve(resp);
                    },
                    function (err) {
                        defer.reject(err);
                    }
                );
                return defer.promise;
            } else {
                if (iOSDevice()) {
                   // var defer = $q.defer();
                    var tool = $injector.get('SavePhotoTool');
                    cordova.plugin.http.setDataSerializer('urlencoded');
                    var option = CustomParam({
                        'groupId': gid,
                        'members': members
                    });
                    var header = { "Content-Type": 'application/x-www-form-urlencoded' };
                    cordova.plugin.http.post(Constant.ServerUrl + "/im/add-members",
                        option, header, function (response) {
                            var data = JSON.parse(response.data);
                            try {
                                $timeout(function () {
                                    return defer.resolve(data);
                                });

                            } catch (error) {

                            }
                        }, function (error) {
                            if (error.status) {
                                tool.interceptors(error.status);
                            }
                            $timeout(function () {
                                return defer.reject(error);
                            });
                        });

                    return defer.promise;

                } else {
                    return $http({
                        method: "post",
                        url: Constant.ServerUrl + "/im/add-members",
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: CustomParam({
                            'groupId': gid,
                            'members': members
                        })
                    });
                }

            }
        };

        chat.createCustomGroup = function (name, sclass, members) {
            var uid = chat.loginInfo.identifier;
            var gid = new Date().getTime() + uid;
            var options = {
                'groupId': gid + "_" + sclass.key,
                'owner': chat.loginInfo.identifier,
                'groupName': name,
                'intro': sclass.value,
                'members': members
            };
            if (iOSDevice()) {
                var defer = $q.defer();
                var tool = $injector.get('SavePhotoTool');
                cordova.plugin.http.setDataSerializer('urlencoded');
                var option = CustomParam(options);
                var header = { "Content-Type": 'application/x-www-form-urlencoded' };
                cordova.plugin.http.post(Constant.ServerUrl + "/im/create-group",
                    option, header, function (response) {
                        var data = JSON.parse(response.data);
                        try {
                            $timeout(function () {
                                return defer.resolve(data);
                            });

                        } catch (error) {

                        }
                    }, function (error) {
                        if (error.status) {
                            tool.interceptors(error.status);
                        }
                        $timeout(function () {
                            return defer.reject(error);
                        });
                    });

                return defer.promise;
            } else {
                return $http({
                    method: "post",
                    url: Constant.ServerUrl + "/im/create-group",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: CustomParam(options)
                });
            }
        };

        chat.uploadImageAndSend = function (selSess, img, onProgressCallBack) {
            var selType = selSess.type();
            var businessType; //业务类型，1-发群图片，2-向好友发图片
            if (selType == webim.SESSION_TYPE.C2C) { //向好友发图片
                businessType = webim.UPLOAD_PIC_BUSSINESS_TYPE.C2C_MSG;
            } else if (selType == webim.SESSION_TYPE.GROUP) { //发群图片
                businessType = webim.UPLOAD_PIC_BUSSINESS_TYPE.GROUP_MSG;
            }
            var opt = {
                'file': img, //图片对象
                'onProgressCallBack': onProgressCallBack, //上传图片进度条回调函数
                //'abortButton': document.getElementById('upd_abort'), //停止上传图片按钮
                'To_Account': selSess.id(), //接收者
                'businessType': businessType //业务类型
            };
            var msg = new webim.Msg(selSess, true, -1, -1, -1, chat.loginInfo.identifier, 0, chat.loginInfo.identifierNick);
            var rsp = {
                msg: msg,
                type: 'image'
            };
            $rootScope.$broadcast(BroadCast.IM_MSG_SENDING, rsp);
            //上传图片
            webim.uploadPic(opt,
                function (images) {
                    var images_obj = new webim.Msg.Elem.Images(images.File_UUID);
                    for (var i in images.URL_INFO) {
                        var img = images.URL_INFO[i];
                        var newImg;
                        var type;
                        switch (img.PIC_TYPE) {
                            case 1: //原图
                                type = 1; //原图
                                break;
                            case 2: //小图（缩略图）
                                type = 3; //小图
                                break;
                            case 4: //大图
                                type = 2; //大图
                                break;
                        }
                        newImg = new webim.Msg.Elem.Images.Image(type, img.PIC_Size, img.PIC_Width, img.PIC_Height, img.DownUrl);
                        images_obj.addImage(newImg);
                    }
                    msg.addImage(images_obj);
                    rsp = {
                        msg: msg,
                        type: 'image'
                    };
                    webim.sendMsg(msg, function (resp) {
                        if (selType == webim.SESSION_TYPE.C2C) { //私聊时，在聊天窗口手动添加一条发的消息，群聊时，长轮询接口会返回自己发的消息
                            $rootScope.$broadcast(BroadCast.IM_MSG_SENT, rsp);
                        }
                    }, function (err) {
                        console.error(err.ErrorInfo);
                        $rootScope.$broadcast(BroadCast.IM_MSG_SEND_FAIL, rsp);
                    });
                },
                function (err) {
                    console.error(err.ErrorInfo);
                    $rootScope.$broadcast(BroadCast.IM_IMAGE_SEND_FAIL, rsp);
                }
            );
        };

        chat.sendMessage = function (session, msgtosend) {
            var msgLen = webim.Tool.getStrBytes(msgtosend);
            var selType = session.type();
            var maxLen, errInfo;
            if (selType == webim.SESSION_TYPE.C2C) {
                maxLen = webim.MSG_MAX_LENGTH.C2C;
                errInfo = "消息长度超出限制(最多" + Math.round(maxLen / 3) + "汉字)";
            } else {
                maxLen = webim.MSG_MAX_LENGTH.GROUP;
                errInfo = "消息长度超出限制(最多" + Math.round(maxLen / 3) + "汉字)";
            }
            if (msgLen > maxLen) {
                alert(errInfo);
                return;
            }
            if (!session) {
                alert("消息发送失败");
            }
            var isSend = true; //是否为自己发送
            var seq = -1; //消息序列，-1表示sdk自动生成，用于去重
            var random = Math.round(Math.random() * 4294967296); //消息随机数，用于去重
            var msgTime = Math.round(new Date().getTime() / 1000); //消息时间戳
            var subType; //消息子类型
            if (selType == webim.SESSION_TYPE.C2C) {
                subType = webim.C2C_MSG_SUB_TYPE.COMMON;
                if (window.cordova) MobclickAgent.onEvent('app_send_c2c_msg');
            } else {
                //webim.GROUP_MSG_SUB_TYPE.COMMON-普通消息,
                //webim.GROUP_MSG_SUB_TYPE.LOVEMSG-点赞消息，优先级最低
                //webim.GROUP_MSG_SUB_TYPE.TIP-提示消息(不支持发送，用于区分群消息子类型)，
                //webim.GROUP_MSG_SUB_TYPE.REDPACKET-红包消息，优先级最高
                subType = webim.GROUP_MSG_SUB_TYPE.COMMON;
                if (window.cordova) MobclickAgent.onEvent('app_send_group_msg');
            }
            var msg = new webim.Msg(session, isSend, seq, random, msgTime, chat.loginInfo.identifier, subType, chat.loginInfo.identifierNick);

            var text_obj, face_obj, tmsg, emotionIndex, emotion, restMsgIndex;
            //解析文本和表情
            var expr = /\[[^[\]]{1,3}\]/mg;
            var emotions = msgtosend.match(expr);
            if (!emotions || emotions.length < 1) {
                text_obj = new webim.Msg.Elem.Text(msgtosend);
                msg.addText(text_obj);
            } else {

                for (var i = 0; i < emotions.length; i++) {
                    tmsg = msgtosend.substring(0, msgtosend.indexOf(emotions[i]));
                    if (tmsg) {
                        text_obj = new webim.Msg.Elem.Text(tmsg);
                        msg.addText(text_obj);
                    }
                    emotionIndex = webim.EmotionDataIndexs[emotions[i]];
                    emotion = webim.Emotions[emotionIndex];

                    if (emotion) {
                        face_obj = new webim.Msg.Elem.Face(emotionIndex, emotions[i]);
                        msg.addFace(face_obj);
                    } else {
                        text_obj = new webim.Msg.Elem.Text(emotions[i]);
                        msg.addText(text_obj);
                    }
                    restMsgIndex = msgtosend.indexOf(emotions[i]) + emotions[i].length;
                    msgtosend = msgtosend.substring(restMsgIndex);
                }
                if (msgtosend) {
                    text_obj = new webim.Msg.Elem.Text(msgtosend);
                    msg.addText(text_obj);
                }
            }
            var rsp = {
                msg: msg,
                type: 'text'
            };
            $rootScope.$broadcast(BroadCast.IM_MSG_SENDING, rsp);
            webim.sendMsg(msg, function (resp) {
                $rootScope.$broadcast(BroadCast.IM_MSG_SENT, rsp);
            }, function (err) {
                console.error(err.ErrorInfo);
                $rootScope.$broadcast(BroadCast.IM_MSG_SEND_FAIL, rsp);
            });
        };

        chat.logout = function () {
            if (chat.loginInfo) {
                webim.logout(
                    function (resp) {
                        chat.loginInfo = null;
                        $window.location.reload();
                    }
                );
            }
        };

        chat.getUserDetail = function (id) {
            if (iOSDevice()) {
                var defer = $q.defer();
                var tool = $injector.get('SavePhotoTool');
                cordova.plugin.http.setDataSerializer('json');
                var option = { id: id };
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/getinfo",
                    option, header, function (response) {
                        var data = JSON.parse(response.data);
                        try { 
                            $timeout(function () {
                                return defer.resolve(data);
                            });

                        } catch (error) {

                        }
                    }, function (error) {
                        if (error.status) {
                            tool.interceptors(error.status);
                        }
                        $timeout(function () {
                            return defer.reject(error);
                        });
                    });

                return defer.promise;
            } else {
                return $http.get(Constant.ServerUrl + "/getinfo", {
                    params: {
                        id: id
                    }
                })
                    .then(function (response) {
                        return response.data;
                    }, function (error) {
                        return $q.reject(error);
                    });
            }

        };

        return chat;
    }])
    .factory('SavePhotoTool', ['$ionicLoading', '$q', '$injector', function ($ionicLoading, $q, $injector) {
        var tool = {};
        tool.savePhoto = function (url) {
            var config = {
                allowSave: true,
                album: 'Xgenban'
            };
            tool.config = angular.extend({}, config, {
                allowSave: true
            });
            if (window.cordova) {
                cordova.plugins.photoLibrary.getAlbums(
                    function (result) {
                        tool.saveImgUrl(url);
                    },
                    function (err) {
                        if (err.startsWith('Permission')) {
                            cordova.plugins.photoLibrary.requestAuthorization(
                                function () {
                                    tool.saveImgUrl(url);
                                },
                                function (err) {
                                    // User denied the access
                                    console.log(err);
                                }, {
                                read: true,
                                write: true
                            }
                            );
                        }
                    }
                );

            }
        };

        tool.saveImgUrl = function (url) {
            if (window.cordova) {
                cordova.plugins.photoLibrary.saveImage(url + '?ext=.jpg', tool.config.album, function (libraryItem) {
                    $ionicLoading.show({
                        template: '保存成功',
                        duration: 1500
                    });
                }, function (err) {
                    console.log(err);
                });
            }

        };

        var doLogin = function () {
            var state = $injector.get('$state');
            var user = $injector.get('UserPreference');
            var auth = $injector.get('AuthorizeService');
            if (!auth.logining && state.current.url !== '/login') {
                var loginModel = {
                    remember: true,
                    username: user.get('username', ''),
                    password: user.get('password', '')
                };
                if (loginModel.username !== '' && loginModel.password !== '') {
                    console.error('timeout, try auto login.');
                    auth.login(loginModel).then(function (res) {
                        if (res && res.result)
                            state.go('tabsController.mainPage');
                    });
                } else {
                    console.error('username or password not found');
                    state.go('login');
                }
            }
        };

        tool.interceptors = function (status) {

            var toaster = $injector.get('toaster');
            var MESSAGES = $injector.get('MESSAGES');
            var rootScope = $injector.get('$rootScope');
            var BroadCast = $injector.get('BroadCast');
            // console.log(' response ---:' + rejection.status);
            rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
            switch (status) {
                case -1:
                   
                case -4:
                    toaster.warning({
                        title: MESSAGES.REQUEST_ERROR,
                        body: MESSAGES.CONNECT_TIMEOUT_MSG
                    });
                    break;

                case 408:
                    toaster.warning({
                        title: MESSAGES.REQUEST_ERROR,
                        body: MESSAGES.CONNECT_TIMEOUT_MSG
                    });
                    break;
                case 401:
                    doLogin();
                    break;
                case 404:
                    toaster.error({
                        title: MESSAGES.CONNECT_ERROR,
                        body: MESSAGES.CONNECT_ERROR,//rejection.data.message
                    });
                    break;
                case 500:
                    toaster.error({
                        title: MESSAGES.CONNECT_ERROR,
                        body: MESSAGES.SERVER_ERROR
                    });
                    break;
                case 503:
                    toaster.error({
                        title: MESSAGES.CONNECT_ERROR,//rejection.data.message,
                        body: ''
                    });
                    break;
                default:
                    toaster.error({
                        title: MESSAGES.CONNECT_ERROR,
                        body: MESSAGES.CONNECT_ERROR_MSG
                    });
                    break;
            }

        };

        return tool;
    }])
    .factory('Translate', ['$translate', function ($translate) {
        var T = {};
        T.translate = function (key) {
            if (key) {
                return $translate.instant(key);
            }
            return key;
        };
        return T;
    }])
    .factory('DownloadFile', ['$ionicLoading', '$timeout', '$cordovaFileTransfer', '$cordovaFileOpener2', 'toaster', 'MESSAGES', function ($ionicLoading, $timeout, $cordovaFileTransfer, $cordovaFileOpener2, toaster, MESSAGES) {
        var file = {};
        file.readFile = function (url) {
            $ionicLoading.show({
                template: "请稍候，正在读取：0%"
            });

            var fileType = url.slice(url.lastIndexOf('.') + 1);
            var mimeType = '';
            switch (fileType.toLowerCase()) {
                case 'txt':
                    mimeType = 'text/plain';
                    break;
                case 'docx':
                    mimeType = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
                    break;
                case 'doc':
                    mimeType = 'application/msword';
                    break;
                case 'pptx':
                    mimeType = 'application/vnd.openxmlformats-officedocument.presentationml.presentation';
                    break;
                case 'ppt':
                    mimeType = 'application/vnd.ms-powerpoint';
                    break;
                case 'xlsx':
                    mimeType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                    break;
                case 'xls':
                    mimeType = 'application/vnd.ms-excel';
                    break;
                case 'zip':
                    mimeType = 'application/x-zip-compressed';
                    break;
                case 'rar':
                    mimeType = 'application/octet-stream';
                    break;
                case 'pdf':
                    mimeType = 'application/pdf';
                    break;
                case 'jpg':
                    mimeType = 'image/jpeg';
                    break;
                case 'png':
                    mimeType = 'image/png';
                    break;
                default:
                    mimeType = 'application/' + fileType;
                    break;
            }
            var targetPath;
            var isIos = ionic.Platform.isIOS();
            if (window.cordova) {
                if (isIos) {
                    targetPath = cordova.file.tempDirectory + fileType;
                } else {
                    targetPath = cordova.file.dataDirectory + fileType;
                }
            }

            // var targetPath = "file:///sdcard/Xgenban/update.apk"; //APP下载存放的路径，可以使用cordova file插件进行相关配置
            var trustHosts = true;
            var options = {};
            $cordovaFileTransfer.download(url, targetPath, options, trustHosts).then(function (result) {
                //  alert('succeed');
                // 打开下载下来的文件
                if (window.cordova) {
                    // if(!isIos){
                    //     $cordovaFileOpener2.appIsInstalled('com.adobe.reader').then(function(res) {
                    //         if (res.status === 0) {
                    //           alert('未检测到相应的软件能打开此文档');
                    //         } else {
                    //             // Adobe Reader is installed.
                    //         }
                    //     });
                    // }
                    $cordovaFileOpener2.open(targetPath, mimeType).then(function () {
                        // console.log('open succeed');
                        // alert('open succeed');
                        // 成功
                    }, function (err) {
                        // 错误
                        // alert('open error');
                        console.log(err);
                    });
                }

                $ionicLoading.hide();
            }, function (err) {
                toaster.error({
                    title: MESSAGES.DOWNLOAD_ERROR,
                    body: err.exception
                });
                $ionicLoading.hide();
            }, function (progress) {
                $timeout(function () {
                    var downloadProgress = (progress.loaded / progress.total) * 100;
                    $ionicLoading.show({
                        template: "请稍候，正在读取：" + Math.floor(downloadProgress) + "%"
                    });
                    if (downloadProgress > 99) {
                        $ionicLoading.hide();
                    }
                });
            });
        };
        return file;
    }]);
