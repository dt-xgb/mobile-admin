/**
 * Created by hewz on 2016-07-19.
 */
angular.module('app.constants', [])
    .constant('Constant', {
        'debugMode': true,
        'versionCode': 20,
        'version': '1.2.8',
       // 'ServerUrl': 'http://192.168.1.177:8090/mob',//liao bin
       // 'ServerUrl': 'http://192.168.1.190:8090/mob',//xiao chen
        //  'ServerUrl': 'http://192.168.1.225:8090/mob',//xiao feng
     'ServerUrl': 'https://test17.xgenban.com/sctserver/mob',
      //'ServerUrl' : 'https://www.xgenban.com/sctserver/mob',
        'IMAppID': 1400026815, //dev
       //'IMAppID': 1400026570,//pro
        'serverTimeout': 30000,
        'heavyServerTimeout': 120000,
        'reqLimit': 10,
        'SMS_REQ_INTERVAL': 60,
        'CAPTURE_IMAGE_RANGE': 1080,
        'IM_USER_AVATAR': 'img/icon/person.png',
        'IM_GROUP_AVATAR': 'img/icon/group.png',
        'IMAccountType': 10559,
        'NEWS_STATUS': {
            'TEACHER_REVIEW': {
                'key': '3',
                'text': '教师审核中'
            },
            'TEACHER_REVIEW_PASS': {
                'key': '4',
                'text': '审核通过'
            },
            'ADMIN_REVIEW_PASS': {
                'key': '5',
                'text': '审核通过'
            },
            'TEACHER_IGNORE': {
                'key': '6',
                'text': '已拒绝'
            },
            'ADMIN_REVIEW': {
                'key': '7',
                'text': '管理员审核中'
            }
        },
        'USER_ROLES': {
            'PROVINCE_ADMIN': '7',
            'CITY_ADMIN': '8',
            'AREA_ADMIN': '4',
            'DISTRICT_ADMIN': '9',
            'SCHOOL_ADMIN': '5'
        }
    })
    .constant('MESSAGES', {
        'BADGE_UPDATE': 'BADGE_UPDATE',
        'CONNECT_ERROR': '请求失败',
        'DOWNLOAD_ERROR': '下载失败',
        'FEEDBACK_SUCCESS': '反馈成功',
        'SAVE_SUCCESS': '修改成功',
        'PUSH_ON_FAIL': '推送开启失败',
        'PUSH_ON_FAIL_REASON': '检测到系统设置中已禁止应用推送，请先在系统设置中勾选小跟班管理的通知权限',
        'FEEDBACK_SUCCESS_MSG': '感谢您的建议和支持',
        'CONNECT_ERROR_MSG': '连接服务器失败,请检查网络连接',
        'CONNECT_TIMEOUT_MSG': '服务器连接超时，请重试',
        'SERVER_ERROR': '服务器异常,请稍后再试',
        'CONTACT_FETCH_FAIL': '通讯录获取失败',
        'NOT_FOUND': '未找到请求资源',
        'SESSION_TIMEOUT': '登录超时',
        'NEW_NOTICE': '发布成功',
        'NEW_NEWS': '发布成功, 待审核后可见',
        'NEW_NEWS_ADMIN': '发布成功',
        'INFO_UPDATED': '个人资料更新成功',
        'PWD_UPDATED': '密码修改成功。',
        'NO_THUMB': '请选择缩略图',
        'REMIND': '温馨提示',
        'NO_NAME': '请输入姓名',
        'NO_IMAGE_TO_CROP': '此文章未含图片',
        'NO_NEWS_TYPE': '请选择一项风采类型',
        'NO_TARGET_CLASS': '请选择可见班级',
        'NO_OLD_PASSWORD': '请输入当前密码',
        'NO_NEW_PASSWORD': '请输入新密码',
        'NO_CONFIRM_PASSWORD': '请再次输入密码',
        'PASSWORD_CONFIRM_ERROR': '两次输入的密码不一致',
        'LOGIN_ERROR': '登录失败',
        'OPERATE_ERROR': '操作失败',
        'REQUEST_ERROR': '请求失败',
        'AT_LEAST_IMAGE': '请至少上传一张正文图片',
        'LOGIN_NO_USERNAME': '请输入您的登录账号',
        'LOGIN_NO_PASSWORD': '请输入您的登录密码',
        'LOGIN_USERNAME_PATTERN': '您输入的账号中包含特殊字符,仅限字母和数字',
        'LOGIN_PASSWORD_PATTERN': '您输入的密码中包含特殊字符,仅限字母和数字'
    })
    .constant('BroadCast', {
        'NEW_PUSH_REV': 'NEW_PUSH_REV',
        'IMAGE_SELECTED': 'IMAGE_SELECTED',
        'IMAGE_CROP': 'IMAGE_CROP',
        'FEEDBACK': 'FEEDBACK',
        'EDIT_INFO': 'EDIT_INFO',
        'PASSWORD_CHANGE': 'PASSWORD_CHANGE',
        'CHILD_INFO_REV': 'CHILD_INFO_REV',
        'DIC_REV': 'DIC_REV',
        'CONNECT_ERROR': 'CONNECT_ERROR',
        'LOGIN_RESULT_RECEIVED': 'LOGIN_RESULT_RECEIVED',
        'NEW_NOTICE': 'NEW_NOTICE',
        'NEW_NEWS': 'NEW_NEWS',
        'NOTICE_LIST_REV': 'NOTICE_LIST_REV',
        'NEWS_LIST_REV': 'NEWS_LIST_REV',
        'NEWS_STATE_CHANGED': 'NEWS_STATE_CHANGED',
        'STICK_RST_REV': 'STICK_RST_REV',
        'UNDO_STICK_RST_REV': 'UNDO_STICK_RST_REV',
        'SET_FOCUS_RST_REV': 'SET_FOCUS_RST_REV',
        'SEND_TO_ADMIN_RST': 'SEND_TO_ADMIN_RST',
        'ALLOW_PUBLISH_RST_REV': 'ALLOW_PUBLISH_RST_REV',
        'IGNORE_RST_REV': 'IGNORE_RST_REV',
        'BANNER_LIST_REV': 'BANNER_LIST_REV',
        'DELETE_NEWS_REV': 'DELETE_NEWS_REV',
        'CANCEL_PUBLISH_REV': 'CANCEL_PUBLISH_REV',
        'DELETE_NOTICE_REV': 'DELETE_NOTICE_REV',
        'NEWS_TYPE_REV': 'NEWS_TYPE_REV',
        'IM_MSG_SENDING': 'IM_MSG_SENDING',
        'IM_MSG_SENT': 'IM_MSG_SENT',
        'IM_MSG_SEND_FAIL': 'IM_MSG_SEND_FAIL',
        'IM_IMAGE_SEND_FAIL': 'IM_IMAGE_SEND_FAIL',
        'IM_REV_CONTACTS': 'IM_REV_CONTACTS',
        'IM_RECENT_CONTACTS': 'IM_RECENT_CONTACTS',
        'IM_C2C_HISTORY_MESSAGE': 'IM_C2C_HISTORY_MESSAGE',
        'IM_GROUP_HISTORY_MESSAGE': 'IM_GROUP_HISTORY_MESSAGE',
        'IM_NEW_MESSAGE': 'IM_NEW_MESSAGE'
    });

// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('app.controllers', []);
angular.module('app', ['ionic', 'ngCordova', 'app.constants', 'app.controllers', 'app.routes', 'app.services', 'app.directives', 'app.requester',
    'jrCrop', 'monospaced.elastic', 'angular-intro',  'ionicImgCache', 'angular-timeline','toaster', 'ionic-timepicker', 'ionic-datepicker', 'pdfjsViewer', 'ionic-image-view','ion-gallery','angular-timeline'])
    .config(function(ionGalleryConfigProvider) {
        ionGalleryConfigProvider.setGalleryConfig({
                                action_label: 'Done',
                                toggle: false,
                                row_size: 3,
                                fixed_row_size: true
        });
      })
    .run(function ($ionicPlatform, PushService, $ionicPopup, $state, $ionicHistory, SettingService, $rootScope) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                //cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

                cordova.plugins.Keyboard.disableScroll(false);
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
                window.addEventListener('native.keyboardshow', function (keyboardParameters) {
                    document.body.classList.add('keyboard-open');
                    $rootScope.keyboardHeight = keyboardParameters.keyboardHeight;
                });
               
                // if(window.cordova){
                //     window.MobileAccessibility.usePreferredTextZoom(false);
                // }

               SettingService.checkUpdate(true);
                setTimeout(function () {
                    if (navigator.splashscreen)
                        navigator.splashscreen.hide();
                    if (window.StatusBar) {
                        // org.apache.cordova.statusbar required
                        StatusBar.styleLightContent();
                    }
                    if (window.cordova) {
                        if (ionic.Platform.isIOS()) {
                            cordova.plugins.Keyboard.disableScroll(true);
                            MobclickAgent.init('592f6e3c07fe65473d0006a0', 'SCT-Official');
                        }else {
                            MobclickAgent.init('592f6dc3734be444e0002262', 'SCT-Official');
                        }
                        PushService.init();
                    }
                }, 500);
            }

            // Disable BACK button on home
            $ionicPlatform.registerBackButtonAction(function (event) {
                if ($state.current.name.indexOf('tabsController') === 0 || $state.current.name === "login") {
                    var confirmPopup = $ionicPopup.confirm({
                        title: '温馨提示',
                        template: '确认退出应用吗？',
                        cancelText: '取消',
                        okText: '确认',
                        okType: 'button-balanced'
                    });
                    confirmPopup.then(function (res) {
                        if (res) {
                            navigator.app.exitApp();
                        }
                    });
                }
                else {
                    $ionicHistory.goBack();
                }
            }, 100);
        });
    })
    .config(function ($ionicConfigProvider, $httpProvider,  msdElasticConfig, ionicTimePickerProvider, ionicImgCacheProvider, ionicDatePickerProvider,Constant) {
        if (!ionic.Platform.isIOS()) {
            $ionicConfigProvider.scrolling.jsScrolling(false);
        }
        $ionicConfigProvider.views.swipeBackEnabled(false);
        $ionicConfigProvider.views.maxCache(15);
        $httpProvider.defaults.withCredentials = true;
        //enable cors
        $httpProvider.defaults.useXDomain = true;
        $httpProvider.defaults.headers.post["Content-Type"] = "application/json";
        $httpProvider.interceptors.push('httpInterceptor');

        // note that you can also chain configs
        //$ionicConfigProvider.backButton.text('返回').icon('ion-chevron-left');
        $ionicConfigProvider.tabs.position("bottom");
        $ionicConfigProvider.navBar.alignTitle("center");

        msdElasticConfig.append = '\n';

        // cordova-plugin-advanced-http 
        if(iOSDevice()){
            // cordova.plugin.http.setRequestTimeout(10.0);
            // cordova.plugin.http.setHeader(Constant.ServerUrl, 'Header', 'application/json');
        }
       

        //国际化
          //translate安全策略
    //   $translateProvider.translations('ZH', testProvider.$get().providerzh);
    //   $translateProvider.translations('EN', testProvider.$get().provideren);
    //   $translateProvider.preferredLanguage('EN');//首选语言
    //   $translateProvider.fallbackLanguage('EN');

        var timePickerObj = {
            inputTime: (((new Date()).getHours() * 60 * 60) + ((new Date()).getMinutes() * 60)),
            format: 24,
            step: 1,
            setLabel: '确定',
            closeLabel: '取消'
        };
        ionicTimePickerProvider.configTimePicker(timePickerObj);
        var datePickerObj = {
            inputDate: new Date(),
            titleLabel: '请选择日期',
            setLabel: '确定',
            closeLabel: '关闭',
            mondayFirst: true,
            weeksList: ["日", "一", "二", "三", "四", "五", "六"],
            monthsList: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
            templateType: 'popup',
            showTodayButton: false,
            dateFormat: 'yyyy-MM-dd',
            closeOnSelect: false,
            disableWeekdays: []
        };
        ionicDatePickerProvider.configDatePicker(datePickerObj);

        ionicImgCacheProvider.debug(false);
        ionicImgCacheProvider.quota(200);
        ionicImgCacheProvider.folder('Xgenban');
        ionicImgCacheProvider.cacheClearSize(200);
    })
    .filter('nl2br', ['$filter',
        function ($filter) {
            return function (data) {
                if (!data) return data;
                return data.replace(/\n\r?/g, '<br />');
            };
        }
    ])
    .filter('parseUrl', function() {
        var urls = /(\b(https?|ftp):\/\/[A-Z0-9+&@#\/%?=~_|!:,.;-]*[-A-Z0-9+&@#\/%=~_|])/gim;
        //var emails = /(\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6})/gim

        return function(text) {
            if(text.match(urls)) {
                text = text.replace(urls, "<a href=\"$1\" target=\"_system\">$1</a>");
            }
            // if(text.match(emails)) {
            //     text = text.replace(emails, "<a href=\"mailto:$1\">$1</a>");
            // }
            return text;
        };
    })
    .filter('orderContactBy', function () {
        return function (items, field, reverse) {
            var filtered = [];
            angular.forEach(items, function (item) {
                filtered.push(item);
            });
            filtered.sort(function (a, b) {
                return (a[field].localeCompare(b[field]));
            });
            if (reverse) filtered.reverse();
            return filtered;
        };
    })
    .filter('filterweekday', [function () {
        return function (list, day) {
            var i;
            var tempList = [];
            var temp;

            if (angular.isDefined(list) &&
                list.length > 0) {
                for (temp = list[i = 0]; i < list.length; temp = list[++i]) {
                    if (temp.weekNum % 7 == day)
                        tempList.push(temp);
                }
            }
            return tempList;
        };
    }]);

/**
 * Created by hewz on 16/8/9.
 */

Date.prototype.Format = function (fmt) {
	var o = {
		"M+": this.getMonth() + 1, //月份
		"d+": this.getDate(), //日
		"h+": this.getHours(), //小时
		"m+": this.getMinutes(), //分
		"s+": this.getSeconds(), //秒
		"q+": Math.floor((this.getMonth() + 3) / 3), //季度
		"S": this.getMilliseconds() //毫秒
	};
	if (/(y+)/.test(fmt))
		fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	for (var k in o)
		if (new RegExp("(" + k + ")").test(fmt))
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
	return fmt;
};

function formatTimeWithoutSecends(timestamp, format) {
	if (!timestamp) {
		return 0;
	}
	var formatTime;
	format = format || 'yyyy-MM-dd hh:mm';
	var date = new Date(timestamp * 1000);
	var o = {
		"M+": date.getMonth() + 1, //月份
		"d+": date.getDate(), //日
		"h+": date.getHours(), //小时
		"m+": date.getMinutes(), //分
		"s+": date.getSeconds() //秒
	};
	if (/(y+)/.test(format)) {
		formatTime = format.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
	} else {
		formatTime = format;
	}
	for (var k in o) {
		if (new RegExp("(" + k + ")").test(formatTime))
			formatTime = formatTime.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
	}
	return formatTime;
}
//将文本转化成html-suggest
function encodeHtml(str) {
	var html = '';
	html += webim.Tool.formatText2Html(str);

	var urls = /(\b(https?|ftp):\/\/[A-Z0-9+&@#\/%?=~_|!:,.;-]*[-A-Z0-9+&@#\/%=~_|])/gim;
	if (html.match(urls)) {
		html = html.replace(urls, "<a href=\"$1\" target=\"_system\">$1</a>");
	}
	return html;
}

function getWeekday(date) {
	if (date && date.length == 19) {
		var d = new Date(date.replace(/\s+/g, 'T').concat('.000+08:00'));
		return date.substr(0, 10) + " " + getWeekdayByIndex(d.getDay());
	}
	return date;
}

function getWeekdayByIndex(dayIndex) {
	switch (String(dayIndex)) {
		case '1':
			return '周一';
		case '2':
			return '周二';
		case '3':
			return '周三';
		case '4':
			return '周四';
		case '5':
			return '周五';
		case '6':
			return '周六';
		case '0':
			return '周日';
		default:
			return '';
	}
}

function delHtmlTag(str) {
	if (str) {
		str = str.replace(/<\/?[^>]*>/g, ''); //去除HTML tag
		str = str.replace(/[ | ]*\n/g, '\n'); //去除行尾空白
		str = str.replace(/\n[\s| | ]*\r/g, '\n'); //去除多余空行
		str = str.replace(/&nbsp;/ig, ''); //去掉&nbsp;
	}
	return str;
}

function convertMsgtoPushStr(msg) {
	var html = "",
		elems, elem, type, content;
	elems = msg.getElems(); //获取消息包含的元素数组
	var count = elems.length;
	for (var i = 0; i < count; i++) {
		elem = elems[i];
		type = elem.getType(); //获取元素类型
		content = elem.getContent(); //获取元素对象
		var eleHtml;
		switch (type) {
			case webim.MSG_ELEMENT_TYPE.TEXT:
				eleHtml = convertTextMsgToHtml(content);
				//转义，防XSS
				html += webim.Tool.formatText2Html(eleHtml);
				break;
			case webim.MSG_ELEMENT_TYPE.FACE:
				html += getFaceIndexStr(content);
				break;
			case webim.MSG_ELEMENT_TYPE.IMAGE:
				html += '[图片]';
				break;
			default:
				html += '[未知消息类型]';
				webim.Log.error('未知消息元素类型');
				break;
		}
		if (html.length > 20)
			break;
	}
	return delHtmlTag(html);
}

function getFaceIndexStr(content) {
	var emotionDataIndexs = [
		"[惊讶]", "[撇嘴]", "[色]", "[发呆]", "[得意]", "[流泪]", "[害羞]", "[闭嘴]",
		"[睡]", "[大哭]", "[尴尬]", "[发怒]", "[调皮]", "[龇牙]", "[微笑]", "[难过]",
		"[酷]", "[冷汗]", "[抓狂]", "[吐]", "[偷笑]", "[可爱]", "[白眼]", "[傲慢]",
		"[饿]", "[困]", "[惊恐]", "[流汗]", "[憨笑]", "[大兵]", "[奋斗]", "[咒骂]",
		"[疑问]", "[嘘]", "[晕]"
	];
	var data = content.getData();
	var index = webim.EmotionDataIndexs[data];
	if (index >= 0 && index < emotionDataIndexs.length)
		return emotionDataIndexs[index];
	else
		return '';
}

function getSuggestEmotions() {
	var EMOJIS =
		"😀 😃 😄 😁 😆 😅 😂 😊 😇 🙂 😉 😌 😭 😍 😘 😗 😙 😚 😋 😜 😝 😛 🤑 🤗 🤓 😎 😏 😒 😞 😔 😟 😕 🙁 ☹️" +
		" 😣 😖 😫 😩 😤 😠 😡 😶 😐 😑 😯 😦 😧 😮 😲 😵 😳 😱 😨 😰 😢 😥 😓 😪 😴 🙄 🤔 😬 🤐 😷 🤒 🤕 😈 👿 👹 👺" +
		" 💩 👻 💀 ☠️ 👽 👾 🤖 🎃 😺 😸 😹 😻 😼 😽 🙀 😿 😾 👐 🙌 👏 🙏 🤝 👍 👎 👊 ✊" +
		" 👋 🤙 💪 🖕 ✍️ 🤳 💅 🖖 💄 💋 👄 👅 👂 👃 👣 👁 👀 🗣 👤 👥 🕶 🌂 ☂️";
	var EmojiArr = EMOJIS.split(" ");
	var groupNum = Math.ceil(EmojiArr.length / 24);
	var items = [];

	for (var i = 0; i < groupNum; i++) {
		items.push(EmojiArr.slice(i * 24, (i + 1) * 24));
	}
	return items;
}

//将文本转化成html-suggest
function encodeHtml(str) {
	var html = '';
	html += webim.Tool.formatText2Html(str);

	var urls = /(\b(https?|ftp):\/\/[A-Z0-9+&@#\/%?=~_|!:,.;-]*[-A-Z0-9+&@#\/%=~_|])/gim;
	if (html.match(urls)) {
		html = html.replace(urls, "<a href=\"$1\" target=\"_system\">$1</a>");
	}
	return html;
}

//把消息转换成Html
function convertMsgtoHtml(msg) {
	var html = "",
		elems, elem, type, content;
	elems = msg.getElems(); //获取消息包含的元素数组
	var count = elems.length;
	for (var i = 0; i < count; i++) {
		elem = elems[i];
		type = elem.getType(); //获取元素类型
		content = elem.getContent(); //获取元素对象
		var eleHtml;
		switch (type) {
			case webim.MSG_ELEMENT_TYPE.TEXT:
				eleHtml = convertTextMsgToHtml(content);
				//转义，防XSS
				html += webim.Tool.formatText2Html(eleHtml);
				break;
			case webim.MSG_ELEMENT_TYPE.FACE:
				html += convertFaceMsgToHtml(content);
				break;
			case webim.MSG_ELEMENT_TYPE.IMAGE:
				if (i <= count - 2) {
					var customMsgElem = elems[i + 1]; //获取保存图片名称的自定义消息elem
					var imgName = customMsgElem.getContent().getData(); //业务可以自定义保存字段，demo中采用data字段保存图片文件名
					html += convertImageMsgToHtml(content, imgName);
					i++; //下标向后移一位
				} else {
					html += convertImageMsgToHtml(content);
				}
				break;
			case webim.MSG_ELEMENT_TYPE.SOUND:
				html += convertSoundMsgToHtml(content);
				break;
			case webim.MSG_ELEMENT_TYPE.FILE:
				html += convertFileMsgToHtml(content);
				break;
			case webim.MSG_ELEMENT_TYPE.LOCATION:
				html += convertLocationMsgToHtml(content);
				break;
			case webim.MSG_ELEMENT_TYPE.CUSTOM:
				eleHtml = convertCustomMsgToHtml(content);
				//转义，防XSS
				html += webim.Tool.formatText2Html(eleHtml);
				break;
			case webim.MSG_ELEMENT_TYPE.GROUP_TIP:
				eleHtml = convertGroupTipMsgToHtml(content);
				//转义，防XSS
				html += webim.Tool.formatText2Html(eleHtml);
				break;
			default:
				webim.Log.error('未知消息元素类型: elemType=' + type);
				break;
		}
	}
	if (type == webim.MSG_ELEMENT_TYPE.TEXT) {
		var urls = /(\b(https?|ftp):\/\/[A-Z0-9+&@#\/%?=~_|!:,.;-]*[-A-Z0-9+&@#\/%=~_|])/gim;
		if (html.match(urls)) {
			html = html.replace(urls, "<a href=\"$1\" target=\"_system\">$1</a>");
		}
	}
	return html;
}

//解析文本消息元素
function convertTextMsgToHtml(content) {
	return content.getText();
}
//解析表情消息元素
function convertFaceMsgToHtml(content) {
	var faceUrl = null;
	var data = content.getData();
	var index = webim.EmotionDataIndexs[data];

	var emotion = webim.Emotions[index];
	if (emotion && emotion[1]) {
		faceUrl = emotion[1];
	}
	if (faceUrl) {
		return "<img src='" + faceUrl + "'/>";
	} else {
		return data;
	}
}
//解析图片消息元素
function convertImageMsgToHtml(content, imageName) {
	var smallImage = content.getImage(webim.IMAGE_TYPE.SMALL); //小图
	var bigImage = content.getImage(webim.IMAGE_TYPE.LARGE); //大图
	var oriImage = content.getImage(webim.IMAGE_TYPE.ORIGIN); //原图
	if (!bigImage) {
		bigImage = smallImage;
	}
	if (!oriImage) {
		oriImage = smallImage;
	}
	var bigUrl = bigImage.getUrl();
	return '<img name="' + imageName + '" src="' + smallImage.getUrl() + '" id="' + content.getImageId() + '" ng-click=\'onImageClick("' + bigUrl + '")\' style="max-width: 100%" ion-img-cache />';
}
//解析语音消息元素
function convertSoundMsgToHtml(content) {
	var second = content.getSecond(); //获取语音时长
	var downUrl = content.getDownUrl();
	if (webim.BROWSER_INFO.type == 'ie' && parseInt(webim.BROWSER_INFO.ver) <= 8) {
		return '[这是一条语音消息]demo暂不支持ie8(含)以下浏览器播放语音,语音URL:' + downUrl;
	}
	return '<audio id="uuid_' + content.uuid + '" src="' + downUrl + '" controls="controls" onplay="onChangePlayAudio(this)" preload="none"></audio>';
}
//解析文件消息元素
function convertFileMsgToHtml(content) {
	var fileSize, unitStr;
	fileSize = content.getSize();
	unitStr = "Byte";
	if (fileSize >= 1024) {
		fileSize = Math.round(fileSize / 1024);
		unitStr = "KB";
	}
	// return '<a href="' + content.getDownUrl() + '" title="点击下载文件" ><i class="glyphicon glyphicon-file">&nbsp;' + content.getName() + '(' + fileSize + unitStr + ')</i></a>';

	return '<a href="javascript:;" onclick=\'webim.onDownFile("' + content.uuid + '")\' title="点击下载文件" ><i class="glyphicon glyphicon-file">&nbsp;' + content.name + '(' + fileSize + unitStr + ')</i></a>';
}
//解析位置消息元素
function convertLocationMsgToHtml(content) {
	return '经度=' + content.getLongitude() + ',纬度=' + content.getLatitude() + ',描述=' + content.getDesc();
}
//解析自定义消息元素
function convertCustomMsgToHtml(content) {
	var data = content.getData(); //自定义数据
	var desc = content.getDesc(); //描述信息
	var ext = content.getExt(); //扩展信息
	return "data=" + data + ", desc=" + desc + ", ext=" + ext;
}
//解析群提示消息元素
function convertGroupTipMsgToHtml(content) {
	var WEB_IM_GROUP_TIP_MAX_USER_COUNT = 5;
	var text = "";
	var maxIndex = WEB_IM_GROUP_TIP_MAX_USER_COUNT - 1;
	var opType, opUserId, userIdList;
	opType = content.getOpType(); //群提示消息类型（操作类型）
	opUserId = content.getOpUserId(); //操作人id
	var service = angular.element(document.querySelector('[ng-app]')).injector().get("ChatService");
	var info = service.getMemberInfoFromMap(webim.SESSION_TYPE.C2C, opUserId);
	var opUserName = info ? info.name : opUserId;
	if (opUserName === 'XgbAdmin')
		opUserName = '管理员';
	var tmp, username;
	switch (opType) {
		case webim.GROUP_TIP_TYPE.JOIN: //加入群
			userIdList = content.getUserIdList();
			for (var m in userIdList) {
				tmp = service.getMemberInfoFromMap(webim.SESSION_TYPE.C2C, userIdList[m]);
				username = tmp ? tmp.name : '';
				if (username !== '')
					text += (username + '、');
				if (userIdList.length > WEB_IM_GROUP_TIP_MAX_USER_COUNT && m == maxIndex) {
					break;
				}
			}
			if (text.length > 0) {
				text = text.substring(0, text.length - 1);
				if (userIdList.length > WEB_IM_GROUP_TIP_MAX_USER_COUNT)
					text += "等共" + userIdList.length + "人";
				text += "加入该群";
			} else
				return '';
			break;
		case webim.GROUP_TIP_TYPE.QUIT: //退出群
			text += opUserName + "离开该群";
			break;
		case webim.GROUP_TIP_TYPE.KICK: //踢出群
			var tem = opUserName + "将";
			userIdList = content.getUserIdList();
			for (var mm in userIdList) {
				tmp = service.getMemberInfoFromMap(webim.SESSION_TYPE.C2C, userIdList[mm]);
				username = tmp ? tmp.name : '';
				if (username !== '')
					text += (username + '、');
				if (userIdList.length > WEB_IM_GROUP_TIP_MAX_USER_COUNT && mm == maxIndex) {
					break;
				}
			}
			if (text.length > 0) {
				text = text.substring(0, text.length - 1);
				if (userIdList.length > WEB_IM_GROUP_TIP_MAX_USER_COUNT)
					text += "等共" + userIdList.length + "人";
				text = tem + text + "移出该群";
			} else
				return '';
			break;
		case webim.GROUP_TIP_TYPE.SET_ADMIN: //设置管理员
			text += opUserName + "将";
			userIdList = content.getUserIdList();
			for (var mmm in userIdList) {
				tmp = service.getMemberInfoFromMap(webim.SESSION_TYPE.C2C, userIdList[mmm]);
				username = tmp ? tmp.name : userIdList[mmm];
				text += (username + '、');
				if (userIdList.length > WEB_IM_GROUP_TIP_MAX_USER_COUNT && mmm == maxIndex) {
					text += "等共" + userIdList.length + "人";
					break;
				}
			}
			text += "设为管理员";
			break;
		case webim.GROUP_TIP_TYPE.CANCEL_ADMIN: //取消管理员
			text += opUserName + "取消";
			userIdList = content.getUserIdList();
			for (var m4 in userIdList) {
				tmp = service.getMemberInfoFromMap(webim.SESSION_TYPE.C2C, userIdList[m4]);
				username = tmp ? tmp.name : userIdList[m4];
				text += (username + '、');
				if (userIdList.length > WEB_IM_GROUP_TIP_MAX_USER_COUNT && m4 == maxIndex) {
					text += "等共" + userIdList.length + "人";
					break;
				}
			}
			text += "的管理员资格";
			break;

		case webim.GROUP_TIP_TYPE.MODIFY_GROUP_INFO: //群资料变更
			text += opUserName + "修改";
			var groupInfoList = content.getGroupInfoList();
			var type, value;
			for (var m5 in groupInfoList) {
				type = groupInfoList[m5].getType();
				value = groupInfoList[m5].getValue();
				switch (type) {
					case webim.GROUP_TIP_MODIFY_GROUP_INFO_TYPE.FACE_URL:
						text += "了群头像";
						break;
					case webim.GROUP_TIP_MODIFY_GROUP_INFO_TYPE.NAME:
						text += "群名称为" + value;
						break;
					case webim.GROUP_TIP_MODIFY_GROUP_INFO_TYPE.OWNER:
						text += "群主为" + value;
						break;
					case webim.GROUP_TIP_MODIFY_GROUP_INFO_TYPE.NOTIFICATION:
						text += "群公告为" + value;
						break;
					case webim.GROUP_TIP_MODIFY_GROUP_INFO_TYPE.INTRODUCTION:
						text += "群简介为" + value;
						break;
					default:
						text += "未知信息为:type=" + type + ",value=" + value;
						break;
				}
			}
			break;

		case webim.GROUP_TIP_TYPE.MODIFY_MEMBER_INFO: //群成员资料变更(禁言时间)
			text += opUserName + "修改了群成员资料:";
			var memberInfoList = content.getMemberInfoList();
			var userId, shutupTime;
			for (var m6 in memberInfoList) {
				userId = memberInfoList[m6].getUserId();
				shutupTime = memberInfoList[m6].getShutupTime();
				text += userId + ": ";
				if (shutupTime !== null && shutupTime !== undefined) {
					if (shutupTime === 0) {
						text += "取消禁言; ";
					} else {
						text += "禁言" + shutupTime + "秒; ";
					}
				} else {
					text += " shutupTime为空";
				}
				if (memberInfoList.length > WEB_IM_GROUP_TIP_MAX_USER_COUNT && m6 == maxIndex) {
					text += "等" + memberInfoList.length + "人";
					break;
				}
			}
			break;
		default:
			text += "未知群提示消息类型：type=" + opType;
			break;
	}
	return text;
}

function convertImgToBase64URL(url, callback, outputFormat) {
	var img = new Image();
	img.crossOrigin = 'Anonymous';
	img.onload = function () {
		var canvas = document.createElement('CANVAS'),
			ctx = canvas.getContext('2d'),
			dataURL;
		canvas.height = this.height;
		canvas.width = this.width;
		ctx.drawImage(this, 0, 0);
		dataURL = canvas.toDataURL(outputFormat);
		callback(dataURL);
		canvas = null;
	};
	img.src = url;
}

function isEmptyObject(e) {
	var t;
	for (t in e)
		return !1;
	return !0;
}

/**
 * 将对象参数序列化
 * @return {string}
 */
function CustomParam(obj) {
	if (!angular.isObject(obj)) {
		return ((obj === null) ? "" : obj.toString());
	}
	var query = '',
		name, value, fullSubName, subName, subValue, innerObj, i;
	for (name in obj) {
		value = obj[name];
		if (value instanceof Array) {
			for (i in value) {
				subValue = value[i];
				innerObj = {};
				innerObj[name] = subValue;
				query += CustomParam(innerObj) + '&';
			}
		} else if (value instanceof Object) {
			for (subName in value) {
				subValue = value[subName];
				fullSubName = name + '[' + subName + ']';
				innerObj = {};
				innerObj[fullSubName] = subValue;
				query += CustomParam(innerObj) + '&';
			}
		} else if (value !== undefined && value !== null)
			query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
	}
	return query.length ? query.substr(0, query.length - 1) : query;
}


function getChatTimeLabel(msgTime) {
	if (msgTime > 0) {
		var now = new Date();
		var startOfDay = new Date(now.getFullYear(), now.getMonth(), now.getDate());
		var timestamp = startOfDay / 1000;
		var strTime = webim.Tool.formatTimeStamp(msgTime);
		return msgTime > timestamp ? strTime.substr(11, 5) : strTime.substr(5, 5);
	}
	return '';
}

function resetHMSM(currentDate) {
	currentDate.setHours(0);
	currentDate.setMinutes(0);
	currentDate.setSeconds(0);
	currentDate.setMilliseconds(0);
	return currentDate;
}

function insertEnter2Txt(str) {
	var newstr = "";
	for (var i = 0; i < str.length; i += 9) {
		var tmp = str.substring(i, i + 9);
		newstr += tmp + '\n';
	}
	return newstr;
}

//定位光标
function setCaretPosition(tObj, sPos) {
	if (tObj.setSelectionRange) {
		setTimeout(function () {
			tObj.setSelectionRange(sPos + 1, sPos + 1);
			tObj.focus();
		}, 10);
	} else if (tObj.createTextRange) {
		var rng = tObj.createTextRange();
		rng.move('character', sPos);
		rng.select();
	}
}

//获取当前时间
function getNowFormatDate() {
	var date = new Date();
	var seperator1 = "-";
	var seperator2 = ":";
	var month = date.getMonth() + 1;
	var strDate = date.getDate();
	if (month >= 1 && month <= 9) {
		month = "0" + month;
	}
	if (strDate >= 0 && strDate <= 9) {
		strDate = "0" + strDate;
	}
	var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate + " " + date.getHours() + seperator2 + date.getMinutes();
	return currentdate;
}

//获取当前时间
function getNowDate(str) {
	var str1;
	var date;
	if (str) {
		str1 = str.replace(/\-/g, "/");	
		date = new Date(str1);
	}else{
		date = new Date();
	}
	var seperator1 = "-";
	var seperator2 = ":";
	var month = date.getMonth() + 1;
	var strDate = date.getDate();
	if (month >= 1 && month <= 9) {
		month = "0" + month;
	}
	if (strDate >= 0 && strDate <= 9) {
		strDate = "0" + strDate;
	}
	var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate + " " + '00' + seperator2 + '00';
	return currentdate;
}

/**
 * Resize Image
 * @param longSideMax
 * @param url
 * @return promise
 */
function resizeImage(longSideMax, url) {
	var $q = angular.injector(['ng']).get('$q');
	var defer = $q.defer();
	var tempImg = new Image();
	tempImg.src = url;
	tempImg.onload = function () {
		// Get image size and aspect ratio.
		var targetWidth = tempImg.width;
		var targetHeight = tempImg.height;
		var aspect = tempImg.width / tempImg.height;

		// Calculate shorter side length, keeping aspect ratio on image.
		// If source image size is less than given longSideMax, then it need to be
		// considered instead.
		if (tempImg.width > tempImg.height) {
			longSideMax = Math.min(tempImg.width, longSideMax);
			targetWidth = longSideMax;
			targetHeight = longSideMax / aspect;
		} else {
			longSideMax = Math.min(tempImg.height, longSideMax);
			targetHeight = longSideMax;
			targetWidth = longSideMax * aspect;
		}

		// Create canvas of required size.
		var canvas = document.createElement('canvas');
		canvas.width = targetWidth;
		canvas.height = targetHeight;

		var ctx = canvas.getContext("2d");
		// Take image from top left corner to bottom right corner and draw the image
		// on canvas to completely fill into.
		ctx.drawImage(this, 0, 0, tempImg.width, tempImg.height, 0, 0, targetWidth, targetHeight);
		defer.resolve(canvas.toDataURL("image/jpeg"));
	};
	return defer.promise;
}


//判断一个数是否为大于0的整数
function judgeIsInteger(obj) {
	var num = Number(obj);
	return typeof num === 'number' && num % 1 === 0 && num > 0 && num <= 100;

}

function isEmojiCharacter(substring) {
	for (var i = 0; i < substring.length; i++) {
		var hs = substring.charCodeAt(i);
		if (0xd800 <= hs && hs <= 0xdbff) {
			if (substring.length > 1) {
				var ls = substring.charCodeAt(i + 1);
				var uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
				if (0x1d000 <= uc && uc <= 0x1f77f) {
					return true;
				}
			}
		} else if (substring.length > 1) {
			var ls1 = substring.charCodeAt(i + 1);
			if (ls1 == 0x20e3) {
				return true;
			}
		} else {
			if (0x2100 <= hs && hs <= 0x27ff) {
				return true;
			} else if (0x2B05 <= hs && hs <= 0x2b07) {
				return true;
			} else if (0x2934 <= hs && hs <= 0x2935) {
				return true;
			} else if (0x3297 <= hs && hs <= 0x3299) {
				return true;
			} else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50) {
				return true;
			}
		}
	}
}

/**
 1. @param str 要计算的字符串
 2. @param fontSize 字符串的字体大小
 3. 根据需要，还可以添加字体控制，如微软雅黑与Times New Roma的字符宽度肯定不一致
 */
function textWidth(str, fontSize) {
	if (!textWidth.txt) {
		var txt = document.createElement('span');
		txt.style.position = 'absolute';
		//  避免遮挡其他元素
		txt.style.zIndex = -10;
		//  千万不可设置为display：none；
		txt.style.visibility = 'hidden';
		//  一定要加载到DOM中才能计算出字符宽度
		document.body.appendChild(txt);
		textWidth.txt = txt;
	}
	//  控制字符的字体大小
	textWidth.txt.style.fontSize = fontSize + 'px';
	//  设置字符内容
	textWidth.txt.textContent = str;
	//  返回计算结果
	return textWidth.txt.offsetWidth;
}

/**
 * 判断文本中是否由数字和字母组成
 */
function checkStringIsIncludeSpecialChar(str) {
	var re = /^[0-9a-zA-Z]*$/; //判断字符串是否为数字和字母组合     
	if (!re.test(str)) {
		return false;
	} else {
		return true;
	}
}

/**
 * 是不是ios设备
 */
function iOSDevice(){
	var isIosDevice ;
	if(window.cordova){
		isIosDevice = ionic.Platform.isIOS();
	}
	
     return isIosDevice;
}
angular.module('app.routes', [])
    .config(function ($stateProvider, $urlRouterProvider, Constant) {
        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the app can be in.
        // Each state's controller can be found in controllers.js
        var UIPath = '';
        if (Constant.debugMode) UIPath= 'page/';
        $stateProvider
            .state('tabsController.mainPage', {
                url: '/main_page',
                views: {
                    'main_tab': {
                        templateUrl: UIPath + 'home/mainpage/main_page.html',
                        controller: 'mainPageCtrl'
                    }
                }
            })
            .state('market',{
                url:'/market',
                templateUrl:UIPath + 'home/mainpage/market.html',
                controller:'marketCtrl'
            })
            .state('tabsController.schoolPage', {
                url: '/school_page',
                views: {
                    'school_tab': {
                        templateUrl:  UIPath + 'compus/school_page.html',
                        controller: 'schoolPageCtrl'
                    }
                }
            })
            .state('tabsController.communicatePage', {
                url: '/communicate_page',
                views: {
                    'communicate_tab': {
                        templateUrl: UIPath + 'interactive/communicate_page.html',
                        controller: 'communicatePageCtrl'
                    }
                }
            })
            .state('tabsController.general', {
                url: '/general',
                views: {
                    'general_tab': {
                        templateUrl: UIPath + 'home/general/general.html',
                        controller: 'generalCtrl'
                    }
                }
            })
            .state('tabsController.usage', {
                url: '/usage',
                views: {
                    'usage_tab': {
                        templateUrl: UIPath + 'home/usage/usage.html',
                        controller: 'usageCtrl'
                    }
                }
            })
            .state('tabsController.settingPage', {
                url: '/setting_page',
                views: {
                    'setting_tab': {
                        templateUrl: UIPath + 'setting/settingpage/setting_page.html',
                        controller: 'settingPageCtrl'
                    }
                }
            })
            .state('tabsController.assetsStatistic',{
                url:'/assetsStatistic',
                views: {
                    'assets_tab': {
                        templateUrl: UIPath + 'home/others/assetsStatistic/assetsStatistic.html',
                        controller: 'assetsStatisticCtrl'
                    }
                },
            })
            .state('communicate_detail', {
                url: '/communicate_detail',
                templateUrl: UIPath + 'interactive/communicate_detail.html',
                controller: 'communicateDetailCtrl',
                params: {
                    obj: null
                }
            })
            .state('pdfView', {
                url: '/pdfView',
                templateUrl: UIPath + 'common/pdfView.html',
                controller: 'pdfViewCtrl',
                params: {
                    url: null
                }
            })
            .state('devicesOnline', {
                url: '/devicesOnline',
                templateUrl: UIPath + 'home/devicesOnline/devicesOnline.html',
                controller: 'devicesOnlineCtrl'
            })
            .state('general', {
                url: '/general',
                templateUrl: UIPath + 'home/general/general.html',
                controller: 'generalCtrl'
            })
            .state('usage', {
                url: '/usage',
                templateUrl: UIPath + 'home/usage/usage.html',
                controller: 'usageCtrl'
            })
            .state('terminalLocation', {
                url: '/terminalLocation',
                templateUrl: UIPath + 'home/terminalLocation/terminalLocation.html',
                controller: 'terminalLocationCtrl'
            })
            .state('terminalList', {
                url: '/terminalList',
                templateUrl: UIPath + 'home/terminalList/terminalList.html',
                controller: 'terminalListCtrl'
            })
            .state('terminalUsageInfo', {
                url: '/terminalUsageInfo',
                templateUrl: UIPath + 'home/terminalUsageInfo/terminalUsageInfo.html',
                controller: 'terminalUsageInfoCtrl',
                params: {
                    id: null
                }
            })
            .state('rank', {
                url: '/rank',
                templateUrl: UIPath + 'home/rank/rank.html',
                controller: 'rankCtrl',
                params: {
                    type: null
                }
            })
            .state('contact_profile', {
                url: '/contact_profile',
                templateUrl: UIPath + 'interactive/contact_profile.html',
                controller: 'contactProfileCtrl',
                params: {
                    obj: null,
                    self: false
                }
            })
            .state('tabsController', {
                url: '/tab',
                templateUrl: UIPath + 'tabs/tabsController.html',
                controller: 'tabsCtrl',
                abstract: true
            })
            .state('login', {
                url: '/login',
                templateUrl: UIPath + 'login/login.html',
                controller: 'loginCtrl'
            })
            .state('user_profile', {
                url: '/user_profile',
                templateUrl: UIPath + 'setting/userProfile/user_profile.html',
                controller: 'userProfileCtrl'
            })
            .state('edit_profile', {
                url: '/edit_profile',
                templateUrl: UIPath + 'setting/userProfile/edit_profile.html',
                controller: 'editProfileCtrl',
                params: {
                    content: null
                }
            })
            .state('news_detail', {
                url: '/news_detail',
                templateUrl: UIPath + 'home/newsdetail/news_detail.html',
                controller: 'newsDetailCtrl',
                params: {
                    post: null,
                    notice: null,
                    index: null
                }
            })
            .state('generalAbout', {
                url: '/generalAbout',
                templateUrl: UIPath + 'home/generalAbout/generalAbout.html',
                controller: 'generalAboutCtrl'
            })
            .state('about', {
                url: '/about',
                templateUrl: UIPath + 'setting/aboutpage/about.html',
                controller: 'aboutCtrl'
            })
            .state('suggest', {
                url: '/suggest',
                templateUrl: UIPath + 'setting/suggestpage/suggest.html',
                controller: 'suggestCtrl'
            })
            .state('notification_setting', {
                url: '/notification_setting',
                templateUrl: UIPath + 'setting/notificationpage/notification_setting.html',
                controller: 'notificationSettingCtrl'
            })
            .state('classAppraisalList',{
                url:'/classAppraisalList',
                templateUrl:UIPath+'home/classAppraisal/classAppraisalList.html',
                controller:'classAppraisalListCtrl'
            })
            .state('classAppraisalDetail',{
                url:'/classAppraisalDetail',
                templateUrl:UIPath+'home/classAppraisal/classAppraisalDetail.html',
                controller:'classAppraisalDetailCtrl',
                params:{
                    voteTaskId:null,
                    title:null
                }
            })
            .state('versionIntroduce',{
                url:'/versionIntroduce',
                templateUrl:UIPath +'setting/versionIntroduce/versionIntroduce.html',
                controller:'versionIntroduceCtrl'
            })
            .state('abnormalHandle',{
              url:'/abnormalHandle',
              templateUrl:UIPath+'common/abnormalHandle.html',
              controller:'abnormalHandleCtrl',
              params:{
                  notice:null
              }
            })
            .state('repairRecordList',{
                url:'/repairRecordList',
                templateUrl:UIPath + 'home/equipmentRepair/repairRecordList.html',
                controller:'repairRecordListCtrl'
            })
            .state('repairDetail',{
                url:'/repairDetail',
                templateUrl:UIPath + 'home/equipmentRepair/repairDetail.html',
                controller:'repairDetailCtrl',
                params:{
                    repairRecordId:null,
                    readStatus:null
                }
            })
            .state('repairProgress',{
                url:'/repairProgress',
                templateUrl :UIPath + 'home/equipmentRepair/repairProgress.html',
                controller:'repairProgressCtrl',
                params:{
                    repairRecordId:null,
                    manufacturersContactId:null ,//售后人员id
                    manufacturersId:null,//厂家id 
                    isAssociated:null //表示是否已经关联了厂家
                }
            })
            .state('assetsCheckList',{
                    url:'/assetsCheckList',
                    templateUrl :UIPath + 'home/assets/assetsCheckList.html',
                    controller:'assetsCheckListCtrl'
                })
             .state('assetsCheckDetail',{
                    url:'/assetsCheckDetail',
                    templateUrl :UIPath + 'home/assets/assetsCheckDetail.html',
                    controller:'assetsCheckDetailCtrl',
                    params:{
                        assetsId:null,
                        progressStatus:null
                    }
                })
                .state('approvalList',{
                    url:'/approvalList',
                    templateUrl :UIPath + 'home/approvalAdmin/approvalList.html',
                    controller:'approvalListCtrl'
                })
                .state('approvalDetail',{
                    url:'/approvalDetail',
                    templateUrl:UIPath + 'home/approvalAdmin/approvalDetail.html',
                    controller:'approvalDetailCtrl',
                    params:{
                        approvalId:null,
                        
                    }
                })
               //安全管理 演练和安全隐患
                .state('exerciseNoticeList',{
                    url:'/exerciseNoticeList',
                    templateUrl:UIPath + 'home/others/safetyManage/exerciseNoticeList.html',
                    controller :'exerciseNoticeListCtrl'
                })
                .state('exerciseNoticeDetail',{
                    url:'/exerciseNoticeDetail',
                    templateUrl:UIPath + 'home/others/safetyManage/exerciseNoticeDetail.html',
                    controller :'exerciseNoticeDetailCtrl',
                    params: {
                        itemId:null,
                        title:null
                    }
                })
                .state('exerciseRecord',{
                    url:'/exerciseRecord',
                    templateUrl:UIPath + 'home/others/safetyManage/exerciseRecord.html',
                    controller :'exerciseRecordCtrl',
                    params:{
                        itemId:null,
                        startTime:null,
                        endTime:null
                        
                    }
                })
                .state('addExerciseModal',{
                    url:'/addExerciseModal',
                    templateUrl:UIPath + 'home/others/safetyManage/addExerciseModal.html',
                    controller :'addExerciseModalCtrl',
                    params:{
                        programId:null ,
                        programName:null
                    }
                })
                .state('hidenTroubleList',{
                    url:'/hidenTroubleList',
                    templateUrl:UIPath + 'home/others/safetyManage/hidenTroubleList.html',
                    controller :'hidenTroubleListCtrl'
                })
                .state('hidenTroubleDetail',{
                    url:'/hidenTroubleDetail',
                    templateUrl:UIPath + 'home/others/safetyManage/hidenTroubleDetail.html',
                    controller :'hidenTroubleDetailCtrl',
                    params: {
                        itemId:null
                    }
                })
                .state('hidenTroubleReport',{
                    url:'/hidenTroubleReport',
                    templateUrl:UIPath + 'home/others/safetyManage/hidenTroubleReport.html',
                    controller :'hidenTroubleReportCtrl'
                })    
                .state('assetsScanning',{
                    url:'/assetsScanning',
                    templateUrl:UIPath +'home/assets/assetsScanning.html',
                    controller:'assetsScanningCtrl'
                })
                .state('assetsScanningDetail',{
                    url:'/assetsScanningDetail',
                    templateUrl:UIPath +'home/assets/assetsScanningDetail.html',
                    controller:'assetsScanningDetailCtrl'
                })
                .state('portraitList',{
                    url:'/portraitList',
                    templateUrl:UIPath +'home/others/portrait/portraitList.html',
                    controller:'portraitListCtrl'
                })
                .state('babyVideoList',{
                    url:'/babyVideoList',
                    templateUrl:UIPath + 'home/babyVideo/babyVideoList.html',
                    controller :'babyVideoListCtrl' 
                })
                .state('babyVideoDetail',{
                    url:'/babyVideoDetail',
                    templateUrl:UIPath + 'home/babyVideo/babyVideoDetail.html',
                    controller :'babyVideoDetailCtrl' ,
                    params:{
                        url:''
                    }
                })
                .state('covidDailySummery',{
                    url:'/covidDailySummery',
                    templateUrl:UIPath + 'home/covid/dailySummery.html',
                    controller :'covidDailySummeryCtrl' 
                })
                .state('covidWarningDetail',{
                    url:'/covidWarningDetail',
                    templateUrl:UIPath + 'home/covid/warningDetail.html',
                    controller :'covidWarningDetailCtrl',
                    params: {
                        id: {}
                    }
                })
                .state('userProtocol',{
                   url:'/userProtocol' ,
                   templateUrl :UIPath + 'setting/userprotocol/userProtocol.html',
                   controller:'userProtocolCtrl'
                })
            ;

        $urlRouterProvider.otherwise(function ($injector) {
            var $state = $injector.get("$state");
            $state.go('login');
        });

    })
    .run(function ($rootScope, $state, UserPreference, Constant) {
        $rootScope.$on('$stateChangeStart', function (event, next) {
            if (next.url === $state.current.url && next.url !== '/news_detail') {
                if (Constant.debugMode) console.log("same page, go nowhere..");
                event.preventDefault();
            }
        });
    });

angular.module('app.services', [])
    .factory('UserPreference', ['$window', function ($window) {
        return {
            set: function (key, value) {
                $window.localStorage[key] = value;
            },
            get: function (key, defaultValue) {
                return $window.localStorage[key] || defaultValue;
            },
            setObject: function (key, value) {
                $window.localStorage[key] = JSON.stringify(value);
            },
            getObject: function (key) {
                return JSON.parse($window.localStorage[key] || '{}');
            },
            getArray: function (key) {
                return JSON.parse($window.localStorage[key] || '[]');
            },
            getBoolean: function (key) {
                return $window.localStorage[key] === true || $window.localStorage[key] === 'true';
            },
            clear: function () {
                $window.localStorage.clear();
            }
        };
    }])
    .service('scrollDelegateFix', function ($ionicScrollDelegate) {
        //fix a $ionicScrollDelegate bug in ionic v1,which exists in modals
        return {
            $getByHandle: function (name) {
                var instances = $ionicScrollDelegate.$getByHandle(name)._instances;
                return instances.filter(function (element) {
                    //return (element['$$delegateHandle'] === name);
                    return element.$$delegateHandle === name;
                })[0];
            }
        };
    })
    .factory('CameraHelper', function ($q, $ionicPopup, $ionicActionSheet, $rootScope, BroadCast, $cordovaCamera, $jrCrop, Constant) {
        var helper = {};
        helper.cropImage = function (imageURI, width, height, cropTitle) {
            $jrCrop.crop({
                url: imageURI,
                width: width,
                height: height,
                title: cropTitle,
                cancelText: '取消',
                chooseText: '确定'
            }).then(function (canvas) {
                // success!
                $rootScope.$broadcast(BroadCast.IMAGE_CROP, canvas.toDataURL());
            }, function () {
                // User canceled or couldn't load image.
                $rootScope.$broadcast(BroadCast.IMAGE_CROP, undefined);
            });
        };
        helper.selectImage = function (which, imgOpt) {
            if (!navigator.camera) {
                alert("Camera API not supported");
                return;
            }
            $ionicActionSheet.show({
                buttons: [{
                    text: '<i class="icon ion-camera"></i>相机'
                },
                {
                    text: '<i class="icon ion-ios-photos"></i>相册'
                }
                ],
                titleText: '请选择图片来源(单张图片大小不得超过10M)',
                cancelText: '取消',
                cancel: function () {
                    // add cancel code..
                },
                buttonClicked: function (index) {
                    var srcType = Camera.PictureSourceType.SAVEDPHOTOALBUM;
                    if (index === 0) {
                        srcType = Camera.PictureSourceType.CAMERA;
                    }
                    var destinationType = Camera.DestinationType.DATA_URL;
                    if ((imgOpt && imgOpt.allowEdit) || which == 'chat') {
                        destinationType = Camera.DestinationType.FILE_URI;
                    }
                    if (index === 0 || index === 1) {
                        var defaultHeight = 300;
                        var defaultWidth = 300;
                        var cropTitle = '截取高亮区域';
                        var options = {
                            quality: which === 'portrait' ? 95 : 85,
                            destinationType: destinationType,
                            sourceType: srcType,
                            allowEdit: false,
                            encodingType: Camera.EncodingType.JPEG,
                            targetWidth: Constant.CAPTURE_IMAGE_RANGE,
                            targetHeight: Constant.CAPTURE_IMAGE_RANGE,
                            correctOrientation: true,
                            popoverOptions: CameraPopoverOptions,
                            saveToPhotoAlbum: false
                        };
                        if (imgOpt) {
                            if (imgOpt.width && imgOpt.height) {
                                defaultHeight = imgOpt.height;
                                defaultWidth = imgOpt.width;
                            } else {
                                options.targetWidth = defaultWidth * 2;
                                options.targetHeight = defaultHeight * 2;
                            }
                            if (imgOpt.title)
                                cropTitle = imgOpt.title;
                        }
                        console.log(imgOpt);
                        $cordovaCamera.getPicture(options).then(function (imageURI) {

                            if (imgOpt && imgOpt.allowEdit) {
                                $jrCrop.crop({
                                    url: imageURI,
                                    width: defaultWidth,
                                    height: defaultHeight,
                                    title: cropTitle,
                                    cancelText: '取消',
                                    chooseText: '确定'
                                }).then(function (canvas) {
                                    // success!
                                    var data = {
                                        which: which,
                                        source: canvas.toDataURL()
                                    };
                                    $rootScope.$broadcast(BroadCast.IMAGE_SELECTED, data);
                                }, function () {
                                    // User canceled or couldn't load image.
                                    $rootScope.$broadcast(BroadCast.IMAGE_SELECTED, undefined);
                                });
                            } else {
                                if (which == 'chat') {
                                    window.resolveLocalFileSystemURL(imageURI, function (fileEntry) {
                                        fileEntry.file(function (fileObj) {
                                            var data = {
                                                which: which,
                                                source: fileObj
                                            };
                                            $rootScope.$broadcast(BroadCast.IMAGE_SELECTED, data);
                                        });
                                    });
                                } else {
                                    var data = {
                                        which: which,
                                        source: "data:image/jpeg;base64," + imageURI
                                    };
                                    $rootScope.$broadcast(BroadCast.IMAGE_SELECTED, data);
                                }
                            }

                        }, function (err) {
                            $rootScope.$broadcast(BroadCast.IMAGE_SELECTED, undefined);
                            //alert("Read Photo Error: " + err);
                        });
                    }
                    return true;
                }
            });
        };
        helper.selectMultiImage = function (callback, limit) {
            $ionicActionSheet.show({
                buttons: [{
                    text: '<i class="icon ion-camera"></i>相机'
                },
                {
                    text: '<i class="icon ion-ios-photos"></i>相册'
                }
                ],
                titleText: '请选择图片来源(单张图片大小不得超过10M)',
                cancelText: '取消',
                cancel: function () {
                    // add cancel code..
                },
                buttonClicked: function (index) {
                    if (index === 0) {
                        var options = {
                            quality: 85,
                            destinationType: Camera.DestinationType.DATA_URL,
                            sourceType: Camera.PictureSourceType.CAMERA,
                            allowEdit: false,
                            encodingType: Camera.EncodingType.JPEG,
                            targetWidth: Constant.CAPTURE_IMAGE_RANGE,
                            targetHeight: Constant.CAPTURE_IMAGE_RANGE,
                            correctOrientation: true,
                            saveToPhotoAlbum: false
                        };
                        $cordovaCamera.getPicture(options).then(function (imageURI) {
                            callback(imageURI);
                        }, function (err) {
                            console.log(err);
                        });
                    } else if (index === 1) {
                        window.imagePicker.getPictures(
                            function (results) {
                                callback(results);
                            },
                            function (error) {
                                console.log('Error: ' + error);
                            }, {
                            maximumImagesCount: limit,
                            width: Constant.CAPTURE_IMAGE_RANGE
                        }
                        );
                    }
                    return true;
                }
            });
        };
        return helper;
    })
    .factory('httpInterceptor', ['$q', '$injector', function ($q, $injector) {
        var regex = new RegExp('\.(html|js|css)$', 'i');
        var isAsset = function (url) {
            return regex.test(url);
        };
        var doLogin = function () {
            var state = $injector.get('$state');
            var user = $injector.get('UserPreference');
            var auth = $injector.get('AuthorizeService');
            if (!auth.logining && state.current.url !== '/login') {
                var loginModel = {
                    remember: true,
                    username: user.get('username', ''),
                    password: user.get('password', '')
                };
                if (loginModel.username !== '' && loginModel.password !== '') {
                    console.error('timeout, try auto login.');
                    auth.login(loginModel).then(function (res) {
                        if (res && res.result)
                            state.go('tabsController.mainPage');
                    });
                } else {
                    console.error('username or password not found');
                    state.go('login');
                }
            }
        };
        return {
            // optional method
            'request': function (config) {
                // do something on success
                //if(!isAsset(config.url)){            //if the call is not for an asset file
                //	config.url+= "?ts=" +  Date.now();     //append the timestamp
                //}

                if (!isAsset(config.url)) {
                    var Constant = $injector.get('Constant');
                    if (config.url.indexOf('login') > 0 || config.url.indexOf('logout') > 0)
                        config.timeout = Constant.serverTimeout;
                    else
                        config.timeout = Constant.heavyServerTimeout;
                }
                return config;
            },
            // optional method
            'requestError': function (rejection) {
                // do something on error

                return $q.reject(rejection);
            },
            // optional method
            'response': function (response) {
                // do something on success
                if (!isAsset(response.config.url)) {
                    if (response.data.code === -100) {
                        doLogin();
                    }
                }
                return response;
            },
            // optional method
            'responseError': function (rejection) {
                if (!isAsset(rejection.config.url)) {
                    var toaster = $injector.get('toaster');
                    var MESSAGES = $injector.get('MESSAGES');
                    var rootScope = $injector.get('$rootScope');
                    var BroadCast = $injector.get('BroadCast');
                    // console.log(' response ---:' + rejection.status);
                    rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    switch (rejection.status) {
                        case -1:
                        case 408:
                            toaster.warning({
                                title: MESSAGES.REQUEST_ERROR,
                                body: MESSAGES.CONNECT_TIMEOUT_MSG
                            });
                            break;
                        case 401:
                            doLogin();
                            break;
                        case 404:
                            toaster.error({
                                title: MESSAGES.CONNECT_ERROR,
                                body: rejection.data.message
                            });
                            break;
                        case 500:
                            toaster.error({
                                title: MESSAGES.CONNECT_ERROR,
                                body: MESSAGES.SERVER_ERROR
                            });
                            break;
                        case 503:
                            toaster.error({
                                title: rejection.data.message,
                                body: ''
                            });
                            break;
                        default:
                            toaster.error({
                                title: MESSAGES.CONNECT_ERROR,
                                body: MESSAGES.CONNECT_ERROR_MSG
                            });
                            break;
                    }
                }
                return $q.reject(rejection);
            }
        };
    }])
    .service('closePopupService', [
        function () {
            var currentPopup;
            var htmlEl = angular.element(document.querySelector('html'));
            htmlEl.on('click', function (event) {
                if (event.target.nodeName === 'HTML') {
                    if (currentPopup) {
                        currentPopup.close();
                    }
                }
            });

            this.register = function (popup) {
                currentPopup = popup;
            };
        }
    ])
    .factory('PushService', function ($http, $window, Constant, $state, $ionicPopup, $rootScope, BroadCast) {
        var push = {};
        //启动极光推送
        push.init = function (config) {
            var setTagsWithAliasCallback = function (event) {
                console.log(event);
                console.log("Broadcast Rev: Jpush tag and alias settled");
            };
            var receiveNotificationCallback = function (event) {
                console.log(event);
                $rootScope.$broadcast(BroadCast.NEW_PUSH_REV, undefined);
            };
            var openNotificationCallback = function (event) {
                var notice;

                if (device.platform === "Android") {
                    notice = event.extras;

                } else {
                    notice = event;
                    push.setBadgeNumber();
                }
                //    console.log('event notice');
                //    console.log(notice);
                push.clearAllNotification();
                switch (String(notice.messageType)) {
                    case '1':
                    case '2':
                        notice.imageUrls = notice.imageUrls.split(',');
                        $state.go('news_detail', {
                            notice: notice,
                            index: notice.id
                        });
                        break;
                    case '10000':
                        $state.go('tabsController.communicatePage');
                        break;
                    case '100101':
                        //用户反馈
                        $state.go('suggest');
                        break;
                    case '100104':
                        //异常处理 
                        $state.go('abnormalHandle', {
                            notice: notice
                        });
                        break;
                    case '100105':
                        //报修记录 管理员接收
                        $state.go('repairDetail', {
                            repairRecordId: notice.orderId,
                            readStatus: false
                        });
                        break;

                    case '100107':
                        //审批通过 通知
                        $state.go('assetsCheckDetail', {
                            assetsId: notice.orderId,
                            progressStatus: 1
                        });
                        break;

                    case '100111':
                        $state.go('approvalDetail', {
                            archiveId: notice.archiveId
                        });
                        break;


                    case '373373':
                        $state.go('covidDailySummery');
                        break;



                    default:
                        //$state.go('abnormalHandle',{notice: {extras:{createTime:'2018-07-17 10:47',text:'哈哈哈nishi66',title:'标题'}}});  

                        break;
                }
            };

            //  $window.plugins.jPushPlugin.init();
            $window.plugins.jPushPlugin.init();
            push.clearAllNotification();
            //设置tag和Alias触发事件处理
            document.addEventListener('jpush.setTagsWithAlias', setTagsWithAliasCallback, false);
            //打开推送消息事件处理
            document.addEventListener("jpush.openNotification", openNotificationCallback, false);
            document.addEventListener("jpush.receiveNotification", receiveNotificationCallback, false);
            if (ionic.Platform.isIOS()) {
                document.addEventListener("jpush.backgroundNotification", receiveNotificationCallback, false);
                push.setBadgeNumber();
            } else {
                $window.plugins.jPushPlugin.setLatestNotificationNum(2);
            }
            document.addEventListener("pause", function () {
                console.log('app pause');
                push.appBackground = true;
                push.clearAllNotification();
            }, false);
            document.addEventListener("resume", function () {
                console.log('app resume');
                push.appBackground = false;
                push.clearAllNotification();
            }, false);
            $window.plugins.jPushPlugin.setDebugMode(Constant.debugMode);
            $window.plugins.jPushPlugin.setSilenceTime(22, 0, 7, 0);
            push.getRegID();
        };

        //获取状态
        push.isPushStopped = function (fun) {
            $window.plugins.jPushPlugin.isPushStopped(fun);
        };
        //停止极光推送
        push.stopPush = function () {
            $window.plugins.jPushPlugin.stopPush();
        };

        //清除通知
        push.clearAllNotification = function () {
            if (ionic.Platform.isIOS())
                $window.plugins.jPushPlugin.clearAllLocalNotifications();
            else
                $window.plugins.jPushPlugin.clearAllNotification();
        };

        push.getRegID = function () {
            setTimeout(function () {
                $window.plugins.jPushPlugin.getRegistrationID(function (data) {
                    console.log("JPushPlugin:registrationID is " + data);
                    if (data) {
                        push.regID = data;
                    } else {
                        push.getRegID();
                    }
                });
            }, 2000);
        };

        //重启极光推送
        push.resumePush = function () {
            $window.plugins.jPushPlugin.resumePush();
        };

        //设置标签和别名
        push.setTagsWithAlias = function (tags, alias) {

            // $window.plugins.jPushPlugin.setTagsWithAlias(tags, alias, function () {
            //     console.log("Jpush Tag and Alias settled");
            // }, function (msg) {
            //     console.log("Jpush Tag and Alias set failed " + msg);
            //     setTimeout(function () {
            //         push.setTagsWithAlias(tags, alias);
            //     }, 5000);
            // });
            push.setAlias(alias);
            push.setTags(tags);
        };

        //设置标签
        push.setTags = function (tags) {
            //   $window.plugins.jPushPlugin.setTags(tags);
            console.log('is set tags');
            $window.plugins.jPushPlugin.setTags({
                sequence: 1,
                tags: tags
            }, function (res) {
                console.log('set tags succeed');
            }, function (err) {
                console.log('set tags failure');
            });

        };

        //设置别名
        push.setAlias = function (alias) {
            console.log('is set alias ');
            $window.plugins.jPushPlugin.setAlias({
                sequence: 1,
                alias: alias
            }, function (res) {
                console.log('set alias succeed');
            }, function (err) {
                console.log('set alias faliure');
                console.log(err);
            });
            console.log('is set alias 222');
        };

        //删除别名
        push.deleteAlias = function () {
            $window.plugins.jPushPlugin.deleteAlias({
                sequence: 1
            },
                function (result) {
                    var sequence = result.sequence;
                },
                function (error) {
                    var sequence = error.sequence;
                    var errorCode = error.code;
                });
        };

        push.setAndroidPushTime = function (days, startHour, endHour) {
            $window.plugins.jPushPlugin.setPushTime(days, startHour, endHour);
        };

        push.setAndroidSilenceTime = function (startHour, startMinute, endHour, endMinute) {
            $window.plugins.jPushPlugin.setSilenceTime(startHour, startMinute, endHour, endMinute);
        };

        push.getUserNotificationSettings = function (fun) {
            $window.plugins.jPushPlugin.getUserNotificationSettings(fun);
        };

        push.setBasicNotification = function (type) {
            $window.plugins.jPushPlugin.setBasicPushNotificationBuilder(type);
        };

        push.setBadgeNumber = function () {
            $window.plugins.jPushPlugin.getApplicationIconBadgeNumber(function (data) {
                if (data > 0)
                    $window.plugins.jPushPlugin.setApplicationIconBadgeNumber(0);
            });
        };

        return push;
    })

    //结构待优化


    .factory('AuthorizeService', function ($http, Constant, $rootScope, BroadCast, UserPreference, PushService, $ionicPopup, $state, $q, $timeout, $injector) {
        var auth = {};


        auth.login = function (user,success,failure) {
            if (auth.logining) {
                console.log('request is processing.');
                return;
            }
            auth.logining = true;
            if (iOSDevice()) {
                var defer = $q.defer();
                var tool = $injector.get('SavePhotoTool');
                cordova.plugin.http.clearCookies();
                cordova.plugin.http.setRequestTimeout(15.0);
                // UserPreference.set("IOSCookies",cookies);
                //  cordova.plugin.http.setCookie(Constant.ServerUrl + "/login/manage",cookies);
                cordova.plugin.http.setDataSerializer('json');
                var options = {
                    loginname: user.username.trim(),
                    password: md5(user.password).toUpperCase()
                };
                console.log('====login model start===');
                console.log(options);
                console.log('====login model end===');
                var header = { "Content-Type": 'application/json' };

                cordova.plugin.http.post(Constant.ServerUrl + "/login/manage",
                    options, header, function (response) {
                        var cookies = cordova.plugin.http.getCookieString(Constant.ServerUrl + "/login/manage");
                        if (cookies) {
                            UserPreference.set("IOSCookies", cookies);
                            cordova.plugin.http.setCookie(Constant.ServerUrl + "/login/manage", cookies);
                        }
                        try {
                            auth.logining = false;
                            var data = JSON.parse(response.data);
                           // console.log(data);
                            if (data.result) {
                                var tags = [];
                                if (data.data.sex && data.data.sex !== '')
                                    tags.push(data.data.sex);
                                tags.push("JS_" + data.data.rolename.toUpperCase());
                                if (data.data.school) {
                                    tags.push("XX" + data.data.school.id);

                                    UserPreference.set('DefaultSchoolID', data.data.school.id);
                                    UserPreference.set('DefaultSchoolName', data.data.school.schoolName);
                                }
                                if (data.data.area) {
                                    var area = data.data.area.id;
                                    tags.push("SF" + area.substr(0, 2) + "0000");
                                    tags.push("CS" + area.substr(0, 4) + "00");
                                    tags.push("DQ" + area.substr(0, 6));
                                }
                                UserPreference.setObject("user", data.data);
                                auth.isLogin = true;
                                setTimeout(function () {
                                    if (window.cordova) {
                                        MobclickAgent.onEvent('app_login');
                                        console.log('to login ');
                                        PushService.setTagsWithAlias(tags, user.username.toLowerCase());
                                    }
                                    if (Constant.debugMode) console.log(tags);
                                }, 5000);
                            } else {
                                UserPreference.set("password", '');
                                if ($state.current.url !== '/login')
                                    $state.go('login');
                            }
                            // $timeout(function () {
                               
                            // });
                            success(data);
                            return defer.resolve(data);

                        } catch (error) {
                            auth.logining = false;
                            console.error("JSON parsing error");
                        }


                    }, function (error) {
                        auth.logining = false;
                        if (error.status) {
                            tool.interceptors(error.status);
                        }
                        failure(error);
                        return  defer.reject(error);
                    });
              
                return defer.promise;
            } else {
                console.log('login start');
                return $http.post(Constant.ServerUrl + "/login/manage", {
                    loginname: user.username.trim(),
                    password: md5(user.password).toUpperCase()
                }).then(function (response) {
                    //cache user
                    console.log('login result');
                    var data = response.data;
                    if (data.result) {
                        var tags = [];
                        if (data.data.sex && data.data.sex !== '')
                            tags.push(data.data.sex);
                        tags.push("JS_" + data.data.rolename.toUpperCase());
                        if (data.data.school) {
                            tags.push("XX" + data.data.school.id);

                            UserPreference.set('DefaultSchoolID', data.data.school.id);
                            UserPreference.set('DefaultSchoolName', data.data.school.schoolName);
                        }
                        if (data.data.area) {
                            var area = data.data.area.id;
                            tags.push("SF" + area.substr(0, 2) + "0000");
                            tags.push("CS" + area.substr(0, 4) + "00");
                            tags.push("DQ" + area.substr(0, 6));
                        }
                        UserPreference.setObject("user", data.data);
                        auth.isLogin = true;
                        setTimeout(function () {
                            if (window.cordova) {
                                MobclickAgent.onEvent('app_login');
                                console.log('to login ');
                                PushService.setTagsWithAlias(tags, user.username.toLowerCase());
                            }
                            if (Constant.debugMode) console.log(tags);
                        }, 5000);
                    } else {
                        UserPreference.set("password", '');
                        if ($state.current.url !== '/login')
                            $state.go('login');
                    }
                    return data;
                }, function (error) {
                    return $q.reject(error);
                }).finally(function () {
                    setTimeout(function () {
                        auth.logining = false;
                    }, 5000);
                });
            }

        };

        auth.logout = function () {
            auth.isLogin = false;
            var defer = $q.defer();
            if (window.cordova) {
                PushService.setTagsWithAlias([], "");
                MobclickAgent.onEvent('app_logout');
            }
            if(iOSDevice()){
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/logout", {}, header, function (response) { 
                    var data = JSON.parse(response);
                    $timeout(function () {
                        defer.resolve(data);
                   });   
                }, function (response) {
                    $timeout(function () {
                        defer.reject(error);
                    });
                });
                return defer.promise;
            }else{
                return $http.get(Constant.ServerUrl + "/logout");
            }
           
        };

        return auth;
    })
    .factory('SchoolService', function ($http, Constant, $rootScope, BroadCast, UserPreference) {
        var school = {};
        school.newNews = function (reqData) {
            var role = String(UserPreference.getObject('user').rolename);
            if (role === Constant.USER_ROLES.PARENT)
                reqData.stuId = UserPreference.get('DefaultChildID');
            if (iOSDevice()) {
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.post(Constant.ServerUrl + "/campusview/issue", reqData, header, function (response) {
                    var data = JSON.parse(response);
                    try { 
                        $rootScope.$broadcast(BroadCast.NEW_NEWS, data);
                        if (data.result)
                            school.getNewsList(1);
                        if (role == Constant.USER_ROLES.PARENT || role == Constant.USER_ROLES.STUDENT) {
                            if (window.cordova) MobclickAgent.onEvent('app_new_news');
                        } else {
                            if (window.cordova) MobclickAgent.onEvent('app_new_news_teacher');
                        }
                    } catch (e) {
                        console.error('JSON parsing error');
                    }
                }, function (response) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });

            } else {
                $http.post(Constant.ServerUrl + "/campusview/issue", reqData)
                    .success(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.NEW_NEWS, data);
                        if (data.result)
                            school.getNewsList(1);
                        if (role == Constant.USER_ROLES.PARENT || role == Constant.USER_ROLES.STUDENT) {
                            if (window.cordova) MobclickAgent.onEvent('app_new_news');
                        } else {
                            if (window.cordova) MobclickAgent.onEvent('app_new_news_teacher');
                        }
                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    });
            }

        };

        school.stickNews = function (id) {
            if (iOSDevice()) {
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.put(Constant.ServerUrl + "/campusview/topByFocus/" + id, {}, header, function (response) {
                    // prints 200
                    var data = JSON.parse(response);
                    try {
                        data.request = {
                            type: BroadCast.SET_FOCUS_RST_REV,
                            id: id
                        };
                        $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                    } catch (e) {
                        console.error('JSON parsing error');
                    }
                }, function (response) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });

            } else {
                $http.put(Constant.ServerUrl + "/campusview/topByFocus/" + id)
                    .success(function (data, header, config, status) {
                        data.request = {
                            type: BroadCast.SET_FOCUS_RST_REV,
                            id: id
                        };
                        $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);

                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    });
            }

        };

        school.undoStickNews = function (id) {
            if (iOSDevice()) {
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.put(Constant.ServerUrl + "/campusview/focuscancel/" + id, {}, header, function (response) {
                    var data = JSON.parse(response);
                    try {     
                        data.request = {
                            type: BroadCast.UNDO_STICK_RST_REV,
                            id: id
                        };
                        $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                    } catch (e) {
                        console.error('JSON parsing error');
                    }
                }, function (response) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            } else {
                $http.put(Constant.ServerUrl + "/campusview/focuscancel/" + id)
                    .success(function (data, header, config, status) {
                        data.request = {
                            type: BroadCast.UNDO_STICK_RST_REV,
                            id: id
                        };
                        $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);

                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    });
            }

        };

        school.allowPublish = function (reqData) {
            if (reqData && reqData.id) {
                if (iOSDevice()) {
                    cordova.plugin.http.setDataSerializer('json');
                    var header = { "Content-Type": 'application/json' };
                    cordova.plugin.http.post(Constant.ServerUrl + "/campusview/pass", reqData, header, function (response) {
                        var data = JSON.parse(response);
                        try { 
                            data.request = {
                                type: BroadCast.ALLOW_PUBLISH_RST_REV,
                                id: reqData.id
                            };
                            $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                            $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                        } catch (e) {
                            console.error('JSON parsing error');
                        }
                    }, function (response) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    });
                } else {
                    $http.post(Constant.ServerUrl + "/campusview/pass", reqData)
                        .success(function (data, header, config, status) {
                            data.request = {
                                type: BroadCast.ALLOW_PUBLISH_RST_REV,
                                id: reqData.id
                            };
                            $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                        })
                        .error(function (data, header, config, status) {
                            $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                        });
                }
            }
        };

        school.ignorePublish = function (id) {
            if (iOSDevice()) {
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.put(Constant.ServerUrl + "/campusview/ignore/" + id, {}, header, function (response) {
                    var data = JSON.parse(response);
                    try {
                        data.request = {
                            type: BroadCast.IGNORE_RST_REV,
                            id: id
                        };
                        $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                    } catch (e) {
                        console.error('JSON parsing error');
                    }
                }, function (response) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            } else {
                $http.put(Constant.ServerUrl + "/campusview/ignore/" + id)
                    .success(function (data, header, config, status) {
                        data.request = {
                            type: BroadCast.IGNORE_RST_REV,
                            id: id
                        };
                        $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);

                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    });
            }

        };

        school.deleteNews = function (id) {
            if (iOSDevice()) {
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.delete(Constant.ServerUrl + "/campusview/delete?id=" + id, {}, header, function (response) {
                    var data = JSON.parse(response);
                    try {  
                        data.request = {
                            type: BroadCast.DELETE_NEWS_REV,
                            id: id
                        };
                        $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                    } catch (e) {
                        console.error('JSON parsing error');
                    }
                }, function (response) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            } else {
                $http.delete(Constant.ServerUrl + "/campusview/delete?id=" + id)
                    .success(function (data, header, config, status) {
                        data.request = {
                            type: BroadCast.DELETE_NEWS_REV,
                            id: id
                        };
                        $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    });
            }

        };

        school.cancelPublish = function (id) {
            if (iOSDevice()) {
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/campusview/cancelIssue/" + id, {}, header, function (response) {
                    var data = JSON.parse(response);
                    try {
                        data.request = {
                            type: BroadCast.CANCEL_PUBLISH_REV,
                            id: id
                        };
                        $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                    } catch (e) {
                        console.error('JSON parsing error');
                    }
                }, function (response) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            } else {
                $http.get(Constant.ServerUrl + "/campusview/cancelIssue/" + id)
                    .success(function (data, header, config, status) {
                        data.request = {
                            type: BroadCast.CANCEL_PUBLISH_REV,
                            id: id
                        };
                        $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    });
            }

        };

        school.getNewsList = function (page, key, reqId) {
            var req = {
                page: page,
                rows: Constant.reqLimit
            };
            angular.extend(req, key);
            console.log(req);
            if (iOSDevice()) {
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/campusview/list", req, header, function (response) {
                    var data = JSON.parse(response);
                    try { 
                        if (data.result) {
                            var arr = data.data;
                            for (var i = 0; i < arr.length; i++) {
                                arr[i].ispublish = arr[i].state != Constant.NEWS_STATUS.ADMIN_REVIEW.key;
                            }
                            if (page && page > 1)
                                Array.prototype.push.apply(school.list, arr);
                            else {
                                school.list = arr;
                                school.list.forEach(function (item) {
                                    item.picUrls = [];
                                    item.imageUrls.forEach(function (url) {
                                        var urlItem = {
                                            thumb: '',
                                            src: ''
                                        };
                                        urlItem.thumb = url;
                                        urlItem.src = url;
                                        item.picUrls.push(urlItem);
                                    });
                                });
                                if (!key)
                                    UserPreference.setObject("news_list_cache", school.list);
                            }
                            school.listHasMore = arr.length >= Constant.reqLimit / 2;
                            if (Constant.debugMode) {
                                console.log('news data rev, page:' + page);
                                console.log(arr);
                            }
                            data.reqId = reqId;
                        }

                        $rootScope.$broadcast(BroadCast.NEWS_LIST_REV, data);
                    } catch (e) {
                        console.error('JSON parsing error');
                    }
                }, function (response) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            } else {

                $http.get(Constant.ServerUrl + "/campusview/list", {
                    params: req
                })
                    .success(function (data, header, config, status) {

                        if (data.result) {
                            var arr = data.data;
                            for (var i = 0; i < arr.length; i++) {
                                arr[i].ispublish = arr[i].state != Constant.NEWS_STATUS.ADMIN_REVIEW.key;
                            }
                            if (page && page > 1)
                                Array.prototype.push.apply(school.list, arr);
                            else {
                                school.list = arr;
                                school.list.forEach(function (item) {
                                    item.picUrls = [];
                                    item.imageUrls.forEach(function (url) {
                                        var urlItem = {
                                            thumb: '',
                                            src: ''
                                        };
                                        urlItem.thumb = url;
                                        urlItem.src = url;
                                        item.picUrls.push(urlItem);
                                    });
                                });
                                if (!key)
                                    UserPreference.setObject("news_list_cache", school.list);
                            }
                            school.listHasMore = arr.length >= Constant.reqLimit / 2;
                            if (Constant.debugMode) {
                                console.log('news data rev, page:' + page);
                                console.log(arr);
                            }
                            data.reqId = reqId;
                        }

                        $rootScope.$broadcast(BroadCast.NEWS_LIST_REV, data);
                    })
                    .error(function (data, header, config, status) {
                        school.listHasMore = false;
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                        //toaster.error({title: MESSAGES.CONNECT_ERROR, body: MESSAGES.CONNECT_ERROR_MSG});
                    });
            }

        };

        school.getBannerList = function () {
            if (iOSDevice()) {
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/campusview/focuslist", {}, header, function (response) {
                    var data = JSON.parse(response);
                    try {
                        
                        if (data.result) {
                            school.bannerList = data.data;
                            if (Constant.debugMode) {
                                console.log('banner data rev');
                                console.log(school.bannerList);
                            }
                            UserPreference.setObject("news_banner_cache", school.bannerList);
                        }
                        $rootScope.$broadcast(BroadCast.NEWS_STATE_CHANGED, data);
                    } catch (e) {
                        console.error('JSON parsing error');
                    }
                }, function (response) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            } else {
                $http.get(Constant.ServerUrl + "/campusview/focuslist")
                    .success(function (data, header, config, status) {
                        if (data.result) {

                            school.bannerList = data.data;
                            if (Constant.debugMode) {
                                console.log('banner data rev');
                                console.log(school.bannerList);
                            }
                            UserPreference.setObject("news_banner_cache", school.bannerList);
                        }
                        $rootScope.$broadcast(BroadCast.BANNER_LIST_REV, data);
                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                        //toaster.error({title: MESSAGES.CONNECT_ERROR, body: MESSAGES.CONNECT_ERROR_MSG});
                    });
            }

        };

        school.getNewsType = function () {

            if (iOSDevice()) {
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/getdic/show-type/new", {}, header, function (response) {
                    var data = JSON.parse(response);
                    try {
                       
                        if (data.result) {
                            var list = data.data;
                            var selectList = [];
                            for (var i = 0; i < list.length; i++) {
                                if (list[i].parentKey === 2) {
                                    selectList.push(list[i]);
                                }
                            }
                            UserPreference.setObject("news_type_selectable", selectList);
                            UserPreference.setObject("news_type", list);
                            $rootScope.$broadcast(BroadCast.NEWS_TYPE_REV, data);
                        }
                    } catch (e) {
                        console.error('JSON parsing error');
                    }
                }, function (response) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            } else {
                $http.get(Constant.ServerUrl + "/getdic/show-type/new")
                    .success(function (data, header, config, status) {
                        if (data.result) {
                            var list = data.data;
                            var selectList = [];
                            for (var i = 0; i < list.length; i++) {
                                if (list[i].parentKey === 2) {
                                    selectList.push(list[i]);
                                }
                            }
                            UserPreference.setObject("news_type_selectable", selectList);
                            UserPreference.setObject("news_type", list);
                            $rootScope.$broadcast(BroadCast.NEWS_TYPE_REV, data);
                        }
                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    });
            }

        };

        return school;
    })
    .factory('SettingService', function ($http, Constant, $rootScope, BroadCast, UserPreference, toaster, MESSAGES, $ionicLoading, $ionicPopup, $timeout, ChatService) {
        var setting = {};

        setting.sendFeedback = function (text) {
            if (iOSDevice()) {
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.post(Constant.ServerUrl + "/opnionup", text, header, function (response) {
                    var data = JSON.parse(response);
                    try { 
                        $rootScope.$broadcast(BroadCast.FEEDBACK, data);
                        if (window.cordova) MobclickAgent.onEvent('app_suggest');

                    } catch (e) {
                        console.error('JSON parsing error');
                    }
                }, function (response) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            } else {
                $http.post(Constant.ServerUrl + "/opnionup", text)
                    .success(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.FEEDBACK, data);
                        if (window.cordova) MobclickAgent.onEvent('app_suggest');
                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                        //toaster.error({title: MESSAGES.CONNECT_ERROR, body:MESSAGES.CONNECT_ERROR_MSG});
                    });
            }

        };

        setting.changePwd = function (oldPass, newPass) {
            if (iOSDevice()) {
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.post(Constant.ServerUrl + "/changepassword", {
                    curpassword: md5(oldPass).toUpperCase(),
                    newpassword: md5(newPass).toUpperCase()
                }, header, function (response) {
                    // prints 200
                    var data = JSON.parse(response);
                    try {   
                        $rootScope.$broadcast(BroadCast.PASSWORD_CHANGE, data);

                    } catch (e) {
                        console.error('JSON parsing error');
                    }
                }, function (response) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            } else {
                $http.post(Constant.ServerUrl + "/changepassword", {
                    curpassword: md5(oldPass).toUpperCase(),
                    newpassword: md5(newPass).toUpperCase()
                })
                    .success(function (data, header, config, status) {

                        $rootScope.$broadcast(BroadCast.PASSWORD_CHANGE, data);
                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                        //toaster.error({title: MESSAGES.CONNECT_ERROR, body:MESSAGES.CONNECT_ERROR_MSG});
                    });
            }

        };

        setting.editInfo = function (key, value) {

            var param = {};
            if (!key || !value)
                return;
            param.key = key;
            param.value = value;
            if (iOSDevice()) {
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.post(Constant.ServerUrl + "/infoedit", param, header, function (response) {
                    // prints 200
                    try {
                        var data = JSON.parse(response);
                        if (data.result) {
                            var user = UserPreference.getObject('user');
                            if (key === 'name') {
                                user.name = value;
                                if (window.cordova) MobclickAgent.onEvent('app_set_nick');
                            } else if (key === 'logo') {
                                user.logo = 'data:image/jpeg;base64,' + value;
                                if (ChatService.loginInfo)
                                    ChatService.loginInfo.image = user.logo;
                                if (window.cordova) MobclickAgent.onEvent('app_set_avata');
                            } else if (key === 'sex') {
                                user.sex = value;
                                if (window.cordova) MobclickAgent.onEvent('app_set_sex');
                            }
                            UserPreference.setObject('user', user);
                        }

                        $rootScope.$broadcast(BroadCast.EDIT_INFO, data);

                    } catch (e) {
                        console.error('JSON parsing error');
                    }
                }, function (response) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            } else {
                $http.post(Constant.ServerUrl + "/infoedit", param)
                    .success(function (data, header, config, status) {

                        if (data.result) {
                            var user = UserPreference.getObject('user');
                            if (key === 'name') {
                                user.name = value;
                                if (window.cordova) MobclickAgent.onEvent('app_set_nick');
                            } else if (key === 'logo') {
                                user.logo = 'data:image/jpeg;base64,' + value;
                                if (ChatService.loginInfo)
                                    ChatService.loginInfo.image = user.logo;
                                if (window.cordova) MobclickAgent.onEvent('app_set_avata');
                            } else if (key === 'sex') {
                                user.sex = value;
                                if (window.cordova) MobclickAgent.onEvent('app_set_sex');
                            }
                            UserPreference.setObject('user', user);
                        }

                        $rootScope.$broadcast(BroadCast.EDIT_INFO, data);
                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                        //toaster.error({title: MESSAGES.CONNECT_ERROR, body:MESSAGES.CONNECT_ERROR_MSG});
                    });
            }


        };

        setting.checkUpdate = function (silent) {
            var os = ionic.Platform.isIOS() ? 'ios_admin' : 'android_admin';

            function hasNewVersion(v) {
                var vr = v.split('.');
                var vl = Constant.version.split('.');
                if (vr && vr.length === vl.length) {
                    for (var i = 0; i < vr.length; i++) {
                        if (Number(vr[i]) > Number(vl[i]))
                            return true;
                        else if (Number(vr[i]) < Number(vl[i]))
                            return false;
                    }
                }
                return false;
            }
            if (iOSDevice()) {
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/checkversion", {
                    ostype: os,
                    version: Constant.version
                }, header, function (response) {
                    var data = JSON.parse(response);
                    if (data.data && data.data.version) {
                        if (!hasNewVersion(data.data.version)) {
                            if (!silent) $ionicLoading.show({
                                template: '已是最新版应用',
                                noBackdrop: true,
                                duration: 1000
                            });
                        } else {
                            if (Constant.debugMode) console.log(data.data);
                            var confirmPopup = $ionicPopup.confirm({
                                title: '更新提示',
                                template: data.data.content,
                                cancelText: '取消',
                                okText: '升级',
                                okType: 'button-balanced'
                            });
                            if (ionic.Platform.isIOS()) {
                                confirmPopup.then(function (res) {
                                    if (res) {
                                        window.open(data.data.url, '_system');
                                    }
                                });
                            } else if (ionic.Platform.isAndroid()) {

                                confirmPopup.then(function (res) {
                                    if (res) {
                                        window.open('market://details?id=com.sct.xgenban.admin', '_system');
                                    }
                                });
                            }
                        }

                    } else if (!silent) $ionicLoading.show({
                        template: '已是最新版应用',
                        noBackdrop: true,
                        duration: 1000
                    });
                }, function (response) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            } else {
                $http.get(Constant.ServerUrl + "/checkversion", {
                    params: {
                        ostype: os,
                        version: Constant.version
                    }
                })
                    .success(function (data, header, config, status) {
                        if (data.data && data.data.version) {
                            if (!hasNewVersion(data.data.version)) {
                                if (!silent) $ionicLoading.show({
                                    template: '已是最新版应用',
                                    noBackdrop: true,
                                    duration: 1000
                                });
                            } else {
                                if (Constant.debugMode) console.log(data.data);
                                var confirmPopup = $ionicPopup.confirm({
                                    title: '更新提示',
                                    template: data.data.content,
                                    cancelText: '取消',
                                    okText: '升级',
                                    okType: 'button-balanced'
                                });
                                if (ionic.Platform.isIOS()) {
                                    confirmPopup.then(function (res) {
                                        if (res) {
                                            window.open(data.data.url, '_system');
                                        }
                                    });
                                } else if (ionic.Platform.isAndroid()) {

                                    confirmPopup.then(function (res) {
                                        if (res) {
                                            window.open('market://details?id=com.sct.xgenban.admin', '_system');
                                        }
                                    });
                                }
                            }

                        } else if (!silent) $ionicLoading.show({
                            template: '已是最新版应用',
                            noBackdrop: true,
                            duration: 1000
                        });
                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    });
            }


        };

        return setting;
    })
    .factory('NoticeService', function ($http, Constant, $rootScope, BroadCast, UserPreference) {
        var notice = {};

        notice.newNotice = function (reqData) {
            if (iOSDevice()) {
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.post(Constant.ServerUrl + "/notice/issue", reqData, header, function (response) {
                   
                    try { 
                        var data = JSON.parse(response);
                        $rootScope.$broadcast(BroadCast.NEW_NOTICE, data);
                        if (window.cordova) MobclickAgent.onEvent('app_new_notice');

                    } catch (e) {
                        console.error('JSON parsing error');
                    }
                }, function (response) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            } else {
                $http.post(Constant.ServerUrl + "/notice/issue", reqData)
                    .success(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.NEW_NOTICE, data);
                        if (window.cordova) MobclickAgent.onEvent('app_new_notice');
                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    });
            }

        };

        notice.deleteNotice = function (id) {
            if (iOSDevice()) {
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.delete(Constant.ServerUrl + "/notice/delete/" + id, {}, header, function (response) {
                    // prints 200
                    var data = JSON.parse(response);
                    try {     
                        data.request = {
                            type: BroadCast.DELETE_NOTICE_REV,
                            id: id
                        };
                        $rootScope.$broadcast(BroadCast.DELETE_NOTICE_REV, data);

                    } catch (e) {
                        console.error('JSON parsing error');
                    }
                }, function (response) {
                    $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                });
            } else {
                $http.delete(Constant.ServerUrl + "/notice/delete/" + id)
                    .success(function (data, header, config, status) {
                        data.request = {
                            type: BroadCast.DELETE_NOTICE_REV,
                            id: id
                        };
                        $rootScope.$broadcast(BroadCast.DELETE_NOTICE_REV, data);
                    })
                    .error(function (data, header, config, status) {
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                    });
            }

        };

        notice.getNoticeList = function (page, type) {
            var req = {
                page: page,
                rows: Constant.reqLimit
            };
            if (type)
                req.noticeKey = type;
            if (iOSDevice()) {
                req = {
                    page:'1',
                    rows:'10',
                    noticeKey:type? type:''
                };
                console.log('======req:');
                console.log(req);
                console.log('======end:');
                cordova.plugin.http.setRequestTimeout(15.0);
                cordova.plugin.http.setDataSerializer('json');
                var header = { "Content-Type": 'application/json' };
                // cordova.plugin.http.get(Constant.ServerUrl + "/notice/list", req, header, function (response) {
                //     // prints 200
                //     try {
                //         var data = JSON.parse(response);
                //         if (data.result) {
                //             console.log('====news list is :');
                //             console.log(data);
                //             console.log('====end :');
                //             var arr = data.data;
                //             for (var j = 0; j < arr.length; j++) {
                //                 arr[j].time = getWeekday(arr[j].createTime);
                //             }

                //             if (page && page > 1)
                //                 Array.prototype.push.apply(notice.list, arr);
                //             else {
                //                 notice.list = arr;
                //                 UserPreference.setObject("notice_list_cache" + type, notice.list);
                //             }
                //             if (arr.length < Constant.reqLimit)
                //                 notice.listHasMore = false;
                //             else
                //                 notice.listHasMore = true;
                //             if (Constant.debugMode) {
                //                 console.log('notice data rev, page:' + page);
                //                 console.log(arr);
                //             }
                //         }
                //         $rootScope.$broadcast(BroadCast.NOTICE_LIST_REV, data);

                //     } catch (e) {
                //         console.error('JSON parsing error');
                //     }
                // }, function (response) {
                //     notice.listHasMore = false;
                //     console.log('========news errror');
                //     console.log(response);
                //     $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                // });
            } else {
                $http.get(Constant.ServerUrl + "/notice/list", {
                    params: req
                })
                    .success(function (data, header, config, status) {
                        if (data.result) {
                            var arr = data.data;
                            for (var j = 0; j < arr.length; j++) {
                                arr[j].time = getWeekday(arr[j].createTime);
                            }

                            if (page && page > 1)
                                Array.prototype.push.apply(notice.list, arr);
                            else {
                                notice.list = arr;
                                UserPreference.setObject("notice_list_cache" + type, notice.list);
                            }
                            if (arr.length < Constant.reqLimit)
                                notice.listHasMore = false;
                            else
                                notice.listHasMore = true;
                            if (Constant.debugMode) {
                                console.log('notice data rev, page:' + page);
                                console.log(arr);
                            }
                        }
                        $rootScope.$broadcast(BroadCast.NOTICE_LIST_REV, data);
                    })
                    .error(function (data, header, config, status) {
                        notice.listHasMore = false;
                        $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                        //toaster.error({title: MESSAGES.CONNECT_ERROR, body:MESSAGES.CONNECT_ERROR_MSG});
                    });
            }

        };

        return notice;
    })
    .factory('ChatService', function (Constant, UserPreference, $rootScope, BroadCast, $http, $q, $ionicPopup, $window, $state, $injector, $timeout) {
        var chat = {};
        /**
         * 联系人缓存
         */
        chat.infoMap = UserPreference.getObject('ChatUserInfoMap');
        /**
         * 通讯录缓存
         */
        chat.friendsMap = UserPreference.getObject('ChatContactListMap');
        chat.retryCount = 0;
        chat.getTabUnreadCount = function () {
            var count = 0;
            for (var i = 0; i < chat.conversations.length; i++) {
                count += chat.conversations[i].UnreadMsgCount;
            }
            $rootScope.$broadcast(BroadCast.BADGE_UPDATE, {
                type: 'im',
                count: count
            });
        };

        chat.sendPush = function (to, title, message) {
            if (iOSDevice()) {
                var defer = $q.defer();
                var tool = $injector.get('SavePhotoTool');

                cordova.plugin.http.setDataSerializer('json');
                var options = {
                    to: to,
                    title: title,
                    message: message
                };
                var header = { "Content-Type": 'application/json' };

                cordova.plugin.http.post(Constant.ServerUrl + "/im/push",
                    options, header, function (response) {
                        var data = JSON.parse(response.data);
                        try {
                            $timeout(function () {
                                return defer.resolve(data);
                            });

                        } catch (error) {

                        }

                    }, function (error) {
                        if (error.status) {
                            tool.interceptors(error.status);
                        }
                        $timeout(function () {
                            return defer.reject(error);
                        });
                    });

                return defer.promise;
            } else {
                return $http.post(Constant.ServerUrl + "/im/push", {
                    to: to,
                    title: title,
                    message: message
                })
                    .then(function (response) {
                        return response.data;
                    }, function (error) {
                        return $q.reject(error);
                    });
            }

        };

        chat.init = function () {
            var user = UserPreference.getObject("user");
            if (chat.loginInfo || !user.usersig) {
                return;
            }
            var loginInfo = {
                sdkAppID: Constant.IMAppID,
                accountType: Constant.IMAccountType,
                identifier: user.id + '',
                userSig: user.usersig,
                image: user.logo
            };

            var onKickedEventCall = function () {
                var confirmPopup = $ionicPopup.confirm({
                    title: '温馨提示',
                    template: '账号在其它地方登录，您已下线。',
                    cancelText: '取消',
                    okText: '重新登录',
                    okType: 'button-balanced'
                });
                confirmPopup.then(function (res) {
                    if (res) {
                        $state.go('tabsController.mainPage');
                        chat.logout();
                    }
                });
            };

            var onGroupSystemNotifys = function () {
                chat.getContacts();
            };

            var listeners = {
                "onConnNotify": function (resp) {
                    var info;
                    switch (resp.ErrorCode) {
                        case webim.CONNECTION_STATUS.ON:
                            webim.Log.warn('建立连接成功: ' + resp.ErrorInfo);
                            break;
                        case webim.CONNECTION_STATUS.OFF:
                            info = '连接已断开，无法收到新消息，请检查下你的网络是否正常: ' + resp.ErrorInfo;
                            webim.Log.warn(info);
                            break;
                        case webim.CONNECTION_STATUS.RECONNECT:
                            info = '连接状态恢复正常: ' + resp.ErrorInfo;
                            webim.Log.warn(info);
                            break;
                        default:
                            webim.Log.error('未知连接状态: =' + resp.ErrorInfo);
                            break;
                    }
                }, //监听连接状态回调变化事件,必填
                "onMsgNotify": function onMsgNotify(newMsgList) {
                    console.warn(newMsgList);
                    for (var j in newMsgList) { //遍历新消息
                        var newMsg = newMsgList[j];
                        if (newMsg.fromAccount === '@TIM#SYSTEM') {
                            chat.getContacts();
                            continue;
                        }
                        var selSess = newMsg.getSession();
                        var selSessID = selSess.id();
                        var selType = selSess.type();
                        var headUrl = Constant.IM_GROUP_AVATAR,
                            nickName = selSess.name();
                        if (selType == webim.SESSION_TYPE.C2C) {
                            var c2cInfo = chat.getMemberInfoFromMap(selType, selSessID);
                            if (c2cInfo && c2cInfo.name) {
                                nickName = c2cInfo.name;
                            } else {
                                nickName = selSessID;
                            }
                            if (c2cInfo && c2cInfo.image) {
                                headUrl = c2cInfo.image;
                            } else {
                                headUrl = Constant.IM_USER_AVATAR;
                            }
                        }
                        var con = {
                            SessionId: selSessID,
                            SessionImage: headUrl,
                            SessionType: selType,
                            SessionNick: nickName,
                            SessionTime: selSess.time(),
                            MsgTimeStamp: getChatTimeLabel(selSess.time()),
                            UnreadMsgCount: selSess.unread(),
                            MsgShow: delHtmlTag(convertMsgtoPushStr(newMsg))
                        };
                        var k = 0;
                        for (; chat.conversations && k < chat.conversations.length; k++) {
                            if (chat.conversations[k].SessionId == selSess.id()) {
                                chat.conversations[k] = con;
                                break;
                            }
                        }
                        if (!chat.conversations)
                            chat.conversations = [];
                        if (k === chat.conversations.length) {
                            chat.conversations.push(con);
                        }
                    }
                    chat.getTabUnreadCount();
                    $rootScope.$broadcast(BroadCast.IM_NEW_MESSAGE, newMsgList);
                }, //监听新消息(私聊，普通群(非直播聊天室)消息，全员推送消息)事件，必填
                "onGroupSystemNotifys": onGroupSystemNotifys,
                "onKickedEventCall": onKickedEventCall
            };
            var opts = {
                isLogOn: false
            };

            function showLoginError() {
                var confirmPopup = $ionicPopup.confirm({
                    template: '通讯服务器连接失败，请重试！',
                    cancelText: '取消',
                    okText: '重试',
                    okType: 'button-balanced'
                });
                confirmPopup.then(function (res) {
                    if (res) {
                        chat.init();
                    }
                });
            }

            webim.login(loginInfo, listeners, opts,
                function (resp) {
                    chat.loginInfo = loginInfo;
                    chat.retryCount = 0;
                    chat.getRecentContacts();
                    chat.getContacts();
                },
                function (err) {
                    if (chat.retryCount > 2) {
                        chat.retryCount = 0;
                        showLoginError();
                    } else {
                        setTimeout(function () {
                            chat.retryCount++;
                            chat.init();
                        }, 1000);
                    }
                });
        };

        chat.getRecentContacts = function (classid) {
            if (!chat.loginInfo) {
                chat.init();
                console.log("try login firstly..");
                return;
            }
            webim.getRecentContactList({
                'Count': 100
            }, function (resp) {
                //console.log(resp);
                var data = [];
                var tempSess, tempSessMap = {}; //临时会话变量
                if (resp.SessionItem && resp.SessionItem.length > 0) {
                    for (var i in resp.SessionItem) {
                        var item = resp.SessionItem[i];
                        var type = item.Type; //接口返回的会话类型
                        var sessType, typeZh, sessionId, sessionNick = '',
                            sessionImage = '',
                            senderId = '',
                            senderNick = '';
                        if (type == webim.RECENT_CONTACT_TYPE.C2C) { //私聊
                            typeZh = '私聊';
                            sessType = webim.SESSION_TYPE.C2C; //设置会话类型
                            sessionId = item.To_Account; //会话id，私聊时为好友ID或者系统账号（值为@TIM#SYSTEM，业务可以自己决定是否需要展示），注意：从To_Account获取,

                            if (sessionId === '@TIM#SYSTEM') { //先过滤系统消息，，
                                webim.Log.warn('过滤好友系统消息,sessionId=' + sessionId);
                                continue;
                            }
                            var c2cInfo = chat.getMemberInfoFromMap(sessType, sessionId, classid);
                            if (c2cInfo && c2cInfo.name) { //从infoMap获取c2c昵称
                                sessionNick = c2cInfo.name; //会话昵称，私聊时为好友昵称，接口暂不支持返回，需要业务自己获取（前提是用户设置过自己的昵称，通过拉取好友资料接口（支持批量拉取）得到）
                            } else { //没有找到或者没有设置过
                                sessionNick = sessionId; //会话昵称，如果昵称为空，默认将其设成会话id
                            }
                            if (c2cInfo && c2cInfo.image) { //从infoMap获取c2c头像
                                sessionImage = c2cInfo.image; //会话头像，私聊时为好友头像，接口暂不支持返回，需要业务自己获取（前提是用户设置过自己的昵称，通过拉取好友资料接口（支持批量拉取）得到）
                            } else { //没有找到或者没有设置过
                                sessionImage = Constant.IM_USER_AVATAR; //会话头像，默认
                            }
                            senderId = senderNick = ''; //私聊时，这些字段用不到，直接设置为空

                        } else if (type == webim.RECENT_CONTACT_TYPE.GROUP) { //群聊
                            typeZh = '群聊';
                            sessType = webim.SESSION_TYPE.GROUP; //设置会话类型
                            sessionId = item.ToAccount; //会话id，群聊时为群ID，注意：从ToAccount获取
                            sessionNick = item.GroupNick; //会话昵称，群聊时，为群名称，接口一定会返回

                            if (item.GroupImage) {
                                sessionImage = item.GroupImage;
                            } else
                                sessionImage = Constant.IM_GROUP_AVATAR;
                            senderId = item.MsgGroupFrom_Account; //群消息的发送者id

                            if (!senderId) { //发送者id为空
                                webim.Log.warn('群消息发送者id为空,senderId=' + senderId + ",groupid=" + sessionId);
                                continue;
                            }
                            // if (senderId == '@TIM#SYSTEM') {//先过滤群系统消息，因为接口暂时区分不了是进群还是退群等提示消息，
                            // 	webim.Log.warn('过滤群系统消息,senderId=' + senderId + ",groupid=" + sessionId);
                            // 	continue;
                            // }
                        } else {
                            typeZh = '未知类型';
                            sessionId = item.ToAccount;
                        }

                        if (!sessionId) { //会话id为空
                            webim.Log.warn('会话id为空,sessionId=' + sessionId);
                            continue;
                        }

                        if (sessionId === '@TLS#NOT_FOUND') { //会话id不存在，可能是已经被删除了
                            webim.Log.warn('会话id不存在,sessionId=' + sessionId);
                            continue;
                        }

                        if (sessionNick.length > 10) { //帐号或昵称过长，截取一部分
                            sessionNick = sessionNick.substr(0, 10) + "...";
                        }

                        tempSess = tempSessMap[sessType + "_" + sessionId];
                        if (!tempSess) { //先判断是否存在（用于去重），不存在增加一个
                            tempSessMap[sessType + "_" + sessionId] = true;
                            data.push({
                                SessionType: sessType, //会话类型
                                SessionTypeZh: typeZh, //会话类型中文
                                SessionId: webim.Tool.formatText2Html(sessionId), //会话id
                                SessionNick: webim.Tool.formatText2Html(sessionNick), //会话昵称
                                SessionImage: sessionImage, //会话头像
                                C2cAccount: webim.Tool.formatText2Html(senderId), //发送者id
                                C2cNick: webim.Tool.formatText2Html(senderNick), //发送者昵称
                                UnreadMsgCount: item.UnreadMsgCount, //未读消息数
                                MsgSeq: item.MsgSeq, //消息seq
                                MsgRandom: item.MsgRandom, //消息随机数
                                MsgTimeStamp: getChatTimeLabel(item.MsgTimeStamp), //消息时间戳
                                MsgShow: item.MsgShow //消息内容
                            });
                        }
                    }
                }
                chat.conversations = data;

                function initUnreadMsgCount() {
                    var sess;
                    var sessMap = webim.MsgStore.sessMap();
                    for (var i in sessMap) {
                        sess = sessMap[i];
                        for (var j = 0; j < data.length; j++) {
                            if (sess.id() == data[j].SessionId) {
                                data[j].UnreadMsgCount = sess.unread();
                                break;
                            }
                        }
                    }
                    chat.getTabUnreadCount();
                }

                webim.syncMsgs(initUnreadMsgCount);
                $rootScope.$broadcast(BroadCast.IM_RECENT_CONTACTS, data);
            }, function (error) {
                $rootScope.$broadcast(BroadCast.IM_RECENT_CONTACTS, undefined);
                console.error(error);
            });
        };

        chat.getContacts = function () {
            var options = {
                'Member_Account': chat.loginInfo.identifier,
                'Offset': 0,
                'GroupBaseInfoFilter': [
                    'Type',
                    'Name',
                    'Introduction',
                    'Notification',
                    'FaceUrl',
                    'CreateTime',
                    'Owner_Account',
                    'LastInfoTime',
                    'LastMsgTime',
                    'NextMsgSeq',
                    'MemberNum',
                    'MaxMemberNum',
                    'ApplyJoinOption'
                ],
                'SelfInfoFilter': [
                    'Role',
                    'JoinTime',
                    'MsgFlag',
                    'UnreadMsgNum'
                ]
            };
            webim.getJoinedGroupListHigh(
                options,
                function (resp) {
                    chat.retryCount = 0;
                    if (!resp.GroupIdList || resp.GroupIdList.length === 0) {
                        console.log('你目前还没有加入任何群组');
                        return;
                    }
                    var data = [];
                    for (var i = 0; i < resp.GroupIdList.length; i++) {
                        var item = {
                            'SortField': webim.Tool.groupTypeEn2Ch(resp.GroupIdList[i].Type),
                            'GroupId': resp.GroupIdList[i].GroupId,
                            'Name': webim.Tool.formatText2Html(resp.GroupIdList[i].Name),
                            'TypeEn': resp.GroupIdList[i].Type,
                            'Type': webim.Tool.groupTypeEn2Ch(resp.GroupIdList[i].Type),
                            'RoleEn': resp.GroupIdList[i].SelfInfo.Role,
                            'Role': webim.Tool.groupRoleEn2Ch(resp.GroupIdList[i].SelfInfo.Role),
                            'MsgFlagEn': webim.Tool.groupMsgFlagEn2Ch(resp.GroupIdList[i].SelfInfo.MsgFlag),
                            'MsgFlag': webim.Tool.groupMsgFlagEn2Ch(resp.GroupIdList[i].SelfInfo.MsgFlag),
                            'MemberNum': resp.GroupIdList[i].MemberNum,
                            'Notification': webim.Tool.formatText2Html(resp.GroupIdList[i].Notification),
                            'Introduction': webim.Tool.formatText2Html(resp.GroupIdList[i].Introduction),
                            'JoinTime': webim.Tool.formatTimeStamp(resp.GroupIdList[i].SelfInfo.JoinTime),
                            'NextMsgSeq': resp.GroupIdList[i].NextMsgSeq,
                            'FaceUrl': resp.GroupIdList[i].FaceUrl
                        };

                        var classid, needCache = false;
                        var arr = item.GroupId.split('_');
                        if (arr && arr.length > 1) {
                            classid = item.ClassID = arr[arr.length - 1];
                            if (item.TypeEn === 'Public' && (arr[0] === 'all' || arr[0] === 'school')) {
                                needCache = true;
                                if (arr[0] === 'school') {
                                    classid = item.ClassID = 'schoolTeacher';
                                    data.push(item);
                                }
                            } else
                                data.push(item);
                            chat.getGroupMemberInfo(item, needCache, classid);
                        }
                    }
                    chat.groups = data;
                },
                function (err) {
                    console.log(err.ErrorInfo);
                    if (chat.retryCount > 2) {
                        chat.retryCount = 0;
                        $rootScope.$broadcast(BroadCast.IM_REV_CONTACTS, undefined);
                    } else {
                        setTimeout(function () {
                            chat.retryCount++;
                            chat.getContacts();
                        }, 5000);
                    }
                }
            );
        };

        chat.getMemberInfoFromMap = function (sessType, sessionId, classid) {
            var key = sessType + "_" + sessionId;
            if (sessType == webim.SESSION_TYPE.C2C) {
                var ckey = sessType + "_" + sessionId + "_" + classid;
                if (chat.infoMap[ckey])
                    return chat.infoMap[ckey];
                else
                    return chat.infoMap[key];
            }
            return chat.infoMap[key];
        };

        chat.getGroupMemberInfo = function (group, needCache, classid) {
            var options = {
                'GroupId': group.GroupId,
                'Offset': 0,
                'MemberInfoFilter': [
                    'Account',
                    'Role',
                    'NameCard',
                    'JoinTime',
                    'LastSendMsgTime',
                    'ShutUpUntil',
                    'AppMemberDefinedData'
                ]
            };
            webim.getGroupMemberInfo(
                options,
                function (resp) {
                    console.log(resp);
                    chat.retryCount = 0;
                    if (resp.MemberNum <= 0) {
                        console.log(group.Name + '目前没有成员');
                        return;
                    }
                    //console.log(resp);
                    var data = [];
                    for (var i in resp.MemberList) {
                        var account = resp.MemberList[i].Member_Account;
                        var role = webim.Tool.groupRoleEn2Ch(resp.MemberList[i].Role);
                        var join_time = webim.Tool.formatTimeStamp(
                            resp.MemberList[i].JoinTime);
                        var shut_up_until = webim.Tool.formatTimeStamp(
                            resp.MemberList[i].ShutUpUntil);
                        if (shut_up_until === 0) {
                            shut_up_until = '-';
                        }
                        var nick, icon, category;
                        var extra = resp.MemberList[i].AppMemberDefinedData;
                        for (var j = 0; j < extra.length; j++) {
                            if (extra[j].Key === 'Category')
                                category = extra[j].Value;
                            else if (extra[j].Key === 'Nick')
                                nick = extra[j].Value;
                            else if (extra[j].Key === 'Icon')
                                icon = extra[j].Value;
                        }

                        var key = webim.SESSION_TYPE.C2C + "_" + account;
                        var pre = chat.infoMap[key];
                        if (!pre || (pre && nick !== ""))
                            chat.infoMap[key] = {
                                name: nick,
                                image: icon
                            };
                        if (needCache) {
                            chat.infoMap[key + "_" + classid] = chat.infoMap[key];
                            data.push({
                                SortField: nick,
                                GroupId: group.GroupId,
                                Member_Account: account,
                                Role: role,
                                JoinTime: join_time,
                                ShutUpUntil: shut_up_until,
                                Icon: icon,
                                Name: nick,
                                Category: category,
                                ClassID: classid
                            });
                        }
                    }
                    if (needCache) {
                        chat.friendsMap[classid] = data;
                        UserPreference.setObject('ChatUserInfoMap', chat.infoMap);
                        UserPreference.setObject('ChatContactListMap', chat.friendsMap);
                        $rootScope.$broadcast(BroadCast.IM_REV_CONTACTS, data);
                    }
                },
                function (err) {
                    if (needCache) {
                        $rootScope.$broadcast(BroadCast.IM_REV_CONTACTS, undefined);
                        if (chat.retryCount > 2) {
                            chat.retryCount = 0;
                        } else {
                            setTimeout(function () {
                                chat.retryCount++;
                                chat.getContacts();
                            }, 5000);
                        }
                    }
                    console.log(err.ErrorInfo);
                }
            );
        };

        chat.getC2CHistoryMsgs = function (id, lastMsgTime, msgKey) {
            var options = {
                'Peer_Account': id, //好友帐号
                'MaxCnt': 10, //拉取消息条数
                'LastMsgTime': lastMsgTime, //最近的消息时间，即从这个时间点向前拉取历史消息
                'MsgKey': msgKey
            };
            webim.getC2CHistoryMsgs(
                options,
                function (resp) {
                    $rootScope.$broadcast(BroadCast.IM_C2C_HISTORY_MESSAGE, resp);
                },
                function (error) {
                    $rootScope.$broadcast(BroadCast.IM_C2C_HISTORY_MESSAGE, error);
                }
            );
        };

        chat.getGroupHistoryMsgs = function (id, ReqMsgSeq) {
            if (ReqMsgSeq === 0)
                ReqMsgSeq = undefined;
            var options = {
                'GroupId': id,
                'ReqMsgNumber': 10,
                'ReqMsgSeq': ReqMsgSeq
            };
            webim.syncGroupMsgs(
                options,
                function (resp) {
                    $rootScope.$broadcast(BroadCast.IM_GROUP_HISTORY_MESSAGE, resp);
                },
                function (error) {
                    $rootScope.$broadcast(BroadCast.IM_GROUP_HISTORY_MESSAGE, error);
                }
            );
        };

        chat.quitGroup = function (group_id) {
            var defer = $q.defer();
            var options = null;
            if (group_id) {
                options = {
                    'GroupId': group_id
                };
            }
            webim.quitGroup(
                options,
                function (resp) {
                    defer.resolve(resp);
                },
                function (err) {
                    defer.reject(err);
                }
            );
            return defer.promise;
        };

        chat.getGroupInfo = function (id) {
            var defer = $q.defer();
            var options = {
                'GroupIdList': [
                    id
                ],
                'GroupBaseInfoFilter': [
                    'Type',
                    'Name',
                    'Introduction',
                    'Notification',
                    'FaceUrl',
                    'CreateTime',
                    'Owner_Account',
                    'LastInfoTime',
                    'LastMsgTime',
                    'NextMsgSeq',
                    'MemberNum',
                    'MaxMemberNum',
                    'ApplyJoinOption'
                ],
                'MemberInfoFilter': [
                    'Account',
                    'Role',
                    'NameCard',
                    'JoinTime',
                    'LastSendMsgTime',
                    'ShutUpUntil',
                    'AppMemberDefinedData'
                ]
            };
            webim.getGroupInfo(
                options,
                function (resp) {
                    defer.resolve(resp);
                },
                function (err) {
                    defer.reject(err);
                }
            );
            return defer.promise;
        };

        chat.modifyGroupInfo = function (gid, name) {
            var defer = $q.defer();
            var options = {
                'GroupId': gid,
                'Name': name
            };
            webim.modifyGroupBaseInfo(
                options,
                function (resp) {
                    defer.resolve(resp);
                },
                function (err) {
                    defer.reject(err);
                }
            );
            return defer.promise;
        };

        chat.modifyGroupMember = function (gid, members, deleteMode) {
            var defer = $q.defer();
            if (deleteMode) {
                var options = {
                    'GroupId': gid,
                    'MemberToDel_Account': members
                };
                webim.deleteGroupMember(
                    options,
                    function (resp) {
                        defer.resolve(resp);
                    },
                    function (err) {
                        defer.reject(err);
                    }
                );
                return defer.promise;
            } else {
                if (iOSDevice()) {
                   // var defer = $q.defer();
                    var tool = $injector.get('SavePhotoTool');
                    cordova.plugin.http.setDataSerializer('urlencoded');
                    var option = CustomParam({
                        'groupId': gid,
                        'members': members
                    });
                    var header = { "Content-Type": 'application/x-www-form-urlencoded' };
                    cordova.plugin.http.post(Constant.ServerUrl + "/im/add-members",
                        option, header, function (response) {
                            var data = JSON.parse(response.data);
                            try {
                                $timeout(function () {
                                    return defer.resolve(data);
                                });

                            } catch (error) {

                            }
                        }, function (error) {
                            if (error.status) {
                                tool.interceptors(error.status);
                            }
                            $timeout(function () {
                                return defer.reject(error);
                            });
                        });

                    return defer.promise;

                } else {
                    return $http({
                        method: "post",
                        url: Constant.ServerUrl + "/im/add-members",
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: CustomParam({
                            'groupId': gid,
                            'members': members
                        })
                    });
                }

            }
        };

        chat.createCustomGroup = function (name, sclass, members) {
            var uid = chat.loginInfo.identifier;
            var gid = new Date().getTime() + uid;
            var options = {
                'groupId': gid + "_" + sclass.key,
                'owner': chat.loginInfo.identifier,
                'groupName': name,
                'intro': sclass.value,
                'members': members
            };
            if (iOSDevice()) {
                var defer = $q.defer();
                var tool = $injector.get('SavePhotoTool');
                cordova.plugin.http.setDataSerializer('urlencoded');
                var option = CustomParam(options);
                var header = { "Content-Type": 'application/x-www-form-urlencoded' };
                cordova.plugin.http.post(Constant.ServerUrl + "/im/create-group",
                    option, header, function (response) {
                        var data = JSON.parse(response.data);
                        try {
                            $timeout(function () {
                                return defer.resolve(data);
                            });

                        } catch (error) {

                        }
                    }, function (error) {
                        if (error.status) {
                            tool.interceptors(error.status);
                        }
                        $timeout(function () {
                            return defer.reject(error);
                        });
                    });

                return defer.promise;
            } else {
                return $http({
                    method: "post",
                    url: Constant.ServerUrl + "/im/create-group",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: CustomParam(options)
                });
            }
        };

        chat.uploadImageAndSend = function (selSess, img, onProgressCallBack) {
            var selType = selSess.type();
            var businessType; //业务类型，1-发群图片，2-向好友发图片
            if (selType == webim.SESSION_TYPE.C2C) { //向好友发图片
                businessType = webim.UPLOAD_PIC_BUSSINESS_TYPE.C2C_MSG;
            } else if (selType == webim.SESSION_TYPE.GROUP) { //发群图片
                businessType = webim.UPLOAD_PIC_BUSSINESS_TYPE.GROUP_MSG;
            }
            var opt = {
                'file': img, //图片对象
                'onProgressCallBack': onProgressCallBack, //上传图片进度条回调函数
                //'abortButton': document.getElementById('upd_abort'), //停止上传图片按钮
                'To_Account': selSess.id(), //接收者
                'businessType': businessType //业务类型
            };
            var msg = new webim.Msg(selSess, true, -1, -1, -1, chat.loginInfo.identifier, 0, chat.loginInfo.identifierNick);
            var rsp = {
                msg: msg,
                type: 'image'
            };
            $rootScope.$broadcast(BroadCast.IM_MSG_SENDING, rsp);
            //上传图片
            webim.uploadPic(opt,
                function (images) {
                    var images_obj = new webim.Msg.Elem.Images(images.File_UUID);
                    for (var i in images.URL_INFO) {
                        var img = images.URL_INFO[i];
                        var newImg;
                        var type;
                        switch (img.PIC_TYPE) {
                            case 1: //原图
                                type = 1; //原图
                                break;
                            case 2: //小图（缩略图）
                                type = 3; //小图
                                break;
                            case 4: //大图
                                type = 2; //大图
                                break;
                        }
                        newImg = new webim.Msg.Elem.Images.Image(type, img.PIC_Size, img.PIC_Width, img.PIC_Height, img.DownUrl);
                        images_obj.addImage(newImg);
                    }
                    msg.addImage(images_obj);
                    rsp = {
                        msg: msg,
                        type: 'image'
                    };
                    webim.sendMsg(msg, function (resp) {
                        if (selType == webim.SESSION_TYPE.C2C) { //私聊时，在聊天窗口手动添加一条发的消息，群聊时，长轮询接口会返回自己发的消息
                            $rootScope.$broadcast(BroadCast.IM_MSG_SENT, rsp);
                        }
                    }, function (err) {
                        console.error(err.ErrorInfo);
                        $rootScope.$broadcast(BroadCast.IM_MSG_SEND_FAIL, rsp);
                    });
                },
                function (err) {
                    console.error(err.ErrorInfo);
                    $rootScope.$broadcast(BroadCast.IM_IMAGE_SEND_FAIL, rsp);
                }
            );
        };

        chat.sendMessage = function (session, msgtosend) {
            var msgLen = webim.Tool.getStrBytes(msgtosend);
            var selType = session.type();
            var maxLen, errInfo;
            if (selType == webim.SESSION_TYPE.C2C) {
                maxLen = webim.MSG_MAX_LENGTH.C2C;
                errInfo = "消息长度超出限制(最多" + Math.round(maxLen / 3) + "汉字)";
            } else {
                maxLen = webim.MSG_MAX_LENGTH.GROUP;
                errInfo = "消息长度超出限制(最多" + Math.round(maxLen / 3) + "汉字)";
            }
            if (msgLen > maxLen) {
                alert(errInfo);
                return;
            }
            if (!session) {
                alert("消息发送失败");
            }
            var isSend = true; //是否为自己发送
            var seq = -1; //消息序列，-1表示sdk自动生成，用于去重
            var random = Math.round(Math.random() * 4294967296); //消息随机数，用于去重
            var msgTime = Math.round(new Date().getTime() / 1000); //消息时间戳
            var subType; //消息子类型
            if (selType == webim.SESSION_TYPE.C2C) {
                subType = webim.C2C_MSG_SUB_TYPE.COMMON;
                if (window.cordova) MobclickAgent.onEvent('app_send_c2c_msg');
            } else {
                //webim.GROUP_MSG_SUB_TYPE.COMMON-普通消息,
                //webim.GROUP_MSG_SUB_TYPE.LOVEMSG-点赞消息，优先级最低
                //webim.GROUP_MSG_SUB_TYPE.TIP-提示消息(不支持发送，用于区分群消息子类型)，
                //webim.GROUP_MSG_SUB_TYPE.REDPACKET-红包消息，优先级最高
                subType = webim.GROUP_MSG_SUB_TYPE.COMMON;
                if (window.cordova) MobclickAgent.onEvent('app_send_group_msg');
            }
            var msg = new webim.Msg(session, isSend, seq, random, msgTime, chat.loginInfo.identifier, subType, chat.loginInfo.identifierNick);

            var text_obj, face_obj, tmsg, emotionIndex, emotion, restMsgIndex;
            //解析文本和表情
            var expr = /\[[^[\]]{1,3}\]/mg;
            var emotions = msgtosend.match(expr);
            if (!emotions || emotions.length < 1) {
                text_obj = new webim.Msg.Elem.Text(msgtosend);
                msg.addText(text_obj);
            } else {

                for (var i = 0; i < emotions.length; i++) {
                    tmsg = msgtosend.substring(0, msgtosend.indexOf(emotions[i]));
                    if (tmsg) {
                        text_obj = new webim.Msg.Elem.Text(tmsg);
                        msg.addText(text_obj);
                    }
                    emotionIndex = webim.EmotionDataIndexs[emotions[i]];
                    emotion = webim.Emotions[emotionIndex];

                    if (emotion) {
                        face_obj = new webim.Msg.Elem.Face(emotionIndex, emotions[i]);
                        msg.addFace(face_obj);
                    } else {
                        text_obj = new webim.Msg.Elem.Text(emotions[i]);
                        msg.addText(text_obj);
                    }
                    restMsgIndex = msgtosend.indexOf(emotions[i]) + emotions[i].length;
                    msgtosend = msgtosend.substring(restMsgIndex);
                }
                if (msgtosend) {
                    text_obj = new webim.Msg.Elem.Text(msgtosend);
                    msg.addText(text_obj);
                }
            }
            var rsp = {
                msg: msg,
                type: 'text'
            };
            $rootScope.$broadcast(BroadCast.IM_MSG_SENDING, rsp);
            webim.sendMsg(msg, function (resp) {
                $rootScope.$broadcast(BroadCast.IM_MSG_SENT, rsp);
            }, function (err) {
                console.error(err.ErrorInfo);
                $rootScope.$broadcast(BroadCast.IM_MSG_SEND_FAIL, rsp);
            });
        };

        chat.logout = function () {
            if (chat.loginInfo) {
                webim.logout(
                    function (resp) {
                        chat.loginInfo = null;
                        $window.location.reload();
                    }
                );
            }
        };

        chat.getUserDetail = function (id) {
            if (iOSDevice()) {
                var defer = $q.defer();
                var tool = $injector.get('SavePhotoTool');
                cordova.plugin.http.setDataSerializer('json');
                var option = { id: id };
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/getinfo",
                    option, header, function (response) {
                        var data = JSON.parse(response.data);
                        try { 
                            $timeout(function () {
                                return defer.resolve(data);
                            });

                        } catch (error) {

                        }
                    }, function (error) {
                        if (error.status) {
                            tool.interceptors(error.status);
                        }
                        $timeout(function () {
                            return defer.reject(error);
                        });
                    });

                return defer.promise;
            } else {
                return $http.get(Constant.ServerUrl + "/getinfo", {
                    params: {
                        id: id
                    }
                })
                    .then(function (response) {
                        return response.data;
                    }, function (error) {
                        return $q.reject(error);
                    });
            }

        };

        return chat;
    })
    .factory('SavePhotoTool', function ($ionicLoading, $q, $injector) {
        var tool = {};
        tool.savePhoto = function (url) {
            var config = {
                allowSave: true,
                album: 'Xgenban'
            };
            tool.config = angular.extend({}, config, {
                allowSave: true
            });
            if (window.cordova) {
                cordova.plugins.photoLibrary.getAlbums(
                    function (result) {
                        tool.saveImgUrl(url);
                    },
                    function (err) {
                        if (err.startsWith('Permission')) {
                            cordova.plugins.photoLibrary.requestAuthorization(
                                function () {
                                    tool.saveImgUrl(url);
                                },
                                function (err) {
                                    // User denied the access
                                    console.log(err);
                                }, {
                                read: true,
                                write: true
                            }
                            );
                        }
                    }
                );

            }
        };

        tool.saveImgUrl = function (url) {
            if (window.cordova) {
                cordova.plugins.photoLibrary.saveImage(url + '?ext=.jpg', tool.config.album, function (libraryItem) {
                    $ionicLoading.show({
                        template: '保存成功',
                        duration: 1500
                    });
                }, function (err) {
                    console.log(err);
                });
            }

        };

        var doLogin = function () {
            var state = $injector.get('$state');
            var user = $injector.get('UserPreference');
            var auth = $injector.get('AuthorizeService');
            if (!auth.logining && state.current.url !== '/login') {
                var loginModel = {
                    remember: true,
                    username: user.get('username', ''),
                    password: user.get('password', '')
                };
                if (loginModel.username !== '' && loginModel.password !== '') {
                    console.error('timeout, try auto login.');
                    auth.login(loginModel).then(function (res) {
                        if (res && res.result)
                            state.go('tabsController.mainPage');
                    });
                } else {
                    console.error('username or password not found');
                    state.go('login');
                }
            }
        };

        tool.interceptors = function (status) {

            var toaster = $injector.get('toaster');
            var MESSAGES = $injector.get('MESSAGES');
            var rootScope = $injector.get('$rootScope');
            var BroadCast = $injector.get('BroadCast');
            // console.log(' response ---:' + rejection.status);
            rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
            switch (status) {
                case -1:
                   
                case -4:
                    toaster.warning({
                        title: MESSAGES.REQUEST_ERROR,
                        body: MESSAGES.CONNECT_TIMEOUT_MSG
                    });
                    break;

                case 408:
                    toaster.warning({
                        title: MESSAGES.REQUEST_ERROR,
                        body: MESSAGES.CONNECT_TIMEOUT_MSG
                    });
                    break;
                case 401:
                    doLogin();
                    break;
                case 404:
                    toaster.error({
                        title: MESSAGES.CONNECT_ERROR,
                        body: MESSAGES.CONNECT_ERROR,//rejection.data.message
                    });
                    break;
                case 500:
                    toaster.error({
                        title: MESSAGES.CONNECT_ERROR,
                        body: MESSAGES.SERVER_ERROR
                    });
                    break;
                case 503:
                    toaster.error({
                        title: MESSAGES.CONNECT_ERROR,//rejection.data.message,
                        body: ''
                    });
                    break;
                default:
                    toaster.error({
                        title: MESSAGES.CONNECT_ERROR,
                        body: MESSAGES.CONNECT_ERROR_MSG
                    });
                    break;
            }

        };

        return tool;
    })
    .factory('Translate', function ($translate) {
        var T = {};
        T.translate = function (key) {
            if (key) {
                return $translate.instant(key);
            }
            return key;
        };
        return T;
    })
    .factory('DownloadFile', function ($ionicLoading, $timeout, $cordovaFileTransfer, $cordovaFileOpener2, toaster, MESSAGES) {
        var file = {};
        file.readFile = function (url) {
            $ionicLoading.show({
                template: "请稍候，正在读取：0%"
            });

            var fileType = url.slice(url.lastIndexOf('.') + 1);
            var mimeType = '';
            switch (fileType.toLowerCase()) {
                case 'txt':
                    mimeType = 'text/plain';
                    break;
                case 'docx':
                    mimeType = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
                    break;
                case 'doc':
                    mimeType = 'application/msword';
                    break;
                case 'pptx':
                    mimeType = 'application/vnd.openxmlformats-officedocument.presentationml.presentation';
                    break;
                case 'ppt':
                    mimeType = 'application/vnd.ms-powerpoint';
                    break;
                case 'xlsx':
                    mimeType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                    break;
                case 'xls':
                    mimeType = 'application/vnd.ms-excel';
                    break;
                case 'zip':
                    mimeType = 'application/x-zip-compressed';
                    break;
                case 'rar':
                    mimeType = 'application/octet-stream';
                    break;
                case 'pdf':
                    mimeType = 'application/pdf';
                    break;
                case 'jpg':
                    mimeType = 'image/jpeg';
                    break;
                case 'png':
                    mimeType = 'image/png';
                    break;
                default:
                    mimeType = 'application/' + fileType;
                    break;
            }
            var targetPath;
            var isIos = ionic.Platform.isIOS();
            if (window.cordova) {
                if (isIos) {
                    targetPath = cordova.file.tempDirectory + fileType;
                } else {
                    targetPath = cordova.file.dataDirectory + fileType;
                }
            }

            // var targetPath = "file:///sdcard/Xgenban/update.apk"; //APP下载存放的路径，可以使用cordova file插件进行相关配置
            var trustHosts = true;
            var options = {};
            $cordovaFileTransfer.download(url, targetPath, options, trustHosts).then(function (result) {
                //  alert('succeed');
                // 打开下载下来的文件
                if (window.cordova) {
                    // if(!isIos){
                    //     $cordovaFileOpener2.appIsInstalled('com.adobe.reader').then(function(res) {
                    //         if (res.status === 0) {
                    //           alert('未检测到相应的软件能打开此文档');
                    //         } else {
                    //             // Adobe Reader is installed.
                    //         }
                    //     });
                    // }
                    $cordovaFileOpener2.open(targetPath, mimeType).then(function () {
                        // console.log('open succeed');
                        // alert('open succeed');
                        // 成功
                    }, function (err) {
                        // 错误
                        // alert('open error');
                        console.log(err);
                    });
                }

                $ionicLoading.hide();
            }, function (err) {
                toaster.error({
                    title: MESSAGES.DOWNLOAD_ERROR,
                    body: err.exception
                });
                $ionicLoading.hide();
            }, function (progress) {
                $timeout(function () {
                    var downloadProgress = (progress.loaded / progress.total) * 100;
                    $ionicLoading.show({
                        template: "请稍候，正在读取：" + Math.floor(downloadProgress) + "%"
                    });
                    if (downloadProgress > 99) {
                        $ionicLoading.hide();
                    }
                });
            });
        };
        return file;
    });

/*jshint scripturl:true*/
angular.module('app.directives', [])
    .directive('resizeFootBar', ['$ionicScrollDelegate', '$rootScope', function ($ionicScrollDelegate, $rootScope) {
        return {
            replace: false,
            link: function (scope, iElm, iAttrs, controller) {
                scope.$on("taResize", function (e, ta) {
                    if (!ta) return;
                    var scroller = document.body.querySelector('#userMessagesView .scroll-content');
                    var viewScroll = $ionicScrollDelegate.$getByHandle('userMessageScroll');

                    var taHeight = ta[0].offsetHeight;
                    var newFooterHeight = taHeight + 10;
                    newFooterHeight = (newFooterHeight > 44) ? newFooterHeight : 44;
                    iElm[0].style.height = newFooterHeight + 'px';
                    scroller.style.bottom = newFooterHeight + 'px';
                    viewScroll.scrollBottom();
                });
            }
        };
    }])
    .directive('ngFocus', [function () {
        var FOCUS_CLASS = "ng-focused";
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ctrl) {
                ctrl.$focused = false;
                element.bind('focus', function (evt) {
                    element.addClass(FOCUS_CLASS);
                    scope.$apply(function () {
                        ctrl.$focused = true;
                    });
                }).bind('blur', function (evt) {
                    element.removeClass(FOCUS_CLASS);
                    scope.$apply(function () {
                        ctrl.$focused = false;
                    });
                });
            }
        };
    }])
    .directive('compile', ['$compile', function ($compile) {
        return function (scope, element, attrs) {
            scope.$watch(function (scope) {
                    return scope.$eval(attrs.compile);
                },
                function (value) {
                    element.html(value);
                    $compile(element.contents())(scope);
                }
            );
        };
    }])
    .directive('backImg', function () {
        return function (scope, element, attrs) {
            element.css({
                'background-image': 'url(' + attrs.backImg + ')',
                'background-size': 'cover'
            });
        };
    })
    .directive('pwCheck', [function () {
        return {
            require: 'ngModel',
            link: function (scope, elem, attrs, ctrl) {
                var firstPassword = '#' + attrs.pwCheck;
                elem.add(firstPassword).on('keyup', function () {
                    scope.$apply(function () {
                        var v = elem.val() === $(firstPassword).val();
                        ctrl.$setValidity('pwmatch', v);
                    });
                });
            }
        };
    }])
    .directive('imageonload', ['$ionicLoading', function ($ionicLoading) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs, ctrl) {
                element.bind('load', function () {
                    $ionicLoading.hide();
                });
                element.bind('error', function () {
                    $ionicLoading.hide();
                });
            }
        };
    }])
    .directive('backBtn', function () {
        return {
            restrict: 'EA',
            replace: true,
            scope: {
                nav: '@navTo'
            },
            template: [
                '<button class="button button-icon icon " ng-click="goBack()">' +
                '<img src="img/icon/icon-arrow_left.png" style="max-width: 25px;max-height:25px;padding-top:7px;margin-bottom:7px;" alt=""/>' +
                '</button>'
            ].join(''),
            controller: function ($scope, $ionicHistory, $state) {

                $scope.goBack = function() {
                    if ($scope.nav) {
                        $ionicHistory.nextViewOptions({
                            disableBack: true
                        });
                        $state.go($scope.nav);
                    } else {
                        if ($ionicHistory.backView())
                            $ionicHistory.goBack();
                        else {
                            console.log('back view is null, go mainPage');
                            $state.go('tabsController.mainPage');
                        }
                    }
                };
            }
        };
    })
    .directive('rankChart', function () {
        return {
            restrict: 'A',
            link: function ($scope, element, attrs) {
                var myChart = echarts.init(element[0]);
                $scope.$watch(attrs.eData, function (newValue, oldValue, scope) {
                    var option = {
                        color: ['#3398DB'],
                        grid: {
                            left: '5%',
                            right: '4%',
                            top: '20',
                            bottom: '5',
                            containLabel: true
                        },
                        tooltip: {
                            trigger: 'axis',
                            axisPointer: {
                                type: 'shadow',
                                shadowStyle: {
                                    opacity: 0.5
                                }
                            },
                            confine: true
                        },
                        yAxis : [
                            {
                                type : 'category',
                                data : newValue.y,
                                axisLine: {
                                    show: false
                                },
                                splitLine: {show: false},
                                axisTick:false,
                                axisLabel: {
                                    align:'right',
                                    formatter: function (value, index) {
                                        if (value.length > 9) {
                                            return value.substr(0, 9);
                                        }
                                        return value;
                                    }

                                }
                            }
                        ],
                        xAxis : [
                            {
                                type : 'value',
                                show : false
                            }
                        ],
                        series : [
                            {
                                type:'bar',
                                smooth: true,
                                data: newValue.x,
                                label: {
                                    normal: {
                                        show: true,
                                        position: 'inside',
                                        textStyle: {
                                            color: '#fff',
                                            fontSize: 14
                                        },
                                        formatter: '{c}' + newValue.unit
                                    }

                                },
                                barWidth: 18,
                                barMinHeight: 50,
                                itemStyle: {
                                    emphasis: {
                                        barBorderRadius: 25
                                    },
                                    normal: {
                                        barBorderRadius: 25,
                                        color: new echarts.graphic.LinearGradient(
                                            0, 0, 1, 0,
                                            [
                                                {offset: 0, color: '#096fcf'},
                                                {offset: 1, color: '#47fbb9'}
                                            ]
                                        )
                                    }
                                }
                            }
                        ]
                    };
                    setTimeout(function () {
                        myChart.setOption(option);
                        myChart.resize();
                    }, 100);
                }, true);
            }
        };
    })
    .directive('pieChart', function ($state) {
        return {
            restrict: 'A',
            link: function ($scope, element, attrs) {
                var myChart = echarts.init(element[0]);
                var textStyle = {
                    color: 'white',
                    rich: {
                        hh: {
                            fontSize: 15
                        },
                        t1: {
                            fontSize: 20,
                            align: 'center',
                            lineHeight: 30,
                            fontWeight: 'bold'
                        },
                        t2: {
                            fontSize: 20,
                            align: 'center',
                            lineHeight: 30,
                            fontWeight: 'bold'
                        }
                    }
                };
                $scope.$watch(attrs.eData, function (newValue, oldValue, scope) {
                    var option = {
                        title: {
                            right: '15%',
                            top: 50,
                            text: newValue.title,
                            subtext: newValue.subtitle,
                            sublink: "javascript: var state = angular.element(document.querySelector('[ng-app]')).injector().get('$state');state.go('devicesOnline');",
                            subtarget: 'self',
                            textStyle: textStyle,
                            subtextStyle: textStyle
                        },
                        color: ["rgba(255,255,255,.2)", "#fff"],
                        series: [{
                            type: 'pie',
                            radius: ['40%', '70%'],
                            data: newValue.data,
                            avoidLabelOverlap: true,
                            label: {
                                normal: {
                                    show: false,
                                    position: 'center'
                                },
                                emphasis: {
                                    show: false
                                }
                            },
                            labelLine: {
                                normal: {
                                    show: false
                                }
                            }
                        }]
                    };
                    setTimeout(function () {
                        myChart.setOption(option);
                        myChart.resize();
                    }, 100);
                }, true);
            }
        };
    })
    .directive('terminalUseChart', function () {
        return {
            restrict: 'A',
            link: function ($scope, element, attrs) {
                var myChart = echarts.init(element[0]);
                $scope.$watch(attrs.eData, function (newValue, oldValue, scope) {
                    var option = {
                        grid: {
                            top: '40',
                            bottom: '50',
                            left: '40',
                            right: '30'
                        },
                        xAxis: {
                            type: 'category',
                            boundaryGap: false,
                            data: newValue.x
                        },
                        yAxis: {
                            type: 'value',
                            axisLabel: {
                                formatter: '{value}h'
                            }
                        },
                        series: [
                            {
                                type: 'line',
                                data: newValue.y
                            }
                        ]
                    };
                    myChart.setOption(option);
                }, true);
            }
        };
    })
    .directive('lineChart', function () {
        return {
            restrict: 'A',
            link: function ($scope, element, attrs) {
                var myChart = echarts.init(element[0]);
                $scope.$watch(attrs.eData, function (newValue, oldValue, scope) {
                    var option = {
                        title: {
                            top: 7,
                            left: 7,
                            text: '最近7天设备使用数',
                            textStyle: {
                                fontWeight: 'normal',
                                fontSize: 16
                            }
                        },
                        grid: {
                            top: '50',
                            bottom: '30'
                        },
                        xAxis: {
                            type: 'category',
                            boundaryGap: false,
                            data: newValue.date
                        },
                        yAxis: {
                            type: 'value',
                            boundaryGap: [0, '100%'],
                            minInterval: 1
                        },
                        series: [
                            {
                                type: 'line',
                                smooth: true,
                                symbol: 'none',
                                sampling: 'average',
                                itemStyle: {
                                    normal: {
                                        color: 'rgb(255, 70, 131)'
                                    }
                                },
                                areaStyle: {
                                    normal: {
                                        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                                            offset: 0,
                                            color: 'rgb(255, 158, 68)'
                                        }, {
                                            offset: 1,
                                            color: 'rgb(255, 70, 131)'
                                        }])
                                    }
                                },
                                markPoint: {
                                    symbolSize: [30, 20],
                                    data: [
                                        {type: 'max', name: '最大值'},
                                        {type: 'min', name: '最小值'}
                                    ]
                                },
                                data: newValue.data
                            }
                        ]
                    };
                    myChart.setOption(option);
                }, true);
            }
        };
    })
    .directive('itemEnd',function () {
        //用于将某些地方将元素放在容器的右边
        return {
            restrict: 'A',
            link:function (scope,element,attrs,ctrl) {
                element.css({
                    'float': 'right'
                });
            }
        };
    })
    .directive('emotionPicker',function(){
        return {
            restrict :'EA',
            scope:false,
            template:'<div style="height:195px;border-top: 1px solid #cfcece;width:100%;">'+
            '<div style="padding:10px; height:195px;width:100%;">'+
            '<ion-slides >'+
            '<ion-slide-page ng-repeat="item in items" style="height:195px;width:100%;">'+
            '<span ng-repeat="emotion in item"  ng-click="setValue(emotion)" style="display: block;float: left;width: 12.5%; height: 42px; font-size: 1.2em;line-height: 42px;text-align: center;margin-bottom: 10px;">'+
            '{{emotion}}'+
            '</span>'+
            '</ion-slide-page>'+
            '</ion-slides>'+
            '</div>'+
            '</div>',
            link: function (scope, element, attrs) {
                var EMOJIS =
                    "😀 😃 😄 😁 😆 😅 😂 🤣 😊 😇 🙂 😉 😌 😭 😍 😘 😗 😙 😚 😋 😜 😝 😛 🤑 🤗 🤓 😎 🤡 🤠 😏 😒 😞 😔 😟 😕 🙁 ☹️" +
                    " 😣 😖 😫 😩 😤 😠 😡 😶 😐 😑 😯 😦 😧 😮 😲 😵 😳 😱 😨 😰 😢 😥 🤤 😓 😪 😴 🙄 🤔 🤥 😬 🤐 🤢 🤧 😷 🤒 🤕 😈 👿 👹 👺" +
                    " 💩 👻 💀 ☠️ 👽 👾 🤖 🎃 😺 😸 😹 😻 😼 😽 🙀 😿 😾 👐 🙌 👏 🙏 🤝 👍 👎 👊 ✊ 🤛 🤜 🤞 ✌️ 🤘 👌 👈 👉 👆 👇 ☝️ ✋ 🤚 🖐 🖖" +
                    " 👋 🤙 💪 🖕 ✍️ 🤳 💅 🖖 💄 💋 👄 👅 👂 👃 👣 👁 👀 🗣 👤 👥 🕶 🌂 ☂️";
                var EmojiArr = EMOJIS.split(" ");
                var groupNum = Math.ceil(EmojiArr.length / 24);
                scope.items = [];

                for (var i = 0; i < groupNum; i++) {
                    scope.items.push(EmojiArr.slice(i * 24, (i + 1) * 24));
                }
            }
        };
    })

    .directive('popupKeyBoardShow', [function ($rootScope, $ionicPlatform, $timeout, $ionicHistory, $cordovaKeyboard) {
           return {
             link: function (scope, element, attributes) {
             window.addEventListener('native.keyboardshow', function (e) {
               angular.element(element).parent().parent().css({
                 'margin-top': '-' + 260 + 'px'   //这里80可以根据页面设计，自行修改
               });
           });
              window.addEventListener('native.keyboardhide', function (e) {
               angular.element(element).parent().parent().css({
                  'margin-top': 0
                });
              });
            }
        };
     }]);
    


/**
 * Created by hewz on 2017/5/16.
 */
angular.module('app.requester', ['ionic'])
    .factory('Requester', function ($http, Constant, $q, UserPreference, toaster,SavePhotoTool,$timeout) {
        var req = {};


        /**
         * 设置推送消息已读
         * @param noticeId
         * @returns {*}
         */
        req.setNoticeRead = function (noticeId) {
            return $http({
                method: "post",
                url: Constant.ServerUrl + "/notice/readSign",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: CustomParam({
                    noticeId: noticeId
                })
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };


        /**
         * 获取设备概况
         * @returns {*}
         */
        req.getGeneralInfo = function () {
            if(iOSDevice()){
                var defer = $q.defer();
                // var tool = $injector.get('SavePhotoTool');
                cordova.plugin.http.setDataSerializer('json');
                var options = {};
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/terminal/general",
                    options, header, function (response) {  
                        var data = JSON.parse(response.data);
                        try { 
                            $timeout(function () {
                                 defer.resolve(data);
                            });
                        } catch (error) {                
                            console.error("JSON parsing error");
                        }
                    }, function (error) {
                        if (error.status) {
                            SavePhotoTool.interceptors(error.status);
                        }
                        $timeout(function () {
                            defer.reject(error);
                        });
                    });            
                return defer.promise;
            }else{
                return $http.get(Constant.ServerUrl + "/terminal/general")
                .then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }      
        };

        /**
         * 查询最近设备使用数
         * @returns {*}
         */
        req.getUsedTerminalsRecently = function () {
            return $http.get(Constant.ServerUrl + "/terminal/terminalUsedCountDayly")
                .then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
        };


        /**
         * 获取地区设备分布
         * @param area
         * @returns {*}
         */
        req.getTerminalsLocation = function (area) {
            return $http.get(Constant.ServerUrl + "/terminal/info/distribution", {
                params: {
                    areaCode: area,
                    page: 1,
                    rows: 999
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 学校教师使用时长排行
         * @param area
         * @param lastDays
         * @returns {*}
         */
        req.getTeacherUseTimeRank = function (area, lastDays) {
            return $http.get(Constant.ServerUrl + "/terminal/rank/teacherAvgTime", {
                params: {
                    areaCode: area,
                    lastDays: lastDays,
                    page: 1,
                    rows: 50
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };


        /**
         * 学校设备使用时长排行
         * @param area
         * @param lastDays
         * @returns {*}
         */
        req.getTerminalUseTimeRank = function (area, lastDays) {
            return $http.get(Constant.ServerUrl + "/terminal/rank/schoolAvgTime", {
                params: {
                    areaCode: area,
                    lastDays: lastDays,
                    page: 1,
                    rows: 50
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };


        /**
         * 学校设备使用率排行
         * @param type
         * @param area
         * @param lastDays
         * @returns {*}
         */
        req.getTerminalUsageRank = function (type, area, lastDays) {
            return $http.get(Constant.ServerUrl + "/terminal/rank/schoolUseRate", {
                params: {
                    rankCondition: type,
                    areaCode: area,
                    lastDays: lastDays,
                    page: 1,
                    rows: 50
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };


        /**
         * 获取学校列表
         * @param county
         * @returns {*}
         */
        req.getSchools = function (county) {
            return $http.get(Constant.ServerUrl + "/school", {
                params: {
                    county: county,
                    page: 1,
                    rows: 999
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 获取省市区列表
         * @param code
         * @returns {*}
         */
        req.getArea = function (code) {
            return $http.get(Constant.ServerUrl + "/area/get", {
                params: {
                    code: code
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 教师使用统计
         * @param statCondition
         * @returns {*}
         */
        req.getTeacherUseRank = function (statCondition) {
            return $http.get(Constant.ServerUrl + "/terminal/teacherTimeRank", {
                params: {
                    statCondition: statCondition
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 班级使用统计
         * @param statCondition
         * @returns {*}
         */
        req.getClassUseRank = function (statCondition) {
            return $http.get(Constant.ServerUrl + "/terminal/classTimeRank", {
                params: {
                    statCondition: statCondition
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 设备使用明细-查看设备列表
         * @param page
         * @param schoolId
         * @returns {*}
         */
        req.getTerminalList = function (page, schoolId) {
            return $http.get(Constant.ServerUrl + "/terminal/UseDetail/terminal/info", {
                params: {
                    page: page,
                    row: Constant.reqLimit,
                    schoolId: schoolId
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 设备使用明细-查看设备使用时长
         * @param days
         * @param sn
         * @returns {*}
         */
        req.getTerminalUseTime = function (days, sn) {
            return $http.get(Constant.ServerUrl + "/terminal/UseDetail/times-every-day", {
                params: {
                    days: days,
                    sn: sn
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 设备使用明细-查看设备截图
         * @param page
         * @param day
         * @param sn
         * @returns {*}
         */
        req.getTerminalScreenShot = function (page, day, sn) {
            return $http.get(Constant.ServerUrl + "/terminal/UseDetail/snapshot", {
                params: {
                    day: day,
                    sn: sn,
                    page: page,
                    row: Constant.reqLimit
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return error.data;
            });
        };

        /**
         * 获取在线设备列表
         * @returns {*}
         */
        req.getOnlineTerminalList = function () {
            return $http.get(Constant.ServerUrl + "/terminal/?state=RUNNING" + '&i=' + new Date()).then(function (response) {
                return response.data;
            }, function (error) {
                return error.data;
            });
        };

        /**
         * 将指定设备关机
         * @param tid
         * @returns {*}
         */
        req.turnOffTerminals = function (tid) {
            return $http({
                method: "post",
                url: Constant.ServerUrl + "/terminal/shutdown",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: CustomParam({
                    terminalIds: tid
                })
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };
        /**
         * 获取消息通知未读数
         * @param newsId
         */
        req.getNoticeNotReadNumber = function () {
            if(iOSDevice()){
                var defer = $q.defer();
                // var tool = $injector.get('SavePhotoTool');
                cordova.plugin.http.setDataSerializer('json');
                var options = {};
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/notice/unreadTotal/byAppAdmin",
                    options, header, function (response) { 
                        var data = JSON.parse(response.data); 
                        try { 
                            $timeout(function () {
                                 defer.resolve(data);
                            });
                        } catch (error) {                
                            console.error("JSON parsing error");
                        }
                    }, function (error) {
                        console.log("====error=====");
                        console.log(error);
                        if (error.status) {
                            SavePhotoTool.interceptors(error.status);
                        }
                        $timeout(function () {
                            defer.reject(error);
                        });
                    });            
                return defer.promise;
            }else{
                return $http.get(Constant.ServerUrl + "/notice/unreadTotal/byAppAdmin").then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 获取校园风采未读
         * @returns {*}
         */
        req.getCampusUnread = function () {
            var params = {};
            if (String(UserPreference.getObject('user').rolename) === Constant.USER_ROLES.PARENT) {
                params.stuId = UserPreference.get('DefaultChildID');
            }
            if(iOSDevice()){
                var defer = $q.defer();
                // var tool = $injector.get('SavePhotoTool');
                cordova.plugin.http.setDataSerializer('json');
                var options = params;
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/campusview/unreadTotalByList",
                    options, header, function (response) {  
                        var data = JSON.parse(response.data);
                        try { 
                            $timeout(function () {
                                 defer.resolve(data);
                            });
                        } catch (error) {       
                            console.error("JSON parsing error");
                        }
                    }, function (error) {
                        if (error.status) {
                            SavePhotoTool.interceptors(error.status);
                        }
                        $timeout(function () {
                            defer.reject(error);
                        });
                    });
               
                return defer.promise;

            }else{
                return $http.get(Constant.ServerUrl + "/campusview/unreadTotalByList", {
                    params: params
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 校园风采添加评论
         * @param newsId
         * @param comment
         * @returns {*}
         */
        req.addCampusComment = function (newsId, comment) {
            return $http.post(Constant.ServerUrl + "/campus/add-comments", {
                newsId: newsId,
                comments: comment
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 校园风采删除评论
         * @param commentId
         * @returns {*}
         */
        req.deleteCampusComment = function (commentId) {
            return $http.get(Constant.ServerUrl + "/campus/deleteComments", {
                params: {
                    id: commentId
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 校园风采点赞
         */
        req.favorCampus = function (newsId) {
            return $http.post(Constant.ServerUrl + "/campus/add-praise", {
                newsId: newsId
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 校园风采取消点赞
         */
        req.unfavorCampus = function (newsId) {
            return $http.get(Constant.ServerUrl + "/campus/deletePraise", {
                params: {
                    id: newsId
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 校园风采获取点赞列表
         * @param newsId
         */
        req.getCampusNewsFavorList = function (newsId) {
            return $http.get(Constant.ServerUrl + "/campus/getPraiseList", {
                params: {
                    newsId: newsId
                }

            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };


        /**
         * 校园风采获取评论列表
         * @param newsId
         * @param page
         * @returns {*}
         */
        req.getCampusNewsCommentList = function (newsId, page) {
            var params = {
                newsId: newsId,
                page: page,
                rows: Constant.reqLimit
            };
            return $http.get(Constant.ServerUrl + "/campus/getCommentsList", {
                params: params
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * by wl
         * 发表用户反馈
         * content 发表的内容  若为图片类型 则为base 64字符串形式
         * contentType : 消息类型
         */
        req.publishUserFeedback = function (content, contentType) {
            var phoneSy = ionic.Platform.isIOS() ? 'iOS' : 'android';
            return $http.post(Constant.ServerUrl + "/suggestions/public", {
                appVersion: Constant.version,
                content: content,
                phoneOs: phoneSy,
                contentType: contentType
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 标记消息为已读 用户反馈
         * newsId 消息Id
         */
        req.fixNewsReadStatus = function (newsId) {
            return $http.post(Constant.ServerUrl + "/suggestions/signRead", {
                id: newsId
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 获取用户反馈消息列表
         */
        req.getFeedbackDetailList = function () {
            return $http.get(Constant.ServerUrl + "/suggestions/detail", {
                params: {
                    endDate: '',
                    startDate: ''
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 获取用户反馈 未读数
         */
        req.getNoreadNewsNumber = function () {
            return $http.get(Constant.ServerUrl + "/suggestions/getUnreadReplySize", {
                params: {}

            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 班级评比任务列表
         * @param endDate 结束时间
         * @param startDate 开始时间
         * @param page 页面
         */
        req.getClassVoteTasktList = function (page, startDate, endDate) {
            return $http.get(Constant.ServerUrl + "/classvote/list", {
                params: {
                    page: page,
                    rows: Constant.reqLimit,
                    startDate: startDate,
                    endDate: endDate
                }

            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };
        /**
         * 班级评比任务列表 new(1.1.6)
         * @param endDate 结束时间
         * @param startDate 开始时间
         * @param page 页面
         */
        req.getClassVoteTasktList2 = function (page, startDate, endDate) {
            return $http.get(Constant.ServerUrl + "/classvote/list/byTerm", {
                params: {
                    page: page,
                    rows: Constant.reqLimit,
                    startDate: startDate,
                    endDate: endDate
                }

            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };
        /**
         * 添加班级评比任务
         * @param title 任务标题
         * @param startDate 开始时间
         * @param endDate 结束时间
         */
        req.addClassVoteTask = function (params) {
            return $http.post(Constant.ServerUrl + "/classvote/add", params)
                .then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
        };

        /**
         * 删除班级评比任务
         * @param classvoteId 评比任务id
         */
        req.deleteClassVoteTask = function (classvoteId) {
            return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/classvote/delete",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: CustomParam({
                        id: classvoteId
                    })
                })
                .then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
        };

        /**
         * 获取当前学期班级评比任务总数
         *
         */
        req.getCurrentSemesterVoteTaskCount = function () {
            if(iOSDevice()){
                var defer = $q.defer();
                cordova.plugin.http.setDataSerializer('json');
                var options = {};
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/classvote/countByTerm",
                    options, header, function (response) { 
                        var data = JSON.parse(response.data); 
                        try { 
                            $timeout(function () {
                                 defer.resolve(data);
                            });
                        } catch (error) {
                            
                            console.error("JSON parsing error");
                        }
                    }, function (error) {
                        if (error.status) {
                            SavePhotoTool.interceptors(error.status);
                        }
                        $timeout(function () {
                            defer.reject(error);
                        });
                    });
               
                return defer.promise;
            }else{
                console.log('cookies ;;;');
                return $http.get(Constant.ServerUrl + "/classvote/countByTerm", {
                    params: {
    
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
          
        };

        /**
         * 获取年级列表
         */
        req.getGradeList = function () {
            return $http.get(Constant.ServerUrl + "/classvote/grade/list", {
                params: {

                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };


        /**
         * 班级评比结果列表
         * @param classvoteId 评比任务id
         * @param gradeId 年级id 为空时默认选择全部年级
         */
        req.getClassVoteReultList = function (classvoteId, gradeName, page) {
            return $http.get(Constant.ServerUrl + "/classvote/detail/" + classvoteId, {
                params: {
                    gradeName: gradeName,
                    page: page,
                    rows: 20
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };


        /**
         * 添加评分结果
         * @param classId 班级id
         * @param classvoteDetailId 评比详情id
         * @param classvoteItem 评比维度 数组[{id: '评比维度id',score:'分数'}]
         */
        req.addClassScoreResult = function (classId, classvoteDetailId, classvoteItem) {
            return $http({
                method: 'post',
                url: Constant.ServerUrl + "/classvote/result/add",
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    classId: classId,
                    classvoteDetailId: classvoteDetailId,
                    classvoteItem: classvoteItem
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 修改班级评分结果
         *  @param classId 班级id
         *  @param classvoteDetailId 评比详情id
         *   @param classvoteItem 评比维度 数组[{id: '评比维度id',score:'分数'}]
         */
        req.fixClassScoreResult = function (classId, classvoteDetailId, classvoteItem) {
            return $http({
                method: 'post',
                url: Constant.ServerUrl + "/classvote/result/alter",
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    classId: classId,
                    classvoteDetailId: classvoteDetailId,
                    classvoteItem: classvoteItem
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 查询评比维度
         */
        req.selectVoteItem = function () {
            return $http.get(Constant.ServerUrl + "/classvote/item/list").then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 版本说明
         * @param appType 版本类型 0用户app 1管理app
         * @param version 版本号
         */
        req.getCurrentVersionIntroduce = function () {
            var os = ionic.Platform.isIOS() ? 'ios' : 'android';
            return $http.get(Constant.ServerUrl + "/checkversion/describe", {
                params: {
                    appType: 1,
                    version: Constant.version,
                    osType: os
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };


        /**
         * 报修记录列表
         * @param condition 0,全部；1，待解决；2，已关闭
         * @param isManage true管理列表；false,我的列表
         * @param page 当前页面
         */

        req.getRepairRecordList = function (condition, page) {
            return $http.get(Constant.ServerUrl + "/maintainOrders", {
                params: {
                    condition: condition,
                    isManage: true,
                    row: Constant.reqLimit,
                    page: page
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 报修记录详情
         * @param repairRecordId  报修记录id
         */
        req.getRepairRecordDetail = function (repairRecordId) {
            return $http.get(Constant.ServerUrl + "/maintainOrders/" + repairRecordId, {
                params: {

                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 添加进度处理
         * @param repairRecordId 报修记录id
         * @param orderProgress 1:已提交;2:管理员已查看;4:正在处理;2^30:已关闭;maxinteger:已取消
         * @param progressDescribe 进度描述
         * @param manufacturersContactId 厂家id
         */

        req.addRepairProgress = function (repairRecordId, orderProgress, progressDescribe, manufacturersContactId) {
            return $http({
                method: 'post',
                url: Constant.ServerUrl + "/maintainOrders/" + repairRecordId + "/progress",
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    orderProgress: orderProgress,
                    progressDescribe: progressDescribe,
                    manufacturersContactId: manufacturersContactId
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 获取厂家列表
         */
        req.getManufacturesList = function () {
            return $http.get(Constant.ServerUrl + "/manufacturers/getList", {

            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 获取售后人员列表
         * @param factoryId
         */
        req.getManufacturesMermberList = function (factoryId) {
            return $http.get(Constant.ServerUrl + "/manufacturers/getContactByManufacturerId/" + factoryId, {

            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 添加报修记录
         * @param maintainImgs 故障图片 array
         * @param orderDescribe 故障描述
         * @param terminalAddress	设备所在地址
         */
        req.addEquipmentRepairRecord = function (params) {
            return $http({
                method: 'post',
                url: Constant.ServerUrl + "/maintainOrders",
                headers: {
                    'Content-Type': 'application/json'
                },
                data: params
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 二维码 地址打开
         */
        req.getAssetsIdByScan = function (url) {
            return $http({
                method: 'get',
                url: url,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 添加资产申领进度（管理端）
         * @param orderId 资产申领单id
         * @param orderProgress 进度
         * @param progressDescribe 原因
         */
        req.addAssetsApplyProgress = function (orderId, orderProgress, fixApplyCount, progressDescribe) {
            return $http({
                method: 'post',
                url: Constant.ServerUrl + "/asset/applyProgress",
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    orderId: orderId,
                    orderProgress: orderProgress,
                    param1: fixApplyCount,
                    progressDescribe: progressDescribe
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 资产申领详情
         * @param id 资产id
         */
        req.assetsApplyDetail = function (id) {
            return $http.get(Constant.ServerUrl + "/assets/" + id, {
                params: {

                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 资产审批列表 （管理端）
         */
        req.manageAssetsApplyList = function (page) {
            return $http.get(Constant.ServerUrl + "/manage/assets", {
                params: {
                    row: Constant.reqLimit,
                    page: page
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 获取资产未读数
         *
         */
        req.getAssetsUnread = function () {
            if(iOSDevice()){
                var defer = $q.defer();
                cordova.plugin.http.setDataSerializer('json');
                var options = {};
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/manage/assets/getUnreadSize",
                    options, header, function (response) {  
                        var data = JSON.parse(response.data);
                        try { 
                            $timeout(function () {
                                 defer.resolve(data);
                            });
                        } catch (error) {
                            console.error("JSON parsing error");
                        }
                    }, function (error) {
                        if (error.status) {
                            SavePhotoTool.interceptors(error.status);
                        }
                        $timeout(function () {
                            defer.reject(error);
                        });
                    });
               
                return defer.promise;
            }else{
                return $http.get(Constant.ServerUrl + "/manage/assets/getUnreadSize", {
                    params: {
    
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };
        /**
         * 查询资产申领的id
         * @param assetsId 领单id
         */
        req.queryAssetsApplyIds = function (assetsId) {
            return $http.get(Constant.ServerUrl + "/asset/getApplyIssue", {
                params: {
                    id: assetsId
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 获取授权模块列表
         * @returns {*}
         */
        req.getToolkitList = function () {
            return $http.get(Constant.ServerUrl + "/toolkit/list").then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        req.requestToolkit = function (phone, appId) {
            return $http({
                method: 'post',
                url: Constant.ServerUrl + "/toolkit/apply",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: CustomParam({
                    phone: phone,
                    appId: appId
                })
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };
        /** 获取审批管理列表
         * @param schoolId 学校Id
         * @param progress 状态: 1通过 2审批中 3不通过 全部是传空
         */
        req.getApprovalManageList = function (page, schoolId, progress) {
            return $http.get(Constant.ServerUrl + "/archive/admin/list", {
                params: {
                    rows: Constant.reqLimit,
                    page: page,
                    progress: progress,
                    schoolId: schoolId
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };



        /**
         * 获取申请的详细信息
         * @param archiveId 申请的Id
         */
        req.getApprovalManageDetail = function (archiveId) {
            return $http.get(Constant.ServerUrl + "/archive/admin/detail", {
                params: {
                    archiveId: archiveId
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 检测是否过期 fasle为未过期 true过期
         */
        req.checkAuthOrAccountIsOverdue = function () {
            if(iOSDevice()){
                var defer = $q.defer();
                cordova.plugin.http.setDataSerializer('json');
                var options = {};
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/checkAuthExpireNew",
                    options, header, function (response) {  
                        var data = JSON.parse(response.data);
                        try { 
                            $timeout(function () {
                                 defer.resolve(data);
                            });
                        } catch (error) {
                            
                            console.error("JSON parsing error");
                        }
                    }, function (error) {
                        console.log('=====error2=====');
                        if (error.status) {
                            SavePhotoTool.interceptors(error.status);
                        }
                        $timeout(function () {
                            defer.reject(error);
                        });
                    });
               
                return defer.promise;
            }else{
                return $http.get(Constant.ServerUrl + "/checkAuthExpireNew", {
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };



        //应急演练
        /**
         * 演练记录列表
         * @param 
         */
        req.getDrillRecordList = function (page) {
            return $http.get(Constant.ServerUrl + "/drillRecords/list/byDrillDefine", {
                params: {
                    page: page,
                    rows: 10
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 演练详情 -查看详情
         * @param itemId
         */
        req.getDrillDetail = function (itemId) {
            return $http.get(Constant.ServerUrl + "/drillRecords/detail", {
                params: {
                    id: itemId
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 新增记录
         * @param tempDrillDefineId 演练记录id
         * @param remarks 演练记录说明
         * @param strImageUrls 图片数组
         */

        req.addDrillRecord = function (drillId, selected) {
            return $http({
                method: 'post',
                url: Constant.ServerUrl + "/drillRecords/save/byDrillRecords",
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    tempDrillDefineId: drillId,
                    remarks: selected.remarks,
                    picdatas: selected.picdatas,
                    strImageUrls: ''
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /***
         * 演练记录详情
         * @param drillDefineId 演练id
         * @param page  分页
         */

        req.getDrillRedordDetail = function (drillDefineId, page) {
            return $http.get(Constant.ServerUrl + "/drillRecords/list/byDrilRecords", {
                params: {
                    drillDefineId: drillDefineId,
                    page: page,
                    rows: 5,
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 查看演练方案
         * 
         */
        req.getDrillProgramList = function () {
            return $http.get(Constant.ServerUrl + "/drillRecords/drillPlanList", {
                params: {

                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 查看方案详情
         * @param id 方案id
         */
        req.getDrillProgramDetail = function (programId) {
            return $http.get(Constant.ServerUrl + "/drillRecords/drillPlan/detail", {
                params: {
                    id: programId
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 发起演练
         * @param tempDrillPlanId 演练方案编号
         * @param drillTime 演练时间
         * @param address 演练地址
         * @param remarks 演练说明 
         */
        req.sendDrillRecord = function (tempDrillPlanId, selected) {
            return $http({
                method: 'post',
                url: Constant.ServerUrl + "/drillRecords/save/byDrillDefine",
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    tempDrillPlanId: tempDrillPlanId,
                    drillTime: selected.exerciseTime,
                    address: selected.address,
                    remarks: selected.content,
                    title: selected.title
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 修改演练时间（开始时间和结束时间）
         * @param drillDefineId 演练id
         * @param drillStartTime 开始时间
         * @param  drillEndTime 结束时间
         */
        req.fixedDrillStartDateOrEndDate = function (drillDefineId, drillStartTime, drillEndTime) {
            return $http.get(Constant.ServerUrl + "/drillRecords/saveDrillTime", {
                params: {
                    drillDefineId: drillDefineId,
                    drillStartTime: drillStartTime,
                    drillEndTime: drillEndTime
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        //隐患管理
        /**
         * 获取 隐患列表
         * @param status 状态
         */
        req.getHidenTroubleList = function (status, schoolId, page) {
            return $http.get(Constant.ServerUrl + "/teacher/hidden-danger/admin/list", {
                params: {
                    status: status,
                    schoolId: schoolId,
                    page: page,
                    rows: 10
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 获取隐患详情
         */
        req.getHidenTroubleDetail = function (hiddenDangerId) {
            return $http.get(Constant.ServerUrl + "/teacher/hidden-danger/detail", {
                params: {
                    hiddenDangerId: hiddenDangerId
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 获取学校老师列表
         * @param schoolId 学校id
         */

        req.getSchoolTeachersList = function (schoolId) {
            return $http.get(Constant.ServerUrl + "/teacher/hidden-danger/get-teachers", {
                params: {
                    schoolId: schoolId,
                    teacherName: ''
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 添加隐患
         * @param location 隐患位置
         * @param title 隐患标题
         * @param message 隐患信息
         * @param imagesBase64 图片数组
         */
        req.addHidenTrouble = function (selected, schoolId) {
            return $http({
                method: 'post',
                url: Constant.ServerUrl + "/teacher/hidden-danger/admin/upload/" + selected.selectManId,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    location: selected.location,
                    title: selected.title,
                    message: selected.message,
                    imagesBase64: selected.picdatas,
                    schoolId: schoolId
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 增加隐患处理进度
         * @param assignedPersonId 指派人
         * @param description //描述
         * @param hiddenDangerId //隐患id
         * @param status 状态
         */
        req.addHidenTroubleProgress = function (selected, itemId) {
            return $http({
                method: 'post',
                url: Constant.ServerUrl + "/teacher/hidden-danger/add-progress/" + selected.status,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    assignedPersonId: selected.selectManId,
                    description: selected.content,
                    hiddenDangerId: itemId
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };
        /**
         * 获取隐患数量
         */
        req.getHidenTroubleCount = function (schoolId) {
            return $http.get(Constant.ServerUrl + "/teacher/hidden-danger/admin/list/count", {
                params: {
                    schoolId: schoolId
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };


        //资产概况
        /**
         * 
         */
        req.getSchoolAssetsDetail = function () {
            return $http.get(Constant.ServerUrl + "/asset/stat/general", {
                params: {

                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 学校资产排行
         */
        req.getSchoolAssetsRank = function () {
            return $http.get(Constant.ServerUrl + "/asset/stat/getRankBySchl", {
                params: {

                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 厂家资产排行
         */
        req.getFactoryAssetsRank = function () {
            return $http.get(Constant.ServerUrl + "/asset/stat/getRankByManufacturer", {
                params: {

                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };


        //查询疫情 今日概况
        req.getCovidSummary = function () {
            return $http.get(Constant.ServerUrl + "/epidemic/record/count/class_level", {
                params: {}
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        //查询疫情 体温异常名单
        req.getWarningList = function () {
            return $http.get(Constant.ServerUrl + "/epidemic/alarm/list", {
                params: {
                    page: 1,
                    rows: 9999
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };


        //查询疫情 告警详情
        req.getWarningDetail = function (alarmId) {
            return $http.get(Constant.ServerUrl + "/epidemic/alarm/details", {
                params: {
                    alarmId: alarmId
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 疫情跟进
         */
        req.handleWarning = function (alarmId, status, description) {
            return $http({
                method: 'post',
                url: Constant.ServerUrl + "/epidemic/alarm/save/progress",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: CustomParam({
                    alarmId: alarmId,
                    status: status,
                    description: description

                })
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 设置疫情告警已读
         */
        req.covidWarningRead = function () {
            return $http.get(Constant.ServerUrl + "/epidemicAlarm/readSign", {
                params: {}
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 获取学校所有班级列表
         */
        req.getSchoolAllClasses = function (schoolId) {
            var url = Constant.ServerUrl.substr(0, Constant.ServerUrl.length - 3) + 'api';
            return $http.get(url + "/class", {
                params: {
                    schoolId:schoolId
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };
         /**
         * 获取班级同学人像列表
         * @param classId 班级id
         * @param name 学生姓名
         */
        req.getFaceRecognitionClassList = function (classId, name) {
            return $http.get(Constant.ServerUrl + "/faceRecognition/getOfClass", {
                params: {
                    classId: classId,
                    name: name
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };
        /**
         * 获取老师的个人人像
         * @param userId
         */
        req.getTeacherFaceRecognition = function (userId) {
            return $http.get(Constant.ServerUrl + "/faceRecognition/getOne", {
                params: {
                    userId: userId,
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 获取学校老师人像采集接口列表
         */
        req.getTeacherPortraitList = function(schoolId,page) {
            return $http.get(Constant.ServerUrl + "/faceRecognition/getRecognitonTeacherList", {
                params: {
                    teacherName: '',
                    page:1,
                    rows:500
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            }); 
        };

        /**
         * 上传人像
         */
        req.uploadUserPortrait = function (userId, base64Str) {
            return $http({
                method: 'post',
                url: Constant.ServerUrl + "/faceRecognition/uploadImg",
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    userId: userId,
                    imgData: base64Str
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 获取
         */


        return req;
    });

/**
 * Created by hewz on 2018/4/2.
 */
angular.module('ionic-image-view', [])
    .provider('ionicImageView', function () {

        var config = {
            allowSave: true,
            album: 'Xgenban'
        };

        this.config = function (options) {
            angular.extend(config, options);
        };

        this.$get = ['$rootScope', '$ionicModal', '$ionicLoading', 'scrollDelegateFix', 'Constant', function ($rootScope, $ionicModal, $ionicLoading, scrollDelegateFix, Constant) {

            var provider = {};
            var $scope = $rootScope.$new();

            provider.showViewModal = function (options, urls, defaultIndex) {
                $scope.config = angular.extend({}, config, options);
                if (!urls && urls.length === 0) {
                    return;
                }
                if (!defaultIndex || defaultIndex >= urls.length || defaultIndex < 0)
                    $scope.index = 0;
                else
                    $scope.index = defaultIndex;
                $scope.image = urls[$scope.index];

                function applyImage() {
                    $scope.image = urls[$scope.index];
                }

                $scope.onSwipeLeft = function () {
                    if (!$scope.disableSwipe && $scope.index < urls.length - 1) {
                        $scope.index++;
                        applyImage();
                    }
                    else if ($scope.index === urls.length - 1)
                        $scope.closeImageModal();
                };

                $scope.onSwipeRight = function () {
                    if (!$scope.disableSwipe && $scope.index > 0) {
                        $scope.index--;
                        applyImage();
                    } else if ($scope.index === 0)
                        $scope.closeImageModal();
                };

                $scope.onRelease = function() {
                    var scrollDelegate = scrollDelegateFix.$getByHandle('Handle');
                    var zoom = scrollDelegate.getScrollPosition().zoom;
                    $scope.disableSwipe = zoom !== 1;
                };

                var UIPath = '';
                if (Constant.debugMode) UIPath = 'page/';
                $ionicModal.fromTemplateUrl(UIPath + 'common/ionic-image-view.html', {
                    scope: $scope,
                    animation: 'none'
                }).then(function (modal) {
                    $scope.modal = modal;
                    $scope.modal.show();
                });

                $scope.closeImageModal = function () {
                    $scope.modal.hide();
                    $scope.modal.remove();
                };

                function save(url) {
                    cordova.plugins.photoLibrary.saveImage(url + '?ext=.jpg', $scope.config.album, function (libraryItem) {
                        $ionicLoading.show({template: '保存成功', duration: 1500});
                    }, function (err) {
                        console.log(err);
                    });
                }
                $scope.saveImg2Gallery = function (url) {
                    cordova.plugins.photoLibrary.getAlbums(
                        function (result) {
                            save(url);
                        },
                        function (err) {
                            if (err.startsWith('Permission')) {
                                cordova.plugins.photoLibrary.requestAuthorization(
                                    function () {
                                        save(url);
                                    }, function (err) {
                                        // User denied the access
                                        console.log(err);
                                    }, {
                                        read: true,
                                        write: true
                                    }
                                );
                            }
                        }
                    );
                };
            };

            return provider;

        }];

    });
/**
 * Created by hewz on 2018/4/2.
 */
angular.module('app.controllers')
    .controller('pdfViewCtrl', function ($scope, $http, $stateParams, $ionicHistory) {

        $scope.$on("$ionicView.enter", function (event, data) {
            $scope.url = $stateParams.url;
            if ($scope.url) {
                $http.get($scope.url, {
                    responseType: 'arraybuffer'
                }).then(function (response) {
                    $scope.pdfdata = new Uint8Array(response.data);
                });
            } else
                $ionicHistory.goBack();
        });

    });
angular.module('app.controllers')
.controller('abnormalHandleCtrl',function($scope,$state,$stateParams){
    $scope.isIos = ionic.Platform.isIOS();
    $scope.news = $scope.isIos? $stateParams.notice.extras: $stateParams.notice;
    
    $scope.noticeContent = $scope.news.text;
  
});
angular.module('app.controllers')
    .controller('tabsCtrl', function ($scope, BroadCast, Constant, UserPreference, ChatService) {
        $scope.badge = 0;

        $scope.$on(BroadCast.BADGE_UPDATE, function (event, data) {
            if (data.type === 'im') {
                $scope.badge = data.count;
                $scope.$digest();
            } else {
                $scope.campus = data.count;
            }
        });


        var user = UserPreference.getObject('user');
        $scope.userRole = user.role;
        if (String($scope.userRole) === Constant.USER_ROLES.SCHOOL_ADMIN) {
            ChatService.init();
            $scope.schoolAdmin = true;
        } else {
            $scope.authAssetsManage = '';
        }

        $scope.authDeviceManage = user.authDeviceManage && user.authDeviceManage === 1; //设备授权
        $scope.authAssertManage = user.authAssertManage && user.authAssertManage === 1; //资产授权

        $scope.isCityManage = false;
        // $scope.$on("$ionicView.beforeEnter", function (event, data) {

        // });


    });
angular.module('app.controllers')
.controller('loginCtrl', function ($scope, $state, $ionicLoading, AuthorizeService, BroadCast, UserPreference, toaster, MESSAGES) {

    $scope.$on("$ionicView.enter", function (event, data) {
        $scope.loginModel = {
            remember: true,
            username: UserPreference.get('username', ''),
            password: UserPreference.get('password', '')
        };
        if ($scope.loginModel.username !== '' && $scope.loginModel.password !== '') {
            AuthorizeService.login($scope.loginModel);
           $state.go('tabsController.mainPage');
        }
    });

    $scope.isIOS = ionic.Platform.isIOS();

    $scope.login = function () {
        if (this.loginForm.$valid) {
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
            console.log(' manage login');
          

           // var rst = AuthorizeService.login($scope.loginModel);
            
           // if (rst) {
               if(iOSDevice()){
                AuthorizeService.login($scope.loginModel,function(rst){
                    $ionicLoading.hide();
                    var data =   rst;//iOSDevice()?JSON.parse(rst):rst;
                        if (data.result) {
                            UserPreference.set("username", $scope.loginModel.username);
                            if ($scope.loginModel.remember)
                                UserPreference.set("password", $scope.loginModel.password);
                            $state.go('tabsController.mainPage');
                        }
                        else{
                            $ionicLoading.hide();
                            toaster.error({title: MESSAGES.LOGIN_ERROR, body: data.message,timeout:5000});
                        }
                },function(error){
                    $ionicLoading.hide();
                });
               }else{
                AuthorizeService.login($scope.loginModel).then(function (rst) {
                    $ionicLoading.hide();
                    var data =   rst;//iOSDevice()?JSON.parse(rst):rst;
                        if (data.result) {
                            UserPreference.set("username", $scope.loginModel.username);
                            if ($scope.loginModel.remember)
                                UserPreference.set("password", $scope.loginModel.password);
                            $state.go('tabsController.mainPage');
                        }
                        else{
                            $ionicLoading.hide();
                            toaster.error({title: MESSAGES.LOGIN_ERROR, body: data.message,timeout:5000});
                        }
                           
                    }).finally(function () {
                        // console.log('request end');
                        $ionicLoading.hide();
                    });
               }
               
               
             
        } else {
            if (this.loginForm.input_user.$error.required) {
                toaster.warning({title: MESSAGES.LOGIN_ERROR, body: MESSAGES.LOGIN_NO_USERNAME});
            } else if (this.loginForm.input_user.$error.pattern) {
                toaster.warning({title: MESSAGES.LOGIN_ERROR, body: MESSAGES.LOGIN_USERNAME_PATTERN});
            } else if (this.loginForm.input_pwd.$error.required) {
                toaster.warning({title: MESSAGES.LOGIN_ERROR, body: MESSAGES.LOGIN_NO_PASSWORD});
            } else if (this.loginForm.input_pwd.$error.pattern) {
                toaster.warning({title: MESSAGES.LOGIN_ERROR, body: MESSAGES.LOGIN_PASSWORD_PATTERN});
            }
            else
                toaster.error({title: MESSAGES.LOGIN_ERROR, body: 'Unknown Error'});
         }
    };


    $scope.hideKeyBoard = function(){
       if(window.cordova){
        //cordova.plugins.Keyboard.close();
        //Keyboard.hide();
       }
    };

    // $scope.test = function(){
    //  $state.go('abnormalHandle',{notice:{extras:{createTime:'2018-07-17 10:47',text:'哈哈哈nishi',title:'标题'}}});
    // };
});
angular.module('app.controllers')
    .controller('mainPageCtrl', function ($q, $scope, $ionicModal, $sce, $state, CameraHelper, BroadCast, NoticeService, Constant, UserPreference, toaster, MESSAGES, $ionicLoading, $ionicPopup, $ionicListDelegate, Requester, $rootScope, ionicImageView) {
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'page/';
        $ionicModal.fromTemplateUrl(UIPath + 'home/mainpage/new_notice.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
        });
        $scope.openModal = function () {
            $scope.modal.show();
        };
        $scope.closeModal = function () {
            $scope.modal.hide();
        };
        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function () {
            $scope.modal.remove();
        });

        $scope.rmStyle = function (str) {
            return $sce.trustAsHtml(str);
        };


        $scope.goDetail = function (notice) {
            if (notice) {
                $state.go('news_detail', {
                    notice: angular.copy(notice),
                    index: notice.id
                });
                //消息标记为已读
                Requester.setNoticeRead(notice.id).then(function (res) {

                }, function () {

                });
            }
        };
        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            console.log('before');
           
            $scope.noReadNum = 0; //未读数
            $scope.totalNum = 0; //总条数
            // Requester.getNoticeNotReadNumber().then(function (res) {
            //     console.log('=======00000======');
            //     if (res.result) {
            //         res.data.forEach(function (notice) {
            //             if (notice.key == '11') {
            //                 $scope.noReadNum = notice.value;
            //             } else if (notice.key == '99992') {
            //                 $scope.totalNum = notice.value;
            //             }
            //         });

            //     }
            // });


        });

        $scope.data = {
            data: [1, 0],
            title: '',
            subtitle: ''
        };

        $scope.$on("$ionicView.enter", function (event, data) {
            var user = UserPreference.getObject('user');
            console.log('=====user====');
            console.log(user);
            if (String(user.role) === Constant.USER_ROLES.SCHOOL_ADMIN) {
                console.log('--check auth');
               // $scope.checkAuthOrAccountIsOverdue();

            }



            $scope.schoolName = UserPreference.get('DefaultSchoolName');
            $scope.noticeType = '11';
            $scope.userId = user.id;
            $scope.page = 1;
            $scope.noticeList = UserPreference.getArray('notice_list_cache' + $scope.noticeType);


            $scope.authBanBanTong = user.AUTH_TERMINAL_MANAGE === 1;
            $scope.authOA = user.AUTH_SCHOOL_OA === 1;
            $scope.authTeachingManage = user.AUTH_EDUCATION_MANAGE === 1;
            $scope.authAssetsManage = user.AUTH_ASSET_MANAGE === 1;
            $scope.authBanPai = user.AUTH_ADTERMINAL_MANAGE === 1;
            $scope.safeManage = user.AUTH_SAFETY_MANAGE === 1; //安全管理
            $scope.babyVideoManage = user.AUTH_KINDERGARTEN_STATUS ===1;//宝宝视频权限
            $scope.covidProvent = user.AUTH_PROTECT_VIRUS ===1; //疫情防控权限
            $scope.listHasMore = true;
           // $scope.dataInit();
            NoticeService.getNoticeList($scope.page, $scope.noticeType);

            // if ($scope.authBanBanTong)
            //     $scope.getDeviceInfo();

            // Requester.getCampusUnread().then(function (resp) {
            //     if (resp.result) {
            //         console.log('======1111111=====');
            //         var count = resp.data.statePendingAuditNum;
            //         if (count > 0) {
            //             $rootScope.$broadcast(BroadCast.BADGE_UPDATE, {
            //                 type: 'campus',
            //                 count: count
            //             });
            //         }
            //     }
            // });
            
            //获取评比列表
           // $scope.getClassVoteTasktList();

            //资产申领未读数
           // $scope.getAssetsUnreadCount();

        });

        $scope.onChartClick = function () {
            if (ionic.Platform.isIOS()) {
                $state.go('devicesOnline');
            } else {
                $scope.getDeviceInfo();
            }
        };

        $scope.getDeviceInfo = function () {
            var now = new Date().getTime() / 1000;
            if (!$scope.lastGet || now - $scope.lastGet > 5) {
                $scope.lastGet = now;
                $scope.data = {
                    data: [1, 0],
                    title: '',
                    subtitle: ''
                };
                setTimeout(function () {
                    Requester.getGeneralInfo().then(function (resp) {
                        if (resp.result) {
                            console.log('======2222222=====');
                            var terminalNum = resp.data.terminalNum;
                            var terminalOnlineNum = resp.data.terminalOnlineNum;
                            var offLineDevice = terminalNum - terminalOnlineNum;
                            if (offLineDevice === 0)
                                offLineDevice = 1;
                            $scope.data = {
                                data: [offLineDevice, terminalOnlineNum],
                                title: '{hh|设备' + '}\n{t1|' + terminalNum + '}',
                                subtitle: '{hh|' + '在线' + '}\n{t2|' + terminalOnlineNum + '}'
                            };

                        }
                    });
                }, 500);
            }
        };

        $scope.deleteItem = function (item, $event) {
            $event.stopPropagation();
            var confirmPopup = $ionicPopup.confirm({
                title: '删除确认',
                template: '确认删除 ' + item.title + ' ?',
                cancelText: '取消',
                okText: '确认',
                okType: 'button-balanced'
            });
            confirmPopup.then(function (res) {
                if (res) {
                    NoticeService.deleteNotice(item.id);
                    $ionicListDelegate.closeOptionButtons();
                }
            });
        };

        $scope.removeHTMLTag = function (str) {
            str = str.replace(/<\/?[^>]*>/g, ''); //去除HTML tag
            str = str.replace(/[ | ]*\n/g, '\n'); //去除行尾空白
            str = str.replace(/\n[\s| | ]*\r/g, '\n'); //去除多余空行
            str = str.replace(/&nbsp;/ig, ''); //去掉&nbsp;
            return str;
        };

        $scope.$on(BroadCast.DELETE_NOTICE_REV, function (a, rst) {
            if (rst && rst.result) {
                var i = 0;
                for (; i < $scope.noticeList.length; i++) {
                    if ($scope.noticeList[i].id == rst.request.id) {
                        switch (rst.request.type) {
                            case BroadCast.DELETE_NOTICE_REV:
                                $scope.noticeList.splice(i, 1);
                                break;
                        }
                        break;
                    }
                }
            } else
                toaster.error({
                    title: MESSAGES.OPERATE_ERROR,
                    body: rst.message
                });
        });

        $scope.dataInit = function () {
            $scope.attachments = [];
            $scope.selected = {
                key: $scope.noticeType
            };
        };

        $scope.loadData = function (reset) {
            if (Constant.debugMode) console.log('load notice restart ' + reset);
            if (reset) {
                $scope.page = 1;
            } else if (NoticeService.listHasMore)
                $scope.page++;
            else
                return;

            NoticeService.getNoticeList($scope.page, $scope.noticeType);
        };


        $scope.$on(BroadCast.NOTICE_LIST_REV, function (a, rst) {
            console.log('=======rst=====');
            console.log(rst);
            if (rst && rst.result) {
                $scope.noticeList = NoticeService.list;
                console.log('notice data has more:' + $scope.listHasMore);
            }
            $scope.listHasMore = NoticeService.listHasMore;
            $scope.$broadcast('scroll.refreshComplete');
            $scope.$broadcast('scroll.infiniteScrollComplete');
        });

        $scope.selectImg = function () {
            CameraHelper.selectMultiImage(function (resp) {
                if (!resp)
                    return;
                if (resp instanceof Array) {
                    Array.prototype.push.apply($scope.attachments, resp);
                    $scope.$apply();
                } else {
                    $scope.attachments.push({
                        data: "data:image/jpeg;base64," + resp
                    });
                }
            }, 9 - $scope.attachments.length);
        };

        $scope.removeImg = function (index, $event) {
            $event.stopPropagation();
            $scope.attachments.splice(index, 1);
        };

        $scope.preViewImages = function (index) {
            var origin = [];
            $scope.attachments.forEach(function (libraryItem) {
                if (libraryItem.data)
                    origin.push(libraryItem.data);
                else
                    origin.push(libraryItem);
            });
            ionicImageView.showViewModal({
                allowSave: false
            }, origin, index);
        };

        function onImageResized(resp) {
            var start = resp.indexOf('base64,');
            $scope.selected.picdatas.push(resp.substr(start + 7));
        }

        $scope.save = function () {
            $scope.selected.picdatas = [];
            var promiseArr = [];
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
            $scope.attachments.forEach(function (libraryItem) {
                if (libraryItem.data) {
                    onImageResized(libraryItem.data);
                } else {
                    promiseArr.push(resizeImage(Constant.CAPTURE_IMAGE_RANGE, libraryItem).then(onImageResized));
                }
            });
            $q.all(promiseArr).then(function () {
                NoticeService.newNotice($scope.selected);
            });
        };

        $scope.$on(BroadCast.NEW_NOTICE, function (a, rst) {
            if (rst && rst.result) {
                toaster.success({
                    title: MESSAGES.REMIND,
                    body: MESSAGES.NEW_NOTICE
                });
                $scope.closeModal();
                $scope.dataInit();
                $scope.loadData(true);
            } else
                toaster.error({
                    title: MESSAGES.REMIND,
                    body: rst.message
                });
            $ionicLoading.hide();
        });

        $scope.$on(BroadCast.CONNECT_ERROR, function (arg, data) {
            console.log('=====rootscope errror===');
            $scope.$broadcast('scroll.refreshComplete');
            $scope.$broadcast('scroll.infiniteScrollComplete');
            $ionicLoading.hide();
        });


        $scope.getBgColor = function (role) {
            var bgColor = '';
            switch (role) {
                case 'province_admin':
                    bgColor = '#FB6D0F';
                    break;

                case 'city_admin':
                    bgColor = '#448aca'; //市
                    break;

                case 'area_admin':
                    bgColor = '#556fb5'; //区
                    break;

                case 'district_admin':
                    bgColor = '#80c269'; //县级
                    break;

                case 'superadmin':
                    bgColor = '#2bcab2'; //小跟班平台
                    break;

                default:
                    bgColor = '';
                    break;
            }
            return bgColor;

        };

        //班级评比
        $scope.classAppraisal = function () {
            $state.go('classAppraisalList');
        };

        $scope.showMarket = function () {
            $state.go('market');
        };

        //获取评比任务列表
        $scope.getClassVoteTasktList = function () {
            Requester.getCurrentSemesterVoteTaskCount().then(function (res) {
                console.log('======333333=====');
                console.log(res);
                console.log('======333333 end=====');
                $scope.voteLength = res.data ? res.data.size : 0;
            });
        };
        //获取资产申领未读数
        $scope.getAssetsUnreadCount = function () {
            Requester.getAssetsUnread().then(function (res) {
                console.log('======444444=====');
                console.log(res);
                if (res.result)
                    $scope.assetsUnreadCount = res.data.size;
            });
        };

        $scope.goToNextPage = function (url) {
            $state.go(url);
        };

        //关闭授权提示框
        $scope.closeAuthAlert = function () {
            $scope.authAlert.close();
        };

        //检查授权到期提醒
        $scope.checkAuthOrAccountIsOverdue = function () {
            var beforeTamp = UserPreference.get('SchoolAuthAlertDateTamp');
            var currentDate = getNowDate();
            var currentTamp = Date.parse(currentDate.replace(/\-/g, "/")) / 1000;
            var interval = 24 * 60 * 60;
            var flag = currentTamp - beforeTamp >= interval;
            if (!beforeTamp ||beforeTamp==='undefined'|| (beforeTamp && flag)) {
                Requester.checkAuthOrAccountIsOverdue().then(function (resp) {
                    if (resp.result) {
                        console.log('======5555555=====');
                        console.log(resp);
                        console.log('======5555555 end=====');
                        $scope.authList = resp.data;
                        UserPreference.set('SchoolAuthAlertDateTamp', currentTamp);
                        //检查授权到期提醒
                        //大于1天且未曾弹出过 则弹出授权提醒
                        if (resp.data) {
                            $scope.authAlert = $ionicPopup.show({
                                 template: '<div style="color:#666;font-size:15px;padding:10px;width:100%;"><div ng-repeat="item in authList">【{{item.authModeName}}】功能权限将于<span style="color:#2bcab2;font-weight:bold;">{{item.expireDate.substr(0,10)}}</span>过期</div> <div>过期后对应模块在APP及网页端均不能在使用，续期请联系<span style="color:#2bcab2;font-weight:bold;">QQ客服(1833025389)</span>或小跟班业务员</div><div class="button" ng-click="closeAuthAlert()" style="height:30px;width:calc(100% - 80px);margin-left:40px;margin-top:15px;background:#2bcab2;color:white;">知道了</div></div>',
                                //  templateUrl: UIPath + 'common/areaAuthalert.html',
                                title: '<div style="color:white !important;">授权即将到期提醒</div><div class="logo" ><img src="img/icon/logo.png" /></div>',
                                scope: $scope,
                                cssClass: 'auth_alert'
                            });
                        }

                    } else {
                        console.log('未过期');
                    }
                });
            }

        };

    });

angular.module('app.controllers')
    .controller('marketCtrl', function ($scope, Requester, $ionicLoading, $timeout, $state, UserPreference, $ionicPopup, toaster) {

        $scope.$on("$ionicView.enter", function (event, data) {
            //获取用户id
            $scope.user = UserPreference.getObject('user');
            Requester.getToolkitList().then(function (res) {
                if (res.result)
                    $scope.list = res.data;
            });
        });

        $scope.getStatusText = function(code) {
            switch (code) {
                case 0:
                    return '申请开通';
                case 1:
                    return '已申请，待开通';
                case 2:
                    return '已开通';
            }
        };

        $scope.applyModule = function (item) {
            if (item.appStatus !== 0)
                return;
            $scope.data = {
                phone: ''
            };
            $ionicPopup.confirm({
                title: '申请授权',
                scope: $scope,
                template: '<input type="tel" placeholder="请留下手机号码，方便与您联系" ng-model="data.phone"/>',
                cancelText: '取消',
                okText: '确认',
                okType: 'button-balanced'
            }).then(function (res) {
                if (res && $scope.data.phone.length > 0) {
                    Requester.requestToolkit($scope.data.phone, item.id).then(function (res) {
                        if (res.result) {
                            item.appStatus = 1;
                        }else {
                            toaster.warning({title: res.message, body: ''});
                        }
                    });
                } else {
                    toaster.warning({title: '申请已取消', body: ''});
                }
            });
        };

    });

angular.module('app.controllers')
    .controller('newsDetailCtrl', function ($scope, $ionicHistory, $stateParams, Constant,  $sce, Requester, ionicImageView, $state, $ionicPopup, $ionicScrollDelegate, $rootScope) {
        
        $rootScope.$on('saveImgUrl', function (event, url) {
            SavePhotoTool.savePhoto(url);
        });
        function removeStyle(nodes) {
            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].children.length > 0) {
                    removeStyle(nodes[i].children);
                }
                nodes[i].removeAttribute('style');
                if (nodes[i].nodeName === 'IMG') {
                    nodes[i].setAttribute('ng-click', 'openModal(\'' + nodes[i].getAttribute('src') + '\')');
                }
            }
        }

        function getTextContent() {
           
            if ($scope.news.source !== 1) {
                var div = document.createElement('div');
                div.innerHTML = $scope.news.text;
                removeStyle(div.children);
                $scope.news.text = div.innerHTML;
            }
        }

        function getCommentsAndFavors() {
            if ($scope.news && $scope.news.id) {
                $scope.loadComments(true);
                $scope.loadFavorList();
            }
        }

        $scope.loadFavorList = function () {
            Requester.getCampusNewsFavorList($scope.news.id).then(function (resp) {
                $scope.favorList = resp.data;
                $scope.news.praiseNum = $scope.favorList.length;
            });
        };

        $scope.switchCommentTab = function (tab) {
            $scope.favorSelected = tab;
            $ionicScrollDelegate.resize();
        };

        $scope.deleteComment = function (item) {
            var confirmPopup = $ionicPopup.confirm({
                title: '提示',
                template: '确认删除' + item.commentsPersion + '的评论？',
                cancelText: '取消',
                okText: '确认',
                okType: 'button-balanced'
            });
            confirmPopup.then(function (res) {
                if (res) {
                    Requester.deleteCampusComment(item.id).then(function (rst) {
                        if (rst && rst.result) {
                            var i = 0;
                            for (; i < $scope.commentList.length; i++) {
                                if ($scope.commentList[i].id === item.id) {
                                    $scope.commentList.splice(i, 1);
                                }
                            }
                            $scope.news.commentsNum--;
                        }
                    });
                }
            });
        };

        $scope.loadComments = function (reset) {
            if (reset === true) {
                $scope.page = 1;
            }
            else if ($scope.listHasMore)
                $scope.page++;
            else
                return;
            Requester.getCampusNewsCommentList($scope.news.id, $scope.page).then(function (resp) {
                if (reset)
                    $scope.commentList = resp.data.content;
                else
                    Array.prototype.push.apply($scope.commentList, resp.data.content);
                $scope.listHasMore = !resp.data.last;
            }).finally(function () {
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        $scope.$on("$ionicView.enter", function (event, data) {
            $scope.listHasMore = false;
            $scope.picUrls = [];
            if ($stateParams.post) {
                $scope.isCampusView = true;
                $scope.news = $stateParams.post;
                $scope.news.imageUrls.forEach(function (url) {
                    $scope.urlItem = {
                        thumb: '',
                        src: ''
                    };
                    $scope.urlItem.thumb = url;
                    $scope.urlItem.src = url;
                    $scope.picUrls.push($scope.urlItem);
                });
                getTextContent();
                getCommentsAndFavors();
            } else if ($stateParams.notice) {
                $scope.isCampusView = false;
                $scope.news = $stateParams.notice;
                $scope.news.imageUrls.forEach(function (url) {
                    $scope.urlItem = {
                        thumb: '',
                        src: ''
                    };
                    $scope.urlItem.thumb = url;
                    $scope.urlItem.src = url;
                    $scope.picUrls.push($scope.urlItem);
                });
                getTextContent();
            }
            else {
                $ionicHistory.goBack();
                return;
            }
            // console.log('--news');
            // console.log($scope.news);
            if (window.cordova) MobclickAgent.onEvent('app_view_news');
        });

        $scope.viewDoc = function (url) {
            $state.go('pdfView', {url: url});
        };


        $scope.videoUrl = function (url) {
            return $sce.trustAsResourceUrl(url);
        };

        //点赞
        $scope.favorNews = function () {
            if ($scope.news.isFavor === true) {
                Requester.unfavorCampus($scope.news.id).then(function () {
                    $scope.loadFavorList();
                });
                if ($scope.news.praiseNum > 0)
                    $scope.news.praiseNum--;
            }
            else {
                Requester.favorCampus($scope.news.id).then(function () {
                    $scope.loadFavorList();
                });
                $scope.news.praiseNum++;
            }
            $scope.news.isFavor = !$scope.news.isFavor;
        };

        //评论
        $scope.showCommentInput = function () {
            $ionicPopup.show({
                template: '<textarea type="text" ng-model="news.commentContent" style="min-height: 100px;">',
                title: '请输入评论内容',
                scope: $scope,
                buttons: [
                    {text: '取消'},
                    {
                        text: '<b>评论</b>',
                        type: 'button-xgreen',
                        onTap: function (e) {
                            if (!$scope.news.commentContent) {
                                //don't allow the user to close unless he enters wifi password
                                e.preventDefault();
                            } else {
                                return $scope.news.commentContent;
                            }
                        }
                    }
                ]
            }).then(function (res) {
                if (res && res.trim() !== '') {
                    $scope.news.commentContent = undefined;
                    Requester.addCampusComment($scope.news.id, res).then(function (resp) {
                        $scope.loadComments(true);
                        $scope.news.commentsNum++;
                    });
                }
            });
        };

        $scope.getReviewStatus = function (status) {
            if (status == Constant.NEWS_STATUS.TEACHER_REVIEW.key)
                return Constant.NEWS_STATUS.TEACHER_REVIEW.text;
            else if (status == Constant.NEWS_STATUS.TEACHER_IGNORE.key)
                return Constant.NEWS_STATUS.TEACHER_IGNORE.text;
            else
                return Constant.NEWS_STATUS.TEACHER_REVIEW_PASS.text;
        };

        $scope.openArrModal = function (index) {
            ionicImageView.showViewModal({
                allowSave: true
            }, $scope.news.imageUrls, index);
        };

        $scope.openModal = function (pic) {
            ionicImageView.showViewModal({
                allowSave: true
            }, [pic]);
        };

        $scope.goTolinkPage = function(linkUrl){
            if(window.cordova&&ionic.Platform.isIOS()){
                cordova.plugins.browsertab.isAvailable(function(result){
                    if(result){
                        cordova.plugins.browsertab.openUrl(linkUrl).then(function(rest){
                            console.log(rest);
                         });
                    }else{
                        ordova.InAppBrowser.open(linkUrl, '_system','location=no,toolbar=no,toolbarposition=top,closebuttoncaption=关闭'); 
                    }
                });
            }

        };
    });
angular.module('app.controllers')
    .controller('generalCtrl', function ($scope, Constant, UserPreference, $ionicHistory, Requester, $state, $ionicPopup) {
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'page/';
        $scope.userRole = UserPreference.getObject('user').role;
     
        $scope.selectType = 1;

        if (String($scope.userRole) === Constant.USER_ROLES.SCHOOL_ADMIN) {
            $scope.schoolAdmin = true;
            $scope.pageTitle = '设备概况';
        } else if (String($scope.userRole) === Constant.USER_ROLES.DISTRICT_ADMIN) {
            $scope.pageTitle = UserPreference.getObject('user').districtName;
        } else {
            $scope.pageTitle = UserPreference.getObject('user').area.areaName;
        }

        $scope.goBack = function () {
            if ($ionicHistory.backView())
                $ionicHistory.goBack();
            else
                $state.go('tabsController.mainPage');
        };

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            if (String($scope.userRole) != Constant.USER_ROLES.SCHOOL_ADMIN) {
                $scope.checkAuthOrAccountIsOverdue();
            }
            Requester.getGeneralInfo().then(function (resp) {
                if (resp.result) {
                    $scope.classNum = resp.data.classNum;
                    $scope.schlJoinNum = resp.data.schlJoinNum;
                    $scope.schlJoinRate = resp.data.schlJoinRate;
                    $scope.studentNum = resp.data.studentNum;
                    $scope.teacherAvgTime = resp.data.teacherAvgTime;
                    $scope.teacherNum = resp.data.teacherNum;
                    $scope.teacherUseNum = resp.data.teacherUseNum;
                    $scope.teacherUseRate = resp.data.teacherUseRate;
                    $scope.terminalAvgTime = resp.data.terminalAvgTime;
                    $scope.terminalNum = resp.data.terminalNum;
                    $scope.terminalOnlineNum = resp.data.terminalOnlineNum;
                    $scope.terminalOnlineRate = resp.data.terminalOnlineRate;
                    $scope.terminalUseNum = resp.data.terminalUseNum;
                    $scope.terminalUseRate = resp.data.terminalUseRate;
                }
            });



        });

        $scope.checkAuthOrAccountIsOverdue = function () {
            var beforeTamp = UserPreference.get('AreaAuthAlertDateTamp');
            var currentDate = getNowDate();
            var currentTamp = Date.parse(currentDate.replace(/\-/g, "/")) / 1000;
            var interval = 24 * 60 * 60;
            var flag = currentTamp - beforeTamp >= interval;
            if (!beforeTamp ||beforeTamp==='undefined'|| (beforeTamp && flag)) {
                Requester.checkAuthOrAccountIsOverdue().then(function (resp) {
                    if (resp.result) {
                        $scope.authList = resp.data;
                        UserPreference.set('AreaAuthAlertDateTamp', currentTamp);
                        //检查授权到期提醒  
                        if (resp.data) {
                            $scope.authAlert = $ionicPopup.show({
                                template: '<div style="color:#666;font-size:15px;padding:10px;width:100%;"><div ng-repeat="item in authList">{{item.authModeName}}功能权限将于<span style="color:#2bcab2;font-weight:bold;">{{item.expireDate.substr(0,10)}}</span>过期</div> <div>过期后对应模块在APP及网页端均不能在使用，续期请联系<span style="color:#2bcab2;font-weight:bold;">QQ客服(1833025389)</span>或小跟班业务员</div><div class="button" ng-click="closeAuthAlert()" style="height:30px;width:calc(100% - 80px);margin-left:40px;margin-top:15px;background:#2bcab2;color:white;">知道了</div></div>',
                                // templateUrl: UIPath + 'common/areaAuthalert.html',
                                title: '<div style="color:white !important;">授权即将到期提醒</div><div class="logo" ><img src="img/icon/logo.png" /></div>',
                                scope: $scope,
                                cssClass: 'auth_alert'
                            });
                        }

                    } else {
                        console.log('未过期');
                    }
                });
            }

        };

        //关闭授权提示框
        $scope.closeAuthAlert = function () {
            //UserPreference.set('AreaAuthAlertRead', 'read');
            $scope.authAlert.close();
        };

        $scope.selectTypeClick = function (type) {
            $scope.selectType = type;
            if (type == 1) {

            } else {
                //使用分析
                $scope.data = {
                    date: [],
                    data: []
                };
                $scope.refreshData();
            }
        };
        
        // 使用分析

        $scope.refreshData = function () {
            Requester.getUsedTerminalsRecently().then(function (resp) {
                var date = [];
                var data = [];
                if (resp.result) {
                    for (var i = 0; i < resp.data.length; i++) {
                        if (resp.data[i].day.length === 10)
                            date.push(resp.data[i].day.substr(5, 5));
                        else
                            date.push(resp.data[i].day);
                        data.push(resp.data[i].count);
                    }
                    $scope.data = {
                        date: date,
                        data: data
                    };
                }
            });
        };

        //设备在线
        $scope.goToOnlineDevice = function(){
            $state.go('devicesOnline');
        };
        $scope.getDeviceType = function () {
            var result = 'android';
            $scope.isIos = ionic.Platform.isIOS() ;
            if ($scope.isIos) {
                if (window.screen.width >= 375 && window.screen.height >= 812) {
                    result = 'iphoneX';
                } else {
                    result = 'iphone';
                }

            } else {
             result = 'android';
            }
            return result;
        };

    });
angular.module('app.controllers')
.controller('usageCtrl', function ($scope, Constant, UserPreference, $state, $ionicHistory, Requester) {
    $scope.userRole = UserPreference.getObject('user').role;
    if (String($scope.userRole) === Constant.USER_ROLES.SCHOOL_ADMIN) {
        $scope.schoolAdmin = true;
    }
    $scope.data = {
        date: [],
        data: []
    };
    $scope.refreshData = function () {
        Requester.getUsedTerminalsRecently().then(function (resp) {
            var date = [];
            var data = [];
            if (resp.result) {
                for (var i = 0; i < resp.data.length; i++) {
                    if (resp.data[i].day.length === 10)
                        date.push(resp.data[i].day.substr(5, 5));
                    else
                        date.push(resp.data[i].day);
                    data.push(resp.data[i].count);
                }
                $scope.data = {
                    date: date,
                    data: data
                };
            }
        });
    };
    $scope.refreshData();

    $scope.goBack = function () {
        if ($ionicHistory.backView())
            $ionicHistory.goBack();
        else
            $state.go('tabsController.mainPage');
    };

});
angular.module('app.controllers')
.controller('rankCtrl', function ($scope, Constant, UserPreference, $state, $ionicHistory, Requester, $stateParams) {

    $scope.$on("$ionicView.loaded", function (event, data) {
        $scope.unit = '';
        $scope.type = $stateParams.type;
        $scope.userRole = UserPreference.getObject('user').role;
        if (String($scope.userRole) === Constant.USER_ROLES.SCHOOL_ADMIN) {
            $scope.schoolAdmin = true;
        }
        switch ($scope.type) {
            case 'usage':
                $scope.rankTitle = '学校设备使用排行';
                $scope.unit = '%';
                break;
            case 'class':
                $scope.rankTitle = '班级使用统计';
                break;
            case 'teacher':
                $scope.rankTitle = '教师使用统计';
                break;
            case 'time':
                $scope.rankTitle = '学校设备使用时长排行';
                break;
            case 'teacherUseTime':
                $scope.rankTitle = '学校教师使用时长排行';
                break;
        }
        $scope.cities = [];
        $scope.areas = [];
        $scope.data = {};
        $scope.selected = {
            type:0,//0 :设备使用率 1:设备在线率 
            days: 7,
            area: UserPreference.getObject('user').area.id
        };
        $scope.userRole = UserPreference.getObject('user').role;
        if (String($scope.userRole) === Constant.USER_ROLES.PROVINCE_ADMIN) {
            $scope.cityChoosable = true;
            $scope.areaChoosable = true;
            Requester.getArea($scope.selected.area).then(function (resp) {
                $scope.cities = resp;
            });
        } else if (String($scope.userRole) === Constant.USER_ROLES.CITY_ADMIN) {
            $scope.cityChoosable = false;
            $scope.areaChoosable = true;
            Requester.getArea($scope.selected.area).then(function (resp) {
                $scope.areas = resp;
            });
        } else {
            $scope.cityChoosable = false;
            $scope.areaChoosable = false;
        }
        $scope.getData();
    });

    $scope.onSelectedDayChange = function (d) {
        if ($scope.selected.days != d) {
            $scope.selected.days = d;
            $scope.getData();
        }
    };

    $scope.onSelectionChange = function (isCity) {
        if ($scope.cityChoosable && !$scope.selected.city) {
            $scope.selected.area = UserPreference.getObject('user').area.id;
            $scope.areas = [];
        } else if (isCity) {
            Requester.getArea($scope.selected.city).then(function (resp) {
                $scope.areas = resp;
            });
            $scope.selected.area = $scope.selected.city;
        }
        $scope.getData();
    };

    $scope.makeChart = function (y, x) {
        $scope.chartHeight = y.length * 40 + 25;
        $scope.data = {
            y: y,
            x: x,
            unit: $scope.unit
        };
    };

    $scope.getData = function () {
        switch ($scope.type) {
            case 'usage':
                Requester.getTerminalUsageRank($scope.selected.type,$scope.selected.area, $scope.selected.days).then(function (resp) {
                    if (resp.result) {
                        if (resp.data.content.length === 0) {
                            $scope.emptyContent = true;
                        } else {
                            $scope.emptyContent = false;
                            var x = [], y = [];
                            for (var i = resp.data.content.length - 1; i >= 0; i--) {
                                y.push(i + 1 + '.' + resp.data.content[i].schoolName);
                                var rate = resp.data.content[i].rate;
                                x.push(rate.substr(0, rate.length - 1));
                            }
                            $scope.makeChart(y, x);
                        }
                    }
                });
                break;
            case 'class':
                var param;
                if ($scope.selected.days === 30)
                    param = 'lastMonth';
                else
                    param = 'lastWeek';
                Requester.getClassUseRank(param).then(function (resp) {
                    if (resp.result) {
                        if (resp.data.length === 0) {
                            $scope.emptyContent = true;
                        } else {
                            $scope.emptyContent = false;
                            var x = [], y = [];
                            for (var i = resp.data.length - 1; i >= 0; i--) {
                                y.push(i + 1 + '.' + resp.data[i].className);
                                x.push(resp.data[i].time);
                            }
                            $scope.makeChart(y, x);
                        }
                    }
                });
                break;
            case 'teacher':
                if ($scope.selected.days === 30)
                    param = 'lastMonth';
                else
                    param = 'lastWeek';
                Requester.getTeacherUseRank(param).then(function (resp) {
                    if (resp.result) {
                        if (resp.data.length === 0) {
                            $scope.emptyContent = true;
                        } else {
                            $scope.emptyContent = false;
                            var x = [], y = [];
                            for (var i = resp.data.length - 1; i >= 0; i--) {
                                y.push(i + 1 + '.' + resp.data[i].teacherName);
                                x.push(resp.data[i].time);
                            }
                            $scope.makeChart(y, x);
                        }
                    }
                });
                break;
            case 'time':
                Requester.getTerminalUseTimeRank($scope.selected.area, $scope.selected.days).then(function (resp) {
                    if (resp.result) {
                        if (resp.data.content.length === 0) {
                            $scope.emptyContent = true;
                        } else {
                            $scope.emptyContent = false;
                            var x = [], y = [];
                            for (var i = resp.data.content.length - 1; i >= 0; i--) {
                                y.push(i + 1 + '.' + resp.data.content[i].schoolName);
                                x.push(resp.data.content[i].timeonline);
                            }
                            $scope.makeChart(y, x);
                        }
                    }
                });
                break;
            case 'teacherUseTime':
                Requester.getTeacherUseTimeRank($scope.selected.area, $scope.selected.days).then(function (resp) {
                    if (resp.result) {
                        if (resp.data.content.length === 0) {
                            $scope.emptyContent = true;
                        } else {
                            $scope.emptyContent = false;
                            var x = [], y = [];
                            for (var i = resp.data.content.length - 1; i >= 0; i--) {
                                y.push(i + 1 + '.' + resp.data.content[i].name);
                                x.push(resp.data.content[i].time);
                            }
                            $scope.makeChart(y, x);
                        }
                    }
                });
                break;
        }
    };


    $scope.goBack = function () {
        if ($ionicHistory.backView())
            $ionicHistory.goBack();
        else
            $state.go('tabsController.mainPage');
    };

    //设备使用统计类型
    $scope.onSelectedUsedOrOnline = function(type){
        //0:设备使用率 1：设备上课率
        //  $scope.selected.type = type; 
         if ($scope.selected.type != type) {
            $scope.selected.type = type;
            $scope.getData();
        }
    };

});

angular.module('app.controllers')
.controller('devicesOnlineCtrl', function ($scope, Constant, Requester, $state, $ionicHistory, $ionicPopup, toaster) {

    $scope.$on("$ionicView.enter", function (event, data) {
        $scope.loadData();
        $scope.shutdown = '';
    });

    $scope.$on('$ionicView.leave', function () {
        clearInterval($scope.refresh);
    });

    $scope.loadData = function () {
        $scope.loading = true;
        $scope.selected = {
            all: false,
            num: 0
        };
        Requester.getOnlineTerminalList().then(function (resp) {
            $scope.list = resp.data;
            for (var i = 0; $scope.shutdown !== '' && i < $scope.list.length; i++) {
                if ($scope.shutdown.indexOf($scope.list[i].id) >= 0)
                    $scope.list[i].processing = true;
            }
            if (resp.data.length === 0 && $scope.refresh) {
                clearInterval($scope.refresh);
            }
        }).finally(function () {
            $scope.loading = false;
            $scope.$broadcast('scroll.refreshComplete');
        });
    };

    function setListCheckedStatus(sel) {
        for (var i = 0; i < $scope.list.length; i++) {
            $scope.list[i].isChecked = sel;
        }
    }

    $scope.selectAll = function () {
        $scope.selected.all = $scope.selected.all === true;
        setListCheckedStatus($scope.selected.all);
        if ($scope.selected.all === true)
            $scope.selected.num = $scope.list.length;
        else
            $scope.selected.num = 0;
    };

    $scope.confirmShutdown = function () {
        var confirmPopup = $ionicPopup.confirm({
            title: '温馨提示',
            template: '该操作后，选中的设备将被远程关机，只有手动启动后才能继续使用。<p style="color: orange;font-size: small">由于网络环境不同，设备关机时间会有不同程度的延迟，请耐心等待。</p>',
            cancelText: '取消',
            okText: '确认关机',
            okType: 'button-balanced'
        });
        confirmPopup.then(function (res) {
            if (res) {
                var ids = '';
                for (var i = 0; i < $scope.list.length; i++) {
                    if ($scope.list[i].isChecked)
                        ids += ($scope.list[i].id + ',');
                }
                Requester.turnOffTerminals(ids.substr(0, ids.length - 1)).then(function (resp) {
                    toaster.success({title: '', body: '关机指令已发送，请耐心等待..'});
                    $scope.shutdown += ids;
                    $scope.loadData();
                    $scope.refresh = setInterval($scope.loadData, 10000);
                });
            }
        });
    };

    $scope.onCheckedChange = function (item) {
        if (item.isChecked === true) {
            $scope.selected.num++;
            $scope.selected.all = $scope.selected.num === $scope.list.length;
        }
        else {
            $scope.selected.num--;
            $scope.selected.all = false;
        }
    };

    $scope.goBack = function () {
        if ($ionicHistory.backView())
            $ionicHistory.goBack();
        else
            $state.go('tabsController.mainPage');
    };

});
angular.module('app.controllers')
.controller('generalAboutCtrl', function ($scope, Constant, UserPreference, $state, $ionicHistory) {

    $scope.userRole = UserPreference.getObject('user').role;
    if (String($scope.userRole) === Constant.USER_ROLES.SCHOOL_ADMIN) {
        $scope.schoolAdmin = true;
    }

    $scope.goBack = function () {
        if ($ionicHistory.backView())
            $ionicHistory.goBack();
        else
            $state.go('tabsController.mainPage');
    };

});
angular.module('app.controllers')
.controller('terminalListCtrl', function ($scope, Constant, UserPreference, $state, $ionicHistory, Requester) {

    $scope.$on("$ionicView.loaded", function (event, data) {
        $scope.selected = {
            area: UserPreference.getObject('user').area.id
        };
        console.log($scope.selected);
        $scope.userRole = UserPreference.getObject('user').role;
        if (String($scope.userRole) === Constant.USER_ROLES.PROVINCE_ADMIN) {
            $scope.cityChoosable = true;
            $scope.areaChoosable = true;
            Requester.getArea($scope.selected.area).then(function (resp) {
                $scope.cities = resp;
            
            });
        } else if (String($scope.userRole) === Constant.USER_ROLES.CITY_ADMIN) {
            $scope.cityChoosable = false;
            $scope.areaChoosable = true;
            Requester.getArea($scope.selected.area).then(function (resp) {
                $scope.areas = resp;
            });
        } else {
            $scope.cityChoosable = false;
            $scope.areaChoosable = false;
            if (String($scope.userRole) === Constant.USER_ROLES.SCHOOL_ADMIN) {
                $scope.schoolAdmin = true;
                $scope.selected.school = UserPreference.getObject('user').school.id;
                $scope.onSchoolSelected(true);
            } else if (String($scope.userRole) === Constant.USER_ROLES.DISTRICT_ADMIN) {
                $scope.schools = UserPreference.getObject('user').schools;
            } else {
                Requester.getSchools($scope.selected.area).then(function (resp) {
                    $scope.schools = resp.data?resp.data.content:[];
                });
            }
        }
    });

    $scope.onSchoolSelected = function (reset) {
        if ($scope.selected.school) {
            if (reset) {
                $scope.page = 1;
                $scope.list = [];
            }
            else if ($scope.listHasMore)
                $scope.page++;
            else
                return;
            $scope.loading = true;
            
            console.log($scope.selected);
            Requester.getTerminalList($scope.page, $scope.selected.school).then(function (resp) {
                if ($scope.list.length === 0)
                    $scope.list = resp.data.content;
                   
                else {
                    Array.prototype.push.apply($scope.list, resp.data.content);
                }
                $scope.listHasMore = !resp.data.last;
            }).finally(function () {
                console.log($scope.list);
                console.log('--list==');
                $scope.loading = false;
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        }
        else
            $scope.$broadcast('scroll.refreshComplete');
    };

    $scope.goDetail = function (id) {
        $state.go('terminalUsageInfo', {id: id});
    };

    $scope.onSelectionChange = function (isCity) {
        if ($scope.cityChoosable && !$scope.selected.city) {
            $scope.selected.area = UserPreference.getObject('user').area.id;
            $scope.areas = [];
            $scope.schools = [];
        }
        else {
            if (isCity) {
                Requester.getArea($scope.selected.city).then(function (resp) {
                    $scope.areas = resp;
                    $scope.schools = [];
                });
            } else if ($scope.selected.area) {
                Requester.getSchools($scope.selected.area).then(function (resp) {
                    $scope.schools = resp.data.content;
                });
            }
        }
        $scope.list = [];
    };

    $scope.goBack = function () {
        if ($ionicHistory.backView())
            $ionicHistory.goBack();
        else
            $state.go('tabsController.mainPage');
    };

});
angular.module('app.controllers')
.controller('terminalLocationCtrl', function ($scope, Constant, UserPreference, $state, $ionicHistory, Requester, $ionicScrollDelegate) {

    $scope.goBack = function () {
        if ($scope.deep === 0) {
            if ($ionicHistory.backView())
                $ionicHistory.goBack();
            else
                $state.go('tabsController.mainPage');
        } else {
            $scope.deep--;
            $scope.areaName = $scope.history[$scope.deep].areaName;
            $scope.getTerminalsLocation($scope.history[$scope.deep].id);
        }
    };

    $scope.$on("$ionicView.loaded", function (event, data) {
        $scope.areaName = UserPreference.getObject('user').area.areaName;
        $scope.chartHeight = 200;
        $scope.deep = 0;
        $scope.userRole = UserPreference.getObject('user').role;
        if (String($scope.userRole) === Constant.USER_ROLES.PROVINCE_ADMIN) {
            $scope.MAX = 2;
        } else if (String($scope.userRole) === Constant.USER_ROLES.CITY_ADMIN) {
            $scope.MAX = 1;
        } else {
            $scope.MAX = 0;
        }
        $scope.history = [2];
        $scope.history[$scope.deep] = {
            id: undefined,
            areaName: $scope.areaName
        };
        $scope.chart = echarts.init(document.getElementById("bar-chart"));
        $scope.chart.on('click', function (param) {
            if ($scope.deep < $scope.MAX && $scope.areas && $scope.areas.length > param.dataIndex) {
                $scope.deep++;
                var selectedArea = $scope.areas[param.dataIndex];
                $scope.areaName = selectedArea.regionName;
                $scope.getTerminalsLocation(selectedArea.id);
                $scope.history[$scope.deep] = {
                    id: selectedArea.id,
                    areaName: $scope.areaName
                };
            }
        });

        $scope.getTerminalsLocation();
    });

    $scope.getTerminalsLocation = function (id) {
        Requester.getTerminalsLocation(id).then(function (resp) {
            var y = [];
            var x = [];
            $scope.areas = [];
            if (!resp.data) {
                $scope.chartHeight = 0;
                return;
            }
            $scope.chartHeight = resp.data.content.length * 40 + 25;
            for (var i = resp.data.content.length - 1; i >= 0; i--) {
                y.push(i + 1 + '.' + resp.data.content[i].regionName);
                x.push(resp.data.content[i].terminalCount);
                $scope.areas.push(resp.data.content[i]);
            }
            setTimeout(function () {
                var option = {
                    color: ['#3398DB'],
                    grid: {
                        left: '5%',
                        right: '4%',
                        top: '20',
                        bottom: '5',
                        containLabel: true
                    },
                    yAxis: [
                        {
                            type: 'category',
                            data: y,
                            axisLine: {
                                show: false
                            },
                            axisTick: {
                                show: false
                            }
                        }
                    ],
                    xAxis: [
                        {
                            type: 'value',
                            show: false
                        }
                    ],
                    series: [
                        {
                            type: 'bar',
                            smooth: true,
                            data: x,
                            label: {
                                normal: {
                                    show: true,
                                    position: 'inside',
                                    textStyle: {
                                        color: '#fff',
                                        fontSize: 14
                                    },
                                    formatter: '{c}台'
                                }

                            },
                            barWidth: 18,
                            barMinHeight: 50,
                            itemStyle: {
                                emphasis: {
                                    barBorderRadius: 25
                                },
                                normal: {
                                    barBorderRadius: 25,
                                    color: new echarts.graphic.LinearGradient(
                                        0, 0, 1, 0,
                                        [
                                            {offset: 0, color: '#096fcf'},
                                            {offset: 1, color: '#47fbb9'}
                                        ]
                                    )
                                }
                            }
                        }
                    ]
                };
                $scope.chart.setOption(option);
                $scope.chart.resize();
                $ionicScrollDelegate.scrollTop();
            }, 100);
        });
    };

});

angular.module('app.controllers')
.controller('terminalUsageInfoCtrl', function ($scope, Constant, UserPreference,ionicImageView, $state, $ionicHistory, Requester, $stateParams, ionicDatePicker, toaster, $ionicModal) {

    $scope.goBack = function () {
        if ($ionicHistory.backView())
            $ionicHistory.goBack();
        else
            $state.go('tabsController.mainPage');
    };

    $scope.$on('$destroy', function () {
        $scope.imgModal.remove();
    });

    var UIPath = '';
    if (Constant.debugMode) UIPath = 'page/';
    $ionicModal.fromTemplateUrl(UIPath + 'common/ionic-image-view.html', {
        scope: $scope
    }).then(function (modal) {
        $scope.imgModal = modal;
    });

    $scope.closeImageView = function () {
        $scope.imgModal.hide();
    };

    $scope.viewImg = function (url) {
        
        var imgArr = [];
        imgArr.push(url);
        ionicImageView.showViewModal({
            allowSave: true
        }, imgArr, 0);
        //$scope.imgModal.show();
        //$scope.largeImgUrl = url;
    };

    $scope.$on("$ionicView.loaded", function (event, data) {
        $scope.selected = {
            days: 7,
            sn: $stateParams.id,
            date: formatTimeWithoutSecends((new Date()).getTime() / 1000, "yyyy-MM-dd")
        };
        $scope.data = {};
        $scope.chartView = true;
        $scope.getChartData();
        $scope.getScreenShots(true, true);
    });

    $scope.switchView = function (p) {
        $scope.chartView = p;
    };

    $scope.onSelectedDayChange = function (d) {
        if ($scope.selected.days != d) {
            $scope.selected.days = d;
            $scope.getChartData();
        }
    };

    $scope.showDatePicker = function () {
        var ipObj1 = {
            callback: function (val) {
                if (new Date().getTime() < val) {
                    toaster.warning({title: '所选日期不能晚于今天！', body: ''});
                    return;
                }
                $scope.selected.date = formatTimeWithoutSecends(val / 1000, "yyyy-MM-dd");
                $scope.getScreenShots(true);
            }
        };
        ionicDatePicker.openDatePicker(ipObj1);
    };

    $scope.getChartData = function () {
        $scope.totalUseTime = 0;
        Requester.getTerminalUseTime($scope.selected.days, $scope.selected.sn).then(function (resp) {
            if (resp.data) {
                var x = [], y = [];
                for (var i in resp.data) {
                    x.push(i.substr(5, 5));
                    var uses = resp.data[i] / 1000;
                    y.push(uses / 3600);
                    $scope.totalUseTime += uses;
                }
                $scope.data = {
                    x: x,
                    y: y
                };

                $scope.totalTime = getFormattedTimeLabel($scope.totalUseTime);
                $scope.avaTime = getFormattedTimeLabel(Math.floor($scope.totalUseTime / $scope.selected.days));
            }
        });
    };

    function getFormattedTimeLabel(s) {
        var rst = '';
        var hour = Math.floor(s / 3600);
        var min = Math.floor(s / 60) % 60;
        var sec = s % 60;
        if (hour > 0)
            rst += (hour + '小时');
        if (min > 0)
            rst += (min + '分');
        return rst + sec + '秒';
    }

    $scope.getScreenShots = function (reset, ignore) {
        if ($scope.chartView && !ignore) {
            $scope.$broadcast('scroll.refreshComplete');
            return;
        }
        if (reset) {
            $scope.page = 1;
            $scope.list = [];
        }
        else if ($scope.listHasMore)
            $scope.page++;
        else
            return;
        $scope.loading = true;
        Requester.getTerminalScreenShot($scope.page, $scope.selected.date, $scope.selected.sn).then(function (resp) {
            if (resp.result) {
                if ($scope.list.length === 0)
                    $scope.list = resp.data.content;
                else {
                    Array.prototype.push.apply($scope.list, resp.data.content);
                }
                $scope.listHasMore = !resp.data.last;
            } else
                $scope.listHasMore = false;
        }).finally(function () {
            $scope.loading = false;
            $scope.$broadcast('scroll.refreshComplete');
            $scope.$broadcast('scroll.infiniteScrollComplete');
        });
    };

});
angular.module('app.controllers')
  .controller('classAppraisalListCtrl', function ($scope, $ionicPopup, $ionicListDelegate, $state, ionicDatePicker, toaster, $ionicLoading, Requester, Constant, UserPreference, $ionicScrollDelegate,BroadCast) {

    window.addEventListener('native.keyboardshow', function (keyboardParameters) {
     
      var tObj = document.getElementById("apprailId");
      var sPos = tObj.value.length;
      setTimeout(function(){
        setCaretPosition(tObj, sPos);
    },100);
  });
    //进入视图时
    $scope.$on("$ionicView.enter", function (event, data) {
      //  初始化默认选择时间 
      var time = formatTimeWithoutSecends(new Date().getTime() / 1000, "yyyy-MM-dd ");
      $scope.selected = {
        title: '',
        startDate: '',
        endDate: ''
      };
      $scope.page = 1; //当前页面
      $scope.isMoreData = true;
      $scope.voteList = [];
      var array = UserPreference.getArray('VoteTaskList')?UserPreference.getArray('VoteTaskList'):[];
      $scope.voteList = array;
      $scope.getClassVoteTasktList();


    });

    $scope.$on(BroadCast.CONNECT_ERROR, function () {
     
      $scope.isMoreData = false;
       $ionicLoading.hide();
  });

    //获取评比任务列表
    $scope.getClassVoteTasktList = function () {
      Requester.getClassVoteTasktList2($scope.page).then(function (res) {

        if (res.result) {
          $scope.arr = [];
          $scope.arr = res.data.data.content;
          $scope.startDate  = res.data.startDate.substr(0,10);
          $scope.endDate = res.data.endDate.substr(0,10);
          if ($scope.page && $scope.page > 1) {
            $scope.arr.forEach(function (item) {
              $scope.voteList.push(item);
            });
          } else {
            $scope.voteList = $scope.arr;
            UserPreference.setObject('VoteTaskList', $scope.voteList);
          }
         
          if ($scope.arr.length < Constant.reqLimit && $scope.page > 1) {

            $scope.isMoreData = false;
          }
          //获取成功 存取

        } else {
          $scope.isMoreData = false;
        }

      }, function (error) {
        $ionicLoading.show({
          template: error,
          duration: 1500
        });

      }).finally(function () {
        $scope.isMoreData = false;
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');

      });
    };
    //添加班级评比任务
    $scope.addClassVoteTask = function () {
      Requester.addClassVoteTask($scope.selected).then(function (res) {
        if (res.result) {
          $scope.myPopup.close();
          $scope.page = 1;
          $scope.getClassVoteTasktList();
         
        } else {
          
          $ionicLoading.show({
            template: res.message,
            duration: 1500
          });
          return;
        }

      }, function (error) {
        toaster.warning({
          title: "温馨提示",
          body: error.data.message
        });
      });
    };
    //下拉刷新
    $scope.refreshData = function () {
      $scope.voteList = [];
      $scope.isMoreData = true;
      $scope.page = 1;
      $scope.$broadcast('scroll.refreshComplete');
      $scope.getClassVoteTasktList();
    };

    //上拉加载
    $scope.loadMore = function () {
      // $scope.voteList = [];
      $scope.page++;
      $scope.getClassVoteTasktList();

    };

    //弹出提示框
    var UIPath = '';
    if (Constant.debugMode) UIPath = 'page/';
    $scope.openModal = function () {
      // if(!$scope.data)
      // $scope.data = {};
       $scope.myPopup = $ionicPopup.show({
        // template: '<div   style="font-size:15px;line-height:36px;min-height:36px;"><div style="width:67px;margin-top:8px;padding-right:0px;">评比时段:</div> <div ng-click = "selectStartOrEndDate(0)" ng-style="{\'color\':selected.startDate==\'开始时间\'?\'#999999\':\'#333\'}" style="margin-left:5px; border:1px solid #CFCFCF;font-size:14px;width:32%;line-height:32px;height:32px;position:absolute;left:78px;top:64px;border-radius:5px;">&nbsp;{{selected.startDate}}</div><div style="position:absolute;left:calc(32% + 78px);top:60px;width:32%;">&nbsp;至&nbsp;</div><div  ng-style="{\'color\':selected.endDate==\'结束时间\'?\'#999999\':\'#333\'}" style="border:1px solid #CFCFCF;font-size:14px;line-height:32px;height:32px;width:30%;position:absolute;left:calc(32% + 98px);top:64px;border-radius:5px;"  ng-click = "selectStartOrEndDate(1)">&nbsp;{{selected.endDate}}</div></div>' +
        //   '<div style="margin-top:15px;"><label class="item-input" style="padding-left:0px !important;margin-top:19px;"> <div style="width:67px !important;min-width:67px;text-align:right;font-size:15px;padding-right:0px;">标题:</div><input type="text"  style="border:1px solid #CFCFCF;padding-left:2px;padding-right:5px;height:36px;position:reletive;margin-left:6px;font-size:14px;border-radius:5px;" placeholder="请输入评比标题" ng-model="selected.title"></label></div>',
          templateUrl:UIPath + 'home/classAppraisal/addClassAppraisalTask.html',
          title: '<div> <img src="img/classAppraisal/left.png" /><span>添加班级评比任务</span><img src="img/classAppraisal/right.png" /></div>',
        scope: $scope,
        cssClass: 'dateSelectAlert',
        buttons: [{
            text: '取消',
            type: 'button-cancel'
          },
          {
            text: '<b>确定</b>',
            type: 'button-xgreen',
            onTap: function (e) {
        
              var time1 = $scope.selected.startDate.replace(/\-/g, "/");
              var timestamp1 = Date.parse(new Date(time1)) / 1000;
              var time2 = $scope.selected.endDate.replace(/\-/g, "/");
              var timestamp2 = Date.parse(new Date(time2)) / 1000;
              
              if (!$scope.selected.startDate) {
                $ionicLoading.show({
                  template: '请先选择开始时间',
                  duration: 1500
                });
                e.preventDefault();
                return;
              }
              if (!$scope.selected.endDate) {
                $ionicLoading.show({
                  template: '请选择结束时间',
                  duration: 1500
                });
                e.preventDefault();
                return;
              }
              if (!$scope.selected.title) {
                $ionicLoading.show({
                  template: '请输入标题',
                  duration: 1500
                });
                e.preventDefault();
                return;
              }
                if(timestamp1>timestamp2){
                $ionicLoading.show({
                  template: '结束时间不能小于开始时间',
                  duration: 1500
                });
                e.preventDefault();
                return;
              }
              if ($scope.selected.title.length > 30) {
                $ionicLoading.show({
                  template: '请将标题控制在30字以内',
                  duration: 1500
                });
                e.preventDefault();
                return;
              }

            
              $scope.addClassVoteTask();
              e.preventDefault();
              return e;
            }
          }
        ]
      });

      $scope.myPopup.then(function (res) {

     
      });
    };
     
   
    //侧滑删除
    $scope.deleteItem = function (item, $event) {
      $event.stopPropagation();
      var confirmPopup = $ionicPopup.confirm({
        title: '删除确认',
        template: '确认删除 ' + item.title + ' ?',
        cancelText: '取消',
        okText: '确认',
        okType: 'button-balanced'
      });
      confirmPopup.then(function (res) {
        if (res) {
          Requester.deleteClassVoteTask(item.id).then(function (resp) {
            if (resp.result) {
              //刷新
              console.log('delete succeed');
              // $scope.voteList.splice(item,1);
              $scope.page = 1;
              $scope.getClassVoteTasktList();
              $ionicScrollDelegate.$getByHandle('delegateHandler').resize();
             
            }
          });
          $ionicListDelegate.closeOptionButtons();
        }
      });
    };

    //
    $scope.selectStartOrEndDate = function (arg) {
     
      var ipObj1 = {
        callback: function (val) {
          if (typeof (val) !== 'undefined') {
            if (arg == 0) {
              $scope.selected.startDate = formatTimeWithoutSecends(val / 1000).substr(0, 10);
            } else {

              $scope.selected.endDate = formatTimeWithoutSecends(val / 1000).substr(0, 10);
            }

            if ($scope.selected.startDate && $scope.selected.endDate ) {
              $scope.selected.title = $scope.selected.startDate.substr(5, 5) + '至' + $scope.selected.endDate.substr(5, 5) + '班级评比';
            }
          }
        }
      };
      ionicDatePicker.openDatePicker(ipObj1);
    };


    //查看详情
    $scope.goToDetail = function (voteTaskId, title) {

      $state.go('classAppraisalDetail', {
        voteTaskId: voteTaskId,
        title: title
      });
    };
    
   
  });
angular.module('app.controllers')
  .controller('classAppraisalDetailCtrl', function ($scope, $ionicPopup, Constant, $ionicLoading, $stateParams, Requester, UserPreference, $timeout) {

    $scope.$on("$ionicView.beforeEnter", function (event, data) {

      $scope.isIos = ionic.Platform.isIOS();
      $scope.grades = UserPreference.getArray('VoteGrades');
      $scope.isIOS = ionic.Platform.isIOS()?true:false;
      $scope.gradeName = $scope.grades.length > 0 ? $scope.grades[0].name : '';
      console.log("name" + $scope.gradeName);
      $scope.allList = [];
      $scope.hasPassArr = [];
      $scope.unPassArr = [];
      $scope.hasSetVote = true;
      $scope.getGradeList();

      //获取评比维度

      $scope.gradeSelected = 0;
      $scope.voteTaskId = $stateParams.voteTaskId;
      $scope.title = $stateParams.title;
      $scope.page = 1;
      $scope.getVoteItem(); //评比维度   
      $scope.getGradeVoteResultList(); //刷新列表

    });

    //进入视图时
    $scope.$on("$ionicView.enter", function (event, data) {


    });

    //选择年级 
    $scope.switchGrade = function (t, index) {
      $scope.gradeSelected = index;
      $scope.gradeName = t.name;
      $scope.hasPassArr = [];
      $scope.unPassArr = [];
      $scope.getGradeVoteResultList();

    };


    //***** vote result request */

    //获取年级列表
    $scope.getGradeList = function () {

      Requester.getGradeList().then(function (resp) {
        if (resp.result) {
          $scope.grades = resp.data;
         
          $scope.gradeName = $scope.grades[0].name;
          UserPreference.setObject('VoteGrades', $scope.grades);
        } else {
          $scope.grades = [];
        }
      });
    };

    //获取评比结果列表
    $scope.getGradeVoteResultList = function () {
      $scope.hasPassArr = [];
      $scope.unPassArr = [];
      Requester.getClassVoteReultList($scope.voteTaskId, $scope.gradeName, $scope.page).then(function (resp) {
        if (resp.result) {
         
          $scope.allList = resp.data;
          if (resp.data) {
            resp.data.forEach(function (item) {
              if (item.voteFinished) {
                $scope.hasPassArr.push(item);
              } else {
                $scope.unPassArr.push(item);
              }
            });
          }

        }

      }).finally(function () {

      });
    };

    //查询评比维度 
    $scope.getVoteItem = function () {
      $scope.voteItems = []; //评比维度
      Requester.selectVoteItem().then(function (resp) {

        $scope.hasSetVote = resp.result;
        $scope.voteItems = resp.data;
        return resp.data;
      }).then(function (result) {
        if (result) {
          $scope.voteItems.forEach(function (res) {
            res.value = null;
          });
        } else {
          //toaster.warning({ title: "温馨提示", body: "当前还未设置评比维度，请登录管理后台处理!" });
        }
      });
    };
    //修改或新增评分
    $scope.fixOrAddMarkRequest = function (tag, classId, classvoteDetailId, classVoteItem) {
      if (tag == 1) {
        Requester.fixClassScoreResult(classId, classvoteDetailId, classVoteItem).then(function (resp) {
          console.log(resp.data);
          if (resp.result) {
            
            $scope.getGradeVoteResultList(); //刷新列表
          }else{
          }
        });
      } else {
        Requester.addClassScoreResult(classId, classvoteDetailId, classVoteItem).then(function (resp) {
          if (resp.result) {
           
            console.log(resp.data);
            $scope.getGradeVoteResultList(); //刷新列表
          }
        });
      }

    };


    //***** request end */


    //添加平分或者修改平分
    $scope.addGradedOrFix = function (tag, item) {
      console.log(item);
      // $scope.MyFunction = function() {
      //   return true;
      // };

      if (!$scope.data)
        $scope.data = {
          health: null,
          discipline: null,
          attendance: null,
          example1: null,
          example2: null
        };
      var arrLength = item.voteItemScore == null ? 0 : item.voteItemScore.length;
      //$scope.voteItems = [];
      if (item.voteItemScore != null) {
        $scope.voteItems = item.voteItemScore;
      } else {

      }

     $scope.btnStatus = '';
     var UIPath = '';
     if (Constant.debugMode) UIPath = 'page/';
      var myPopup = $ionicPopup.show({
        templateUrl:UIPath+ 'home/classAppraisal/classAppraisalAlert.html',
       //template: '<div ng-repeat="voteItem in voteItems"><label class="item-input" style="padding-left:0px !important;"> <span class="input-label" style="width:70px;text-align:right;">' + '{{voteItem.name}}' + ':</span><input type="number"  pattern="[0-9]*" oninput="if(value.length>3)value=value.slice(0,3)" style="border:1px solid #CFCFCF;padding-left:4px;padding-right:5px;height:30px;"  placeholder="1-100" ng-model="voteItem.value" autofocus><span style="margin-left:10px;margin-right:40px;">分</span></label></div>',
        title: '<div> <img src="img/classAppraisal/left.png" /><span>' + item.className + '</span><img src="img/classAppraisal/right.png" /></div>',
        scope: $scope,
        cssClass: 'dateSelectAlert',
        buttons: [{
            text: '取消',
            type: 'button-cancel'
          },
          {
            text: '<b>确定</b>',
            type: 'button-xgreen',
            onTap: function (e) {
              console.log($scope.voteItems);
              for (var i = 0; i < $scope.voteItems.length; i++) {
                if (  $scope.voteItems[i].value == null || $scope.voteItems[i].value == undefined) {
                  $ionicLoading.show({
                    template: '评分不能有空值',
                    duration: 1500
                  });
                  e.preventDefault();
                  break;
                }
                if (!judgeIsInteger($scope.voteItems[i].value)) {
                  $ionicLoading.show({
                    template: '请保持评分在1-100的整数',
                    duration: 1500
                  });
                  e.preventDefault();
                  break;
                }
              }
              var classVoteItem = [];
              for (var j = 0; j < $scope.voteItems.length; j++) {
                classVoteItem.push({
                  id: $scope.voteItems[j].id,
                  score: $scope.voteItems[j].value
                });
              }

              return classVoteItem;

            }
          }
        ]
      });
    
      myPopup.then(function (classVoteItem) {
        //请求
        console.log(classVoteItem);
        if (classVoteItem)
          $scope.fixOrAddMarkRequest(tag, item.classId, item.id, classVoteItem);
      });

    };

    $scope.getDeviceType = function () {
      var result = 'android';
      $scope.isIos = ionic.Platform.isIOS() ;
      if ($scope.isIos) {
          if (window.screen.width >= 375 && window.screen.height >= 812) {
              result = 'iphoneX';
          } else {
              result = 'iphone';
          }

      } else {
       result = 'android';
      }
      return result;
  };

  });
angular.module('app.controllers')
    .controller('repairRecordListCtrl', function ($scope, $ionicModal, Constant, $q, CameraHelper, $timeout, $state, Requester, UserPreference, $ionicLoading, toaster, BroadCast) {

        $scope.$on("$ionicView.enter", function (event, data) {
           // console.log('enter');
            $scope.page = 1; //当前页面
            $scope.repairList = $scope.selectType == 1 ? UserPreference.getArray('WaitFix') ? UserPreference.getArray('WaitFix') : [] : UserPreference.getArray('Closed') ? UserPreference.getArray('Closed') : [];
            $scope.isMoreData = true;
            $scope.repairImgs = [];
            // $scope.repairData = {unclosedSize:0,closedSize:0};
            $scope.selected = {
                terminalAddress: '',
                orderDescribe: '',
                maintainImgs: [],
                assetNumber:''
            };
            $scope.getRepairRecordList($scope.selectType);

        });
        $scope.$on("$ionicView.loaded", function (event, data) {
            $scope.selectType = 1;
        });

        $scope.$on(BroadCast.CONNECT_ERROR, function () {
            $scope.isLoading = false;
            $scope.isMoreData = false;
            $ionicLoading.hide();
        });
        //选择类型
        $scope.selectTypeClick = function (type) {
            $scope.selectType = type;
            $scope.repairList = type == 1 ? UserPreference.getArray('WaitFix') : UserPreference.getArray('Closed');
            $scope.page = 1;
            $scope.isMoreData = true;
            $scope.getRepairRecordList(type);
        };

        //获取报修记录列表
        $scope.getRepairRecordList = function (condition) {
            Requester.getRepairRecordList(condition, $scope.page).then(function (res) {
                if (res.result) {
                    $scope.repairList = res.data.data.content;
                    $scope.repairData = res.data;
                    // UserPreference.setObject('RepairListData',$scope.repairData);
                    if (condition == 1) {
                        UserPreference.setObject('WaitFix', $scope.repairList);
                    } else {
                        UserPreference.setObject('Closed', $scope.repairList);
                    }

                } else {
                    $scope.isMoreData = false;
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };
        //下拉刷新
        $scope.refreshData = function () {
            $scope.page = 0;
            $scope.getRepairRecordList($scope.selectType);
        };

        //上拉加载
        $scope.loadMore = function () {
            $scope.page++;
            Requester.getRepairRecordList($scope.selectType, $scope.page).then(function (res) {
                if (res.result) {
                    res.data.data.content.forEach(function (item) {
                        $scope.repairList.push(item);
                    });
                    //关闭刷新
                    if (!res.data.data.content || res.data.data.content.length < Constant.reqLimit) {
                        $scope.isMoreData = false;
                    }
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        //查看详情
        $scope.goToDetail = function (repairRecordId, readStatus) {

            $state.go('repairDetail', {
                repairRecordId: repairRecordId,
                readStatus: readStatus
            });
        };

        //*** modal */
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'page/';
        $ionicModal.fromTemplateUrl(UIPath + 'home/equipmentRepair/postRepairInfo.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
        });

        $scope.openModal = function () {
            $scope.isLoading = false;
            $scope.requestCount = 0;
            $scope.modal.show();
        };

        $scope.closeModal = function () {
            $scope.modal.hide();
        };

        $scope.selectImg = function () {
            //if(!isWeixin())
            if (window.cordova) {
                CameraHelper.selectMultiImage(function (resp) {
                    console.log(resp);
                    if (!resp)
                        return;
                    if (resp instanceof Array) {

                        Array.prototype.push.apply($scope.repairImgs, resp);
                        $scope.$apply();
                    } else {
                        $scope.repairImgs.push("data:image/jpeg;base64," + resp);
                    }
                }, 9 - $scope.repairImgs.length);
            } else {
                //web 端
                var input = document.getElementById("capture");
                input.click();
            }

        };

        // input形式打开系统系统相册
        $scope.getFile = function (files) {
            var file = files[0];
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (theFile) {
                $timeout(function () {
                    $scope.testImg = theFile.target.result; //设置一个中间值
                    $scope.repairImgs.push($scope.testImg);

                }, 100);
            };
        };

        $scope.removeImg = function (index, $event) {
            $event.stopPropagation();
            $scope.repairImgs.splice(index, 1);
        };

        function onImageResized(resp) {
           
            $scope.selected.maintainImgs.push(resp);
        }

        //立即提交
        $scope.commitRepair = function () {

            var result = checkStringIsIncludeSpecialChar($scope.selected.assetNumber);
            if(!result||($scope.selected.assetNumber&&$scope.selected.assetNumber.length > 30)){
                toaster.warning({
                    title: "提交失败",
                    body: '资产ID不存在',
                    timeout: 3000
                });
             return;
            }
            if (isEmojiCharacter($scope.selected.orderDescribe)) {
                $ionicLoading.show({
                    template: '请不要输入表情字符',
                    duration: 1500
                });
                return;
            }
            $scope.selected.maintainImgs = [];
            var promiseArr = [];
            $scope.repairImgs.forEach(function (img) {
                promiseArr.push(resizeImage(Constant.CAPTURE_IMAGE_RANGE, img).then(onImageResized));
            });
            $scope.isLoading = true;
            //$scope.requestCount++;
            console.log($scope.selected) ;
            $q.all(promiseArr).then(function () {
                // if ($scope.requestCount === 1) {
                    $ionicLoading.show({
                        noBackdrop: true,
                        template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
                    });
                    Requester.addEquipmentRepairRecord($scope.selected).then(function (res) {
                        if (res.result) {
                            toaster.success({
                                title: "提交成功",
                                body: "成功报修，请耐心等待管理员处理",
                                timeout: 3000
                            });

                            $scope.isShow = true;
                            $scope.isLoading = false;
                            $ionicLoading.hide();
                            $scope.closeModal();
                            $scope.repairImgs = [];
                            $scope.selected = {
                                terminalAddress: '',
                                orderDescribe: '',
                                maintainImgs: [],
                                assetNumber:''
                            };

                            return res;
                        }else{
                            $ionicLoading.hide();
                            $scope.isLoading = false;
                            $scope.requestCount = 0;
                          //  $scope.selected =  $scope.selected;
                            toaster.warning({
                                title: "提交失败",
                                body: res.message,
                                timeout: 3000
                            }); 
                        }
                    }).then(function (res) {
                        if (res.result) {
                            //$scope.updateRepairRecordStatus(res.data);
                            $scope.page = 1;
                            $scope.getRepairRecordList($scope.selectType);

                        }
                    }).finally(function(){
                        setTimeout(function(){
                            $scope.isLoading = false;
                        },5000);
                    });
               // }

            });


        };

        $scope.updateRepairRecordStatus = function (repairRecordId) {
            Requester.addRepairProgress(repairRecordId, 2, '').then(function (res) {
                $scope.page = 1;
                $scope.getRepairRecordList($scope.selectType);
            });
        };

        //扫码
        $scope.scanningView = function(event){
            $scope.isIos = ionic.Platform.isIOS();
           if( $scope.isIos){
            $scope.getAssetsIdByScanning();
           }else{
            var permissions = cordova.plugins.permissions;
            permissions.hasPermission(permissions.CAMERA, function (status) {
                if (!status.hasPermission) {
                    var errorCallback = function () {};
                    permissions.requestPermission(
                        permissions.CAMERA,
                        function (status) {
                            if (!status.hasPermission) {
                                errorCallback();
                            } else {
                                $scope.getAssetsIdByScanning();
                            }
                        }, errorCallback);
                } else
                    $scope.getAssetsIdByScanning();
            }, null);
           }
           
        };


        $scope.getAssetsIdByScanning = function () {
            window.scanning.startScanning(function (result) {
                $scope.selected.assetNumber = '';
                Requester.getAssetsIdByScan(result).then(function (res) {
                    if (res.result) {
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.selected.assetNumber = res.data;
                            });

                        }, 200);
                    } else {
                        toaster.warning({
                            title: "获取失败",
                            body: res.message,
                            timeout: 3000
                        });
                    }

                });

            }, function (error) {
                // alert('error');
            }, {});
        };

    });
angular.module('app.controllers')
    .controller('repairDetailCtrl', function ($scope, $rootScope, $state, Constant, $timeout, Requester, SavePhotoTool, $stateParams, UserPreference) {

        //收到通知 保存图片
        $rootScope.$on('saveImgUrl', function (event, url) {
            SavePhotoTool.savePhoto(url);
        });

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.isIos = ionic.Platform.isIOS();
            $scope.repairRecordId = $stateParams.repairRecordId;
            $scope.readStatus = $stateParams.readStatus;
            // $scope.picUrls = UserPreference.getArray('RepairPictures');
            // $scope.repairDetail = UserPreference.getObject('RepairDetail');
            // $scope.events = UserPreference.getArray('RepairEvents');
            if ($scope.readStatus) {
                $scope.getRepairDetail();

            } else {
                $scope.updateRepairRecordStatus();

            }
        });

        //即将进入
        $scope.$on("$ionicView.enter", function (event, data) {

        });



        //获取维修记录详情
        $scope.getRepairDetail = function () {
            Requester.getRepairRecordDetail($scope.repairRecordId).then(function (res) {
                if (res.result) {
                    $scope.repairDetail = res.data;
                    $scope.picUrls = [];
                    $scope.events = []; //维修进度
                    $scope.manufacturersContactId = res.data.manufacturersContactId;//售后人员id
                    $scope.manufacturersId = res.data.manufacturersId;//厂家id
                    $scope.isAssociated = res.data.manufacturersContactId;//是否关联了厂家 售后人员id
                    if ($scope.repairDetail) {
                        $scope.repairDetail.tbMaintainImgs.forEach(function (pic) {
                            $scope.picUrls.push({
                                src: pic.url,
                                thumb: pic.thumb
                            });
                        });
                        // UserPreference.setObject('RepairDetail', $scope.repairDetail);
                        // UserPreference.setObject('RepairPictures', $scope.picUrls);
                       console.log($scope.repairDetail); 
                        var count = 0;
                        for (var i = 0; i < $scope.repairDetail.tbMaintainProgresses.length; i++) {
                            var pro = $scope.repairDetail.tbMaintainProgresses[i];
                            var icon;
                            if ($scope.repairDetail.tbMaintainProgresses.length > 2) {
                                if (i == 0) {
                                    icon = 'img/repair/selectRepair.png';
                                    // if(pro.orderProgress==Math.pow(2,30)){
                                    //     $scope.isClose = true; 
                                    //  }
                                } else if (i == $scope.repairDetail.tbMaintainProgresses.length - 1) {
                                    icon = 'img/repair/normalRepair.png';
                                } else if (i == $scope.repairDetail.tbMaintainProgresses.length - 2) {
                                    icon = 'img/repair/normalEmail.png';
                                } else {
                                    icon = 'img/repair/normalRepair.png';
                                }
                            } else {
                                if (i == $scope.repairDetail.tbMaintainProgresses.length - 1) {
                                    icon = 'img/repair/normalRepair.png';

                                } else {
                                    icon = 'img/repair/normalEmail.png';
                                    if (pro.orderProgress == Math.pow(2, 30)) {
                                        icon = 'img/repair/selectRepair.png';
                                        // $scope.isClose = true; 
                                    }
                                }
                            }
                            var manStatus = pro.orderProgress == 1 ? '申请人: ' : '处理人: '; //pro.orderProgress == 8 ? '通知售后:':pro.orderProgress == 16?'售后回复:':'处理人: ';
                            var userPhone = i == $scope.repairDetail.tbMaintainProgresses.length - 1 ? pro.userPhone : '';
                            if (pro.orderProgress == 8 ) {
                               // $scope.manufacturersContactId = pro.manufacturersContactId;
                               // count++;
                            }
                            $scope.events.push({
                                iconImg: icon,
                                badgeIconClass: 'tm-icon',
                                title: pro.progressName,
                                operateMan: manStatus + pro.userName,
                                operatePhone: userPhone,
                                text: pro.progressDescribe,
                                date: pro.operateTime.substr(5, 6),
                                time: pro.operateTime.substr(11, 5)
                            });

                        }
                        for (var k = 0; k < $scope.repairDetail.tbMaintainProgresses.length; k++) {
                            var proc = $scope.repairDetail.tbMaintainProgresses[k];
                            if (proc.orderProgress == Math.pow(2, 30)) {
                                $scope.isClose = false;
                                return;
                            } else {
                                $scope.isClose = true;
                            }
                        }
                        // UserPreference.setObject('RepairEvents', $scope.events);

                    }

                } else {

                }
            });
        };

        //修改报修记录已读状态
        $scope.updateRepairRecordStatus = function () {
            Requester.addRepairProgress($scope.repairRecordId, 2, '').then(function (res) {
                if (res.result) {
                    // console.log('update is readed');
                    $scope.getRepairDetail();
                }
            });
        };


        //改变维修进度
        $scope.fixRepairProgress = function () {
            $state.go('repairProgress', {
                repairRecordId: $scope.repairRecordId,
                manufacturersContactId: $scope.manufacturersContactId,
                manufacturersId : $scope.manufacturersId,
                isAssociated:$scope.isAssociated
            });
        };
    });
angular.module('app.controllers')
.controller('repairProgressCtrl',function($scope,Requester,$stateParams, $ionicHistory,toaster){
    $scope.$on("$ionicView.beforeEnter", function (event, data){
       
        $scope.repairRecordId = $stateParams.repairRecordId;
        $scope.isNotice = $stateParams.manufacturersContactId?true:false;//是否已经通知过售后了
        console.log('$scope.isNotice:'+$scope.isNotice+'manufacturersContactId:'+ $stateParams.manufacturersContactId);
        $scope.isAssociated  = $stateParams.isAssociated;//关联的厂家id
        //进度状态
        $scope.progressStatues = [
            {key:1,value:'已经联系售后了'},
            {key:2,value:'今天会解决'},
            {key:3,value:'马上过来修'},
            {key:4,value:'设备没坏不用修'},
            {key:5,value:'设备修好了'},
            {key:6,value:'设备报废了'},
        ];
        $scope.closedNum =  Math.pow(2,30);
        //提交进度信息
        $scope.select = {
          key:0,
          status :$scope.isNotice===true?8:4,
          content:'',
          manufacturersContactId: $stateParams.manufacturersId ?$stateParams.manufacturersId :'',
          saleMerberId: $scope.isAssociated?$scope.isAssociated:$stateParams.manufacturersContactId,
        };
        // console.log($scope.select);
        $scope.saleMerbers = [];
        if($scope.select.manufacturersContactId){
            //获取售后人员
            $scope.getManufacturesMermberList();
        }      
        $scope.getManufacturesList();
    
       //$scope.select.vendorId = $scope.vendorList[1].id;
       
     });

     $scope.changeSelectStatus = function(status){
        $scope.select.status = status;
     };
     $scope.loadData = function(){
        //  console.log('--load data');
        $scope.saleMerbers = [];
        $scope.select.saleMerberId = '';
        if($scope.select.manufacturersContactId)
        $scope.getManufacturesMermberList();
     };
     $scope.loadData2 = function(){
       
     };
     //确定
     $scope.confirm = function(){
        
        if($scope.select.status!==8)$scope.select.saleMerberId = null;
         Requester.addRepairProgress($scope.repairRecordId, $scope.select.status,$scope.select.content, $scope.select.saleMerberId).then(function(res){
            if(res.result){ 
                if($scope.select.status== Math.pow(2,30)){
                    toaster.success({title: "关闭成功", body: "该报修记录成功关闭",timeout:3000});
                }else{
                    toaster.success({title: "更新成功", body: "该报修记录更新进度成功",timeout:3000});
                }
                $ionicHistory.goBack();
                $scope.select = {
                    key:0,
                    status :4,
                    content:''
                  };
            }else{

            }
         });
     };

     //获取厂家列表
     $scope.getManufacturesList  = function(){
        Requester.getManufacturesList().then(function(resp){
            if(resp.result){
                //$scope.manufacturersList = resp.data;
                if(resp.data&&resp.data.length>0){
                    $scope.manufacturersList =  resp.data;
                    
                }
            }else{
                toaster.warning({
                    title: "温馨提示",
                    body: resp.message
                });
            }

        }) ;
     };

     //获取售后人员列表
     $scope.getManufacturesMermberList = function(){
        Requester.getManufacturesMermberList($scope.select.manufacturersContactId).then(function(resp){
            if(resp.result){
                console.log(resp.data);
                $scope.saleMerbers = resp.data;
            }else{
                toaster.warning({
                    title: "温馨提示",
                    body: resp.message
                });
            }
        });
     };
     


   
});
angular.module('app.controllers')
  .controller('assetsCheckDetailCtrl', function ($scope, $ionicModal, Constant, $stateParams, $state, toaster, $ionicPopup, Requester, $ionicLoading) {

    $scope.$on("$ionicView.beforeEnter", function (event, data) {
      $scope.assetsId = $stateParams.assetsId;
      $scope.progressStatus = $stateParams.progressStatus;
      $scope.select = {};

      $scope.hasGot = Math.pow(2, 30);
      $scope.cancelStatus = Math.pow(2, 31) - 1;

      if ($scope.progressStatus == 1) {
        $scope.fixAssetsApplyStatus(2);
      } else {
        $scope.fixAssetsApplyStatus(2);
      }
    });
    //即将进入
    $scope.$on("$ionicView.enter", function (event, data) {

    });



    //资产详情Request
    $scope.getAssetsDetail = function () {
      $scope.events = [];
      Requester.assetsApplyDetail($scope.assetsId).then(function (res) {
        if (res.result) {
          $scope.assetsDetail = res.data;
          $scope.progressStatus = res.data.orderProgress;
          if (res.data.progress && res.data.progress.length > 0) {
            for (var i = 0; i < res.data.progress.length; i++) {
              var assetsProgress = res.data.progress[i];
              var checkStatus = assetsProgress.orderProgress;
              var progressName = checkStatus == 1 ? '提交申请' : checkStatus == 2 ? '正在审核' : checkStatus == 4 ? '拒绝申请' : checkStatus == 8 ? '审核通过' : checkStatus == $scope.hasGot ? '已领取' : checkStatus == $scope.cancelStatus ? '已取消' : '审核通过';
              var prefix = checkStatus == 1 ? '申请人:' : checkStatus == 4 ? '审批人:' : checkStatus == 8 ? '审批人:' : checkStatus == $scope.hasGot ? '发放人:' : '审批人:';
              var afterFix = assetsProgress.userDepartment ? '(' + assetsProgress.userDepartment + '）' : '';
              if (checkStatus != 2) {
                $scope.events.push({
                  badgeIconClass: 'tm-icon',
                  title: progressName,
                  operateMan: prefix + ' ' + assetsProgress.userName + afterFix,
                  text: checkStatus == $scope.hasGot ? '查看资产ID' : assetsProgress.progressDescribe,
                  date: assetsProgress.operateTime.substr(0, 10),
                  status: assetsProgress.orderProgress
                });
              }

            }
          }


        } else {

        }
      });
    };

    $scope.readAssetsId = function (event) {
      if (event.status == $scope.hasGot) {
        $scope.idModal.show();
        $scope.queryAssetsIds();
      }
    };




    $scope.checkAssets = function (type) {

      if (type == 1) {
        //通过
        $scope.modal.show();
      } else {
        //拒绝
        $scope.select = {};
        $scope.myPopup = $ionicPopup.show({
          template: '<textarea name="chatInput" style="width:100%;height: 100px;font-size:15px; padding: 7px 8px;background-color:#F3F4F6;border-radius: 8px;margin-top:15px;"placeholder="必填,200字以内" maxlength="200"  ng-model="select.refusedReason" ></textarea>',
          title: '<div> <img src="img/classAppraisal/left.png" /><span>拒绝申请</span><img src="img/classAppraisal/right.png" /></div>',
          scope: $scope,
          cssClass: 'dateSelectAlert',
          buttons: [{
              text: '取消',
              type: 'button-cancel'
            },
            {
              text: '<b>确定</b>',
              type: 'button-xgreen',
              onTap: function (e) {
                if (!$scope.select.refusedReason) {
                  $ionicLoading.show({
                    template: '请填写拒绝申请的理由',
                    duration: 1500
                  });
                  e.preventDefault();
                  return;
                }
                $scope.fixAssetsApplyStatus(4, $scope.select.refusedReason);
                e.preventDefault();
                return;
              }
            }
          ]
        });

        $scope.myPopup.then(function (res) {


        });
      }
    };




    /**
     * modal 审核通过
     */
    var UIPath = '';
    if (Constant.debugMode) UIPath = 'page/';
    $ionicModal.fromTemplateUrl(UIPath + 'home/assets/assetsCheckPass.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.modal = modal;
    });
    $scope.closeModal = function () {
      $scope.modal.hide();

    };

    /**
     * modal 查看资产id
     */
    // var UIPath = '';
    // if (Constant.debugMode) UIPath = 'page/';
    $ionicModal.fromTemplateUrl(UIPath + 'home/assets/assetsIds.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {

      $scope.idModal = modal;
    });

    $scope.closeIdModal = function () {
      $scope.idModal.hide();
    };

    //已申领的资产id 
    $scope.queryAssetsIds = function () {
      Requester.queryAssetsApplyIds($scope.assetsId).then(function (res) {
        if (res.result) {
          $scope.assetsApplyIds = res.data;
        }
      });
    };

    //审核通过
    $scope.applyPass = function () {
      if ($scope.select.count > $scope.assetsDetail.applyAmount || $scope.select.count < 1) {
        toaster.warning({
          title: '审核失败',
          body: "审批数量错误",
          timeout: 3000
        });
        return;
      }
      $scope.fixAssetsApplyStatus(8, $scope.select.content, $scope.select.count);
    };

    //将资产申领标记为已读
    $scope.fixAssetsApplyStatus = function (status, describe, fixApplyCount) {
      //staus : 2:管理员已查;4:拒绝;8:审核通过
      Requester.addAssetsApplyProgress($scope.assetsId, status, fixApplyCount, describe).then(function (res) {
        if (res.result) {
          $scope.getAssetsDetail();
          if (status == 8) {
            //查看详情     
            toaster.success({
              title: "审核成功",
              body: "资产申领审核通过",
              timeout: 3000
            });
            $scope.modal.hide();
          } else if (status == 4) {
            //查看详情
            toaster.success({
              title: "审核成功",
              body: "资产申领审核拒绝",
              timeout: 3000
            });
            $scope.myPopup.close();
          }

        } else {
          toaster.warning({
            title: '审核失败',
            body: res.message,
            timeout: 3000
          });
          $scope.myPopup.close();
        }
      });
    };


  });
angular.module('app.controllers')
    .controller('assetsCheckListCtrl', function ($scope, $ionicModal, Constant, $state, Requester,BroadCast,$ionicLoading,UserPreference) {

        //即将进入
        $scope.$on("$ionicView.enter", function (event, data) {

            $scope.isMoreData = true;
            $scope.page = 1;
            $scope.hasGot = Math.pow(2, 30);
            $scope.assetsList = UserPreference.getArray('AssetsApplyList');
            $scope.getAssetsApplyList();
        });


        $scope.$on(BroadCast.CONNECT_ERROR, function () {
             $ionicLoading.hide();
            $scope.isMoreData = false;
        });

        //下拉刷新
        $scope.refreshData = function () {
            $scope.page = 1;
             $scope.getAssetsApplyList();
        };

        //上拉加载
        $scope.loadMore = function () {
            $scope.page++;
            $scope.loading = true;
            Requester.manageAssetsApplyList($scope.page).then(function (res) {
                if (res.result) {
                    $scope.loading = false;
                    if (res.data.content && res.data.content.length > 0) {
                        for (var i = 0; i < res.data.content.length; i++) {
                            $scope.assetsList.push(res.data.content(i));
                        }
                    }
                    //关闭刷新
                    if (!res.data.content || res.data.content.length < Constant.reqLimit) {
                        $scope.isMoreData = false;
                    }
                } else {
                    $scope.isMoreData = false;
                }

            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        //获取资产申领的列表
        //审核状态 1:已提交申请;2:管理员已查;4:已拒绝;8:待领取;2^30:已领取;maxinteger:已取消;
        $scope.getAssetsApplyList = function () {
            $scope.loading = true;
            Requester.manageAssetsApplyList($scope.page).then(function (res) {
                if (res.result) {
                    $scope.loading = false;
                    $scope.assetsList = res.data.content;
                    UserPreference.setObject('AssetsApplyList',res.data.content);
                } else {
                    $scope.isMoreData = false;
                }

            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };



       

        //查看申领详情
        $scope.goToDetail = function (assets) {
        
            $state.go('assetsCheckDetail',{assetsId:assets.id,progressStatus:assets.orderProgress});
        };



    });

angular.module('app.controllers')
    .controller('approvalListCtrl', function ($scope, $state, $ionicModal, Constant, $window, BroadCast, $ionicLoading, Requester, UserPreference, toaster) {

        $scope.$on("$ionicView.enter", function (event, data) {

            $scope.page = 1;
            $scope.getApprovalManageList();
        });
        $scope.$on("$ionicView.loaded", function (event, data) {


            $scope.schoolId = UserPreference.get('DefaultSchoolID');
            //console.log('schoolId:' + $scope.schoolId);
            $scope.select = {
                selectIndex2: '',
            };
            $scope.recordList = [];
            $scope.allList = [];
            $scope.passList = [];
            $scope.failList = [];
            $scope.checkList = [];
            $scope.isMoreData = true;
            $scope.progress = '';

        });

        //审批状态选择
        $scope.tab2Click = function (progress) {
            $scope.select.selectIndex2 = progress;
            $scope.progress = progress == 1 ? 'PASS' : progress == 2 ? 'APPROVAL' : progress == 3 ? 'FAIL' : '';
            $scope.page = 1;
            $scope.isLoading = false;
            $scope.isMoreData = true;
            $scope.getApprovalManageList();
        };

        //下拉刷新
        $scope.refreshData = function () {
            $scope.page = 1;
            $scope.getApprovalManageList();

        };

        //上拉加载
        $scope.loadMore = function () {
            $scope.page++;
            $scope.isLoading = true;
            $scope.isMoreData = true;
            // $scope.getApprovalManageList();
            $scope.loadRequest();
        };


        //查看审批详情
        $scope.goToDetail = function (approvalId) {
            $state.go('approvalDetail', {
                approvalId: approvalId
            });
        };


        $scope.getList = function (type, arr) {
            if (type === 1) {
                $scope.passList = arr;
            } else if (type === 2) {
                $scope.checkList = arr;
            } else if (type === 3) {
                $scope.failList = arr;
            } else {
                $scope.allList = arr;
            }
        };

        $scope.getRecordList = function (type) {
            if (type === 1) {
                $scope.recordList = $scope.passList;
            } else if (type === 2) {
                $scope.recordList = $scope.checkList;
            } else if (type === 3) {
                $scope.recordList = $scope.failList;
            } else {
                $scope.recordList = $scope.allList;
            }
            //console.log($scope.recordList);
        };

        $scope.currentApprovalStatus = function (progress) {
            var result;
            result = progress == 'PASS' ? '通过' : progress == 'APPROVAL' ? '审批中' : progress == 'FAIL' ? '不通过' : '';
            return result;
        };

        //request--查看列表
        $scope.getApprovalManageList = function () {

            Requester.getApprovalManageList($scope.page, $scope.schoolId, $scope.select.selectIndex2).then(function (data) {
                if (data.result) {
                    $scope.getList($scope.select.selectIndex2, data.data.content);

                    $scope.getRecordList($scope.select.selectIndex2);
                } else {
                    $scope.isMoreData = false;
                    toaster.warning({
                        title: "温馨提示",
                        body: data.message
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        //刷新方法
        $scope.loadRequest = function () {
            Requester.getApprovalManageList($scope.page, $scope.schoolId, $scope.select.selectIndex2).then(function (data) {
                if (data.result) {
                    for (var i = 0; i < data.data.content.length; i++) {
                        if ($scope.select.selectIndex2 === 1) {
                            $scope.passList.push(data.data.content[i]);
                        } else if ($scope.select.selectIndex2 === 2) {
                            $scope.checkList.push(data.data.content[i]);
                        } else if ($scope.select.selectIndex2 === 3) {
                            $scope.failList.push(data.data.content[i]);
                        } else {
                            $scope.allList.push(data.data.content[i]);
                        }
                    }
                    //关闭刷新
                    if (!data.data.content || data.data.content.length < Constant.reqLimit) {
                        $scope.isMoreData = false;
                    }
                    $scope.getRecordList($scope.select.selectIndex2);
                    // $scope.recordList.push(data.data.content[i]);
                } else {
                    $scope.isMoreData = false;
                    toaster.warning({
                        title: "温馨提示",
                        body: data.message
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

    });

angular.module('app.controllers')
   .controller('approvalDetailCtrl', function ($scope, $stateParams, Constant, Requester, DownloadFile) {
      $scope.$on("$ionicView.enter", function (event, data) {
         $scope.approvalId = $stateParams.approvalId;
         //提交审批
         $scope.select = {
            detail: {}
         };
         $scope.imgCount = 0;
         $scope.getApprovalManageDetail();
      });

      $scope.getNameWidth = function (nameStr) {
         var width = textWidth(nameStr, 16) + 30 + 'px';
         return width;
      };
      //查看文件
      $scope.openFile = function (url) {
         //  url = 'https://test17.xgenban.com/wmdp/comup/201805/18/19603499049049307.pdf';
         DownloadFile.readFile(url);
      };

      $scope.currentApprovalStatus = function (progress) {
         var result;
         result = progress == 'PASS' ? '通过' : progress == 'APPROVAL' ? '审批中' : progress == 'FAIL' ? '不通过' : '';
         return result;
      };


      //request--detail
      $scope.getApprovalManageDetail = function () {
         $scope.picUrls = [];
         $scope.docs = [];
         $scope.controls = [];
         $scope.inputs = [];//单行文本控件数组
         $scope.fulltexts = [];//多行文本控件数组
         $scope.dates = [];//日期控件数组
         $scope.dateRanges = [];//日期区间数组
         $scope.radios = [];//单选框控件数组
         $scope.checkboxes = [];//多选框控件数组
         $scope.timeList = [];//时间控件数组

         Requester.getApprovalManageDetail($scope.approvalId).then(function (data) {
            if (data.result) {
               $scope.select.detail = data.data;
               $scope.inputs = data.data.controls.inputs;
               $scope.fulltexts =  data.data.controls.fulltexts;
               $scope.dates = data.data.controls.dates;
               $scope.dateRanges = data.data.controls.dateRanges?data.data.controls.dateRanges:[];
               $scope.radios =  data.data.controls.radios;
               $scope.checkboxes =  data.data.controls.checkboxes;

               $scope.inputs.forEach(function(item){
                  $scope.controls.push({
                     name:item.name,
                     text:item.text,
                     type:1,
                     index:item.idx?item.idx:1
                  });
               });
               
               $scope.fulltexts.forEach(function(item){
                  $scope.controls.push({
                     name:item.name,
                     fulltext:item.fulltext,
                     type:2,
                     index:item.idx?item.idx:2
                  });
               });

               $scope.dates.forEach(function(item){
                  $scope.controls.push({
                     name:item.name,
                      date:item.date,
                     type:3,
                     index:item.idx?item.idx:3
                  });
               });

               $scope.dateRanges.forEach(function(item){
                  $scope.controls.push({
                     name:item.name,
                     startDate:item.startDate,
                     endDate:item.endDate,
                     type:4,
                     index:item.idx?item.idx:4
                  });
               });
               $scope.radios.forEach(function(item){
                  $scope.controls.push({
                     name:item.name,
                     checkOption:item.checkOption,
                     type:5,
                     index:item.idx?item.idx:5
                  });
               });
               $scope.checkboxes.forEach(function(item){
                  $scope.controls.push({
                     name:item.name,
                     checkOptions:item.checkOptions,
                     type:6,
                     index:item.idx?item.idx:6
                  });
               });
               
               $scope.timeList.forEach(function(item){
                  $scope.controls.push({
                     name:item.name,
                     time:item.time,
                     type:7,
                     index:item.idx?item.idx:7
                  });
               });



               for (var i = 0; i < data.data.attaches.length; i++) {
                  var url = data.data.attaches[i].attachUrl;
                  var fileType = url.slice(url.lastIndexOf('.') + 1);
                  if (fileType === 'png' || fileType === 'jpg' || fileType === 'jpeg' || fileType === 'bmp'|| fileType.length>10) {
                     $scope.picUrls.push({
                        src: url,
                        thumb: url
                     });
                  } else {
                     $scope.docs.push({
                        attachUrl: url,
                        attachName: data.data.attaches[i].attachName
                     });
                  }
               }
            } else {
               console.log('detail failed');
            }


         });
      };
   });
angular.module('app.controllers')
    .controller('schoolPageCtrl', function ($q, $scope, $ionicModal, CameraHelper, BroadCast,$timeout, UserPreference, SchoolService, MESSAGES, toaster, $state, Constant, $ionicLoading, $ionicPopup, $ionicListDelegate, ionicImageView, closePopupService, Requester, $rootScope) {
        //收到通知 保存图片
        $rootScope.$on('saveImgUrl', function (event, url) {
            var config = {
                allowSave: true,
                album: 'Xgenban'
            };
            $scope.config = angular.extend({}, config, {
                allowSave: true
            });
            if (window.cordova) {
                cordova.plugins.photoLibrary.getAlbums(
                    function (result) {
                        saveImgUrl(url);
                    },
                    function (err) {
                        if (err.startsWith('Permission')) {
                            cordova.plugins.photoLibrary.requestAuthorization(
                                function () {
                                    saveImgUrl(url);
                                },
                                function (err) {
                                    // User denied the access
                                    console.log(err);
                                }, {
                                    read: true,
                                    write: true
                                }
                            );
                        }
                    }
                );

            }
        });

        function saveImgUrl(url) {
            if (window.cordova) {
                cordova.plugins.photoLibrary.saveImage(url + '?ext=.jpg', $scope.config.album, function (libraryItem) {
                    $ionicLoading.show({
                        template: '保存成功',
                        duration: 1500
                    });
                }, function (err) {
                    console.log(err);
                });
            }

        }


        $scope.$on("$ionicView.enter", function (event, data) {
            $scope.getCampusUnread();
            if (!$scope.newsTypeSelectable || $scope.newsTypeSelectable.length === 0) {
                SchoolService.getNewsType();
            }
        });

        if (window.cordova) MobclickAgent.onEvent('app_school_page');
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'page/';
        $ionicModal.fromTemplateUrl(UIPath + 'compus/new_news.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
        });
        $scope.openModal = function () {
            $scope.modal.show();
        };
        $scope.closeModal = function () {
            $scope.modal.hide();
        };
        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function () {
            $scope.modal.remove();
        });


        $scope.canCancel = function (item) {
            if (item.authorId == $scope.userId)
                return false;
            return item.state == Constant.NEWS_STATUS.ADMIN_REVIEW_PASS.key && (!item.role || item.role === 'school_admin');

        };
        $scope.canDelete = function (item) {
            return item.authorId == $scope.userId;
        };

        $scope.openTypeSelectModal = function () {
            $scope.typeChooser = $ionicPopup.show({
                template: '<div class="item" ng-repeat="item in newsTypeQueryable" ng-click="onTypeSelected(item)" style="padding: 10px 20px;border-radius: 0">{{item.name}}</div>',
                title: '风采类型',
                scope: $scope
            });
            closePopupService.register($scope.typeChooser);
        };

        $scope.onTypeSelected = function (item) {
            $scope.queryParam.key = item.key;
            $scope.queryParam.parentKey = item.parentKey;
            $scope.selectedTypeame = item.name;
            $scope.page = 1;
            console.log($scope.queryParam);
            SchoolService.getNewsList($scope.page, $scope.queryParam, $scope.reqId);
            if ($scope.typeChooser)
                $scope.typeChooser.close();
        };


        function getFilterList() {
            $scope.newsTypeList = UserPreference.getArray('news_type');
            $scope.newsTypeSelectable = UserPreference.getArray('news_type_selectable');
            $scope.newsTypeQueryable = angular.copy($scope.newsTypeSelectable);
            $scope.newsTypeQueryable.unshift({
                key: undefined,
                parentKey: undefined,
                name: '全部类别'
            });
        }

        $scope.changeFilter = function (arg) {
            if ($scope.activeTab === arg) {
                return;
            }
            $scope.activeTab = arg;
            $scope.queryParam = {};
            if (arg === 'pending') {
                $scope.queryParam.status = Constant.NEWS_STATUS.ADMIN_REVIEW.key;
                $scope.disableTypeSelect = true;
            } else {
                $scope.queryParam.status = Constant.NEWS_STATUS.ADMIN_REVIEW_PASS.key;
                $scope.disableTypeSelect = false;
            }
            $scope.reqId++;
            $scope.onTypeSelected({
                key: undefined,
                parentKey: undefined,
                name: '全部类别'
            });
        };


        $scope.attachments = [];
        $scope.selected = {};
        $scope.queryParam = {
            status: Constant.NEWS_STATUS.ADMIN_REVIEW_PASS.key
        };
        $scope.activeTab = 'all';
        $scope.selectedTypeame = '全部类别';
        $scope.userRole = UserPreference.getObject('user').role;
        $scope.userId = UserPreference.getObject('user').id;

        $scope.listFilter = {
            ispublish: true
        };
        $scope.page = 1;
        $scope.reqId = 0;
        $scope.newsList = UserPreference.getArray('news_list_cache');

        $scope.listHasMore = SchoolService.listHasMore;
        $scope.bannerList = UserPreference.getArray('news_banner_cache');
        $scope.classes = UserPreference.getArray('class');
        $scope.schoolName = UserPreference.get('DefaultSchoolName');
        getFilterList();

        SchoolService.getNewsType();
        SchoolService.getNewsList(1, $scope.queryParam);
        SchoolService.getBannerList();

        $scope.loadData = function (reset) {
            if (Constant.debugMode) console.log('load news restart ' + reset);
            if (reset) {
                $scope.page = 1;
                SchoolService.getBannerList();
            } else if ($scope.listHasMore)
                $scope.page++;
            else
                return;

            SchoolService.getNewsList($scope.page, $scope.queryParam);
            $scope.getCampusUnread();
        };

        $scope.getDisplayTime = function (time) {
            var now = new Date().Format("yyyy-MM-dd");
            if (now === time.substr(0, 10))
                return time.substr(11, 5);
            else
                return time.substr(5, 11);
        };

        $scope.getNewsType = function (key) {
            for (var i = 0; i < $scope.newsTypeList.length; i++) {
                if (key == $scope.newsTypeList[i].key)
                    return $scope.newsTypeList[i].name;
            }
            return '';
        };

        $scope.delHtmlTag = function (str) {
            return delHtmlTag(str);
        };

        $scope.deleteItem = function (item, $event) {
            $event.stopPropagation();
            var pre = '';
            if (item.top)
                pre = '该风采已置顶。';
            var confirmPopup = $ionicPopup.confirm({
                title: '删除确认',
                template: pre + '确认删除?',
                cancelText: '取消',
                okText: '确认',
                okType: 'button-balanced'
            });
            confirmPopup.then(function (res) {
                if (res) {
                    SchoolService.deleteNews(item.id);
                    $ionicListDelegate.closeOptionButtons();
                }
            });
        };
        $scope.cancelItem = function (item, $event) {
            $event.stopPropagation();
            var pre = '';
            if (item.top)
                pre = '该风采已置顶。';
            var confirmPopup = $ionicPopup.confirm({
                title: '撤回确认',
                template: pre + '撤回后该条消息将被忽略，确认撤回?',
                cancelText: '取消',
                okText: '确认',
                okType: 'button-balanced'
            });
            confirmPopup.then(function (res) {
                if (res) {
                    SchoolService.cancelPublish(item.id);
                    $ionicListDelegate.closeOptionButtons();
                }
            });
        };

        //点赞
        $scope.favorNews = function (item, $event) {
            $event.stopPropagation();
            if (item.isFavor === true) {
                Requester.unfavorCampus(item.id);
                if (item.praiseNum > 0)
                    item.praiseNum--;
            } else {
                Requester.favorCampus(item.id);
                item.praiseNum++;
            }
            item.isFavor = !item.isFavor;
        };


        $scope.$on(BroadCast.NEWS_LIST_REV, function (a, rst) {
            if (rst && rst.result) {
                if (!rst.reqId || (rst.reqId && rst.reqId === $scope.reqId)) {
                    $scope.newsList = SchoolService.list;
                    $scope.newsList.forEach(function (item) {
                        item.picUrls = [];
                        item.imageUrls.forEach(function (url) {
                            $scope.urlItem = {
                                thumb: '',
                                src: ''
                            };
                            $scope.urlItem.thumb = url;
                            $scope.urlItem.src = url;
                            item.picUrls.push($scope.urlItem);
                        });
                    });
                } else {
                    if (Constant.debugMode) console.log('req is deprecated');
                }
            }
            $scope.listHasMore = SchoolService.listHasMore;
            $scope.$broadcast('scroll.refreshComplete');
            $scope.$broadcast('scroll.infiniteScrollComplete');
        });

        function onImageResized(resp) {
            var start = resp.indexOf('base64,');
            $scope.selected.picdatas.push(resp.substr(start + 7));
        }

        $scope.save = function () {
            if (!$scope.selected.key || $scope.selected.key === '') {
                toaster.warning({
                    title: MESSAGES.OPERATE_ERROR,
                    body: MESSAGES.NO_NEWS_TYPE
                });
                return;
            }

            if ($scope.attachments.length === 0) {
                toaster.warning({
                    title: MESSAGES.OPERATE_ERROR,
                    body: MESSAGES.AT_LEAST_IMAGE
                });
                return;
            }
            $scope.selected.picdatas = [];
            var promiseArr = [];
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
            $scope.attachments.forEach(function (libraryItem) {
                if (libraryItem.data) {
                    onImageResized(libraryItem.data);
                } else {
                    promiseArr.push(resizeImage(Constant.CAPTURE_IMAGE_RANGE, libraryItem).then(onImageResized));
                }
            });
            $q.all(promiseArr).then(function () {
                SchoolService.newNews($scope.selected);
            });
        };

        $scope.$on(BroadCast.NEW_NEWS, function (a, rst) {
            if (rst && rst.result) {
                toaster.success({
                    title: MESSAGES.REMIND,
                    body: MESSAGES.NEW_NEWS_ADMIN
                });
                $scope.closeModal();
                $scope.selected = {};
                $scope.attachments = [];
                $scope.activeTab = 'all';
            } else
                toaster.error({
                    title: MESSAGES.REMIND,
                    body: rst.message
                });
            $ionicLoading.hide();
        });

        $scope.$on(BroadCast.BANNER_LIST_REV, function (a, rst) {
            if (rst && rst.result)
                $scope.bannerList = SchoolService.bannerList;
        });

        $scope.$on(BroadCast.NEWS_TYPE_REV, function (a, rst) {
            if (rst && rst.result) {
                getFilterList();
            }
        });

        $scope.selectImg = function () {
            // CameraHelper.selectMultiImage(function (resp) {
            //     if (!resp)
            //         return;
            //     if (resp instanceof Array) {
            //         Array.prototype.push.apply($scope.attachments, resp);
            //         $scope.$apply();
            //     } else {
            //         $scope.attachments.push({
            //             data: "data:image/jpeg;base64," + resp
            //         });
            //     }
            // }, 9 - $scope.attachments.length);

            if (window.cordova) {
                CameraHelper.selectMultiImage(function (resp) {
                    if (!resp)
                        return;
                    if (resp instanceof Array) {
                        Array.prototype.push.apply($scope.attachments, resp);
                        $scope.$apply();
                    } else {
                        $scope.attachments.push({
                            data: "data:image/jpeg;base64," + resp
                        });
                    }
                }, 9 - $scope.attachments.length);
            } else {
                //web 端
                var input = document.getElementById("capture");
                input.click();
            }
        };

         // input形式打开系统系统相册
         $scope.getFile = function (files) {
            var file = files[0];
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (theFile) {
                $timeout(function () {
                    $scope.testImg = theFile.target.result; //设置一个中间值
                    $scope.attachments.push({
                        data: $scope.testImg
                    });

                }, 100);
            };
        };

        $scope.removeImg = function (index, $event) {
            $event.stopPropagation();
            $scope.attachments.splice(index, 1);
        };

        $scope.$on(BroadCast.CONNECT_ERROR, function (arg, data) {
            $scope.$broadcast('scroll.refreshComplete');
            $ionicLoading.hide();
        });

        $scope.setStick = function (item, $event) {
            $event.stopPropagation();
            if (item.top)
                SchoolService.undoStickNews(item.id);
            else
                SchoolService.stickNews(item.id);
        };

        $scope.getStickText = function (isTop) {
            if (isTop)
                return '取消置顶';
            return '置顶';
        };

        $scope.setIgnore = function (item, $event) {
            $event.stopPropagation();
            var confirmPopup = $ionicPopup.confirm({
                title: '忽略确认',
                template: '确认忽略此风采?',
                cancelText: '取消',
                okText: '确认',
                okType: 'button-balanced'
            });
            confirmPopup.then(function (res) {
                if (res) {
                    SchoolService.ignorePublish(item.id);
                }
            });
        };

        $scope.setPublish = function (item, $event) {
            $event.stopPropagation();
            $scope.publishData = {
                key: item.key,
                id: item.id
            };
            $ionicPopup.show({
                title: '发布确认',
                scope: $scope,
                template: '<p>请选择发布风采的类型：</p> <div style="padding: 1px 15px !important;"> <button class="button button-outline " ng-repeat="btn in newsTypeSelectable" ng-click="publishData.key=btn.key" ng-class="publishData.key==btn.key?\'button-positive\':\'button-stable\'" style="margin: 2px 3px;">{{btn.name}} </button> </div>',
                buttons: [{
                        text: '取消'
                    },
                    {
                        text: '<b>确认</b>',
                        type: 'button-balanced',
                        onTap: function (e) {
                            if ($scope.publishData.key)
                                SchoolService.allowPublish($scope.publishData);
                            else {
                                e.preventDefault();
                                toaster.warning({
                                    title: MESSAGES.NO_NEWS_TYPE,
                                    body: ''
                                });
                            }
                        }
                    }
                ]
            });
        };

        $scope.goDetail = function (item) {
            $state.go('news_detail', {
                post: item,
                index: item.id
            });
        };

        $scope.viewImages = function (urls, index, $event) {
            $event.stopPropagation();
            ionicImageView.showViewModal({
                allowSave: true
            }, urls, index);
        };

        $scope.preViewImages = function (index) {
            var origin = [];
            $scope.attachments.forEach(function (libraryItem) {
                if (libraryItem.data)
                    origin.push(libraryItem.data);
                else
                    origin.push(libraryItem);
            });
            ionicImageView.showViewModal({
                allowSave: false
            }, origin, index);
        };

        $scope.$on(BroadCast.NEWS_STATE_CHANGED, function (a, rst) {
            if (Constant.debugMode) console.log(rst);
            if (rst && rst.result) {
                var i = 0;
                for (; i < $scope.newsList.length; i++) {
                    if ($scope.newsList[i].id == rst.request.id) {
                        switch (rst.request.type) {
                            case BroadCast.UNDO_STICK_RST_REV:
                                $scope.newsList[i].top = false;
                                SchoolService.getBannerList();
                                break;
                            case BroadCast.SET_FOCUS_RST_REV:
                                $scope.newsList[i].top = true;
                                if (rst.data) {
                                    for (var j = 0; j < $scope.newsList.length; j++) {
                                        if ($scope.newsList[j].id == rst.data) {
                                            $scope.newsList[j].top = false;
                                            break;
                                        }
                                    }
                                }
                                SchoolService.getBannerList();
                                break;
                            case BroadCast.IGNORE_RST_REV:
                            case BroadCast.ALLOW_PUBLISH_RST_REV:
                                $scope.newsList.splice(i, 1);
                                break;
                            case BroadCast.DELETE_NEWS_REV:
                            case BroadCast.CANCEL_PUBLISH_REV:
                                $scope.newsList.splice(i, 1);
                                SchoolService.getBannerList();
                                break;
                        }
                        break;
                    }
                }
                $scope.getCampusUnread();
            } else
                toaster.error({
                    title: MESSAGES.OPERATE_ERROR,
                    body: rst.message
                });
        });

        $scope.getCampusUnread = function () {
            Requester.getCampusUnread().then(function (resp) {
                if (resp.result) {
                    var count = resp.data.myCampusViewUnrendNum + resp.data.statePendingAuditNum;
                    $scope.unreadCount = resp.data.myCampusViewUnrendNum;
                    $scope.unreadList = resp.data.newsCampusViewCommentsVoList;
                    $scope.pendingPublishCount = resp.data.statePendingAuditNum;
                    if (count < 0 || !Number.isInteger(count)) {
                        count = 0;
                    }
                    $rootScope.$broadcast(BroadCast.BADGE_UPDATE, {
                        type: 'campus',
                        count: count
                    });
                }
            });
        };
    });
angular.module('app.controllers')
.controller('communicatePageCtrl', function (UserPreference, $scope, $ionicScrollDelegate, $state, ChatService, BroadCast, Constant, $ionicModal, $ionicPopup, toaster, MESSAGES, $rootScope, closePopupService) {
    $scope.contactView = false;
    $scope.switchView = function (contactV) {
        $scope.contactView = contactV;
        $ionicScrollDelegate.resize();
    };

    $scope.toggle = function (group, length) {
        if (length > 0) {
            group.show = !group.show;
            $ionicScrollDelegate.resize();
        }
    };
    $scope.sel = {};
    var UIPath = '';
    if (Constant.debugMode) UIPath = 'page/';
    $ionicModal.fromTemplateUrl(UIPath + 'interactive/add_conversation.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.modal = modal;
    });

    function initAddView() {
        $scope.selCount = 0;
        $scope.modalTitle = '发起聊天';
        for (var i = 0; i < $scope.addModeContactTree.length; i++) {
            for (var j = 0; j < $scope.addModeContactTree[i].list.length; j++) {
                $scope.addModeContactTree[i].list[j].isChecked = false;
            }
        }
        $scope.modal.show();
    }

    $scope.openModal = function () {
        $scope.addModeContactTree = $scope.ContactTree;
        initAddView();
    };
    $scope.closeModal = function () {
        $scope.modal.hide();
    };
    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function () {
        $scope.modal.remove();
    });

    $scope.saveConversation = function () {
        var arr = [];
        var name = '群聊:';
        for (var i = 0; i < $scope.addModeContactTree.length; i++) {
            for (var j = 0; j < $scope.addModeContactTree[i].list.length; j++) {
                var p = $scope.addModeContactTree[i].list[j];
                if (p.isChecked) {
                    arr.push(p.Member_Account);
                    if (arr.length < 2) {
                        name += p.Name;
                        name += ',';
                    }
                    else if (arr.length === 2)
                        name += p.Name;
                }
            }
        }
        if (arr.length === 1) {
            var info = ChatService.getMemberInfoFromMap(webim.SESSION_TYPE.C2C, arr[0], $scope.sel.class.key);
            var obj = {
                SessionType: webim.SESSION_TYPE.C2C,
                SessionId: arr[0],
                SessionNick: info.name,
                SessionImage: info.image
            };
            $scope.modal.hide();
            $state.go('communicate_detail', {obj: obj});
        }
        else {
            if (name.length > 10) {
                name = name.substr(0, 10);
                name += '..';
            }

            ChatService.createCustomGroup(name, $scope.sel.class, arr).then(function (resp) {
                //console.log(resp);
                if (resp.data.result) {
                    ChatService.getContacts();
                    $scope.modal.hide();

                    var rsp = JSON.parse(resp.data.data);
                    var obj = {
                        GroupId: rsp.groupId,
                        TypeEn: 'Private',
                        Name: name
                    };
                    $scope.conversationDetail(obj);
                    if (window.cordova) MobclickAgent.onEvent('app_private_group');
                }
            });
        }
    };

    $scope.onAddModeClassSelected = function (key) {
        if (key && $scope.classChooser) {
            $scope.sel.class = key;
            $scope.classChooser.close();
            getContactTree(key.key, true);
            initAddView();
        }
    };

    $scope.selectionChanged = function (contact) {
        if (contact.isChecked)
            $scope.selCount++;
        else
            $scope.selCount--;
        if ($scope.selCount > 0)
            $scope.modalTitle = '已选择' + $scope.selCount + '人';
        else
            $scope.modalTitle = '发起聊天';
    };

    function getContactTree(classid, addMode) {
        $scope.cachedCID = angular.copy(classid);
        var friends = ChatService.friendsMap[classid];
        var groups = ChatService.groups;
        var filterGroups = [];
        for (var j in groups) {
            if (groups[j].ClassID == classid)
                filterGroups.push(groups[j]);
        }
        if (friends && friends.length > 0) {
            $scope.emptyContacts = false;
            var teachers = [];
            for (var i in friends) {
                var p = friends[i];
                switch (p.Category) {
                    case 't':
                        teachers.push(p);
                        break;
                }
            }
            var contactTree = [
                {
                    name: '老师',
                    list: teachers,
                    num: teachers.length,
                    icon: 'img/icon/chat_teacher.png',
                    isGroup: false
                },
                {
                    name: '我的群组',
                    list: filterGroups,
                    num: filterGroups.length,
                    icon: 'img/icon/chat_group.png',
                    isGroup: true
                }
            ];
            if (addMode)
                $scope.addModeContactTree = contactTree;
            else
                $scope.ContactTree = contactTree;
        }
        else {
            $scope.emptyContacts = true;
            if (addMode)
                $scope.addModeContactTree = [];
            else
                $scope.ContactTree = [];
        }
    }

    $scope.$on('$ionicView.beforeEnter', function () {
      
        if (window.cordova&&ionic.Platform.isIOS()) {
            cordova.plugins.Keyboard.disableScroll(true);
        }
        $scope.userID = UserPreference.getObject('user').id;
        $scope.sel.class = {
            key: 'schoolTeacher'
        };
    });

    $scope.refreshContacts = function (contactView) {
        if (contactView)
            ChatService.getContacts();
        else
            ChatService.getRecentContacts($scope.sel.class.key);
    };

    $scope.$on('$ionicView.enter', function () {
        if ($rootScope.ContactProfileChanged === true)
            ChatService.getContacts();
        else if ($scope.sel.class.key != $scope.cachedCID) {
            getContactTree($scope.sel.class.key);
        }
        ChatService.getRecentContacts($scope.sel.class.key);
    });

    $scope.loading = true;
    setTimeout(function () {
        ChatService.getRecentContacts($scope.sel.class.key);
    }, 5000);

    $scope.onSelectionChange = function () {
        if ($scope.sel.class) {
            if ($scope.sel.class.key !== 'schoolTeacher')
                UserPreference.set("DefaultClassID", $scope.sel.class.key);
            getContactTree($scope.sel.class.key);
        }
    };

    $scope.conversationDetail = function (item, isConversation) {
        if (isConversation) {
            $state.go('communicate_detail', {obj: item});
        }
        else {
            if (item.Member_Account) {
                var obj = {
                    SessionId: item.Member_Account
                };
                $state.go('contact_profile', {obj: obj});
            }
            else
                $state.go('communicate_detail', {obj: item});
        }
    };

    $scope.$on(BroadCast.IM_RECENT_CONTACTS, function (event, resp) {
        if (resp) {
            $scope.conversations = resp;
            $scope.$digest();
        }
        $scope.$broadcast('scroll.refreshComplete');
        $scope.loading = false;
    });

    $scope.$on(BroadCast.IM_REV_CONTACTS, function (event, data) {
        if (data)
            getContactTree($scope.sel.class.key);
        else
            toaster.error({title: MESSAGES.REMIND, body: MESSAGES.CONTACT_FETCH_FAIL});
        $scope.$broadcast('scroll.refreshComplete');
    });

    $scope.$on(BroadCast.IM_NEW_MESSAGE, function (event, newMsgList) {
        $scope.conversations = ChatService.conversations;
        $scope.$digest();
    });
});
angular.module('app.controllers')
.controller('communicateDetailCtrl', function ($ionicModal, CameraHelper, $ionicPopover, Constant, $scope, $state, $stateParams, ChatService, $ionicActionSheet, $ionicPopup, $ionicScrollDelegate, $timeout, $interval, $ionicHistory, BroadCast, $rootScope, toaster, MESSAGES, UserPreference) {
    $scope.goBack = function () {
        if ($ionicHistory.backView())
            $ionicHistory.goBack();
        else
            $state.go('tabsController.mainPage');
    };

    if(window.cordova){
        if(ionic.Platform.isIOS())   cordova.plugins.Keyboard.disableScroll(false);
    }

    $scope.focus = function(){
       
        $timeout(function(){
            // if(ionic.Platform.isIOS())
            // $scope.footHeight = $rootScope.keyboardHeight;
        },50);

    };

    $scope.blur = function(){
        console.log('blur--');
        $timeout(function(){
            $scope.footHeight =  0;
        },50);
    };


    $scope.user = {};
    $scope.messages = [];

    $scope.input = {
        message: ''
    };

    $scope.convertMsgtoHtml = function (msg) {
        return convertMsgtoHtml(msg);
    };

    var UIPath = '';
    if (Constant.debugMode) UIPath = 'page/';
    $ionicPopover.fromTemplateUrl(UIPath + 'interactive/emotions.html', {
        scope: $scope
    }).then(function (popover) {
        $scope.popover = popover;
    });

    $scope.hidePopover = function () {
        if ($scope.popover)
            $scope.popover.hide();
    };

    $scope.emotions = webim.Emotions;

    $scope.showEmotions = function ($event) {
        setTimeout(function () {
            if (window.cordova)
                cordova.plugins.Keyboard.close();
            $scope.popover.show($event);
        }, 100);
    };

    $scope.selectEmotion = function (e) {
        if ($scope.input.message)
            $scope.input.message += e;
        else
            $scope.input.message = e;
    };

    $ionicModal.fromTemplateUrl(UIPath + 'interactive/chat_members.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.modal = modal;
    });
    $scope.openModal = function () {
        $scope.modal.show();
    };
    $scope.closeModal = function (edit) {
        if (edit)
            $scope.editModal.hide();
        else
            $scope.modal.hide();
    };

    $ionicModal.fromTemplateUrl(UIPath + 'common/ionic-image-view.html', {
        scope: $scope
    }).then(function (modal) {
        $scope.imageModal = modal;
    });

    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function () {
        $scope.modal.remove();
        $scope.editModal.remove();
        $scope.imageModal.remove();
    });

    $scope.$on('popover.hidden', function () {
        // Execute action
    });

    $ionicModal.fromTemplateUrl(UIPath + 'interactive/add_conversation.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.editModal = modal;
    });
    $scope.showEditMode = function (mode) {
        getContactTree();
        $scope.selCount = 0;
        if (mode === 'add') {
            $scope.editFilter = {isMember: false};
            $scope.constantTitle = '添加群成员';
        } else {
            $scope.editFilter = {isMember: true};
            $scope.constantTitle = '移除群成员';
        }
        $scope.modalTitle = $scope.constantTitle;
        $scope.editModal.show();
    };

    function getContactTree() {
        var friends = ChatService.friendsMap[$scope.classID];
        if (friends && friends.length > 0) {
            var teachers = [];
            for (var i in friends) {
                var p = friends[i];
                p.isMember = false;
                p.isChecked = false;
                for (var j = 0; j < $scope.groupInfo.MemberList.length; j++) {
                    if (p.Member_Account == $scope.groupInfo.MemberList[j].Member_Account) {
                        p.isMember = true;
                        break;
                    }
                }
                switch (p.Category) {
                    case 't':
                        teachers.push(p);
                        break;
                }
            }
            $scope.addModeContactTree = [
                {
                    name: '老师',
                    list: teachers,
                    num: teachers.length,
                    icon: 'img/icon/chat_teacher.png',
                    isGroup: false
                }
            ];
            console.log($scope.addModeContactTree);
        }
    }

    function getGroupDetail(init) {
        ChatService.getGroupInfo($scope.SessionId).then(
            function (resp) {
                //console.log(resp);
                $scope.groupInfo = resp.GroupInfo[0];
                $scope.isGroupAdmin = ($scope.groupInfo.Owner_Account == $scope.userID);
                if (init) {
                    webim.MsgStore.delSessByTypeId($scope.SessionType, $scope.SessionId);
                    $scope.session = undefined;
                    $scope.NextMsgSeq = $scope.groupInfo.NextMsgSeq - 1;
                    ChatService.getGroupHistoryMsgs($scope.SessionId, $scope.NextMsgSeq);
                }
            }, function (error) {
                $scope.doneLoading = true;
                toaster.error({title: MESSAGES.CONNECT_ERROR, body: MESSAGES.CONNECT_ERROR_MSG});
            }
        );
    }

    $scope.toggle = function (group, length) {
        if (length > 0) {
            group.show = !group.show;
            $ionicScrollDelegate.resize();
        }
    };

    $scope.closeImageView = function () {
        $scope.imageModal.hide();
    };

    $scope.onImageClick = function (pic) {
        $scope.largeImgUrl = pic;
        $scope.imageModal.show();
    };

    $scope.selectionChanged = function (contact) {
        if (contact.isChecked)
            $scope.selCount++;
        else
            $scope.selCount--;
        if ($scope.selCount > 0)
            $scope.modalTitle = '已选择' + $scope.selCount + '人';
        else
            $scope.modalTitle = $scope.constantTitle;
    };

    $scope.saveConversation = function () {
        var arr = [];
        for (var i = 0; i < $scope.addModeContactTree.length; i++) {
            for (var j = 0; j < $scope.addModeContactTree[i].list.length; j++) {
                var p = $scope.addModeContactTree[i].list[j];
                if (p.isChecked) {
                    arr.push(p.Member_Account);
                }
            }
        }
        if (arr.length > 0) {
            ChatService.modifyGroupMember($scope.groupInfo.GroupId, arr, $scope.editFilter.isMember).then(function (resp) {
                getGroupDetail();
                $scope.closeModal('edit');
            });
        }
    };

    var viewScroll = $ionicScrollDelegate.$getByHandle('userMessageScroll');
    var txtInput = angular.element(document.getElementsByName("chatInput"));

    $scope.$on('$ionicView.loaded', function () {
        if ($stateParams.obj) {
            $scope.user = ChatService.loginInfo;
            $scope.userID = $scope.user.identifier;
            $scope.SessionId = $stateParams.obj.SessionId;
            $scope.isGroupAdmin = false;
            if ($scope.SessionId) {
                //会话列表进入
                $scope.SessionType = $stateParams.obj.SessionType;
                $scope.SessionName = $stateParams.obj.SessionNick;
                $scope.SessionImage = $stateParams.obj.SessionImage;
            }
            else {
                //通讯录进入
                if ($stateParams.obj.TypeEn) {
                    //群
                    $scope.SessionId = $stateParams.obj.GroupId;
                    $scope.SessionType = webim.SESSION_TYPE.GROUP;
                    $scope.SessionName = $stateParams.obj.Name;
                }
                else {
                    //私聊
                    $scope.SessionId = $stateParams.obj.Member_Account;
                    $scope.SessionType = webim.SESSION_TYPE.C2C;
                    $scope.SessionName = $stateParams.obj.Name;
                    $scope.SessionImage = $stateParams.obj.Icon;
                }
            }

            if ($scope.SessionType == webim.SESSION_TYPE.C2C) {
                $scope.msgKey = '';
                $scope.msgLastTime = 0;
                $scope.doneLoading = false;
                ChatService.getC2CHistoryMsgs($scope.SessionId, $scope.msgLastTime, $scope.msgKey);
            } else {
                $scope.doneLoading = false;
                var arr = $scope.SessionId.split('_');
                if (arr && arr.length > 1) {
                    $scope.classID = arr[arr.length - 1];
                }
                getGroupDetail(true);
            }
        }
        //console.log($scope);
    });

    $scope.changeGroupName = function () {
        if ($scope.groupInfo.Type == 'Private') {
            $scope.groupInfo.tmpName = angular.copy($scope.groupInfo.Name);
            var myPopup = $ionicPopup.show({
                template: '<input type="text" placeholder="群名需在10字以内" maxlength="10" ng-model="groupInfo.tmpName">',
                title: '群名称设置',
                scope: $scope,
                buttons: [
                    {text: '取消'},
                    {
                        text: '<b>确定</b>',
                        type: 'button-balanced',
                        onTap: function (e) {
                            if ($scope.groupInfo.tmpName && $scope.groupInfo.tmpName.length > 0) {
                                ChatService.modifyGroupInfo($scope.groupInfo.GroupId, $scope.groupInfo.tmpName).then(
                                    function (resp) {
                                        $rootScope.ContactProfileChanged = true;
                                        $scope.groupInfo.Name = $scope.groupInfo.tmpName;
                                        $scope.SessionName = $scope.groupInfo.tmpName;
                                        myPopup.close();
                                    }
                                );
                            }
                            else {
                                e.preventDefault();
                            }
                        }
                    }
                ]
            });
        }
    };

    $scope.getC2CImage = function (id) {
        if ($scope.SessionType == webim.SESSION_TYPE.C2C)
            return $scope.SessionImage;
        else {
            var info = ChatService.getMemberInfoFromMap(webim.SESSION_TYPE.C2C, id, $scope.classID);
            return info ? info.image : Constant.IM_USER_AVATAR;
        }
    };

    $scope.getC2CName = function (id, max) {
        var limit = 5;
        if (max)
            limit = max;
        if ($scope.SessionType == webim.SESSION_TYPE.C2C)
            return '';
        else {
            var info = ChatService.getMemberInfoFromMap(webim.SESSION_TYPE.C2C, id, $scope.classID);
            var name = info ? info.name : id;
            return name.length > limit ? name.substr(0, limit) : name;
        }
    };

    $scope.quitGroup = function () {
        var confirmPopup = $ionicPopup.confirm({
            title: '确认退出该群？',
            template: '退出后群内的聊天记录将一并清空。',
            cancelText: '取消',
            okText: '确认',
            okType: 'button-balanced'
        });

        confirmPopup.then(function (res) {
            if (res) {
                ChatService.quitGroup($scope.groupInfo.GroupId).then(function (resp) {
                    if (resp.ErrorCode === 0) {
                        $rootScope.ContactProfileChanged = true;
                        $scope.closeModal();
                        $scope.goBack();
                    }
                });
            }
        });
    };

    $scope.$on('$ionicView.leave', function () {
        if ($scope.session)
            webim.setAutoRead($scope.session, false, false);
        if ($scope.SessionType != webim.SESSION_TYPE.C2C) {
            webim.MsgStore.delSessByTypeId($scope.SessionType, $scope.SessionId);
            $scope.session = undefined;
        }

    });

    $scope.loadMore = function () {
        if ($scope.SessionType == webim.SESSION_TYPE.C2C)
            ChatService.getC2CHistoryMsgs($scope.SessionId, $scope.msgLastTime, $scope.msgKey);
        else
            ChatService.getGroupHistoryMsgs($scope.SessionId, $scope.NextMsgSeq);
    };

    function getSessionFromMsgIfNotExist() {
        if (!$scope.session && $scope.messages.length > 0) {
            $scope.session = $scope.messages[0].getSession();
            webim.setAutoRead($scope.session, true, true);
        }
    }

    $scope.$on(BroadCast.IM_NEW_MESSAGE, function (event, newMsgList) {
        for (var j in newMsgList) {
            var newMsg = newMsgList[j];
            if (newMsg.getSession().id() == $scope.SessionId) {
                for (var i = 0; i < $scope.messages.length; i++) {
                    if ($scope.messages[i].random == newMsg.random) {
                        $scope.messages.splice(i, 1);
                        break;
                    }
                }
                $scope.messages.push(newMsg);
                getSessionFromMsgIfNotExist();
            }
        }
        $scope.$digest();
        $timeout(function () {
            viewScroll.scrollBottom(true);
        }, 100);
        if ($scope.session)
            webim.setAutoRead($scope.session, true, true);
    });

    function insertTimeLabelToMsg(i) {
        for (; i >= 0; i--) {
            var msg = $scope.messages[i];
            var msgDay = formatTimeWithoutSecends(msg.time).substr(8, 2);
            var now = formatTimeWithoutSecends(new Date().getTime() / 1000).substr(8, 2);
            if (i > 0) {
                var preMsg = $scope.messages[i - 1];
                var preMsgDay = formatTimeWithoutSecends(preMsg.time).substr(8, 2);
                if (now != msgDay) {
                    if (msgDay != preMsgDay)
                        msg.timeLabel = formatTimeWithoutSecends(msg.time);
                    else
                        msg.timeLabel = undefined;
                }
                else if (msg.time - preMsg.time >= 60 * 5)
                    msg.timeLabel = formatTimeWithoutSecends(msg.time).substr(11, 5);
                else
                    msg.timeLabel = undefined;
            }
            else {
                if (now != msgDay) {
                    msg.timeLabel = formatTimeWithoutSecends(msg.time);
                }
                else
                    msg.timeLabel = formatTimeWithoutSecends(msg.time).substr(11, 5);
            }
        }
    }

    function createSessionIfNotExist() {
        $scope.session = webim.MsgStore.sessByTypeId($scope.SessionType, $scope.SessionId);
        if (!$scope.session)
            $scope.session = new webim.Session($scope.SessionType, $scope.SessionId, $scope.SessionName, $scope.SessionImage, Math.round(new Date().getTime() / 1000));
        webim.setAutoRead($scope.session, true, true);
    }

    function onRevHistoryMsg(data) {
        if (!Array.isArray(data)) {
            console.log('message data is not array');
            $scope.doneLoading = true;
            $scope.$broadcast('scroll.refreshComplete');
            return;
        }
        var i;
        if ($scope.messages.length > 0) {
            for (var j = data.length - 1; j >= 0; j--) {
                $scope.messages.unshift(data[j]);
            }
            i = data.length;
        }
        else {
            $scope.messages = data;
            i = $scope.messages.length - 1;
            getSessionFromMsgIfNotExist();
        }

        insertTimeLabelToMsg(i);
        if ($scope.messages.length == data.length) {
            $timeout(function () {
                viewScroll.scrollBottom(true);
            }, 1000);
        }
        $scope.doneLoading = true;
        $scope.$broadcast('scroll.refreshComplete');
    }

    $scope.$on(BroadCast.IM_C2C_HISTORY_MESSAGE, function (event, data) {
        if (data) {
            if ($scope.msgKey === '')
                $scope.messages = [];
            $scope.msgKey = data.MsgKey;
            $scope.msgLastTime = data.LastMsgTime;
        }
        onRevHistoryMsg(data.MsgList);
    });


    $scope.$on(BroadCast.IM_GROUP_HISTORY_MESSAGE, function (event, data) {
        var filter = [];
        if (data.length > 0) {
            $scope.NextMsgSeq = data[0].seq - 1;
            for (var i = 0; i < data.length; i++) {
                // var senderId = data[i].fromAccount;
                // if (!senderId || senderId == '@TIM#SYSTEM' ) {
                // 	continue;
                // }
                if (convertMsgtoHtml(data[i]) !== '')
                    filter.push(data[i]);
            }
        }
        onRevHistoryMsg(filter);
    });

    $scope.$on(BroadCast.IM_MSG_SENT, function (event, msg) {
        if (!msg)
            return;
        if ($scope.SessionType == webim.SESSION_TYPE.C2C) {
            if (msg.type === 'image') {
                var message = msg.msg;
                for (var i = 0; i < $scope.messages.length; i++) {
                    if ($scope.messages[i].random == message.random) {
                        $scope.messages.splice(i, 1);
                        break;
                    }
                }
                $scope.messages.push(message);
                $scope.$digest();
                $timeout(function () {
                    viewScroll.scrollBottom(true);
                }, 100);
            }
        }
    });

    $scope.$on(BroadCast.IM_MSG_SENDING, function (event, msg) {
        if (!msg)
            return;
        $scope.input.message = '';
        var title = UserPreference.getObject("user").name;
        var pushMessage = '给您发送了一条消息', to = [];
        var msgStr = convertMsgtoPushStr(msg.msg);
        if (msgStr !== "") {
            pushMessage = msgStr;
        }
        if ($scope.SessionType == webim.SESSION_TYPE.C2C || msg.type === 'image') {
            var message = angular.copy(msg.msg);
            if (msg.type === 'image') {
                var text_obj = new webim.Msg.Elem.Text("图片发送中..");
                message.addText(text_obj);
                pushMessage = '[图片]';
            }
            console.log(message);
            $scope.messages.push(message);
            $timeout(function () {
                viewScroll.scrollBottom(true);
            }, 100);
        }
        if ($scope.SessionType == webim.SESSION_TYPE.GROUP) {
            for (var j = 0; j < $scope.groupInfo.MemberList.length; j++) {
                if ($scope.groupInfo.MemberList[j].Member_Account != $scope.userID)
                    to.push($scope.groupInfo.MemberList[j].Member_Account);
            }
        } else {
            to.push($scope.SessionId);
        }
        ChatService.sendPush(to, title, pushMessage);
    });

    $scope.sendMessage = function () {

        keepKeyboardOpen();
        if (!$scope.session)
            createSessionIfNotExist();
        if ($scope.input.message && $scope.input.message.length > 0) {
            ChatService.sendMessage($scope.session, $scope.input.message);
            $scope.isSending = true;
        }
        $timeout(function () {
            $scope.isSending = false;
        }, 1000);
    };

    $scope.selectImg = function () {
        if (!$scope.isSending) {
            $timeout(function () {
                if (window.cordova)
                    cordova.plugins.Keyboard.close();
                $ionicScrollDelegate.resize();
                CameraHelper.selectImage('chat');
            }, 10);
        }
    };

    $scope.$on(BroadCast.IMAGE_SELECTED, function (a, rst) {
        if (rst && rst.which === 'chat') {
            ChatService.uploadImageAndSend($scope.session, rst.source);
        }
    });

    // this keeps the keyboard open on a device only after sending a message, it is non obtrusive
    function keepKeyboardOpen() {
        console.log('keepKeyboardOpen');
        $timeout(function () {
            $ionicScrollDelegate.resize();
            viewScroll.scrollBottom(true);
        }, 100);
        txtInput.one('blur', function () {
            console.log('textarea blur, focus back on it');
            txtInput[0].focus();
        });
    }

    $scope.onMessageHold = function (e, itemIndex, message) {
        console.log('onMessageHold');
        // $ionicActionSheet.show({
        // 	buttons: [{
        // 		text: '复制'
        // 	}, {
        // 		text: '删除消息'
        // 	}],
        // 	buttonClicked: function(index) {
        // 		switch (index) {
        // 			case 0: // Copy Text
        // 				//cordova.plugins.clipboard.copy(message.text);
        //
        // 				break;
        // 			case 1: // Delete
        // 				// no server side secrets here :~)
        // 				$scope.messages.splice(itemIndex, 1);
        // 				$timeout(function() {
        // 					viewScroll.resize();
        // 				}, 0);
        //
        // 				break;
        // 		}
        //
        // 		return true;
        // 	}
        // });
    };

    // this prob seems weird here but I have reasons for this in my app, secret!
    $scope.viewProfile = function (id) {
        var obj = {};
        if (id === $scope.user.identifier) {
            obj.self = true;
        }
        obj.SessionId = id;
        $state.go('contact_profile', {obj: obj});
    };

});
angular.module('app.controllers')
.controller('contactProfileCtrl', function ($scope, $ionicHistory, $stateParams, $state, ChatService, Constant) {
    $scope.goBack = function () {
        if ($ionicHistory.backView())
            $ionicHistory.goBack();
        else
            $state.go('tabsController.mainPage');
    };
    function getUserRoleCN(role) {
        var arr = {};
        arr.school_admin = '学校管理员';
        arr['3'] = '老师';
        arr['4'] = '家长';
        arr['2'] = '学生';
        return arr[role];
    }

    $scope.$on('$ionicView.enter', function () {
        $scope.SessionId = $stateParams.obj.SessionId;
        $scope.showSendBtn = !$stateParams.obj.self;
        ChatService.getUserDetail($scope.SessionId).then(
            function (data) {
                var user = data.data;
                $scope.SessionNick = user.name;
                $scope.SessionImage = user.logo;
                if (!$scope.SessionImage)
                    $scope.SessionImage = Constant.IM_USER_AVATAR;
                $scope.phone = user.phoneNumber;
                $scope.role = user.rolename;
                $scope.rolename = getUserRoleCN($scope.role);
                if (user.sex) {
                    if (user.sex.toUpperCase() === 'MALE')
                        $scope.sex = '男';
                    else if (user.sex.toUpperCase() === 'FEMALE')
                        $scope.sex = '女';
                }
                else
                    $scope.sex = '';
                $scope.children = user.student;
                $scope.subjects = user.subjects;
                $scope.classes = user.classNoList;
                $scope.families = user.familys;
                console.log(user);
            }
        );

    });

    $scope.goChat = function () {
        var opt = {
            SessionId: $scope.SessionId,
            SessionNick: $scope.SessionNick,
            SessionImage: $scope.SessionImage,
            SessionType: webim.SESSION_TYPE.C2C
        };
        $state.go('communicate_detail', {obj: opt});
    };

});

angular.module('app.controllers')
.controller('settingPageCtrl', function ($scope, $state, $ionicLoading, AuthorizeService, SettingService, Requester,UserPreference, BroadCast, Constant, ngIntroService, ChatService, $ionicHistory) {

    $scope.$on("$ionicView.beforeEnter", function (event, data) {
        if (window.cordova&&ionic.Platform.isIOS()) {
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if( UserPreference.get('VersionIntroduce')){
         console.log('exit:'+UserPreference.get('VersionIntroduce'));
         $scope.isRead = UserPreference.get('VersionIntroduce')=='UNREAD'?false:true;
        }else{
         console.log('not exit');
         UserPreference.set('VersionIntroduce','UNREAD');
         $scope.isRead = false;
        }
       
     });

    $scope.$on("$ionicView.enter", function (event, data) {
        $scope.user = UserPreference.getObject("user");
        if (!$scope.user.logo || $scope.user.logo.trim() === '')
            $scope.user.logo = Constant.IM_USER_AVATAR;
        $scope.childName = UserPreference.get('DefaultChildName');
        $scope.userAccount = UserPreference.get('username');
        $scope.needCardFee = UserPreference.getBoolean('NeedCardFee');
        //获取意见反馈的条数
        $scope.getFeedbackNotreadNum();
    });

    $scope.user = UserPreference.getObject("user");
    if (!$scope.user.logo || $scope.user.logo.trim() === '')
        $scope.user.logo = Constant.IM_USER_AVATAR;
    $scope.childName = UserPreference.get('DefaultChildName');
    $scope.userAccount = UserPreference.get('username');
    if ($scope.user.logo === Constant.IM_USER_AVATAR && !UserPreference.getBoolean("ShowSettingTip")) {
        var IntroOptions = {
            steps: [
                {
                    element: document.querySelector('#focusItem'),
                    intro: "<strong>点击头像可以修改个人信息和重置密码</strong>"
                }],
            showStepNumbers: false,
            showBullets: false,
            exitOnOverlayClick: true,
            exitOnEsc: true
        };
        ngIntroService.onHintClick(function () {
            ngIntroService.exit();
        });
        ngIntroService.setOptions(IntroOptions);
        ngIntroService.start();
        UserPreference.set("ShowSettingTip", true);
    }
    $scope.isAndroid = ionic.Platform.isAndroid();

    $scope.checkUpdate = function () {
        SettingService.checkUpdate(false);
    };

    $scope.hideTip = function () {
        ngIntroService.exit();
    };

    function logout() {
        $ionicLoading.hide();
        setTimeout(function () {
            $ionicHistory.clearCache();
            $ionicHistory.clearHistory();
        }, 1000);
        var currentVersion = Constant.version;
        var rst = UserPreference.getBoolean(currentVersion);
        var lastUser = UserPreference.get('username', '');
        var tip = UserPreference.getBoolean("ShowSettingTip");
        var readStatue =  UserPreference.get('VersionIntroduce');
        var schoolBeforeTamp = UserPreference.get('SchoolAuthAlertDateTamp');
        var areaBeforeTamp = UserPreference.get('AreaAuthAlertDateTamp');
        var areaAssetsBeforeTamp = UserPreference.get('AreaAssetsAuthAlertDateTamp');
       // var localTeacherList = UserPreference.getArray('TeacherList');

        UserPreference.clear();
        UserPreference.set(currentVersion, rst);
        UserPreference.set("username", lastUser);
        UserPreference.set("ShowSettingTip", tip);
        UserPreference.set('VersionIntroduce',readStatue);
        UserPreference.set('SchoolAuthAlertDateTamp', schoolBeforeTamp);
        UserPreference.set('AreaAuthAlertDateTamp', areaBeforeTamp);
        UserPreference.set('AreaAssetsAuthAlertDateTamp',areaAssetsBeforeTamp);
       // UserPreference.setObject('TeacherList', localTeacherList);
        $state.go('login');
        ChatService.logout();
    }

    $scope.doLogout = function () {
        $ionicLoading.show({
            noBackdrop: true,
            template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
        });
        console.log('to exit ');
        AuthorizeService.logout().finally(logout());
    };
    //获取意见反馈未读的条数
    $scope.getFeedbackNotreadNum = function(){
        $scope.noReadNum = 0;
        Requester.getNoreadNewsNumber().then(function (resp) {
            if (resp.result) {
                console.log('获取反馈未读数成功');
                $scope.noReadNum = resp.data.size;

            }
        });
    };
    
    $scope.clearCache = function () {
        $ionicLoading.show({
            noBackdrop: true,
            template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
        });
        setTimeout(function () {
            $ionicLoading.show({
                noBackdrop: true,
                template: '清理完成'
            });
            setTimeout(function () {
                $ionicLoading.hide();
            }, 1000);
        }, 1000);
    };

    $scope.openEditView = function (content) {
        $state.go('edit_profile', {content: content});
    };

    $scope.$on(BroadCast.CONNECT_ERROR, function (arg, data) {
        $ionicLoading.hide();
    });
});
angular.module('app.controllers')
.controller('aboutCtrl', function ($scope, Constant, $ionicHistory, $state) {
    $scope.version = Constant.version;
    $scope.versionCode = Constant.versionCode;
    $scope.goBack = function () {
        $ionicHistory.nextViewOptions({
            disableBack: true
        });
        $state.go('tabsController.settingPage');
    };
});
/**
 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
    .controller('suggestCtrl', function ($scope, BroadCast, Requester, toaster, $timeout, UserPreference, $ionicScrollDelegate, $cordovaCamera, Constant, ionicImageView, $ionicPopover, CameraHelper,$rootScope) {
        $scope.suggest = {
            text: ''
        };
        // document.querySelector('body').style.height = document.documentElement.clientHeight + 'px';
        // document.querySelector('body').style.width = document.documentElement.clientWidth + 'px';
        $scope.isIos = ionic.Platform.isIOS();
        if (window.cordova&&$scope.isIos) {
            cordova.plugins.Keyboard.disableScroll(false);
        }

        $scope.footHeight = 0;

        var UIPath = '';
        if (Constant.debugMode) UIPath = 'page/';
        $ionicPopover.fromTemplateUrl(UIPath + 'setting/suggestpage/suggestEmotions.html', {
            scope: $scope
        }).then(function (popover) {
            $scope.popover = popover;
        });


        $scope.items = getSuggestEmotions();

        $scope.$on("$ionicView.loaded", function (event, data) {
            //获取用户id
            var user = UserPreference.getObject('user');

            $scope.userName = String(user.name);
            $scope.userHead = user.logo;
            $scope.screenHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
            console.log('屏幕高度为：' + $scope.screenHeight);
            $scope.input = {
                message: '',
                messageType: 0
            };
            $scope.input.message = '';
        });


        $scope.$on("$ionicView.enter", function (event, data) {
            $scope.page = 1;
            $scope.isRight = false;
            $scope.isShow = false; //是否弹出底部视图 相机
            $scope.emotionShow = false;
            $scope.feedBackList = [];
            $scope.imageUrls = [];
            $scope.feedBackList = UserPreference.getArray('feedback_list'); //用于存放反馈的数组消息
            console.log('local feedback length:' + $scope.feedBackList.length);
            if ($scope.feedBackList.length > 0) {
                $scope.insertTimeLabelToMsg($scope.feedBackList.length - 1);
                if ($scope.feedBackList[0].userName == $scope.userName) {
                    $scope.isRight = true;

                } else {
                    $scope.isRight = false;
                }
            }

            $timeout(function () {
                $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollBottom(true);
            }, 200);

            $scope.getFeedbackList();

            window.addEventListener('native.keyboardshow', function (e) {
                $scope.isShow = false;
                $scope.emotionShow = false;
                $scope.popover.hide();
                $timeout(function () {
                    $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollBottom(true);
                    if($scope.isIos&&window.scrollY<100){
                          window.scrollTo(0, e.keyboardHeight);
                    }
                }, 100);
                console.log('keyboard show');
                //$scope.$apply();
            });

            window.addEventListener('native.keyboardhide', function (e) {
                // todo 进行键盘不可用时操作
                console.log('关闭时');
                
                $timeout(function () {
                    $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollBottom(true);
                    if($scope.isIos&&window.scrollY!=0){
                        window.scrollTo(0,0);
                  }
                }, 100);
                // $scope.$apply();
            });

            // window.addEventListener('keyboardWillShow', function (event) {
            //     console.log('keyboard show 1111');
            //     console.log(event);
            //     $timeout(function(){
            //         $scope.footHeight = 0;//event.keyboardHeight;
            //     },50);
            // });

            // window.addEventListener('keyboardDidHide', function () {
            //     // Describe your logic which will be run each time keyboard is closed.
            //     console.log('keyboard hide 2222');
            //     $timeout(function(){
            //         $scope.footHeight = 0;
            //     },50);
            // });

        

        });
        $scope.focus = function(){
            $timeout(function(){
                if(ionic.Platform.isIOS())
                $scope.footHeight =  $rootScope.keyboardHeight;
                $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollTo(0,-260);
            },50);

        };

        $scope.blur = function(){
            $timeout(function(){
                $scope.footHeight =  0;
            },50);
        };

        //点击 发送 按钮 
        $scope.sendMessage = function () {
            $scope.sendFeedbackRequest('0', '');
        };

        //发送 消息
        $scope.sendFeedbackRequest = function (type, imgContent) {

            $scope.arr = [];
            if (type === '0') {
                if ($scope.input.message === '') {
                    return;
                }
                var txtInput = angular.element(document.getElementsByName("chatInput"));
                $scope.arr = $scope.input.message.split(",");
            } else {
                $scope.arr.push(imgContent);
            }

            Requester.publishUserFeedback($scope.arr, type).then(function (resp) {
                console.log(resp);
                if (resp.result) {
                    $scope.input.message = '';
                    $scope.isShow = false; //隐藏相机弹框
                    $scope.emotionShow = false; //隐藏表情弹框
                } else {
                    toaster.warning({
                        title: "温馨提示",
                        body: "系统异常"
                    });
                }
                if (type == 1) {
                    var now = getNowFormatDate();
                    var timestamp = Date.parse(new Date(now.replace(/\-/g, "/"))) / 1000;
                    $scope.suggestionMsg = {
                        contentType: 1,
                        imgContent: [{
                            thumb: imgContent,
                            url: imgContent
                        }],
                        // textContent:'999999999',
                        id: timestamp,
                        datetime: now,
                        userName: $scope.userName,
                        userHead: $scope.userHead
                    };
                    $scope.feedBackList.push($scope.suggestionMsg);
                }

                //发送反馈成功
                return resp.result;

            }).then(function (result) {

                // 发送成功后 获取反馈列表
                if (result) {
                    $scope.getFeedbackList();
                }
                return result;

            }).then(function (result) {
                if (result) {
                    console.log('获取列表成功了:' + result);
                    // $scope.$apply();
                    // $timeout(function () {
                    //     $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollBottom(true);
                    // }, 300);
                    var resp = $ionicScrollDelegate.$getByHandle('userMessageScroll').getScrollPosition();
                    $timeout(function () {
                        $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollTo(0, resp.top + 75, true);
                    }, 500);

                }

            });
        };

        //获取消息列表 
        $scope.getFeedbackList = function () {
            //$scope.feedBackList = [];
            Requester.getFeedbackDetailList().then(function (resp) {
                if (resp.result) {
                    $scope.feedBackList = resp.data.suggestionMsgs;
                    $scope.feedBackList.forEach(function (message) {
                        $scope.imageUrls = [];
                        if (message.contentType == 1 && message.imgContent.length > 0) {
                            message.imgContent.forEach(function (image) {
                                $scope.imageUrls.push(image.url);
                            });
                            message.imageUrls = $scope.imageUrls;
                        }
                    });
                    UserPreference.setObject("feedback_list", $scope.feedBackList);
                    $scope.insertTimeLabelToMsg($scope.feedBackList.length - 1);
                    console.log(resp);
                    if ($scope.feedBackList[0].userName == $scope.userName) {
                        $scope.isRight = true;
                    } else {
                        $scope.isRight = false;
                    }

                }
            });
        };

        //插入一条时间
        $scope.insertTimeLabelToMsg = function (i) {
            for (; i >= 0; i--) {
                var msg = $scope.feedBackList[i];
                var time1 = msg.datetime.replace(/\-/g, "/");
                var timestamp = Date.parse(new Date(time1)) / 1000;

                var msgDay = msg.datetime.substr(8, 2);
                var now = getNowFormatDate().substr(8, 2);

                if (i > 0) {
                    var preMsg = $scope.feedBackList[i - 1];
                    var timestamp1 = Date.parse(new Date(preMsg.datetime.replace(/\-/g, "/"))) / 1000;
                    var preMsgDay = preMsg.datetime.substr(8, 2);
                    if (now != msgDay) {
                        if (msgDay != preMsgDay)
                            msg.timeLabel = msg.datetime.substr(0,16); //formatTimeWithoutSecends(timestamp);
                        else
                            msg.timeLabel = undefined;
                    } else if (timestamp - timestamp1 >= 60 * 5) {
                        msg.timeLabel = msg.datetime.substr(11, 5); //formatTimeWithoutSecends(timestamp).substr(11, 5);
                    } else {
                        msg.timeLabel = undefined;
                    }

                } else {
                    if (now != msgDay) {
                        msg.timeLabel = msg.datetime.substr(0,16); //formatTimeWithoutSecends(timestamp);
                    } else
                        msg.timeLabel = msg.datetime.substr(11, 5); //formatTimeWithoutSecends(timestamp).substr(11, 5);
                }
            }
        };
        //点击图片放大
        $scope.viewImages = function (urls, index, $event) {
            $event.stopPropagation();
            ionicImageView.showViewModal({
                allowSave: true
            }, urls, index);
        };

        //弹出底部视图
        $scope.selectImg = function () {
            $timeout(function () {
                if (window.cordova)
                    cordova.plugins.Keyboard.close();
            }, 10);
            var resp = $ionicScrollDelegate.$getByHandle('userMessageScroll').getScrollPosition();
            //$scope.isShow = !$scope.isShow;
            $scope.emotionShow = false;
            $timeout(function () {
                $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollBottom(true);
            }, 100);

            $timeout(function () {
                $ionicScrollDelegate.resize();
                CameraHelper.selectImage('suggest');
            }, 100);

        };

        $scope.$on(BroadCast.IMAGE_SELECTED, function (a, rst) {
            if (rst && rst.which === 'suggest') {
                //上传
                $scope.sendFeedbackRequest('1', rst.source);

            }
        });
        //底部弹出表情
        $scope.showEmotions = function ($event) {

            setTimeout(function () {
                if (window.cordova)
                    cordova.plugins.Keyboard.close();
                $scope.popover.show($event);
            }, 100);

            $timeout(function () {
                $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollBottom(true);
            }, 100);

            //   var resp  = $ionicScrollDelegate.$getByHandle('userMessageScroll').getScrollPosition();
            //     $scope.emotionShow = !$scope.emotionShow;
            //     $scope.isShow = false;

            //     if($scope.emotionShow){
            //         $timeout(function () {
            //             $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollTo(0,resp.top+200,true); 
            //         }, 100);

            //     }else{
            //         $timeout(function () {
            //             $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollBottom(true);
            //         }, 100);
            //     }
        };

        $scope.switchPhoto = function (index) {
            //1相册 0 拍照
            var srcType = Camera.PictureSourceType.PHOTOLIBRARY;
            if (index === 0) {
                srcType = Camera.PictureSourceType.CAMERA;
            }
            var imgOpt = {
                allowEdit: false
            };
            var destinationType = Camera.DestinationType.DATA_URL;
            if ((imgOpt && imgOpt.allowEdit)) {
                destinationType = Camera.DestinationType.FILE_URI;
            }
            if (index === 0 || index === 1) {
                var defaultHeight = 300;
                var defaultWidth = 300;
                // var cropTitle = '截取高亮区域';
                var options = {
                    quality: 80,
                    destinationType: destinationType,
                    sourceType: srcType,
                    allowEdit: false,
                    encodingType: Camera.EncodingType.JPEG,
                    targetWidth: Constant.CAPTURE_IMAGE_RANGE,
                    targetHeight: Constant.CAPTURE_IMAGE_RANGE,
                    correctOrientation: true,
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: false
                };
                if (imgOpt) {
                    if (imgOpt.width && imgOpt.height) {
                        defaultHeight = imgOpt.height;
                        defaultWidth = imgOpt.width;
                    } else {
                        options.targetWidth = defaultWidth * 2;
                        options.targetHeight = defaultHeight * 2;
                    }
                    if (imgOpt.title)
                        cropTitle = imgOpt.title;
                }
                $cordovaCamera.getPicture(options).then(function (imageURI) {
                    var base64Str = 'data:image/jpeg;base64,' + imageURI;
                    //上传
                    $scope.sendFeedbackRequest('1', base64Str);

                }, function (err) {

                });
            }
        };

        $scope.setValue = function (e) {
            console.log(e);
            if ($scope.input.message)
                $scope.input.message += e;
            else
                $scope.input.message = e;

            var tObj = document.getElementById("suggestTextinput");
            var sPos = tObj.value.length;
            setCaretPosition(tObj, sPos);
            $scope.popover.hide();
        };
        //隐藏footView
        $scope.hideFootView = function () {
            if ($scope.emotionShow === true || $scope.isShow === true) {
                $scope.emotionShow = false;
                $scope.isShow = false;
                $timeout(function () {
                    $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollBottom(true);
                }, 100);
            }

        };

        //下拉刷新
        $scope.loadMore = function () {
            //关闭下拉刷新
            $scope.$broadcast('scroll.refreshComplete');
        };

        //离开页面时  将所有未读的 标记为已读状态
        $scope.$on("$ionicView.leave", function (event, data) {
            $scope.selected = true;
            // $scope.feedBackList.array.forEach(element => {

            // });
            $scope.fixFeedbackState();
        });

        //标记消息为已读
        $scope.fixFeedbackState = function () {

            Requester.fixNewsReadStatus('').then(function (resp) {
                if (resp.result) {
                    console.log('已成功标记为已读');
                }
            });
        };

        $scope.convertMsgtoHtml = function (msg) {
            return encodeHtml(msg);
        };

    });
angular.module('app.controllers')
.controller('userProfileCtrl', function ($scope, $ionicHistory, $ionicActionSheet, $ionicPopup, CameraHelper, $state, BroadCast, UserPreference, SettingService, MESSAGES, $ionicLoading, toaster) {

    $scope.$on("$ionicView.beforeEnter", function (event, data) {
        $scope.user = UserPreference.getObject("user");
        if (!$scope.user.logo || $scope.user.logo.trim() === '')
            $scope.user.logo = 'img/icon/person.png';
    });

    $scope.openEditView = function (content) {
        $state.go('edit_profile', {content: content});
    };

    $scope.goBack = function () {
        $ionicHistory.nextViewOptions({
            disableBack: true
        });
        $state.go('tabsController.settingPage');
    };

    $scope.$on(BroadCast.IMAGE_SELECTED, function (a, rst) {
        if (rst && rst.which === 'av') {
            $scope.user.logo = rst.source;
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
            SettingService.editInfo('logo', rst.source.substr(rst.source.indexOf('base64,') + 7));
        }
    });

    $scope.$on(BroadCast.EDIT_INFO, function (a, rst) {
        if (rst && rst.result)
            toaster.success({title: MESSAGES.SAVE_SUCCESS, body: MESSAGES.INFO_UPDATED});
        else
            toaster.error({title: MESSAGES.REMIND, body: rst.message});
        $ionicLoading.hide();
    });

    $scope.$on(BroadCast.CONNECT_ERROR, function (arg, data) {
        $ionicLoading.hide();
    });

    $scope.selectImage = function () {
        CameraHelper.selectImage('av', {allowEdit: true});
    };


    $scope.onSexChange = function (sex) {
        SettingService.editInfo('sex', sex);
    };
});
angular.module('app.controllers')
.controller('editProfileCtrl', function ($scope, Constant, $ionicHistory, $stateParams, UserPreference, SettingService, BroadCast, toaster, MESSAGES, $ionicLoading, $state, Requester, CameraHelper) {
    $scope.visibleArea = $stateParams.content;
    $scope.user = UserPreference.getObject("user");
    $scope.defaultChildID = UserPreference.get('DefaultChildID');
    switch ($scope.visibleArea) {
        case 'name':
            $scope.pageTitle = "修改姓名";
            break;
        case 'password':
            $scope.pageTitle = "修改密码";
            break;
        case 'children':
            $scope.pageTitle = "子女信息";
            break;
    }

    $scope.pwd = {
        _old: '',
        _new: '',
        _confirm: ''
    };
    $scope.save = function () {
        switch ($scope.visibleArea) {
            case 'name':
                SettingService.editInfo('name', $scope.user.name);
                break;
            case 'password':
                var reg = /^[a-zA-Z0-9]+$/;
                if (!$scope.pwd._old.match(reg) || !$scope.pwd._new.match(reg)) {
                    toaster.warning({title: MESSAGES.REMIND, body: MESSAGES.LOGIN_PASSWORD_PATTERN});
                    return;
                }
                if ($scope.pwd._new != $scope.pwd._confirm) {
                    toaster.warning({title: MESSAGES.REMIND, body: MESSAGES.PASSWORD_CONFIRM_ERROR});
                    return;
                }
                SettingService.changePwd($scope.pwd._old, $scope.pwd._new);
                break;
        }
        $ionicLoading.show({
            noBackdrop: true,
            template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
        });
    };

    $scope.setDefaultChild = function (child) {
        $scope.defaultChildID = child.id;
        UserPreference.set('DefaultChildID', child.id);
        UserPreference.set('DefaultClassID', child.classno.id);
        UserPreference.set('DefaultChildName', child.student_name);
        UserPreference.set('DefaultSchoolName', child.school.schoolName);
        UserPreference.set('DefaultSchoolID', child.school.id);
    };

    $scope.goBack = function () {
        $ionicHistory.nextViewOptions({
            disableBack: true
        });
        if ($scope.visibleArea === 'children') {
            $state.go('tabsController.settingPage');
        }
        else {
            $state.go('user_profile');
        }
    };

    $scope.selectImage = function (child) {
        $scope.toChangeChild = child;
        CameraHelper.selectImage('av', {allowEdit: true});
    };

    $scope.$on(BroadCast.IMAGE_SELECTED, function (a, rst) {
        if (rst && rst.which === 'av') {
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
            Requester.modifyStuInfo($scope.toChangeChild.id, 'logo', rst.source.substr(rst.source.indexOf('base64,') + 7)).then(function (resp) {
                if (resp.result) {
                    toaster.success({title: '修改成功'});
                    $scope.toChangeChild.logo = rst.source;
                    for (var i = 0; i < $scope.user.student; i++) {
                        if ($scope.user.student[i].id == $scope.toChangeChild.id) {
                            $scope.user.student[i] = $scope.toChangeChild;
                            UserPreference.setObject('user', user);
                            break;
                        }
                    }
                }
            }).finally(function () {
                $ionicLoading.hide();
            });
        }
    });

    $scope.$on(BroadCast.EDIT_INFO, function (a, rst) {
        if (rst && rst.result) {
            toaster.success({title: MESSAGES.SAVE_SUCCESS, body: MESSAGES.INFO_UPDATED});
            $scope.goBack();
        }
        else
            toaster.error({title: MESSAGES.REMIND, body: rst.message});
        $ionicLoading.hide();
    });

    $scope.$on(BroadCast.PASSWORD_CHANGE, function (a, rst) {
        if (rst && rst.result) {
            toaster.success({title: MESSAGES.REMIND, body: MESSAGES.PWD_UPDATED});
            $scope.goBack();
        }
        else
            toaster.error({title: MESSAGES.REMIND, body: rst.message});
        $ionicLoading.hide();
    });

    $scope.$on(BroadCast.CONNECT_ERROR, function (arg, data) {
        $ionicLoading.hide();
    });
});
angular.module('app.controllers')
.controller('notificationSettingCtrl', function ($scope, $ionicHistory, PushService, UserPreference, toaster, MESSAGES, $state, $cordovaInAppBrowser, $rootScope) {
    $scope.goBack = function () {
        $ionicHistory.nextViewOptions({
            disableBack: true
        });
        $state.go('tabsController.settingPage');
    };

    $scope.revStatus = UserPreference.getObject('PushSetting');

    PushService.getUserNotificationSettings(function (result) {
        if (result === 0) {
            // 系统设置中已关闭应用推送。
            $scope.revStatus.checked = false;
            toaster.warning({title: MESSAGES.PUSH_ON_FAIL_REASON, body: ''});
        } else if (result > 0) {
            // 系统设置中打开了应用推送。
        }
    });

    if (isEmptyObject($scope.revStatus))
        $scope.revStatus = {checked: true, soundChecked: true, vibrateChecked: true};

    $scope.pushNotificationChange = function () {
        //console.log($scope.revStatus.checked);
        if ($scope.revStatus.checked) {
            PushService.resumePush();
            PushService.getUserNotificationSettings(function (result) {
                if (result === 0) {
                    // 系统设置中已关闭应用推送。
                    $scope.revStatus.checked = false;
                    setTimeout(function () {
                        toaster.warning({title: MESSAGES.PUSH_ON_FAIL, body: MESSAGES.PUSH_ON_FAIL_REASON});
                    }, 0);
                } else if (result > 0) {
                    // 系统设置中打开了应用推送。
                }
            });
        }
        else
            PushService.stopPush();
        UserPreference.setObject('PushSetting', $scope.revStatus);
    };

    $scope.pushNotificationBehaveChange = function () {
        if ($scope.revStatus.soundChecked && $scope.revStatus.vibrateChecked) {
            //allow both
            PushService.setBasicNotification("all");
        }
        else if (!$scope.revStatus.soundChecked && $scope.revStatus.vibrateChecked) {
            //allow only vibrate
            PushService.setBasicNotification("vibrate");
        }
        else if ($scope.revStatus.soundChecked && !$scope.revStatus.vibrateChecked) {
            //allow only sound
            PushService.setBasicNotification("sound");
        }
        else {
            PushService.setBasicNotification(null);
        }
        UserPreference.setObject('PushSetting', $scope.revStatus);
    };

    $scope.goInstruction = function () {
        var options = {
            location: 'no',
            clearcache: 'yes',
            toolbar: 'no',
            hidden: 'yes'
        };
        $cordovaInAppBrowser.open('http://xgenban.com/wmdp/mobile/msgtips1.html', '_blank', options)
            .then(function (event) {
                // success
                $cordovaInAppBrowser.show();
            })
            .catch(function (event) {
                // error
                console.log(event);
                $cordovaInAppBrowser.close();
            });
    };

    $rootScope.$on('$cordovaInAppBrowser:loadstart', function (e, event) {
        var url = event.url;
        var iOut = url.indexOf("CloseNewsTab");
        if (iOut >= 0) {
            $cordovaInAppBrowser.close();
        }
    });

    $scope.isAndroid = function () {
        return ionic.Platform.isAndroid();
    };
});

angular.module('app.controllers')
.controller('versionIntroduceCtrl',function($scope,Requester,UserPreference){
    UserPreference.set('VersionIntroduce','READ');

    Requester.getCurrentVersionIntroduce().then(function(res){
        if(res.result){
            console.log("版本说明 succeed");
            console.log(res);   
            $scope.text = res.data.versionDescribe;
        }else{
            console.log("版本说明 failure");
        }

    });
});
angular.module('app.controllers')
.controller('userProtocolCtrl', function ($scope, Constant, $ionicHistory, $state) {
    

});
angular.module('app.controllers')
    .controller('assetsStatisticCtrl', function ($scope, Constant, $state, $ionicModal, Requester, toaster, UserPreference,$ionicPopup) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {

            $scope.select = {
                selectIndex1: 0
            };
            $scope.detail = UserPreference.getObject('SchoolAssetsDetail');
            $scope.getSchoolAssetsDetail();
            $scope.emptyContent = false;
            $scope.data = {
                y: [],
                x: [],
                unit: '%' //$scope.unit
            };
            var user = UserPreference.getObject('user');
            $scope.authDeviceManage = user.authDeviceManage && user.authDeviceManage === 1; //设备授权
            $scope.authAssertManage = user.authAssertManage && user.authAssertManage === 1; //资产授权
            if( !$scope.authDeviceManage&&$scope.authAssertManage){
                $scope.checkAuthOrAccountIsOverdue();
            }
        });

        $scope.$on("$ionicView.loaded", function (event, data) {

        });
        $scope.tab1Click = function (index) {
            $scope.select.selectIndex1 = index;
            $scope.data = {
                y: [],
                x: [],
                unit: '' //$scope.unit
            };
            if (index === 0) {
                $scope.getSchoolAssetsRank();
            } else {
                $scope.getFactoryAssetsRank();
            }
        };

        $scope.makeChart = function (y, x) {
            $scope.chartHeight = y.length * 40 + 25;
            $scope.data = {
                y: y,
                x: x,
                unit: '' //$scope.unit
            };
        };

        //request --资产概况
        $scope.getSchoolAssetsDetail = function () {
            Requester.getSchoolAssetsDetail().then(function (rest) {
                if (rest.result) {
                    $scope.detail = rest.data;
                    UserPreference.setObject('SchoolAssetsDetail', rest.data);
                } else {
                    // $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).then(function () {
                $scope.getSchoolAssetsRank();
            });
        };

        //学校资产排行
        $scope.getSchoolAssetsRank = function () {
            Requester.getSchoolAssetsRank().then(function (rest) {
                if (rest.result) {
                    //rest.data = [{name:'阿里一中1',count:240},{name:'阿里一中2',count:180},{name:'阿里一中3',count:60}];
                    if (rest.data && rest.data.length > 0) {
                        $scope.emptyContent = false;
                        var x = [],
                            y = [];
                        var dataArr = rest.data.length>10?rest.data.slice(0,10):rest.data;
                        for (var i = dataArr.length-1; i >=0; i--) {
                           
                            var sd = dataArr[i];
                            y.push(i + 1 + '.' + sd.name);
                            var rate = sd.count;
                           x.push(rate);
                        }
                        $scope.makeChart(y, x);
                    }else{
                        $scope.emptyContent = true;  
                    }
                } else {
                    // $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };

        //厂家资产排行
        $scope.getFactoryAssetsRank = function () {
            Requester.getFactoryAssetsRank().then(function (rest) {
                if (rest.result) {
                   // rest.data = [{name:'阿里一中32',count:20},{name:'阿里一中21',count:18},{name:'阿里一中11',count:15}];
                    if (rest.data && rest.data.length > 0) {
                        $scope.emptyContent = false;
                        var x = [],
                            y = [];
                        var dataArr = rest.data.length>10?rest.data.slice(0,10):rest.data;
                        for (var i = dataArr.length-1; i >=0; i--) {
                            var sd = dataArr[i];
                            y.push(i + 1 + '.' + sd.name);
                            var rate = sd.count;
                           x.push(rate);
                        }
                        $scope.makeChart(y, x);
                    }else{
                        $scope.emptyContent = true;  
                    }
                } else {
                    // $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };

        $scope.checkAuthOrAccountIsOverdue = function () {
            var beforeTamp = UserPreference.get('AreaAssetsAuthAlertDateTamp');
            var currentDate = getNowDate();
            var currentTamp = Date.parse(currentDate.replace(/\-/g, "/")) / 1000;
            var interval = 24 * 60 * 60;
            var flag = currentTamp - beforeTamp >= interval;  
            if (!beforeTamp || beforeTamp==='undefined'||(beforeTamp && flag)) {
                Requester.checkAuthOrAccountIsOverdue().then(function (resp) {
                    if (resp.result) {
                        $scope.authList = resp.data;
                        UserPreference.set('AreaAssetsAuthAlertDateTamp', currentTamp);
                        // console.log('flag:'+flag+'--beforeTamp :'+beforeTamp +'current:'+ currentTamp );
                        if(resp.data){
                            $scope.authAlert = $ionicPopup.show({
                                template: '<div style="color:#666;font-size:15px;padding:10px;width:100%;"><div ng-repeat="item in authList">{{item.authModeName}}功能权限将于<span style="color:#2bcab2;font-weight:bold;">{{item.expireDate.substr(0,10)}}</span>过期</div> <div>过期后对应模块在APP及网页端均不能在使用，续期请联系<span style="color:#2bcab2;font-weight:bold;">QQ客服(1833025389)</span>或小跟班业务员</div><div class="button" ng-click="closeAuthAlert()" style="height:30px;width:calc(100% - 80px);margin-left:40px;margin-top:15px;background:#2bcab2;color:white;">知道了</div></div>',
                                // templateUrl: UIPath + 'common/areaAuthalert.html',
                                title: '<div style="color:white !important;">授权即将到期提醒</div><div class="logo" ><img src="img/icon/logo.png" /></div>',
                                scope: $scope,
                                cssClass: 'auth_alert'
                            }); 
                        }
                                  
                    } else {
                        console.log('未过期');
                    }
                });
            }
           
        };

        //关闭授权提示框
        $scope.closeAuthAlert = function () {
            //UserPreference.set('AreaAuthAlertRead', 'read');
            $scope.authAlert.close();
        };

    });
angular.module('app.controllers')
    .controller('exerciseNoticeDetailCtrl', function ($scope, Constant, $stateParams, $ionicModal, Requester, toaster, UserPreference) {
        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.drillId = $stateParams.itemId;
            $scope.title = $stateParams.title;
            $scope.getDrillDetail(); 

        });

        $scope.$on("$ionicView.loaded", function (event, data) {
            // $scope.drillId = ''; //演练id

            $scope.selected = {
                drillTime:'',
                detail:{}
            };
        });

        //查看方案详情
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'page/';
        $ionicModal.fromTemplateUrl(UIPath + 'home/others/safetyManage/exerciseProgramDetail.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.programDetailModal = modal;
        });
        $scope.goToProgramDetail = function () {
            $scope.programDetailModal.show();
        };
        //关闭演练方案详情
        $scope.hideProgramModal = function () {
            $scope.programDetailModal.hide();
        };

        //request --演练详情
        $scope.getDrillDetail = function () {
            Requester.getDrillDetail($scope.drillId).then(function (rest) {
                if (rest.result) {
                    $scope.selected.detail = rest.data;
                    $scope.selected.programName = rest.data.drillPlanName;
                    $scope.programId = rest.data.tempDrillPlanId;
                    return rest.result;
                } else {
                    // $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).then(function(result){
                if(result){
                 // $scope.getDrillProgramDetail($scope.programId); 
                }
          });
        };

        //request --查看演练方案详情
        $scope.getDrillProgramDetail = function (programId) {
            Requester.getDrillProgramDetail(programId).then(function (rest) {
                if (rest.result) {
                    $scope.programDetailText = rest.data.content;
                } else {
                    // $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };
    });
angular.module('app.controllers')
    .controller('exerciseNoticeListCtrl', function ($scope, Constant, $state, $ionicModal, $ionicPopup, Requester, toaster, UserPreference, ionicDatePicker, ionicTimePicker,BroadCast) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
           
            $scope.page = 1;
           // $scope.programList = []; //方案列表
            $scope.selected = {
                programName: '',
                exerciseTime: '',
                title: '',
                address: '',
                content: '',
                programId :0,
               
            };

          $scope.getDrillRecordList();
          $scope.programList = UserPreference.getArray('programList')?UserPreference.getArray('programList'):[];
          $scope.getDrillProgramList(); //演练方案列表
          
        });

        $scope.$on("$ionicView.loaded", function (event, data) {
          console.log('--load');
        });

        //下拉刷新
        $scope.refreshData = function () {
            $scope.page = 1;
            $scope.getDrillRecordList();
        };
        //上拉记载
        $scope.loadMore = function () {
            $scope.page++;
            $scope.getMoreDataList();
        };

        //详情或记录
        $scope.goToNextPage = function (type,item) {
            if (type === 'detail') {
                $state.go('exerciseNoticeDetail',{itemId:item.id,title:item.title});
            } else {
                $state.go('exerciseRecord',{itemId:item.id,startTime:item.drillStartTime,endTime:item.drillEndTime});
            }
        };


        //添加演练
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'page/';
        $ionicModal.fromTemplateUrl(UIPath + 'home/others/safetyManage/addExerciseModal.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.exerciseModal = modal;
        });
        $scope.addExerciseModalFunc = function () {
        
            $scope.exerciseAlert = $ionicPopup.show({
                templateUrl: UIPath + 'home/others/safetyManage/selectExerciseTypeAlert.html',
                title: '<div style="color:white !important;">选择演练方案</div>',
                scope: $scope,
                cssClass: 'approval_alert',
            });

        };
        $scope.addExerciseProgram = function (item) {
            $scope.selected.programName = item.drillName;
            $scope.selected.title =  $scope.selected.programName + '演练通知';
            $scope.selected.programId = item.id;
            $scope.exerciseAlert.close();
            // $scope.exerciseModal.show();
            $state.go('addExerciseModal',{
                programId : $scope.selected.programId,
                programName:item.drillName
            });
        };
        //
        $scope.hideAddExerciseModal = function () {
            $scope.exerciseModal.hide();
        };

        //关闭弹窗
        $scope.cancelChoose = function () {
            $scope.exerciseAlert.close();
        };
        //查看演练方案详情
        if (Constant.debugMode) UIPath = 'page/';
        $ionicModal.fromTemplateUrl(UIPath + 'home/others/safetyManage/exerciseProgramDetail.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.programDetailModal = modal;
        });
        $scope.goToProgramDetail = function(){
            $scope.getDrillProgramDetail($scope.selected.programId);
            $scope.programDetailModal.show();
        };
        //关闭演练方案详情
        $scope.hideProgramModal = function(){
            $scope.programDetailModal.hide();
        };

        //选择演练时间
        $scope.selectExerciseTime = function (event) {
            event.stopPropagation();
            if (window.cordova) cordova.plugins.Keyboard.close();
            var ipObj1 = {
                callback: function (val) {
                    if (typeof (val) !== 'undefined') {

                        $scope.recordTime = formatTimeWithoutSecends(val / 1000).substr(0, 10);
                        $scope.showTimePicker();
                    }
                }
            };
            ionicDatePicker.openDatePicker(ipObj1);
        };

        $scope.showTimePicker = function () {
            var ipObj1 = {
                inputTime: (((new Date()).getHours() * 60 * 60) + ((new Date()).getMinutes() * 60)),
                callback: function (val) {
                    if (typeof (val) === 'undefined') {} else {
                        var showTime = new Date(val * 1000);
                        var h = showTime.getUTCHours();
                        var m = showTime.getUTCMinutes();
                        var time = (h < 10 ? '0' + h : h) + ':' + (m < 10 ? '0' + m : m);
                        //  $scope.recordTime = time;
                        if(time)
                        $scope.selected.exerciseTime = $scope.recordTime + ' ' + time;
                    }
                }

            };
            //
            setTimeout(function(){
                ionicTimePicker.openTimePicker(ipObj1);
            },100);
           
        };

        //发起演练
        $scope.saveExercise = function(){
          //  console.lo
          console.log($scope.selected);
          if(!$scope.selected.exerciseTime){
            toaster.warning({
                // title: "温馨提示",
                body: '请选择演练时间'
            }); 
            
            return;
          }
            $scope.sendDrillRecord($scope.selected.programId); 
        };

        $scope.$on(BroadCast.CONNECT_ERROR, function () {
            $scope.isMoreData = true;
           // $ionicLoading.hide();
        });

        //request --获取列表
        $scope.getDrillRecordList = function () {
            Requester.getDrillRecordList($scope.page).then(function (rest) {
                if (rest.result) {
                    console.log('调课记录列表');
                    $scope.list = rest.data.content;
                } else {
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        //request --加载更多
        $scope.getMoreDataList = function () {
            Requester.getDrillRecordList($scope.page).then(function (rest) {
                if (rest.result) {
                    var data = rest.data.content;
                    for (var i = 0; i < data.length; i++) {
                        $scope.list.push(data[i]);
                    }
                    if (!data || (data && data.length < Constant.reqLimit)) {
                        $scope.isMoreData = true;
                    }
                } else {
                    $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };
        //request --获取演练方案列表
        $scope.getDrillProgramList = function () {
            Requester.getDrillProgramList().then(function (rest) {
                if (rest.result) {
                    $scope.programList = rest.data;
                   
                    UserPreference.setObject('programList',rest.data);
                } else {
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };

        //request --查看演练方案详情
        $scope.getDrillProgramDetail = function (programId) {
            Requester.getDrillProgramDetail(programId).then(function (rest) {
                if (rest.result) {
                    $scope.programDetailText = rest.data.content;
                } else {
                    // $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };

        //发起演练
        $scope.sendDrillRecord = function (tempDrillPlanId) {
            Requester.sendDrillRecord(tempDrillPlanId, $scope.selected).then(function (rest) {
                if (rest.result) {
                    $scope.exerciseModal.hide();
                    toaster.success({
                        title: "",
                        body: "发起演练成功",
                        timeout: 3000
                    });
                    $scope.selected = {
                        programName: '',
                        exerciseTime: '',
                        title: '',
                        address: '',
                        content: '',
                        programId :0,
                       
                    };
                    $scope.page = 1;
                    $scope.getDrillRecordList();
                } else {
                    // $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };



    });
angular.module('app.controllers')
    .controller('exerciseRecordCtrl', function ($scope, Constant, $state, CameraHelper,$ionicModal, BroadCast, Requester, $stateParams, $q, toaster, $ionicLoading, ionicDatePicker, ionicTimePicker) {
        $scope.$on("$ionicView.beforeEnter", function (event, data) {

            // $scope.title = $stateParams.title;
            //    $scope.getDrillRedordDetail(); 

        });

        $scope.$on("$ionicView.loaded", function (event, data) {

            $scope.startTime = String($stateParams.startTime);
            $scope.endTime = $stateParams.endTime;
            $scope.recordStartTime = $scope.startTime; //用于记录修改前的开始时间
            $scope.recordEndTime = $scope.endTime;
            $scope.intervalTime = $scope.getTimeDistant($scope.startTime, $scope.endTime);
            // console.log('start:'+startTime);
            $scope.selected = {
                remarks: '',
                picdatas: []
            };

            $scope.drillId = $stateParams.itemId; //演练id

            $scope.reportImgs = [];

            $scope.page = 1;

            $scope.getDrillRedordDetail();
        });

        //添加记录
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'page/';
        $ionicModal.fromTemplateUrl(UIPath + 'home/others/safetyManage/addExerciseRecord.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.recordModal = modal;
        });
        $scope.addRecordModal = function () {
            $scope.recordModal.show();
        };

        $scope.hideModal = function () {
            $scope.recordModal.hide();
        };


        //选择图片
        $scope.selectImg = function () {
            if (window.cordova) {
                CameraHelper.selectMultiImage(function (resp) {
                    if (!resp)
                        return;
                    if (resp instanceof Array) {
                        Array.prototype.push.apply($scope.reportImgs, resp);
                        $scope.$apply();
                    } else {
                        $scope.reportImgs.push(
                            "data:image/jpeg;base64," + resp
                        );
                    }
                }, 9 - $scope.reportImgs.length);
            } else {
                //web 端
                var input = document.getElementById("capture");
                input.click();
            }
        };
        // input形式打开系统系统相册
        $scope.getFile = function (files) {
            var file = files[0];
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (theFile) {
                setTimeout(function () {
                    $scope.testImg = theFile.target.result; //设置一个中间值
                    $scope.reportImgs.push($scope.testImg);

                }, 100);
            };
        };

        $scope.removeImg = function (index, $event) {
            $event.stopPropagation();
            $scope.reportImgs.splice(index, 1);
        };
        //选择日期
        $scope.selectTime = function (arg) {
            var ipObj1 = {
                callback: function (val) {
                    if (typeof (val) !== 'undefined') {
                        var nowTime = '';

                        if (arg === 'start') {
                            nowTime = formatTimeWithoutSecends(val / 1000).substr(0, 10);
                        } else if (arg === 'end') {
                            nowTime = formatTimeWithoutSecends(val / 1000).substr(0, 10);
                        }
                        $scope.showTimePicker(arg, nowTime);
                    }
                }
            };
            ionicDatePicker.openDatePicker(ipObj1);
        };

        $scope.showTimePicker = function (arg, nowTime) {
            var ipObj1 = {
                inputTime: (((new Date()).getHours() * 60 * 60) + ((new Date()).getMinutes() * 60)),
                callback: function (val) {
                    if (typeof (val) === 'undefined') {} else {
                        var showTime = new Date(val * 1000);
                        var h = showTime.getUTCHours();
                        var m = showTime.getUTCMinutes();
                        var time = (h < 10 ? '0' + h : h) + ':' + (m < 10 ? '0' + m : m);
                        // $scope.timeBtnTxt = time;
                        if (arg === 'start') {
                            $scope.recordStartTime = nowTime + ' ' + time;
                            var startTamp1 = Date.parse(new Date( $scope.recordStartTime.replace(/\-/g, "/"))) / 1000;
                            var endtamp1 = '';
                            if($scope.endTime)
                             endtamp1 = Date.parse(new Date( $scope.endTime.replace(/\-/g, "/"))) / 1000;
                            if($scope.endTime&&endtamp1<startTamp1){
                                toaster.warning({
                                   // title: "温馨提示",
                                    body: '结束时间不能早于开始时间'
                                });
                                return;
                            }
                            $scope.fixedDrillStartDateOrEndDate($scope.recordStartTime, $scope.recordEndTime);
                        } else {
                            $scope.recordEndTime = nowTime + ' ' + time;
                            var startTamp = Date.parse(new Date( $scope.startTime.replace(/\-/g, "/"))) / 1000;
                            var endtamp = Date.parse(new Date( $scope.recordEndTime.replace(/\-/g, "/"))) / 1000;
                            if(endtamp<startTamp){
                                toaster.warning({
                                   // title: "温馨提示",
                                    body: '结束时间不能早于开始时间'
                                });
                                return;
                            }
                            $scope.fixedDrillStartDateOrEndDate($scope.recordStartTime, $scope.recordEndTime);
                        }

                    }
                }

            };
            ionicTimePicker.openTimePicker(ipObj1);
        };

        function onImageResized(resp) {
            $scope.selected.picdatas.push(resp);
        }
        //提交演练记录
        $scope.commitAddExerciseRecord = function () {
           
            $scope.selected.picdatas = [];
            var promiseArr = [];
            $scope.reportImgs.forEach(function (img) {
                promiseArr.push(resizeImage(Constant.CAPTURE_IMAGE_RANGE, img).then(onImageResized));
            });
            $scope.isLoading = true;
            $q.all(promiseArr).then(function () {
                $ionicLoading.show({
                    noBackdrop: true,
                    template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
                });
                $scope.addDrillRecord();

            });

        };
        $scope.$on(BroadCast.CONNECT_ERROR, function () {
            $scope.isLoading = false;
            $scope.isMoreData = true;
            $ionicLoading.hide();
        });

        //加载更多
        $scope.refreshData = function () {
            $scope.page = 1;
            $scope.getDrillRedordDetail();
        };

        //加载更多
        $scope.loadMore = function () {
            $scope.page++;
            $scope.getMoreData();
        };

        // 计算时长的
        $scope.getTimeDistant = function (start, end) {
            var result = '--';
            if (!end || !start) {
                return result;
            }
            //获取当前时间的时间戳
            var startTamp = Date.parse(new Date(start.replace(/\-/g, "/"))) / 1000;
            var endtamp = Date.parse(new Date(end.replace(/\-/g, "/"))) / 1000;
            var interval = (endtamp - startTamp) / 60;
            // if (interval > 60) {
            //     result = (interval - interval % 60) / 60 + '小时' + interval % 60 + '分钟';
            // } else {
            //     result = interval + '分钟';
            // }
            result = interval + '分钟';
            return result;
        };

        //request ---演练记录详情
        $scope.getDrillRedordDetail = function () {

            Requester.getDrillRedordDetail($scope.drillId, $scope.page).then(function (rest) {
                if (rest.result) {
                    $scope.list = rest.data.content;

                    $scope.list.forEach(function (item) {
                        var picUrls = [];
                        item.picUrls = [];
                        picUrls = item.strImageUrls ? item.strImageUrls.split(',') : [];
                        picUrls.forEach(function (url) {
                            item.picUrls.push({
                                thumb: url,
                                src: url
                            });
                        });
                    });
                } else {
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });

        };

        //request --加载更多
        $scope.getMoreData = function () {
            Requester.getDrillRedordDetail($scope.drillId, $scope.page).then(function (rest) {
                if (rest.result) {
                    var data = rest.data.content;
                    for (var i = 0; i < data.length; i++) {
                        //var item = data[i];
                        $scope.list.push(data[i]);
                    }
                    $scope.list.forEach(function (item) {
                        var picUrls = [];
                        item.picUrls = [];
                        picUrls = item.strImageUrls ? item.strImageUrls.split(',') : [];
                        picUrls.forEach(function (url) {
                            item.picUrls.push({
                                thumb: url,
                                src: url
                            });
                        });
                    });
                    if (!data || (data && data.length < Constant.reqLimit)) {
                        $scope.isMoreData = true;
                    }
                } else {
                    $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        //request --添加演练记录
        $scope.addDrillRecord = function () {
            Requester.addDrillRecord($scope.drillId, $scope.selected).then(function (rest) {
                if (rest.result) {
                    $ionicLoading.hide();
                    $scope.isLoading = false;
                    $scope.recordModal.hide();
                    toaster.success({
                        title: "",
                        body: "添加成功",
                        timeout: 3000
                    });

                    $scope.selected = {
                        remarks: '',
                        picdatas: []
                    };
                    $scope.reportImgs = [];
                    $scope.page = 1;
                    $scope.getDrillRedordDetail();
                } else {
                    $ionicLoading.hide();
                    $scope.isLoading = false;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).finally(function () {
                setTimeout(function () {
                    $ionicLoading.hide();
                    $scope.isLoading = false;
                }, 5000);
            });
        };

        //修改演练时间
        $scope.fixedDrillStartDateOrEndDate = function (startTime, endTime) {
            Requester.fixedDrillStartDateOrEndDate($scope.drillId, startTime, endTime).then(function (rest) {
                if (rest.result) {
                    toaster.success({
                        title: "",
                        body: "修改成功",
                        timeout: 3000
                    });
                    $scope.startTime = startTime;
                    $scope.endTime = endTime;
                    $scope.intervalTime = $scope.getTimeDistant($scope.startTime, $scope.endTime);
                    // console.log('修改成功');
                    // console.log(rest);

                } else {
                    // $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };
    });
angular.module('app.controllers')
    .controller('hidenTroubleListCtrl', function ($scope, Constant, $state, $ionicModal, BroadCast,Requester, toaster, UserPreference, $ionicLoading, $rootScope) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
           
            
            $scope.waitDealCount = 0;
            $scope.dealingCount = 0;
            $scope.closedCount = 0;
            $scope.schoolId = UserPreference.get('DefaultSchoolID');
            $scope.page = 1;
            $scope.getHidenTroubleList();
        });

        $rootScope.$on('ADD_HidenTrouble_SUCCEED', function (arg) {
            // $scope.isMoreData = true;
            $scope.select.selectIndex1 = '';
        });

        $scope.$on("$ionicView.loaded", function (event, data) {
            console.log('load');
            $scope.select = {
                selectIndex1: '' //1待处理；2处理中；3关闭
            };
        });

        //隐患上报
        $scope.troubleReport = function () {
            $state.go('hidenTroubleReport');
        };
        $scope.tab1Click = function (index) {
            $scope.select.selectIndex1 = index;
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
            $scope.page = 1;
            $scope.isMoreData = false;
            $scope.getHidenTroubleList();
        };

        $scope.getDeviceType = function () {
            var result = 'android';
            $scope.isIos = ionic.Platform.isIOS();
            if ($scope.isIos) {
                if (window.screen.width >= 375 && window.screen.height >= 812) {
                    result = 'iphoneX';
                } else {
                    result = 'iphone';
                }

            } else {
                result = 'android';
            }
            return result;
        };

        //详情
        $scope.goToDetail = function (item) {
            $state.go('hidenTroubleDetail', {
                itemId: item.id
            });
        };


        //下拉刷新
        $scope.refreshData = function () {
            $scope.page = 1;
            $scope.getHidenTroubleList();
        };
        //上拉记载
        $scope.loadMore = function () {
            $scope.page++;
            $scope.getMoreDataList();
        };
        $scope.$on(BroadCast.CONNECT_ERROR, function () {
            $scope.isMoreData = true;
            $ionicLoading.hide();
        });

        //request 获取列表
        $scope.getHidenTroubleList = function () {
            Requester.getHidenTroubleList($scope.select.selectIndex1, $scope.schoolId, $scope.page).then(function (rest) {
                if (rest.result) {
                    $ionicLoading.hide();
                    $scope.list = rest.data.content;
                    $scope.getHidenTroubleCount();
                    // $scope.totalNumber = 
                    $scope.list.forEach(function (item) {
                        var picUrls = [];
                        item.picUrls = [];
                        picUrls = item.imgUrlList? item.imgUrlList : [];
                        picUrls.forEach(function (url) {
                            item.picUrls.push({
                                thumb: url,
                                src: url
                            });
                        });
                    });
                    
                } else {
                    $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
                setTimeout(function () {
                    $ionicLoading.hide();
                }, 5000);
            });
        };
        //request --加载更多
        $scope.getMoreDataList = function () {
            Requester.getHidenTroubleList($scope.select.selectIndex1, $scope.schoolId, $scope.page).then(function (rest) {
                if (rest.result) {
                    var data = rest.data.content;
                    for (var i = 0; i < data.length; i++) {
                        $scope.list.push(data[i]);
                    }
                    $scope.list.forEach(function (item) {
                        var picUrls = [];
                        item.picUrls = [];
                        picUrls = item.imgUrlList? item.imgUrlList : [];
                        picUrls.forEach(function (url) {
                            item.picUrls.push({
                                thumb: url,
                                src: url
                            });
                        });
                    });
                    if (!data || (data && data.length < Constant.reqLimit)) {
                        $scope.isMoreData = true;
                    }
                } else {
                    $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };
         //request-获取数量
         $scope.getHidenTroubleCount = function () {
            Requester.getHidenTroubleCount( $scope.schoolId).then(function (rest) {
                if (rest.result) {
                    for(var i=0;i<rest.data.length;i++){
                        if(rest.data[i].status===1){
                            $scope.waitDealCount = rest.data[i].count; 
                        }else if(rest.data[i].status===2){
                            $scope.dealingCount = rest.data[i].count; 
                        }else{
                            $scope.closedCount= rest.data[i].count;    
                        }
                    }
                } else {
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };


    });
angular.module('app.controllers')
    .controller('hidenTroubleDetailCtrl', function ($scope, Constant, $state, $ionicModal, Requester, toaster, UserPreference, $stateParams) {
        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.select = {
                selectManName: null,
                selectManId: null,
                content: '',
                status: 2
            };
            $scope.detail = {};

            $scope.list = [1, 2, 3];
            $scope.picUrls = [];

            $scope.schoolId = UserPreference.get('DefaultSchoolID');
            $scope.getSchoolTeachersList(); //获取学校教师列表

        });
        $scope.$on("$ionicView.loaded", function (event, data) {
            $scope.itemId = $stateParams.itemId;
            $scope.getHidenTroubleDetail();
        });
        //增加进度
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'page/';
        $ionicModal.fromTemplateUrl(UIPath + 'home/others/safetyManage/addDealHidenTroubleProgress.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.progressModal = modal;
        });
        $scope.addProgress = function () {
            $scope.requestCount = 0;
            $scope.progressModal.show();
        };

        $scope.hideAddProgressModal = function () {
            $scope.progressModal.hide();
        };

        //保存增加进度
        $scope.saveProgress = function () {
            if ($scope.select.status === 3) $scope.select.selectManId = 0;
            if($scope.requestCount===0) $scope.addHidenTroubleProgress();
            $scope.requestCount ++;
            
        };

        //选择指派人
        $ionicModal.fromTemplateUrl(UIPath + 'home/others/safetyManage/selectDealHidenTroubleMan.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.selectManModal = modal;
        });
        $scope.selectMan = function () {
            $scope.selectManModal.show();
        };
        //关闭选择指派人弹窗
        $scope.hideSelectManModal = function () {
            $scope.selectManModal.hide();
        };

        //删除指派人
        $scope.removeMan = function () {
            $scope.select.selectManId = 0;
            $scope.select.selectManName = '';
            $scope.select.isChecked = 0;
            // for (var i = 0; i < $scope.teacherList.length; i++) {
            //     var man = $scope.teacherList[i];
            //     man.checked = false;
            // }
        };

        //确定选择的人
        $scope.saveSelectMan = function () {
            //  console.log('select man :' + $scope.select.selectManId);
            //  console.log($scope.teacherList);
            for (var i = 0; i < $scope.teacherList.length; i++) {
                var man = $scope.teacherList[i];
                if (man.id == $scope.select.isChecked) {
                    $scope.select.selectManId = man.id;
                    $scope.select.selectManName = man.chname;
                    break;
                }
            }
            $scope.selectManModal.hide();
        };
        $scope.chooseMan = function (man) {
         
            // $scope.select.selectManId = man.id;
            // $scope.select.selectManName = man.chname;
            // console.log('select man :' + $scope.select.selectManId);
        };

        //request --隐患详情
        $scope.getHidenTroubleDetail = function () {
            Requester.getHidenTroubleDetail($scope.itemId).then(function (rest) {
                if (rest.result) {
                    $scope.detail = rest.data;
                    $scope.picUrls = [];
                    $scope.events = []; //处理进度
                    if (rest.data.imgUrlList && rest.data.imgUrlList.length > 0) {
                        rest.data.imgUrlList.forEach(function (img) {
                            $scope.picUrls.push({
                                src: img
                            });

                        });
                    }
                    $scope.events = $scope.detail.processResList;

                } else {
                    // $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).then(function () {

            });

        };


        //request --获取学校教师列表
        $scope.getSchoolTeachersList = function () {
            Requester.getSchoolTeachersList($scope.schoolId).then(function (rest) {
                if (rest.result) {
                    $scope.teacherList = rest.data;
                    for(var i=0 ;i<$scope.teacherList.length;i++){
                        $scope.teacherList[i].checked = false;
                    }
                } else {

                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });

        };

        //request --增加隐患进度
        $scope.addHidenTroubleProgress = function () {
            Requester.addHidenTroubleProgress($scope.select, $scope.itemId).then(function (rest) {
                $scope.requestCount = 0;
                if (rest.result) {
                    $scope.progressModal.hide();
                    toaster.success({
                        title: "",
                        body: "添加成功",
                        timeout: 3000
                    });

                    $scope.select = {
                        selectManName: null,
                        selectManId: 0,
                        content: '',
                        status: 2
                    };
                    $scope.getHidenTroubleDetail();
                } else {
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };

    });
angular.module('app.controllers')
    .controller('hidenTroubleReportCtrl', function ($scope, Constant, $state, $ionicModal, Requester, toaster,$timeout, UserPreference,CameraHelper, $q, $ionicHistory, $ionicLoading, $rootScope) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.select = {
                selectManName: '',
                selectManId: 0,
                location: '',
                title: '',
                message: '',
                picdatas: []
            };
            $scope.schoolId = UserPreference.get('DefaultSchoolID');
            $scope.reportImgs = [];
            $scope.getSchoolTeachersList();
        });

        $scope.$on("$ionicView.loaded", function (event, data) {
           
        });

        function onImageResized(resp) {
            $scope.select.picdatas.push(resp);
        }
         //提交隐患
         $scope.commitTrouble = function () {
             if( $scope.select.selectManId ===0|| !$scope.select.selectManId ){
                toaster.warning({
                    // title: "温馨提示",
                    body:'请选择要指派的老师'
                });
                return;
             }
            $scope.select.picdatas = [];
            var promiseArr = [];
          
            for(var i=0;i< $scope.reportImgs.length;i++ ){
                var img = $scope.reportImgs[i];
                promiseArr.push(resizeImage(Constant.CAPTURE_IMAGE_RANGE, img).then(onImageResized)); 
            }
            $scope.isLoading = true;
            $q.all(promiseArr).then(function () {
                $ionicLoading.show({
                    noBackdrop: true,
                    template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
                });
                $scope.addHidenTrouble();

            });
        };

        //选择图片
        $scope.selectImg = function () {
           
            if (window.cordova) {
                CameraHelper.selectMultiImage(function (resp) {
                    if (!resp)
                        return;
                    if (resp instanceof Array) {
                        Array.prototype.push.apply($scope.reportImgs, resp);
                        $scope.$apply();
                    } else {
                        $scope.reportImgs.push(
                            "data:image/jpeg;base64," + resp
                        );
                    }
                }, 9 - $scope.reportImgs.length);
            } else {
                
            //web 端
                var input = document.getElementById("capture101");
                input.click();
            }
        };

         // input形式打开系统系统相册
         $scope.getFile = function (files) {
            var file = files[0];
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (theFile) {
                $timeout(function () {
                    $scope.testImg = theFile.target.result; //设置一个中间值
                    $scope.reportImgs.push($scope.testImg);

                }, 100);
            };
        };
        $scope.removeImg = function (index, $event) {
            $event.stopPropagation();
            $scope.reportImgs.splice(index, 1);
        };

        //选择指派人
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'page/';
        $ionicModal.fromTemplateUrl(UIPath + 'home/others/safetyManage/selectDealHidenTroubleMan.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.selectManModal = modal;
        });
        $scope.selectMan = function () {
            $scope.selectManModal.show();
        };
        //关闭选择指派人弹窗
        $scope.hideSelectManModal = function () {
            $scope.selectManModal.hide();
        };

        //删除指派人
        $scope.removeMan = function () {
            $scope.select.selectManId = 0;
            $scope.select.selectManName = '';
            $scope.select.isChecked = 0;
            // for (var i = 0; i < $scope.teacherList.length; i++) {
            //     var man = $scope.teacherList[i];
            //     man.checked = false;
            // }
        };

        //确定选择的人
        $scope.saveSelectMan = function () {
            for (var i = 0; i < $scope.teacherList.length; i++) {
                var man = $scope.teacherList[i];
                if (man.id == $scope.select.isChecked) {
                    $scope.select.selectManId = man.id;
                    $scope.select.selectManName = man.chname;
                    break;
                }
            }
            $scope.selectManModal.hide();
        };
        $scope.chooseMan = function (man) {
            // $scope.select.selectManId = man.id;
            // $scope.select.selectManName = man.chname;
        };

        //request --隐患上报
        $scope.addHidenTrouble = function () {
            Requester.addHidenTrouble($scope.select, $scope.schoolId ).then(function (rest) {
                if (rest.result) {
                    $ionicLoading.hide();
                    $scope.isLoading = false;
                    $rootScope.$broadcast('ADD_HidenTrouble_SUCCEED', 'succeed');
                    $ionicHistory.goBack();
                    toaster.success({
                        title: "",
                        body: "添加成功",
                        timeout: 3000
                    });
                   
                    $scope.reportImgs = [];
                } else {
                    $ionicLoading.hide();
                    $scope.isLoading = false;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).finally(function () {
                setTimeout(function () {
                    $ionicLoading.hide();
                    $scope.isLoading = false;
                }, 5000);
            });
        };

        //request --获取学校教师列表
        $scope.getSchoolTeachersList = function () {
            Requester.getSchoolTeachersList($scope.schoolId).then(function (rest) {
                if (rest.result) {
                    $scope.teacherList = rest.data;
                } else {

                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });

        };



    });
angular.module('app.controllers')
    .controller('addExerciseModalCtrl', function ($scope, Constant, $state, $ionicModal, $ionicPopup, $ionicHistory, Requester, $stateParams, toaster, UserPreference, ionicDatePicker, ionicTimePicker, BroadCast) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.selected = {
                programName: '',
                exerciseTime: '',
                title: '',
                address: '',
                content: '',
                programId: 0,

            };
            $scope.selected.programName = $stateParams.programName;
            $scope.selected.title = $scope.selected.programName + '演练通知';
            $scope.selected.programId = $stateParams.programId;
        });

        //选择演练时间
        $scope.selectExerciseTime = function (event) {
            event.stopPropagation();
            if (window.cordova) cordova.plugins.Keyboard.close();
            var ipObj1 = {
                callback: function (val) {
                    if (typeof (val) !== 'undefined') {

                        $scope.recordTime = formatTimeWithoutSecends(val / 1000).substr(0, 10);
                        $scope.showTimePicker();
                    }
                }
            };
            ionicDatePicker.openDatePicker(ipObj1);
        };

        $scope.showTimePicker = function () {
            var ipObj1 = {
                inputTime: (((new Date()).getHours() * 60 * 60) + ((new Date()).getMinutes() * 60)),
                callback: function (val) {
                    if (typeof (val) === 'undefined') {} else {
                        var showTime = new Date(val * 1000);
                        var h = showTime.getUTCHours();
                        var m = showTime.getUTCMinutes();
                        var time = (h < 10 ? '0' + h : h) + ':' + (m < 10 ? '0' + m : m);
                        //  $scope.recordTime = time;
                        if (time)
                            $scope.selected.exerciseTime = $scope.recordTime + ' ' + time;
                    }
                }

            };
            //
            setTimeout(function () {
                ionicTimePicker.openTimePicker(ipObj1);
            }, 100);

        };

        //发起演练
        $scope.saveExercise = function () {
            //  console.lo
            console.log($scope.selected);
            if (!$scope.selected.exerciseTime) {
                toaster.warning({
                    // title: "温馨提示",
                    body: '请选择演练时间'
                });

                return;
            }
            $scope.sendDrillRecord($scope.selected.programId);
        };

        //查看演练方案详情
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'page/';
        $ionicModal.fromTemplateUrl(UIPath + 'home/others/safetyManage/exerciseProgramDetail.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.programDetailModal = modal;
        });
        $scope.goToProgramDetail = function () {
            $scope.getDrillProgramDetail($scope.selected.programId);
            $scope.programDetailModal.show();
        };
        //关闭演练方案详情
        $scope.hideProgramModal = function(){
            $scope.programDetailModal.hide();
        };

        //request --发起演练
        $scope.sendDrillRecord = function (tempDrillPlanId) {
            Requester.sendDrillRecord(tempDrillPlanId, $scope.selected).then(function (rest) {
                if (rest.result) {
                    //$scope.exerciseModal.hide();
                    $ionicHistory.goBack();
                    toaster.success({
                        title: "",
                        body: "发起演练成功",
                        timeout: 3000
                    });
                    $scope.selected = {
                        programName: '',
                        exerciseTime: '',
                        title: '',
                        address: '',
                        content: '',
                        programId: 0,

                    };
                    $scope.page = 1;
                    //$scope.getDrillRecordList();
                } else {
                    // $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };

         //request --查看演练方案详情
         $scope.getDrillProgramDetail = function (programId) {
            Requester.getDrillProgramDetail(programId).then(function (rest) {
                if (rest.result) {
                    $scope.programDetailText = rest.data.content;
                } else {
                    // $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };


    });
angular.module('app.controllers')
    .controller('assetsScanningCtrl', function ($q, $scope, $ionicModal, $state, CameraHelper, BroadCast, NoticeService, Constant, UserPreference, toaster, MESSAGES, $ionicLoading, $ionicPopup, $ionicListDelegate, Requester, $rootScope, ionicImageView) {
        $scope.$on("$ionicView.enter", function (event, data) {

        });

        $scope.goToDetail = function () {
            $state.go('assetsScanningDetail');
        };

        
    });
angular.module('app.controllers')
    .controller('assetsScanningDetailCtrl', function ($q, $scope, $ionicModal, $state, CameraHelper, BroadCast, NoticeService, Constant, UserPreference, toaster, MESSAGES, $ionicLoading, $ionicPopup, $ionicListDelegate, Requester, $rootScope, ionicImageView) {
        $scope.$on("$ionicView.enter", function (event, data) {
            $scope.selectType = 1;
        });

        $scope.selectTypeClick = function(type){
            $scope.selectType = type;
            $scope.events = [{
                createTime:'2019-10-28',
                title:'教育入库',
                resignMen:'张三',
                useMan:'哈哈哈',
                addType:'上级发放',
                statue:0
            },
            {
                createTime:'2019-10-29',
                title:'发放',
                resignMen:'李四',
                usePart:'DT',
                useMan:'哈哈哈',
                addType:'上级发放',
                statue:1
            },
            {
                createTime:'2019-10-30',
                title:'发放',
                resignMen:'王五',
                usePart:'DT',
                useMan:'李思思',
                addType:'上级发放',
                statue:2
            }
        ];

        };

        $scope.getDeviceType = function () {
            var result = 'android';
            $scope.isIos = ionic.Platform.isIOS() ;
            if ($scope.isIos) {
                if (window.screen.width >= 375 && window.screen.height >= 812) {
                    result = 'iphoneX';
                } else {
                    result = 'iphone';
                }

            } else {
             result = 'android';
            }
            return result;
        };
    });
angular.module('app.controllers')
  .controller('portraitListCtrl', function ($scope, UserPreference, ionicImageView, $ionicModal, Constant, CameraHelper, BroadCast, $state, Requester, toaster, $ionicLoading, $timeout, $ionicScrollDelegate, $q) {

    var UIPath = '';
    if (Constant.debugMode) UIPath = 'page/';
    $ionicModal.fromTemplateUrl(UIPath + 'home/others/portrait/attentionModal.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.attentionModal = modal;
    });

    $scope.$on("$ionicView.enter", function (event, data) {

      // $scope.isIos = ionic.Platform.isIOS() && !isWeixin();
      // $scope.remarkArr = UserPreference.getArray('RemarkReadStatus') ? UserPreference.getArray('RemarkReadStatus') : [];
      //$scope.userId = UserPreference.getObject('user').id;

      $scope.sampleImgs = [{
        url: 'img/others/smile_over.jpg',
        lab: '过度微笑'
      }, {
        url: 'img/others/chuckled.jpg',
        lab: '抿嘴'
      }, {
        url: 'img/others/frown.jpg',
        lab: '皱眉'
      }, {
        url: 'img/others/background_notclean.jpg',
        lab: '背景不干净'
      }, {
        url: 'img/others/reflective.jpg',
        lab: '眼睛反光'
      }, {
        url: 'img/others/keep_out.jpg',
        lab: '镜框遮挡'
      }, {
        url: 'img/others/wear_hat.jpg',
        lab: '戴帽子'
      }];

      console.log('enter');

    });

    //监听输入框值改变
    // $scope.$watch('selected.add_search', function (newValue, oldValue) {
    //   console.log($scope.selected.add_search);
     
    // });


    $scope.$on("$ionicView.loaded", function (event, data) {
      $scope.selected = {
        add_search: '',
        isChecked: false
      };
      $scope.roleArray = [{
          key: 3,
          value: '老师'
        },
        {
          key: 2,
          value: '学生'
        }
      ];
      $scope.selected.defaultRole = 3;
      $scope.schoolId = UserPreference.get('DefaultSchoolID');

      $scope.teacherList = [];
      //获取学校所有老师
      //$scope.getSchoolTeachersList();
      $scope.getTeacherPortraitList();
      //获取班级列表
      $scope.getSchoolClasses();

      // $scope.getFaceRecognitionClassList();

    });



    //选择角色
    $scope.chooseRole = function (item) {
      //$scope.selected.defaultRole = item.key;
      $ionicScrollDelegate.$getByHandle('mainScroll').scrollTop();
      if ($scope.selected.defaultRole === 2) {
        $scope.getFaceRecognitionClassList();
      } else {
        $scope.getTeacherPortraitList();
      }
    };
    //更新年级
    $scope.updateGrade = function (item) {
      // $scope.recordClassList = item.classList;
      for (var i = 0; i < $scope.gradeList.length; i++) {
        if ($scope.selected.gradeId === $scope.gradeList[i].id) {
          $scope.recordClassList = $scope.gradeList[i].classList;
          break;
        }
      }
      $scope.selected.class_id = $scope.recordClassList[0].id;
      $scope.getFaceRecognitionClassList();

    };
    //更新班级
    $scope.updateClass = function () {
      //$scope.selected.class_id = item.id;
      $scope.getFaceRecognitionClassList();

    };

    //刷新列表
    $scope.refreshData = function () {
      if ($scope.selected.defaultRole === 3) {
        $scope.getTeacherPortraitList();
      } else {
        $scope.getFaceRecognitionClassList();
      }

    };

    //点击图片放大
    $scope.viewImages = function (item, index, $event) {
      var url = 'img/icon/person.png';
      if (item.userImgSrc) url = item.userImgSrc;
      var urls = [];
      index = 0;
      urls.push(url);
      $event.stopPropagation();
      ionicImageView.showViewModal({
        allowSave: false
      }, urls, index);
    };

    //选择班级后重新加载数据
    $scope.loadData = function () {
      console.log('select id:' + $scope.selected.class_id);
      $scope.getFaceRecognitionClassList();
    };



    //选择图片
    $scope.selectImg = function (item) {
      console.log(item);
      $scope.selected.userId = item.userId;
      if (window.cordova) {
        CameraHelper.selectImage('portrait', {
          allowEdit: false,
          width: 800,
          height: 800
        });
      } else {
        var input = document.getElementById("capture");
        input.click();
      }
    };

    //我已阅读
    $scope.haveKnow = function () {

      // 把状态和用户id 关联起来 是为了在采集过程中出现 一台设备 多个老师账户登录
      $scope.setReadStatus();
      $scope.attentionModal.hide();
      if (window.cordova) {
        CameraHelper.selectImage('portrait', {
          allowEdit: false,
          width: 800,
          height: 800
        });
      } else {
        var input = document.getElementById("capture");
        input.click();
      }

    };


    $scope.$on(BroadCast.IMAGE_SELECTED, function (a, rst) {
      if (rst && rst.which === 'portrait') {
        //上传图片
        // $scope.sendFeedbackRequest('1', rst.source);
        setTimeout(function () {
          var userId = $scope.selected.userId;
          console.log('userId:' + userId);
          $scope.uploadUserPortrait(userId, rst.source);
        }, 50);
      }
    });

    //web端选择文件
    $scope.getFiles = function (files) {
      var file = files[0];
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = function (theFile) {
        $timeout(function () {
          $scope.testImg = theFile.target.result;
          $ionicLoading.show({
            noBackdrop: true,
            template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
          });
          // var
          $scope.uploadUserPortrait($scope.selected.userId, $scope.testImg);
        }, 100);
      };
    };

    $scope.hideAttentionModal = function () {
      $scope.attentionModal.hide();
    };

    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function () {
      $scope.attentionModal.remove();
    });
    // Execute action on hide modal
    $scope.$on('modal.hidden', function () {
      // Execute action
    });
    // Execute action on remove modal
    $scope.$on('modal.removed', function () {
      // Execute action
    });

    //获取学校所有班级
    $scope.getSchoolClasses = function () {
      Requester.getSchoolAllClasses($scope.schoolId).then(function (resp) {
        if (resp.result) {
          $scope.gradeList = resp.data;
          if ($scope.gradeList && $scope.gradeList.length > 0) {
            $scope.selected.gradeId = $scope.gradeList[0].id;
            $scope.recordClassList = $scope.gradeList[0].classList;
            $scope.selected.class_id = $scope.recordClassList[0].id;
          }

        } else {
          toaster.error({
            title: resp.message,
            body: ''
          });
        }
      });
    };

    //request --获取学校教师人像列表
    $scope.getTeacherPortraitList = function () {
      $scope.teacherList = [];

      Requester.getTeacherPortraitList($scope.schoolId).then(function (rest) {
        if (rest.result) {

          $scope.teacherList = rest.data.content;

        } else {

          toaster.warning({
            title: "温馨提示",
            body: rest.message
          });
        }
      }).finally(function () {
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
      });
      // }

    };


    //requester ---获取班级人像列表
    $scope.getFaceRecognitionClassList = function () {
      Requester.getFaceRecognitionClassList($scope.selected.class_id).then(function (resp) {
        if (resp.result) {
          //regcStatus: 处理状态。0，未采集;1,采集图片上传成功;2,提取特征值成功;3,提取特征值失败
          $scope.stuList = resp.data;
        } else {
          toaster.error({
            title: resp.message,
            body: ''
          });
        }
      }).finally(function () {
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
      });
    };

    //requester ---上传
    $scope.uploadUserPortrait = function (userId, base64Str) {
      Requester.uploadUserPortrait(userId, base64Str).then(function (resp) {
        $ionicLoading.hide();
        if (resp.result) {
          toaster.success({
            body: '上传成功'
          });
          if ($scope.selected.defaultRole === 3) {
            $scope.getTeacherPortraitList();
          } else {
            $scope.getFaceRecognitionClassList();
          }
        } else {
          toaster.error({
            title: resp.message,
            body: ''
          });
        }
      }).finally(function () {
        setTimeout(function () {
          $ionicLoading.hide();
        }, 3000);

      });
    };

    $scope.goIntroduce = function () {
      // $state.go('myPortrait');
    };

    $scope.getTypeStr = function (type) {
      var result = type === 0 ? '未采集' : type === 1 ? '人像提取中' : type === 2 ? '采集完成' : '采集失败';
      return result;

    };



    //存状态数组
    $scope.setReadStatus = function () {
      var readMark = $scope.selected.isChecked ? 'read' : 'unread';
      var count = 0;
      if ($scope.remarkArr && $scope.remarkArr.length > 0) {
        for (var i = 0; i < $scope.remarkArr.length; i++) {
          //如果用户已存在则是更改状态
          if ($scope.userId === $scope.remarkArr[i].key) {
            $scope.remarkArr[i].value = readMark;
            break;
          } else {
            count++;
          }
        }
        if ($scope.remarkArr.length === count) {
          $scope.remarkArr.push({
            key: $scope.userId,
            value: readMark
          });
        }

      } else {
        $scope.remarkArr.push({
          key: $scope.userId,
          value: readMark
        });

      }

      UserPreference.setObject('RemarkReadStatus', $scope.remarkArr);
    };



    //取状态数组
    $scope.getReadStatus = function () {
      var result;
      var statusArr = UserPreference.getArray('RemarkReadStatus');
      if (statusArr && statusArr.length > 0) {
        for (var i = 0; i < statusArr.length; i++) {
          if (statusArr[i].key === $scope.userId && statusArr[i].value === 'read') {
            result = 'read';
            break;
          } else {
            result = 'unread';
          }
        }

      } else {
        result = 'unread';
      }
      return result;
    };

    $scope.goIntroduce = function () {
      $scope.attentionModal.show();
    };

    //关闭说明弹窗
    $scope.hideAttentionModal = function () {
      $scope.attentionModal.hide();
    };

    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function () {
      $scope.attentionModal.remove();
    });
    // Execute action on hide modal
    $scope.$on('modal.hidden', function () {
      // Execute action
    });
    // Execute action on remove modal
    $scope.$on('modal.removed', function () {
      // Execute action
    });


  });
angular.module('app.controllers')
    .controller('babyVideoListCtrl', function ($scope, $state, UserPreference) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            var user = UserPreference.getObject('user');
            $scope.videoList = user.cameraManageList&&user.cameraManageList.length>0?user.cameraManageList:[];
        });

        $scope.onVideClick = function(item){
                console.log(item);
                $state.go('babyVideoDetail', {url: item.pcBroadcastUrl});
        };

    });
angular.module('app.controllers')
    .controller('babyVideoDetailCtrl', function ($scope, Constant, $state, $sce, toaster, $stateParams) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.videoUrl = $sce.trustAsResourceUrl($stateParams.url);
            console.log($scope.videoUrl);
        });
    });
angular.module('app.controllers')
    .controller('covidDailySummeryCtrl', function ($scope, Constant, $state, Requester, toaster, UserPreference) {

        $scope.$on("$ionicView.loaded", function (event, data) {
            $scope.selectedStatus = '';
            $scope.activeTab = 'all';
            $scope.warningList = [];
        });

        function setProcess(val){
            var process =  document.querySelector("#circle-process");
            var circleLength = Math.floor(2 * Math.PI * 50);
            val = Math.max(0,val);
            val = Math.min(100,val);
            process.setAttribute("stroke-dasharray", "" + circleLength * val + ",10000");
        }

        $scope.$on("$ionicView.enter", function (event, data) {
            setProcess(0);
            $scope.loadData();
        });

        $scope.loadData = function() {
            Requester.getCovidSummary().then(function(res){
                $scope.summary = res.data;
                if($scope.summary.abnormalNum && $scope.summary.abnormalNum != 0)
                    setProcess($scope.summary.normalNum/$scope.summary.abnormalNum);
                    else
                    setProcess(0);
                $scope.warningCounts = [];
                for(var i=0;i<$scope.summary.abnormalNumList.length;i++){
                    var data = $scope.summary.abnormalNumList[i].split('#');
                    $scope.warningCounts.push({
                        day: data[0],
                        count: data[1]
                    });
                }
            });
            Requester.getWarningList().then(function(res){
                if(res.data && res.data.content) {
                    $scope.warningList = res.data.content;
                    Requester.covidWarningRead();
                }
                else
                    $scope.warningList = [];
            });
        };

        $scope.changeFilter = function (arg) {
            if ($scope.activeTab === arg) {
                return;
            }
            $scope.activeTab = arg;
            if (arg === 'clear') {
                $scope.selectedStatus = '2';
            } else if (arg === 'pending') {
                $scope.selectedStatus = '1';
            } else {
                $scope.selectedStatus = '';
            }
        };


        $scope.goFunction = function (where, args) {
            if (where) {
                if (args)
                    $state.go(where, {id: args});
                else
                    $state.go(where);
            }
        };
    });
angular.module('app.controllers')
    .controller('covidWarningDetailCtrl', function ($scope, Constant, $ionicPopup, Requester, toaster, $stateParams) {


        window.addEventListener('native.keyboardshow', function (keyboardParameters) {
           // console.log('keyboard open warning');
            var tObj = document.getElementById("desc");
            var sPos = tObj.value.length;
            setTimeout(function(){
                if(ionic.Platform.isIOS())
                setCaretPosition(tObj, sPos);
            },100);
           
        });

        // $scope.blur = function(){
        //     var tObj = document.getElementById("desc");
        //     var sPos = tObj.value.length;
        //     setCaretPosition(tObj, sPos);
        // };
        $scope.$on("$ionicView.enter", function (event, data) {
            $scope.warning = $stateParams.id;
            $scope.events = [];
            Requester.getWarningDetail($scope.warning.id).then(function(res){
                $scope.events = res.data;
            });
            $scope.selectedStatus = '1';
            $scope.activeTab = 'pending';
            $scope.formData = {
                desc: ''
            };
        });

        $scope.changeStatus = function (arg) {
            if ($scope.activeTab === arg) {
                return;
            }
            $scope.activeTab = arg;
            if (arg === 'clear') {
                $scope.selectedStatus = '2';
            } else {
                $scope.selectedStatus = '1';
            }
        };

        var UIPath = '';
        if (Constant.debugMode) UIPath = 'page/';
        $scope.openModal = function(){
            $scope.selectModel = $ionicPopup.show({
                templateUrl: UIPath + 'home/covid/handleWarning.html',
                scope: $scope,
                cssClass: 'approval_alert'
            });
        };

        $scope.cancelChoose = function(){
            $scope.selectModel.close();
        };

        $scope.submit = function(){
            Requester.handleWarning($scope.warning.id, $scope.selectedStatus, $scope.formData.desc).then(function(res){
                if(res.result) {
                    toaster.success({title: '跟进成功!', body: '',timeout:'3000'});
                    $scope.cancelChoose();
                    Requester.getWarningDetail($scope.warning.id).then(function(res){
                        $scope.events = res.data;
                    });
                }else{
                    toaster.warning({title: res.message, body: '',timeout:'3000'}); 
                    $scope.selectModel.close();
                }
            });
        };
    });