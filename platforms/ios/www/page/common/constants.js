/**
 * Created by hewz on 2016-07-19.
 */
angular.module('app.constants', [])
    .constant('Constant', {
        'debugMode': true,
<<<<<<< HEAD
        'versionCode': 21,
<<<<<<< HEAD
        'version': '1.2.9',
=======
        'version': '1.3.0',
>>>>>>> wl_admin_1.3.1
=======
        'versionCode': 31,
        'version': '1.3.1',
>>>>>>> wl_admin_1.3.1
       // 'ServerUrl': 'http://192.168.1.177:8090/mob',//liao bin
       // 'ServerUrl': 'http://192.168.1.190:8090/mob',//xiao chen
       //  'ServerUrl': 'http://192.168.1.127:9020/mob',//xiao feng
   'ServerUrl': 'http://test17.xgenban.com/sctserver/mob',
    // 'ServerUrl' : 'http://xgenban.com/sctserver/mob',
        'IMAppID': 1400026815, //dev
    // 'IMAppID': 1400026570,//pro
        'serverTimeout': 30000,
        'heavyServerTimeout': 120000,
        'reqLimit': 10,
        'SMS_REQ_INTERVAL': 60,
        'CAPTURE_IMAGE_RANGE': 1080,
        'IM_USER_AVATAR': 'img/icon/person.png',
        'IM_GROUP_AVATAR': 'img/icon/group.png',
        'IMAccountType': 10559,
        'NEWS_STATUS': {
            'TEACHER_REVIEW': {
                'key': '3',
                'text': '教师审核中'
            },
            'TEACHER_REVIEW_PASS': {
                'key': '4',
                'text': '审核通过'
            },
            'ADMIN_REVIEW_PASS': {
                'key': '5',
                'text': '审核通过'
            },
            'TEACHER_IGNORE': {
                'key': '6',
                'text': '已拒绝'
            },
            'ADMIN_REVIEW': {
                'key': '7',
                'text': '管理员审核中'
            }
        },
        'USER_ROLES': {
            'PROVINCE_ADMIN': '7',
            'CITY_ADMIN': '8',
            'AREA_ADMIN': '4',
            'DISTRICT_ADMIN': '9',
            'SCHOOL_ADMIN': '5'
        }
    })
    .constant('MESSAGES', {
        'BADGE_UPDATE': 'BADGE_UPDATE',
        'CONNECT_ERROR': '请求失败',
        'DOWNLOAD_ERROR': '下载失败',
        'FEEDBACK_SUCCESS': '反馈成功',
        'SAVE_SUCCESS': '修改成功',
        'PUSH_ON_FAIL': '推送开启失败',
        'PUSH_ON_FAIL_REASON': '检测到系统设置中已禁止应用推送，请先在系统设置中勾选小跟班管理的通知权限',
        'FEEDBACK_SUCCESS_MSG': '感谢您的建议和支持',
        'CONNECT_ERROR_MSG': '连接服务器失败,请检查网络连接',
        'CONNECT_TIMEOUT_MSG': '服务器连接超时，请重试',
        'SERVER_ERROR': '服务器异常,请稍后再试',
        'CONTACT_FETCH_FAIL': '通讯录获取失败',
        'NOT_FOUND': '未找到请求资源',
        'SESSION_TIMEOUT': '登录超时',
        'NEW_NOTICE': '发布成功',
        'NEW_NEWS': '发布成功, 待审核后可见',
        'NEW_NEWS_ADMIN': '发布成功',
        'INFO_UPDATED': '个人资料更新成功',
        'PWD_UPDATED': '密码修改成功。',
        'NO_THUMB': '请选择缩略图',
        'REMIND': '温馨提示',
        'NO_NAME': '请输入姓名',
        'NO_IMAGE_TO_CROP': '此文章未含图片',
        'NO_NEWS_TYPE': '请选择一项风采类型',
        'NO_TARGET_CLASS': '请选择可见班级',
        'NO_OLD_PASSWORD': '请输入当前密码',
        'NO_NEW_PASSWORD': '请输入新密码',
        'NO_CONFIRM_PASSWORD': '请再次输入密码',
        'PASSWORD_CONFIRM_ERROR': '两次输入的密码不一致',
        'LOGIN_ERROR': '登录失败',
        'OPERATE_ERROR': '操作失败',
        'REQUEST_ERROR': '请求失败',
        'AT_LEAST_IMAGE': '请至少上传一张正文图片',
        'LOGIN_NO_USERNAME': '请输入您的登录账号',
        'LOGIN_NO_PASSWORD': '请输入您的登录密码',
        'LOGIN_USERNAME_PATTERN': '您输入的账号中包含特殊字符,仅限字母和数字',
        'LOGIN_PASSWORD_PATTERN': '您输入的密码中包含特殊字符,仅限字母和数字'
    })
    .constant('BroadCast', {
        'NEW_PUSH_REV': 'NEW_PUSH_REV',
        'IMAGE_SELECTED': 'IMAGE_SELECTED',
        'IMAGE_CROP': 'IMAGE_CROP',
        'FEEDBACK': 'FEEDBACK',
        'EDIT_INFO': 'EDIT_INFO',
        'PASSWORD_CHANGE': 'PASSWORD_CHANGE',
        'CHILD_INFO_REV': 'CHILD_INFO_REV',
        'DIC_REV': 'DIC_REV',
        'CONNECT_ERROR': 'CONNECT_ERROR',
        'LOGIN_RESULT_RECEIVED': 'LOGIN_RESULT_RECEIVED',
        'NEW_NOTICE': 'NEW_NOTICE',
        'NEW_NEWS': 'NEW_NEWS',
        'NOTICE_LIST_REV': 'NOTICE_LIST_REV',
        'NEWS_LIST_REV': 'NEWS_LIST_REV',
        'NEWS_STATE_CHANGED': 'NEWS_STATE_CHANGED',
        'STICK_RST_REV': 'STICK_RST_REV',
        'UNDO_STICK_RST_REV': 'UNDO_STICK_RST_REV',
        'SET_FOCUS_RST_REV': 'SET_FOCUS_RST_REV',
        'SEND_TO_ADMIN_RST': 'SEND_TO_ADMIN_RST',
        'ALLOW_PUBLISH_RST_REV': 'ALLOW_PUBLISH_RST_REV',
        'IGNORE_RST_REV': 'IGNORE_RST_REV',
        'BANNER_LIST_REV': 'BANNER_LIST_REV',
        'DELETE_NEWS_REV': 'DELETE_NEWS_REV',
        'CANCEL_PUBLISH_REV': 'CANCEL_PUBLISH_REV',
        'DELETE_NOTICE_REV': 'DELETE_NOTICE_REV',
        'NEWS_TYPE_REV': 'NEWS_TYPE_REV',
        'IM_MSG_SENDING': 'IM_MSG_SENDING',
        'IM_MSG_SENT': 'IM_MSG_SENT',
        'IM_MSG_SEND_FAIL': 'IM_MSG_SEND_FAIL',
        'IM_IMAGE_SEND_FAIL': 'IM_IMAGE_SEND_FAIL',
        'IM_REV_CONTACTS': 'IM_REV_CONTACTS',
        'IM_RECENT_CONTACTS': 'IM_RECENT_CONTACTS',
        'IM_C2C_HISTORY_MESSAGE': 'IM_C2C_HISTORY_MESSAGE',
        'IM_GROUP_HISTORY_MESSAGE': 'IM_GROUP_HISTORY_MESSAGE',
        'IM_NEW_MESSAGE': 'IM_NEW_MESSAGE'
    });
