angular.module('app.controllers')
    .controller('approvalListCtrl', function ($scope, $state, $ionicModal, Constant, $window, BroadCast, $ionicLoading, Requester, UserPreference, toaster) {

        $scope.$on("$ionicView.enter", function (event, data) {

            $scope.page = 1;
            $scope.getApprovalManageList();
        });
        $scope.$on("$ionicView.loaded", function (event, data) {


            $scope.schoolId = UserPreference.get('DefaultSchoolID');
            //console.log('schoolId:' + $scope.schoolId);
            $scope.select = {
                selectIndex2: '',
            };
            $scope.recordList = [];
            $scope.allList = [];
            $scope.passList = [];
            $scope.failList = [];
            $scope.checkList = [];
            $scope.isMoreData = true;
            $scope.progress = '';

        });

        //审批状态选择
        $scope.tab2Click = function (progress) {
            $scope.select.selectIndex2 = progress;
            $scope.progress = progress == 1 ? 'PASS' : progress == 2 ? 'APPROVAL' : progress == 3 ? 'FAIL' : '';
            $scope.page = 1;
            $scope.isLoading = false;
            $scope.isMoreData = true;
            $scope.getApprovalManageList();
        };

        //下拉刷新
        $scope.refreshData = function () {
            $scope.page = 1;
            $scope.getApprovalManageList();

        };

        //上拉加载
        $scope.loadMore = function () {
            $scope.page++;
            $scope.isLoading = true;
            $scope.isMoreData = true;
            // $scope.getApprovalManageList();
            $scope.loadRequest();
        };


        //查看审批详情
        $scope.goToDetail = function (approvalId) {
            $state.go('approvalDetail', {
                approvalId: approvalId
            });
        };


        $scope.getList = function (type, arr) {
            if (type === 1) {
                $scope.passList = arr;
            } else if (type === 2) {
                $scope.checkList = arr;
            } else if (type === 3) {
                $scope.failList = arr;
            } else {
                $scope.allList = arr;
            }
        };

        $scope.getRecordList = function (type) {
            if (type === 1) {
                $scope.recordList = $scope.passList;
            } else if (type === 2) {
                $scope.recordList = $scope.checkList;
            } else if (type === 3) {
                $scope.recordList = $scope.failList;
            } else {
                $scope.recordList = $scope.allList;
            }
            //console.log($scope.recordList);
        };

        $scope.currentApprovalStatus = function (progress) {
            var result;
            result = progress == 'PASS' ? '通过' : progress == 'APPROVAL' ? '审批中' : progress == 'FAIL' ? '不通过' : '';
            return result;
        };

        //request--查看列表
        $scope.getApprovalManageList = function () {

            Requester.getApprovalManageList($scope.page, $scope.schoolId, $scope.select.selectIndex2).then(function (data) {
                if (data.result) {
                    $scope.getList($scope.select.selectIndex2, data.data.content);

                    $scope.getRecordList($scope.select.selectIndex2);
                } else {
                    $scope.isMoreData = false;
                    toaster.warning({
                        title: "温馨提示",
                        body: data.message
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        //刷新方法
        $scope.loadRequest = function () {
            Requester.getApprovalManageList($scope.page, $scope.schoolId, $scope.select.selectIndex2).then(function (data) {
                if (data.result) {
                    for (var i = 0; i < data.data.content.length; i++) {
                        if ($scope.select.selectIndex2 === 1) {
                            $scope.passList.push(data.data.content[i]);
                        } else if ($scope.select.selectIndex2 === 2) {
                            $scope.checkList.push(data.data.content[i]);
                        } else if ($scope.select.selectIndex2 === 3) {
                            $scope.failList.push(data.data.content[i]);
                        } else {
                            $scope.allList.push(data.data.content[i]);
                        }
                    }
                    //关闭刷新
                    if (!data.data.content || data.data.content.length < Constant.reqLimit) {
                        $scope.isMoreData = false;
                    }
                    $scope.getRecordList($scope.select.selectIndex2);
                    // $scope.recordList.push(data.data.content[i]);
                } else {
                    $scope.isMoreData = false;
                    toaster.warning({
                        title: "温馨提示",
                        body: data.message
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

    });
