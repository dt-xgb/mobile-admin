angular.module('app.controllers')
    .controller('marketCtrl', function ($scope, Requester, $ionicLoading, $timeout, $state, UserPreference, $ionicPopup, toaster) {

        $scope.$on("$ionicView.enter", function (event, data) {
            //获取用户id
            $scope.user = UserPreference.getObject('user');
            Requester.getToolkitList().then(function (res) {
                if (res.result)
                    $scope.list = res.data;
            });
        });

        $scope.getStatusText = function(code) {
            switch (code) {
                case 0:
                    return '申请开通';
                case 1:
                    return '已申请，待开通';
                case 2:
                    return '已开通';
            }
        };

        $scope.applyModule = function (item) {
            if (item.appStatus !== 0)
                return;
            $scope.data = {
                phone: ''
            };
            $ionicPopup.confirm({
                title: '申请授权',
                scope: $scope,
                template: '<input type="tel" placeholder="请留下手机号码，方便与您联系" ng-model="data.phone"/>',
                cancelText: '取消',
                okText: '确认',
                okType: 'button-balanced'
            }).then(function (res) {
                if (res && $scope.data.phone.length > 0) {
                    Requester.requestToolkit($scope.data.phone, item.id).then(function (res) {
                        if (res.result) {
                            item.appStatus = 1;
                        }else {
                            toaster.warning({title: res.message, body: ''});
                        }
                    });
                } else {
                    toaster.warning({title: '申请已取消', body: ''});
                }
            });
        };

    });
