angular.module('app.controllers')
    .controller('exerciseNoticeListCtrl', function ($scope, Constant, $state, $ionicModal, $ionicPopup, Requester, toaster, UserPreference, ionicDatePicker, ionicTimePicker,BroadCast) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
           
            $scope.page = 1;
           // $scope.programList = []; //方案列表
            $scope.selected = {
                programName: '',
                exerciseTime: '',
                title: '',
                address: '',
                content: '',
                programId :0,
               
            };

          $scope.getDrillRecordList();
          $scope.programList = UserPreference.getArray('programList')?UserPreference.getArray('programList'):[];
          $scope.getDrillProgramList(); //演练方案列表
          
        });

        $scope.$on("$ionicView.loaded", function (event, data) {
          console.log('--load');
        });

        //下拉刷新
        $scope.refreshData = function () {
            $scope.page = 1;
            $scope.getDrillRecordList();
        };
        //上拉记载
        $scope.loadMore = function () {
            $scope.page++;
            $scope.getMoreDataList();
        };

        //详情或记录
        $scope.goToNextPage = function (type,item) {
            if (type === 'detail') {
                $state.go('exerciseNoticeDetail',{itemId:item.id,title:item.title});
            } else {
                $state.go('exerciseRecord',{itemId:item.id,startTime:item.drillStartTime,endTime:item.drillEndTime});
            }
        };


        //添加演练
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'page/';
        $ionicModal.fromTemplateUrl(UIPath + 'home/others/safetyManage/addExerciseModal.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.exerciseModal = modal;
        });
        $scope.addExerciseModalFunc = function () {
        
            $scope.exerciseAlert = $ionicPopup.show({
                templateUrl: UIPath + 'home/others/safetyManage/selectExerciseTypeAlert.html',
                title: '<div style="color:white !important;">选择演练方案</div>',
                scope: $scope,
                cssClass: 'approval_alert',
            });

        };
        $scope.addExerciseProgram = function (item) {
            $scope.selected.programName = item.drillName;
            $scope.selected.title =  $scope.selected.programName + '演练通知';
            $scope.selected.programId = item.id;
            $scope.exerciseAlert.close();
            // $scope.exerciseModal.show();
            $state.go('addExerciseModal',{
                programId : $scope.selected.programId,
                programName:item.drillName
            });
        };
        //
        $scope.hideAddExerciseModal = function () {
            $scope.exerciseModal.hide();
        };

        //关闭弹窗
        $scope.cancelChoose = function () {
            $scope.exerciseAlert.close();
        };
        //查看演练方案详情
        if (Constant.debugMode) UIPath = 'page/';
        $ionicModal.fromTemplateUrl(UIPath + 'home/others/safetyManage/exerciseProgramDetail.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.programDetailModal = modal;
        });
        $scope.goToProgramDetail = function(){
            $scope.getDrillProgramDetail($scope.selected.programId);
            $scope.programDetailModal.show();
        };
        //关闭演练方案详情
        $scope.hideProgramModal = function(){
            $scope.programDetailModal.hide();
        };

        //选择演练时间
        $scope.selectExerciseTime = function (event) {
            event.stopPropagation();
            if (window.cordova) cordova.plugins.Keyboard.close();
            var ipObj1 = {
                callback: function (val) {
                    if (typeof (val) !== 'undefined') {

                        $scope.recordTime = formatTimeWithoutSecends(val / 1000).substr(0, 10);
                        $scope.showTimePicker();
                    }
                }
            };
            ionicDatePicker.openDatePicker(ipObj1);
        };

        $scope.showTimePicker = function () {
            var ipObj1 = {
                inputTime: (((new Date()).getHours() * 60 * 60) + ((new Date()).getMinutes() * 60)),
                callback: function (val) {
                    if (typeof (val) === 'undefined') {} else {
                        var showTime = new Date(val * 1000);
                        var h = showTime.getUTCHours();
                        var m = showTime.getUTCMinutes();
                        var time = (h < 10 ? '0' + h : h) + ':' + (m < 10 ? '0' + m : m);
                        //  $scope.recordTime = time;
                        if(time)
                        $scope.selected.exerciseTime = $scope.recordTime + ' ' + time;
                    }
                }

            };
            //
            setTimeout(function(){
                ionicTimePicker.openTimePicker(ipObj1);
            },100);
           
        };

        //发起演练
        $scope.saveExercise = function(){
          //  console.lo
          //console.log($scope.selected);
          if(!$scope.selected.exerciseTime){
            toaster.warning({
                // title: "温馨提示",
                body: '请选择演练时间'
            }); 
            
            return;
          }
            $scope.sendDrillRecord($scope.selected.programId); 
        };

        $scope.$on(BroadCast.CONNECT_ERROR, function () {
            $scope.isMoreData = true;
           // $ionicLoading.hide();
        });

        //request --获取列表
        $scope.getDrillRecordList = function () {
            Requester.getDrillRecordList($scope.page).then(function (rest) {
                if (rest.result) {
                    //console.log('调课记录列表');
                    $scope.list = rest.data.content;
                } else {
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        //request --加载更多
        $scope.getMoreDataList = function () {
            Requester.getDrillRecordList($scope.page).then(function (rest) {
                if (rest.result) {
                    var data = rest.data.content;
                    for (var i = 0; i < data.length; i++) {
                        $scope.list.push(data[i]);
                    }
                    if (!data || (data && data.length < Constant.reqLimit)) {
                        $scope.isMoreData = true;
                    }
                } else {
                    $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };
        //request --获取演练方案列表
        $scope.getDrillProgramList = function () {
            Requester.getDrillProgramList().then(function (rest) {
                if (rest.result) {
                    $scope.programList = rest.data;
                   
                    UserPreference.setObject('programList',rest.data);
                } else {
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };

        //request --查看演练方案详情
        $scope.getDrillProgramDetail = function (programId) {
            Requester.getDrillProgramDetail(programId).then(function (rest) {
                if (rest.result) {
                    $scope.programDetailText = rest.data.content;
                } else {
                    // $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };

        //发起演练
        $scope.sendDrillRecord = function (tempDrillPlanId) {
            Requester.sendDrillRecord(tempDrillPlanId, $scope.selected).then(function (rest) {
                if (rest.result) {
                    $scope.exerciseModal.hide();
                    toaster.success({
                        title: "",
                        body: "发起演练成功",
                        timeout: 3000
                    });
                    $scope.selected = {
                        programName: '',
                        exerciseTime: '',
                        title: '',
                        address: '',
                        content: '',
                        programId :0,
                       
                    };
                    $scope.page = 1;
                    $scope.getDrillRecordList();
                } else {
                    // $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };



    });