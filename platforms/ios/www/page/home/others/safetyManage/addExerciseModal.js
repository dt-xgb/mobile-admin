angular.module('app.controllers')
    .controller('addExerciseModalCtrl', function ($scope, Constant, $state, $ionicModal, $ionicPopup, $ionicHistory, Requester, $stateParams, toaster, UserPreference, ionicDatePicker, ionicTimePicker, BroadCast) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.selected = {
                programName: '',
                exerciseTime: '',
                title: '',
                address: '',
                content: '',
                programId: 0,

            };
            $scope.selected.programName = $stateParams.programName;
            $scope.selected.title = $scope.selected.programName + '演练通知';
            $scope.selected.programId = $stateParams.programId;
        });

        //选择演练时间
        $scope.selectExerciseTime = function (event) {
            event.stopPropagation();
            if (window.cordova) cordova.plugins.Keyboard.close();
            var ipObj1 = {
                callback: function (val) {
                    if (typeof (val) !== 'undefined') {

                        $scope.recordTime = formatTimeWithoutSecends(val / 1000).substr(0, 10);
                        $scope.showTimePicker();
                    }
                }
            };
            ionicDatePicker.openDatePicker(ipObj1);
        };

        $scope.showTimePicker = function () {
            var ipObj1 = {
                inputTime: (((new Date()).getHours() * 60 * 60) + ((new Date()).getMinutes() * 60)),
                callback: function (val) {
                    if (typeof (val) === 'undefined') {} else {
                        var showTime = new Date(val * 1000);
                        var h = showTime.getUTCHours();
                        var m = showTime.getUTCMinutes();
                        var time = (h < 10 ? '0' + h : h) + ':' + (m < 10 ? '0' + m : m);
                        //  $scope.recordTime = time;
                        if (time)
                            $scope.selected.exerciseTime = $scope.recordTime + ' ' + time;
                    }
                }

            };
            //
            setTimeout(function () {
                ionicTimePicker.openTimePicker(ipObj1);
            }, 100);

        };

        //发起演练
        $scope.saveExercise = function () {
            //  console.lo
            console.log($scope.selected);
            if (!$scope.selected.exerciseTime) {
                toaster.warning({
                    // title: "温馨提示",
                    body: '请选择演练时间'
                });

                return;
            }
            $scope.sendDrillRecord($scope.selected.programId);
        };

        //查看演练方案详情
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'page/';
        $ionicModal.fromTemplateUrl(UIPath + 'home/others/safetyManage/exerciseProgramDetail.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.programDetailModal = modal;
        });
        $scope.goToProgramDetail = function () {
            $scope.getDrillProgramDetail($scope.selected.programId);
            $scope.programDetailModal.show();
        };
        //关闭演练方案详情
        $scope.hideProgramModal = function(){
            $scope.programDetailModal.hide();
        };

        //request --发起演练
        $scope.sendDrillRecord = function (tempDrillPlanId) {
            Requester.sendDrillRecord(tempDrillPlanId, $scope.selected).then(function (rest) {
                if (rest.result) {
                    //$scope.exerciseModal.hide();
                    $ionicHistory.goBack();
                    toaster.success({
                        title: "",
                        body: "发起演练成功",
                        timeout: 3000
                    });
                    $scope.selected = {
                        programName: '',
                        exerciseTime: '',
                        title: '',
                        address: '',
                        content: '',
                        programId: 0,

                    };
                    $scope.page = 1;
                    //$scope.getDrillRecordList();
                } else {
                    // $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };

         //request --查看演练方案详情
         $scope.getDrillProgramDetail = function (programId) {
            Requester.getDrillProgramDetail(programId).then(function (rest) {
                if (rest.result) {
                    $scope.programDetailText = rest.data.content;
                } else {
                    // $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };


    });