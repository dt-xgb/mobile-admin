angular.module('app.controllers')
  .controller('portraitListCtrl', function ($scope, UserPreference, ionicImageView, $ionicModal, Constant, CameraHelper, BroadCast, $state, Requester, toaster, $ionicLoading, $timeout, $ionicScrollDelegate, $q) {

    var UIPath = '';
    if (Constant.debugMode) UIPath = 'page/';
    $ionicModal.fromTemplateUrl(UIPath + 'home/others/portrait/attentionModal.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.attentionModal = modal;
    });

    $scope.$on("$ionicView.enter", function (event, data) {

      // $scope.isIos = ionic.Platform.isIOS() && !isWeixin();
      // $scope.remarkArr = UserPreference.getArray('RemarkReadStatus') ? UserPreference.getArray('RemarkReadStatus') : [];
      //$scope.userId = UserPreference.getObject('user').id;

      $scope.sampleImgs = [{
        url: 'img/others/smile_over.jpg',
        lab: '过度微笑'
      }, {
        url: 'img/others/chuckled.jpg',
        lab: '抿嘴'
      }, {
        url: 'img/others/frown.jpg',
        lab: '皱眉'
      }, {
        url: 'img/others/background_notclean.jpg',
        lab: '背景不干净'
      }, {
        url: 'img/others/reflective.jpg',
        lab: '眼睛反光'
      }, {
        url: 'img/others/keep_out.jpg',
        lab: '镜框遮挡'
      }, {
        url: 'img/others/wear_hat.jpg',
        lab: '戴帽子'
      }];

      console.log('enter');

    });

    //监听输入框值改变
    // $scope.$watch('selected.add_search', function (newValue, oldValue) {
    //   console.log($scope.selected.add_search);
     
    // });


    $scope.$on("$ionicView.loaded", function (event, data) {
      $scope.selected = {
        add_search: '',
        isChecked: false
      };
      $scope.roleArray = [{
          key: 3,
          value: '老师'
        },
        {
          key: 2,
          value: '学生'
        }
      ];
      $scope.selected.defaultRole = 3;
      $scope.schoolId = UserPreference.get('DefaultSchoolID');

      $scope.teacherList = [];
      //获取学校所有老师
      //$scope.getSchoolTeachersList();
      $scope.getTeacherPortraitList();
      //获取班级列表
      $scope.getSchoolClasses();

      // $scope.getFaceRecognitionClassList();

    });



    //选择角色
    $scope.chooseRole = function (item) {
      //$scope.selected.defaultRole = item.key;
      $ionicScrollDelegate.$getByHandle('mainScroll').scrollTop();
      if ($scope.selected.defaultRole === 2) {
        $scope.getFaceRecognitionClassList();
      } else {
        $scope.getTeacherPortraitList();
      }
    };
    //更新年级
    $scope.updateGrade = function (item) {
      // $scope.recordClassList = item.classList;
      for (var i = 0; i < $scope.gradeList.length; i++) {
        if ($scope.selected.gradeId === $scope.gradeList[i].id) {
          $scope.recordClassList = $scope.gradeList[i].classList;
          break;
        }
      }
      $scope.selected.class_id = $scope.recordClassList[0].id;
      $scope.getFaceRecognitionClassList();

    };
    //更新班级
    $scope.updateClass = function () {
      //$scope.selected.class_id = item.id;
      $scope.getFaceRecognitionClassList();

    };

    //刷新列表
    $scope.refreshData = function () {
      if ($scope.selected.defaultRole === 3) {
        $scope.getTeacherPortraitList();
      } else {
        $scope.getFaceRecognitionClassList();
      }

    };

    //点击图片放大
    $scope.viewImages = function (item, index, $event) {
      var url = 'img/icon/person.png';
      if (item.userImgSrc) url = item.userImgSrc;
      var urls = [];
      index = 0;
      urls.push(url);
      $event.stopPropagation();
      ionicImageView.showViewModal({
        allowSave: false
      }, urls, index);
    };

    //选择班级后重新加载数据
    $scope.loadData = function () {
      console.log('select id:' + $scope.selected.class_id);
      $scope.getFaceRecognitionClassList();
    };



    //选择图片
    $scope.selectImg = function (item) {
      console.log(item);
      $scope.selected.userId = item.userId;
      if (window.cordova) {
        CameraHelper.selectImage('portrait', {
          allowEdit: false,
          width: 800,
          height: 800
        });
      } else {
        var input = document.getElementById("capture");
        input.click();
      }
    };

    //我已阅读
    $scope.haveKnow = function () {

      // 把状态和用户id 关联起来 是为了在采集过程中出现 一台设备 多个老师账户登录
      $scope.setReadStatus();
      $scope.attentionModal.hide();
      if (window.cordova) {
        CameraHelper.selectImage('portrait', {
          allowEdit: false,
          width: 800,
          height: 800
        });
      } else {
        var input = document.getElementById("capture");
        input.click();
      }

    };


    $scope.$on(BroadCast.IMAGE_SELECTED, function (a, rst) {
      if (rst && rst.which === 'portrait') {
        //上传图片
        // $scope.sendFeedbackRequest('1', rst.source);
        setTimeout(function () {
          var userId = $scope.selected.userId;
          console.log('userId:' + userId);
          $scope.uploadUserPortrait(userId, rst.source);
        }, 50);
      }
    });

    //web端选择文件
    $scope.getFiles = function (files) {
      var file = files[0];
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = function (theFile) {
        $timeout(function () {
          $scope.testImg = theFile.target.result;
          $ionicLoading.show({
            noBackdrop: true,
            template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
          });
          // var
          $scope.uploadUserPortrait($scope.selected.userId, $scope.testImg);
        }, 100);
      };
    };

    $scope.hideAttentionModal = function () {
      $scope.attentionModal.hide();
    };

    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function () {
      $scope.attentionModal.remove();
    });
    // Execute action on hide modal
    $scope.$on('modal.hidden', function () {
      // Execute action
    });
    // Execute action on remove modal
    $scope.$on('modal.removed', function () {
      // Execute action
    });

    //获取学校所有班级
    $scope.getSchoolClasses = function () {
      Requester.getSchoolAllClasses($scope.schoolId).then(function (resp) {
        if (resp.result) {
          $scope.gradeList = resp.data;
          if ($scope.gradeList && $scope.gradeList.length > 0) {
            $scope.selected.gradeId = $scope.gradeList[0].id;
            $scope.recordClassList = $scope.gradeList[0].classList;
            $scope.selected.class_id = $scope.recordClassList[0].id;
          }

        } else {
          toaster.error({
            title: resp.message,
            body: ''
          });
        }
      });
    };

    //request --获取学校教师人像列表
    $scope.getTeacherPortraitList = function () {
      $scope.teacherList = [];

      Requester.getTeacherPortraitList($scope.schoolId).then(function (rest) {
        if (rest.result) {

          $scope.teacherList = rest.data.content;

        } else {

          toaster.warning({
            title: "温馨提示",
            body: rest.message
          });
        }
      }).finally(function () {
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
      });
      // }

    };


    //requester ---获取班级人像列表
    $scope.getFaceRecognitionClassList = function () {
      Requester.getFaceRecognitionClassList($scope.selected.class_id).then(function (resp) {
        if (resp.result) {
          //regcStatus: 处理状态。0，未采集;1,采集图片上传成功;2,提取特征值成功;3,提取特征值失败
          $scope.stuList = resp.data;
        } else {
          toaster.error({
            title: resp.message,
            body: ''
          });
        }
      }).finally(function () {
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
      });
    };

    //requester ---上传
    $scope.uploadUserPortrait = function (userId, base64Str) {
      Requester.uploadUserPortrait(userId, base64Str).then(function (resp) {
        $ionicLoading.hide();
        if (resp.result) {
          toaster.success({
            body: '上传成功'
          });
          if ($scope.selected.defaultRole === 3) {
            $scope.getTeacherPortraitList();
          } else {
            $scope.getFaceRecognitionClassList();
          }
        } else {
          toaster.error({
            title: resp.message,
            body: ''
          });
        }
      }).finally(function () {
        setTimeout(function () {
          $ionicLoading.hide();
        }, 3000);

      });
    };

    $scope.goIntroduce = function () {
      // $state.go('myPortrait');
    };

    $scope.getTypeStr = function (type) {
      var result = type === 0 ? '未采集' : type === 1 ? '人像提取中' : type === 2 ? '采集完成' : '采集失败';
      return result;

    };



    //存状态数组
    $scope.setReadStatus = function () {
      var readMark = $scope.selected.isChecked ? 'read' : 'unread';
      var count = 0;
      if ($scope.remarkArr && $scope.remarkArr.length > 0) {
        for (var i = 0; i < $scope.remarkArr.length; i++) {
          //如果用户已存在则是更改状态
          if ($scope.userId === $scope.remarkArr[i].key) {
            $scope.remarkArr[i].value = readMark;
            break;
          } else {
            count++;
          }
        }
        if ($scope.remarkArr.length === count) {
          $scope.remarkArr.push({
            key: $scope.userId,
            value: readMark
          });
        }

      } else {
        $scope.remarkArr.push({
          key: $scope.userId,
          value: readMark
        });

      }

      UserPreference.setObject('RemarkReadStatus', $scope.remarkArr);
    };



    //取状态数组
    $scope.getReadStatus = function () {
      var result;
      var statusArr = UserPreference.getArray('RemarkReadStatus');
      if (statusArr && statusArr.length > 0) {
        for (var i = 0; i < statusArr.length; i++) {
          if (statusArr[i].key === $scope.userId && statusArr[i].value === 'read') {
            result = 'read';
            break;
          } else {
            result = 'unread';
          }
        }

      } else {
        result = 'unread';
      }
      return result;
    };

    $scope.goIntroduce = function () {
      $scope.attentionModal.show();
    };

    //关闭说明弹窗
    $scope.hideAttentionModal = function () {
      $scope.attentionModal.hide();
    };

    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function () {
      $scope.attentionModal.remove();
    });
    // Execute action on hide modal
    $scope.$on('modal.hidden', function () {
      // Execute action
    });
    // Execute action on remove modal
    $scope.$on('modal.removed', function () {
      // Execute action
    });


  });