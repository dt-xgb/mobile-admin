angular.module('app.controllers')
    .controller('babyVideoDetailCtrl', function ($scope, Constant, $state, $sce, toaster, $stateParams) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.videoUrl = $sce.trustAsResourceUrl($stateParams.url);
            console.log($scope.videoUrl);
        });
    });