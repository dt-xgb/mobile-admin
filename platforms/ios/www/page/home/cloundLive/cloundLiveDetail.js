angular.module('app.controllers')
    .controller('cloundLiveDetailCtrl', function ($scope, Constant, $state, $ionicModal, Requester, toaster, UserPreference, $ionicPopup, BroadCast, CameraHelper, $timeout, $stateParams,$sce,$ionicLoading, $ionicHistory) {
     
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'page/';
        $ionicModal.fromTemplateUrl(UIPath + 'home/cloundLive/playbackModal.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.playbackModal = modal;
        });
        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.cloundLiveId = $stateParams.cloundLiveId;
            $scope.status = $stateParams.status;//0 :未开始 1：直播中 2：已结束
            $scope.mobLiveUrl = $sce.trustAsResourceUrl($stateParams.mobLiveUrl)?$sce.trustAsResourceUrl($stateParams.mobLiveUrl):'';
           // $scope.mobLookbackUrl = $sce.trustAsResourceUrl($stateParams.mobLookbackUrl)?$sce.trustAsResourceUrl($stateParams.mobLookbackUrl):'';
           console.log(' $scope.mobLiveUrl:'+ $scope.mobLiveUrl);
           $scope.schoolName = UserPreference.get('DefaultSchoolName');
            $scope.getCloundLiveDetail($scope.cloundLiveId);

             // videojs 简单使用
            //  var player = new TcPlayer('id_test_video', {
            //     "m3u8": "http://2157.liveplay.myqcloud.com/2157_358535a.m3u8", //请替换成实际可用的播放地址
            //     "autoplay" : true,      //iOS 下 safari 浏览器，以及大部分移动端浏览器是不开放视频自动播放这个能力的
            //     "poster" : "http://www.test.com/myimage.jpg",
            //     "width" :  '480',//视频的显示宽度，请尽量使用视频分辨率宽度
            //     "height" : '320'//视频的显示高度，请尽量使用视频分辨率高度
            //     });
             // videojs 简单使用
    var myVideo = videojs('myVideo', {
        bigPlayButton: true,
        textTrackDisplay: false,
        posterImage: false,
        errorDisplay: false,
    });
    myVideo.play();


        });


        //查看回放
        $scope.readReview = function(){
         console.log('回放');
         if($scope.details.transcodingStatus===0){
            toaster.warning({
                title: "正在转码中，请稍后再看",
                body: rest.message
            });
            return ;
         }
         $scope.playbackModal.show();
        };

        //回放返回
        $scope.hidePlaybackModal= function(){
            $scope.playbackModal.hide();
        };

        //编辑直播
        $scope.editLive = function(type){
            // console.log('edit');
            if(type===1){
              //编辑
              $state.go('addCloundLive',{
                  id:$scope.details.id,
                  status:$scope.details.status,
                  title:$scope.details.title,
                  speaker:$scope.details.speaker,
                  liveDay:$scope.details.liveDay,
                  startTime:$scope.details.startTime.substr(0,5),
                  endTime:$scope.details.endTime.substr(0,5),
                  islookBack:$scope.details.islookBack
                });
            }else{
               //删除
              
               $ionicPopup.show({
                title: '温馨提示',
                scope: $scope,
                template: '<ion-scroll scrollX="false" scrollY="true" scrollbar-y="false" direction="y" class="list"><div style="padding: 1px 15px !important;">确定删除这场直播吗？</ion-scroll>',
                buttons: [{
                        text: '取消'
                    },
                    {
                        text: '<b>确认</b>',
                        type: 'button-balanced',
                        onTap: function (e) {
                            $ionicLoading.show({
                                noBackdrop: true,
                                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
                            });
                            $scope.deleteCloundLive($scope.details.id);
                        }
                    }
                ]
            });
            }

        };

         // Cleanup the modal when we're done with it!
         $scope.$on('$destroy', function () {
           // $scope.playbaclModal.remove();
           
        });
        // Execute action on hide modal
        $scope.$on('modal.hidden', function () {
            // Execute action
        });
        // Execute action on remove modal
        $scope.$on('modal.removed', function () {
            // Execute action
        });

        //返回
        $scope.goBack = function(){
            $ionicHistory.goBack();
        };

        //requester
        $scope.getCloundLiveDetail = function(id){
            Requester.cloundLiveDetail(id).then(function(rest){
                if (rest.result) {
                    $scope.details = {};
                    $scope.details = rest.data;
                    $scope.mobLookbackUrl= $scope.details.mobLookbackUrl?$sce.trustAsResourceUrl($scope.details.mobLookbackUrl):'';
                //   console.log(' $scope.videoUrl:'+ $scope.videoUrl);
                } else {
                    
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });

                }
            });
        };

        //删除
        $scope.deleteCloundLive = function(id){
            Requester.cloundLiveDelete(id).then(function(rest){
                if (rest.result) {
                    $ionicLoading.hide();
                    toaster.success({
                        body: '删除成功',
                        timeout: 3000
                    });

                    $ionicHistory.goBack();
                  } else {
                    $ionicLoading.hide();
                      toaster.warning({
                          title: "温馨提示",
                          body: rest.message
                      });
                  }
            });
        };
       

    });