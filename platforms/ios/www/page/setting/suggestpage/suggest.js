/**
 * Created by hewz on 2018/3/30.
 */
angular.module('app.controllers')
    .controller('suggestCtrl', function ($scope, BroadCast, Requester, toaster, $timeout, UserPreference, $ionicScrollDelegate, $cordovaCamera, Constant, ionicImageView, $ionicPopover, CameraHelper,$rootScope) {
        $scope.suggest = {
            text: ''
        };
        // document.querySelector('body').style.height = document.documentElement.clientHeight + 'px';
        // document.querySelector('body').style.width = document.documentElement.clientWidth + 'px';
        $scope.isIos = ionic.Platform.isIOS();
        if (window.cordova&&$scope.isIos) {
            //cordova.plugins.Keyboard.disableScroll(false);
        }

        $scope.footHeight = 0;

        var UIPath = '';
        if (Constant.debugMode) UIPath = 'page/';
        $ionicPopover.fromTemplateUrl(UIPath + 'setting/suggestpage/suggestEmotions.html', {
            scope: $scope
        }).then(function (popover) {
            $scope.popover = popover;
        });


        $scope.items = getSuggestEmotions();

        $scope.$on("$ionicView.loaded", function (event, data) {
            //获取用户id
            var user = UserPreference.getObject('user');

            $scope.userName = String(user.name);
            $scope.userHead = user.logo;
            $scope.screenHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
           // console.log('屏幕高度为：' + $scope.screenHeight);
            $scope.input = {
                message: '',
                messageType: 0
            };
            $scope.input.message = '';
        });


        $scope.$on("$ionicView.enter", function (event, data) {
            $scope.page = 1;
            $scope.isRight = false;
            $scope.isShow = false; //是否弹出底部视图 相机
            $scope.emotionShow = false;
            $scope.feedBackList = [];
            $scope.imageUrls = [];
            $scope.feedBackList = UserPreference.getArray('feedback_list'); //用于存放反馈的数组消息
           // console.log('local feedback length:' + $scope.feedBackList.length);
            if ($scope.feedBackList.length > 0) {
                $scope.insertTimeLabelToMsg($scope.feedBackList.length - 1);
                if ($scope.feedBackList[0].userName == $scope.userName) {
                    $scope.isRight = true;

                } else {
                    $scope.isRight = false;
                }
            }

            $timeout(function () {
                $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollBottom(true);
            }, 200);

            $scope.getFeedbackList();

            window.addEventListener('native.keyboardshow', function (e) {
                $scope.isShow = false;
                $scope.emotionShow = false;
                $scope.popover.hide();
                $timeout(function () {
                    $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollBottom(true);
                    if($scope.isIos&&window.scrollY<100){
                          window.scrollTo(0, e.keyboardHeight);
                    }
                }, 100);
                //console.log('keyboard show');
                //$scope.$apply();
            });

            window.addEventListener('native.keyboardhide', function (e) {
                // todo 进行键盘不可用时操作
               // console.log('关闭时');
                
                $timeout(function () {
                    $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollBottom(true);
                    if($scope.isIos&&window.scrollY!=0){
                        window.scrollTo(0,0);
                  }
                }, 100);
                // $scope.$apply();
            });

            // window.addEventListener('keyboardWillShow', function (event) {
            //     console.log('keyboard show 1111');
            //     console.log(event);
            //     $timeout(function(){
            //         $scope.footHeight = 0;//event.keyboardHeight;
            //     },50);
            // });

            // window.addEventListener('keyboardDidHide', function () {
            //     // Describe your logic which will be run each time keyboard is closed.
            //     console.log('keyboard hide 2222');
            //     $timeout(function(){
            //         $scope.footHeight = 0;
            //     },50);
            // });

        

        });
        $scope.focus = function(){
            $timeout(function(){
                if(ionic.Platform.isIOS())
                $scope.footHeight =  $rootScope.keyboardHeight;
                $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollTo(0,-260);
            },50);

        };

        $scope.blur = function(){
            $timeout(function(){
                $scope.footHeight =  0;
            },50);
        };

        //点击 发送 按钮 
        $scope.sendMessage = function () {
            $scope.sendFeedbackRequest('0', '');
        };

        //发送 消息
        $scope.sendFeedbackRequest = function (type, imgContent) {

            $scope.arr = [];
            if (type === '0') {
                if ($scope.input.message === '') {
                    return;
                }
                var txtInput = angular.element(document.getElementsByName("chatInput"));
                $scope.arr = $scope.input.message.split(",");
            } else {
                $scope.arr.push(imgContent);
            }

            Requester.publishUserFeedback($scope.arr, type).then(function (resp) {
               // console.log(resp);
                if (resp.result) {
                    $scope.input.message = '';
                    $scope.isShow = false; //隐藏相机弹框
                    $scope.emotionShow = false; //隐藏表情弹框
                } else {
                    toaster.warning({
                        title: "温馨提示",
                        body: "系统异常"
                    });
                }
                if (type == 1) {
                    var now = getNowFormatDate();
                    var timestamp = Date.parse(new Date(now.replace(/\-/g, "/"))) / 1000;
                    $scope.suggestionMsg = {
                        contentType: 1,
                        imgContent: [{
                            thumb: imgContent,
                            url: imgContent
                        }],
                        // textContent:'999999999',
                        id: timestamp,
                        datetime: now,
                        userName: $scope.userName,
                        userHead: $scope.userHead
                    };
                    $scope.feedBackList.push($scope.suggestionMsg);
                }

                //发送反馈成功
                return resp.result;

            }).then(function (result) {

                // 发送成功后 获取反馈列表
                if (result) {
                    $scope.getFeedbackList();
                }
                return result;

            }).then(function (result) {
                if (result) {
                   // console.log('获取列表成功了:' + result);
                    // $scope.$apply();
                    // $timeout(function () {
                    //     $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollBottom(true);
                    // }, 300);
                    var resp = $ionicScrollDelegate.$getByHandle('userMessageScroll').getScrollPosition();
                    $timeout(function () {
                        $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollTo(0, resp.top + 75, true);
                    }, 500);

                }

            });
        };

        //获取消息列表 
        $scope.getFeedbackList = function () {
            //$scope.feedBackList = [];
            Requester.getFeedbackDetailList().then(function (resp) {
                if (resp.result) {
                    $scope.feedBackList = resp.data.suggestionMsgs;
                    $scope.feedBackList.forEach(function (message) {
                        $scope.imageUrls = [];
                        if (message.contentType == 1 && message.imgContent.length > 0) {
                            message.imgContent.forEach(function (image) {
                                $scope.imageUrls.push(image.url);
                            });
                            message.imageUrls = $scope.imageUrls;
                        }
                    });
                    UserPreference.setObject("feedback_list", $scope.feedBackList);
                    $scope.insertTimeLabelToMsg($scope.feedBackList.length - 1);
                   // console.log(resp);
                    if ($scope.feedBackList[0].userName == $scope.userName) {
                        $scope.isRight = true;
                    } else {
                        $scope.isRight = false;
                    }

                }
            });
        };

        //插入一条时间
        $scope.insertTimeLabelToMsg = function (i) {
            for (; i >= 0; i--) {
                var msg = $scope.feedBackList[i];
                var time1 = msg.datetime.replace(/\-/g, "/");
                var timestamp = Date.parse(new Date(time1)) / 1000;

                var msgDay = msg.datetime.substr(8, 2);
                var now = getNowFormatDate().substr(8, 2);

                if (i > 0) {
                    var preMsg = $scope.feedBackList[i - 1];
                    var timestamp1 = Date.parse(new Date(preMsg.datetime.replace(/\-/g, "/"))) / 1000;
                    var preMsgDay = preMsg.datetime.substr(8, 2);
                    if (now != msgDay) {
                        if (msgDay != preMsgDay)
                            msg.timeLabel = msg.datetime.substr(0,16); //formatTimeWithoutSecends(timestamp);
                        else
                            msg.timeLabel = undefined;
                    } else if (timestamp - timestamp1 >= 60 * 5) {
                        msg.timeLabel = msg.datetime.substr(11, 5); //formatTimeWithoutSecends(timestamp).substr(11, 5);
                    } else {
                        msg.timeLabel = undefined;
                    }

                } else {
                    if (now != msgDay) {
                        msg.timeLabel = msg.datetime.substr(0,16); //formatTimeWithoutSecends(timestamp);
                    } else
                        msg.timeLabel = msg.datetime.substr(11, 5); //formatTimeWithoutSecends(timestamp).substr(11, 5);
                }
            }
        };
        //点击图片放大
        $scope.viewImages = function (urls, index, $event) {
            $event.stopPropagation();
            ionicImageView.showViewModal({
                allowSave: true
            }, urls, index);
        };

        //弹出底部视图
        $scope.selectImg = function () {
            $timeout(function () {
                if (window.cordova)
                    cordova.plugins.Keyboard.close();
            }, 10);
            var resp = $ionicScrollDelegate.$getByHandle('userMessageScroll').getScrollPosition();
            //$scope.isShow = !$scope.isShow;
            $scope.emotionShow = false;
            $timeout(function () {
                $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollBottom(true);
            }, 100);

            $timeout(function () {
                $ionicScrollDelegate.resize();
                CameraHelper.selectImage('suggest');
            }, 100);

        };

        $scope.$on(BroadCast.IMAGE_SELECTED, function (a, rst) {
            if (rst && rst.which === 'suggest') {
                //上传
                $scope.sendFeedbackRequest('1', rst.source);

            }
        });
        //底部弹出表情
        $scope.showEmotions = function ($event) {

            setTimeout(function () {
                if (window.cordova)
                    cordova.plugins.Keyboard.close();
                $scope.popover.show($event);
            }, 100);

            $timeout(function () {
                $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollBottom(true);
            }, 100);

            //   var resp  = $ionicScrollDelegate.$getByHandle('userMessageScroll').getScrollPosition();
            //     $scope.emotionShow = !$scope.emotionShow;
            //     $scope.isShow = false;

            //     if($scope.emotionShow){
            //         $timeout(function () {
            //             $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollTo(0,resp.top+200,true); 
            //         }, 100);

            //     }else{
            //         $timeout(function () {
            //             $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollBottom(true);
            //         }, 100);
            //     }
        };

        $scope.switchPhoto = function (index) {
            //1相册 0 拍照
            var srcType = Camera.PictureSourceType.PHOTOLIBRARY;
            if (index === 0) {
                srcType = Camera.PictureSourceType.CAMERA;
            }
            var imgOpt = {
                allowEdit: false
            };
            var destinationType = Camera.DestinationType.DATA_URL;
            if ((imgOpt && imgOpt.allowEdit)) {
                destinationType = Camera.DestinationType.FILE_URI;
            }
            if (index === 0 || index === 1) {
                var defaultHeight = 300;
                var defaultWidth = 300;
                // var cropTitle = '截取高亮区域';
                var options = {
                    quality: 80,
                    destinationType: destinationType,
                    sourceType: srcType,
                    allowEdit: false,
                    encodingType: Camera.EncodingType.JPEG,
                    targetWidth: Constant.CAPTURE_IMAGE_RANGE,
                    targetHeight: Constant.CAPTURE_IMAGE_RANGE,
                    correctOrientation: true,
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: false
                };
                if (imgOpt) {
                    if (imgOpt.width && imgOpt.height) {
                        defaultHeight = imgOpt.height;
                        defaultWidth = imgOpt.width;
                    } else {
                        options.targetWidth = defaultWidth * 2;
                        options.targetHeight = defaultHeight * 2;
                    }
                    if (imgOpt.title)
                        cropTitle = imgOpt.title;
                }
                $cordovaCamera.getPicture(options).then(function (imageURI) {
                    var base64Str = 'data:image/jpeg;base64,' + imageURI;
                    //上传
                    $scope.sendFeedbackRequest('1', base64Str);

                }, function (err) {

                });
            }
        };

        $scope.setValue = function (e) {
           // console.log(e);
            if ($scope.input.message)
                $scope.input.message += e;
            else
                $scope.input.message = e;

            var tObj = document.getElementById("suggestTextinput");
            var sPos = tObj.value.length;
            setCaretPosition(tObj, sPos);
            $scope.popover.hide();
        };
        //隐藏footView
        $scope.hideFootView = function () {
            if ($scope.emotionShow === true || $scope.isShow === true) {
                $scope.emotionShow = false;
                $scope.isShow = false;
                $timeout(function () {
                    $ionicScrollDelegate.$getByHandle('userMessageScroll').scrollBottom(true);
                }, 100);
            }

        };

        //下拉刷新
        $scope.loadMore = function () {
            //关闭下拉刷新
            $scope.$broadcast('scroll.refreshComplete');
        };

        //离开页面时  将所有未读的 标记为已读状态
        $scope.$on("$ionicView.leave", function (event, data) {
            $scope.selected = true;
            // $scope.feedBackList.array.forEach(element => {

            // });
            $scope.fixFeedbackState();
        });

        //标记消息为已读
        $scope.fixFeedbackState = function () {

            Requester.fixNewsReadStatus('').then(function (resp) {
                if (resp.result) {
                    console.log('已成功标记为已读');
                }
            });
        };

        $scope.convertMsgtoHtml = function (msg) {
            return encodeHtml(msg);
        };

    });