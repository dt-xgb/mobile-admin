angular.module('app.controllers')
<<<<<<< HEAD
    .controller('banpaiListCtrl', function ($scope, Constant, $state, $ionicModal, Requester, toaster, UserPreference,$ionicPopup) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
        });
=======
    .controller('banpaiListCtrl', function ($scope, Constant, $state, $ionicModal, Requester, toaster, UserPreference, $ionicPopup, BroadCast, CameraHelper, $timeout, ionicDatePicker, ionicTimePicker,$ionicLoading) {
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'page/';
        $ionicModal.fromTemplateUrl(UIPath + 'home/banpaiManager/publishNotice.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.noticeModal = modal;
        });

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.schoolId = UserPreference.get('DefaultSchoolID');
            $scope.list = UserPreference.getObject('BanPaiList') ? UserPreference.getObject('BanPaiList') : [];
            $scope.allCount = 0;
         
            $scope.getBanPaiList();//获取所有列表
            $scope.getSchoolAllClasses(); //获取所有班级


            $scope.selectClassName = '';
            $scope.select = {
                isSchoolSet: 0,//学校模式
                isDisplayStyle: 0,
                isDisplayModel: 0,
                isMoreSet: 0,
                isSystemBtn: 0,//系统按钮
                selectNum: 0,
             //   noticeType: 0//0：文本 1：图片 ，默认文本,

            };
            $scope.displayList = [{ key: 1, value: '班牌模式' },
            { key: 2, value: '校牌模式' },
            { key: 3, value: '通知模式' },
            { key: 5, value: '上课模式' },
            { key: 6, value: '场所模式' },
            { key: 7, value: '会议模式' },
            { key: 8, value: '考试模式' }
            ];

            $scope.filterList = [];
            //$scope.gradeList = [{ "name": "二年级", "classList": [{ "name": "二(1)班", "classList": [], "id": "51655" }, { "name": "二(2)班", "classList": [], "id": "51654" }, { "name": "二(3)班", "classList": [], "id": "51653" }, { "name": "二(4)班", "classList": [], "id": "51652" }, { "name": "二(5)班", "classList": [], "id": "51651" }, { "name": "二(6)班", "classList": [], "id": "51650" }, { "name": "二(7)班", "classList": [], "id": "51649" }, { "name": "二(8)班", "classList": [], "id": "51648" }, { "name": "二(9)班", "classList": [], "id": "51647" }, { "name": "二(10)班", "classList": [], "id": "51646" }, { "name": "二(11)班", "classList": [], "id": "51645" }], "id": "A_PRIMARY_SCHOOL1" }, { "name": "三年级", "classList": [{ "name": "三(1)班", "classList": [], "id": "38338" }, { "name": "三(2)班", "classList": [], "id": "38339" }, { "name": "三(3)班", "classList": [], "id": "38340" }, { "name": "三(4)班", "classList": [], "id": "38341" }, { "name": "三(5)班", "classList": [], "id": "38342" }, { "name": "三(6)班", "classList": [], "id": "38343" }, { "name": "三(7)班", "classList": [], "id": "39523" }], "id": "A_PRIMARY_SCHOOL2" }, { "name": "四年级", "classList": [{ "name": "四(1)班", "classList": [], "id": "15522" }, { "name": "四(2)班", "classList": [], "id": "15523" }, { "name": "四(3)班", "classList": [], "id": "15524" }, { "name": "四(4)班", "classList": [], "id": "15525" }, { "name": "四(5)班", "classList": [], "id": "15526" }, { "name": "四(6)班", "classList": [], "id": "15527" }, { "name": "四(7)班", "classList": [], "id": "15528" }, { "name": "四(8)班", "classList": [], "id": "15532" }, { "name": "四(9)班", "classList": [], "id": "15534" }], "id": "A_PRIMARY_SCHOOL3" }, { "name": "五年级", "classList": [{ "name": "五(1)班", "classList": [], "id": "15540" }, { "name": "五(2)班", "classList": [], "id": "15541" }, { "name": "五(3)班", "classList": [], "id": "15542" }, { "name": "五(4)班", "classList": [], "id": "15543" }, { "name": "五(5)班", "classList": [], "id": "15544" }, { "name": "五(6)班", "classList": [], "id": "15545" }], "id": "A_PRIMARY_SCHOOL4" }, { "name": "六年级", "classList": [{ "name": "六(1)班", "classList": [], "id": "15552" }, { "name": "六(2)班", "classList": [], "id": "15553" }, { "name": "六(3)班", "classList": [], "id": "51656" }], "id": "A_PRIMARY_SCHOOL5" }];

            // $scope.select.grade_id = $scope.gradeList[0].id;
            // $scope.classList = $scope.gradeList[0].classList;
        });

        $scope.onCheckedChange = function (item) {
            if (item.isChecked) {

                $scope.select.selectNum++;
                $scope.select.all = $scope.select.selectNum === $scope.list.length;
            } else {
                $scope.select.selectNum--;
                $scope.select.all = false;
            }
            $scope.filterList = $scope.getFilterList();
            checkIsExitChecked();
        };

        //全选
        $scope.selectAll = function () {
            //  $scope.filterList = $scope.getFilterList();
            $scope.select.all = $scope.select.all === true;
            $scope.select.isLight = $scope.select.all;//是否点亮下方选择按钮
            setListCheckedStatus($scope.select.all);
            if ($scope.select.all) {
                $scope.select.selectNum = $scope.list.length;
            } else {
                $scope.select.selectNum = 0;
            }



        };
        function setListCheckedStatus(sel) {
            for (var i = 0; i < $scope.list.length; i++) {
                $scope.list[i].isChecked = sel;
            }
        }
        //判断是否有选中的
        function checkIsExitChecked() {
            for (var i = 0; i < $scope.filterList.length; i++) {
                if ($scope.filterList[i].isChecked) {
                    $scope.select.isLight = true;
                    return;
                } else {
                    $scope.select.isLight = false;
                }
            }
        }

        $scope.loadData = function () {
            $scope.loading = false;
            $scope.getBanPaiList();//获取所有列表
            $scope.add_search = '';
           
            
            $scope.$broadcast('scroll.refreshComplete');
        };

        //筛选班级
        $scope.filterClass = function () {
            //选择班级
            $scope.selectClassPopup = $ionicPopup.show({
                templateUrl: UIPath + 'home/banpaiManager/classSelect.html',
                title: '<div style="font-size:18px;">筛选</div>',
                scope: $scope,
                cssClass: 'banpaiSetAlert'
            });
        };
        //重置班级选择
        $scope.resetClass = function () {
            $scope.selectClassPopup.close();
            $scope.list = $scope.recordeList;
            $scope.select.class_id = '';
            $scope.select.className = '';
        };
        //选择年级
        $scope.selectGrade = function (item) {
            $scope.classList = item.classList;
            $scope.select.grade_id = item.id;
        };
        //选择班级
        $scope.selectClass = function (item) {
            $scope.select.class_id = item.id;
            $scope.select.className = item.name;
           

            $scope.selectClassPopup.close();

            $scope.list = $scope.getFilterListByClassId();

        };


        $scope.setFunc = function (type) {
            //提交时 只提交选中 及 满足筛选条件的
            var ids = [];
            $scope.selectList = [];//关机
            for (var i = 0; i < $scope.list.length; i++) {
                if ($scope.list[i].isChecked) {
                    if (!$scope.select.add_search) {
                        ids.push($scope.list[i].id);
                        $scope.selectList.push($scope.list[i]);
                    } else if (($scope.list[i].schoolSite && $scope.list[i].schoolSite.site_name && $scope.list[i].schoolSite.site_name.indexOf($scope.select.add_search) != -1) || $scope.list[i].sn.indexOf($scope.select.add_search) != -1) {
                        ids.push($scope.list[i].id);
                        $scope.selectList.push($scope.list[i]);
                    }

                }

            }

         
            $scope.adterminalIds = ids.join(',');
            clearInterval($scope.refresh);

            if (type === 0) {
                //显示模式
                setTimeout(function(){
                    $scope.displayPopup = $ionicPopup.show({
                        template:'<div class="row row-wrap" style="padding:0px;margin-top:10px;background-color: white;"> <div ng-repeat="item in displayList" ng-click="chooseDisplayModel(item)" ng-style="{\'background\':select.displayKey===item.key?\'#2bcab2\':\'#eee\',\'color\':select.displayKey===item.key?\'white\':\'#666\'}" style="border-radius: 5px;width:80px;font-size: 15px;margin-left:calc(25% - 60px);margin-top:15px;height:38px;line-height: 38px;text-align: center;">{{item.value}}</div> <div class="row" style="margin-top: 25px;margin-bottom: 20px;"><div class="button"  ng-click="confirmDisplay()" style="height:40px;background:#2bcab2;color:white;width:85px;border-radius:5px;margin-left:calc(50% - 100px);">确定</div> <div class="button" ng-click="cancelDisplay()" style="height:40px;background:#ddd;color:#666;width:85px;border-radius:5px;margin-left:30px ;">取消</div></div></div>',
                       // templateUrl: UIPath + 'home/banpaiManager/displayModel.html',
                        title: '<p style="font-size:18px;">设置显示模式</p>',
                        cssClass: 'banpaiSetAlert',
                        scope: $scope
                    });
                  
                },100);
               
            } else if (type === 1) {
                //锁定模式
                setTimeout(function(){
                    $scope.lockPopup = $ionicPopup.show({
                        templateUrl: UIPath + 'home/banpaiManager/lockSetAlert.html',
                        title: '<p style="font-size:18px;">锁定设置</p>',
                        cssClass: 'banpaiSetAlert',
                        scope: $scope
                    });

                },100);
               
            } else {
                //关机
                $scope.shutPopup = $ionicPopup.show({
                    templateUrl: UIPath + 'home/banpaiManager/warnAlert.html',
                    title: '<p style="font-size:18px;">温馨提示</p>',
                    cssClass: 'banpaiSetAlert',
                    scope: $scope
                });
            }
        };

        //选择显示模式
        $scope.chooseDisplayModel = function (item) {
            $scope.select.displayKey = item.key;
        };

        //确定选择显示模式
        $scope.confirmDisplay = function () {
            // console.log('----select');
            // console.log($scope.select);
            $scope.displayPopup.close();
            if ($scope.select.displayKey === 3) {
                //通知模式
                $scope.select.noticeType = 0;//默认为文本
                $scope.displayPopup.close();
                $scope.noticeModal.show();
            } else {
                $scope.setDisplayModel($scope.adterminalIds);
            }
           
           
           
        };

        //选择发布通知模式样式 0：文本 1：图片
        $scope.chooseNoticeType = function (type) {
            $scope.select.noticeType = type;
        };

        //选择时效
        $scope.chooseNoticeTime = function () {
            setTimeout(function () {
                if (window.cordova) cordova.plugins.Keyboard.close();
                var ipObj1 = {
                    callback: function (val) {
                        if (typeof (val) !== 'undefined') {
                            $scope.select.noticeTime = formatTimeWithoutSecends(val / 1000).substr(0, 10);
                            $scope.showTimePicker();

                        }
                    }
                };
                ionicDatePicker.openDatePicker(ipObj1);
            }, 100);

        };


        //选择时间
        $scope.showTimePicker = function () {
            var ipObj1 = {
                inputTime: (((new Date()).getHours() * 60 * 60) + ((new Date()).getMinutes() * 60)),
                callback: function (val) {
                    if (typeof (val) === 'undefined') { } else {
                        var showTime = new Date(val * 1000);
                        var h = showTime.getUTCHours();
                        var m = showTime.getUTCMinutes();
                        var time = (h < 10 ? '0' + h : h) + ':' + (m < 10 ? '0' + m : m);
                        // $scope.timeBtnTxt = time;
                        $scope.select.noticeTime = $scope.select.noticeTime + ' ' + time + ':00';
                    }
                }

            };
            ionicTimePicker.openTimePicker(ipObj1);
        };


        //选择图片
        $scope.selectImg = function () {
            if (window.cordova) {
                CameraHelper.selectImage('portrait', {
                    allowEdit: false,
                    width: 800,
                    height: 800
                });
            } else {
                var input = document.getElementById("capture");
                input.click();
            }
        };
        //移除图片
        $scope.removeImg = function () {
            $scope.select.noticeImg = '';
            $scope.select.imageUrl = '';
        };

        $scope.$on(BroadCast.IMAGE_SELECTED, function (a, rst) {
            if (rst && rst.which === 'portrait') {
                //上传图片
                setTimeout(function () {
                    // rst.source
                    var base64 =  rst.source;
                    // console.log('rst.source');
                    //console.log(rst.source);
                    var file = dataURLtoFile(base64, 'image1.jpeg');
                    $scope.uploadPictureByFileType(file);
                }, 50);
            }
        });

        //web端选择文件
        $scope.getFiles = function (files) {
            var file = files[0];
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (theFile) {
                $timeout(function () {
                    $scope.testImg = theFile.target.result;
                    $scope.select.noticeImg = $scope.testImg;

                }, 100);
            };

            $scope.uploadPictureByFileType(file);
        };



        //发布通知
        $scope.publishNotice = function () {
           // console.log('发布通知');
            //时效时间要大于当前时间
            if (!$scope.select.noticeTime) {
                toaster.warning({
                    title: "",
                    body: '请选择时效',
                    timeout: 3000
                });
                return;
            }
            var remindStamp = Date.parse(new Date($scope.select.noticeTime.replace(/\-/g, "/"))) / 1000;
            $scope.my_date = new Date();
            $scope.currentTamp = Date.parse($scope.my_date) / 1000;
            if (remindStamp - $scope.currentTamp < 0) {
                toaster.warning({
                    title: "",
                    body: '时效时间不能早于当前时间',
                    timeout: 3000
                });
                return;
            }

            $scope.setDisplayModel($scope.adterminalIds,3);

        };



        //取消显示模式
        $scope.cancelDisplay = function () {
            $scope.select.displayKey = '';
            $scope.displayPopup.close();
        };



        //确定锁定设置
        $scope.confirmLock = function () {
            //$scope.adterminalIds = 
            var str = String(0) + String(0);
            var lockset = String($scope.select.isSchoolSet) + String($scope.select.isDisplayStyle) + String($scope.select.isDisplayModel) + String($scope.select.isMoreSet) + String($scope.select.isSystemBtn);
            $scope.setLockModel($scope.adterminalIds, lockset);
            $scope.lockPopup.close();
        };
        //取消锁定设置
        $scope.cancelLock = function () {
            $scope.select.isSchoolSet = 0;
            $scope.select.isDisplayStyle = 0;
            $scope.select.isDisplayModel = 0;
            $scope.select.isMoreSet = 0;
            $scope.select.isSystemBtn = 0;
            $scope.lockPopup.close();
        };

        //确定关机
        $scope.confirmShutDown = function () {
         for(var j =0;j< $scope.selectList.length;j++){
             if($scope.selectList[j].stateStr!=='在线'){
                 toaster.warning({
                     body:'只能对在线设备进行关机',
                     timeout:3000
                 });
                 return;
             }
         }
          // console.log($scope.adterminalIds);
            $scope.setEquipmentShutDown($scope.adterminalIds);
            $scope.shutPopup.close();
        };

        //取消关机
        $scope.cancelShutDown = function () {
            $scope.shutPopup.close();
        };

        //根据搜索字样筛选获取筛选数组
        $scope.getFilterList = function () {
            var filterList = [];
            if ($scope.list && $scope.list.length > 0) {
                if (!$scope.select.add_search) {
                    filterList = $scope.list;
                } else {
                    for (var j = 0; j < $scope.list.length; j++) {
                        if (($scope.list[j].schoolSite && $scope.list[j].schoolSite.site_name && $scope.list[j].schoolSite.site_name.indexOf($scope.select.add_search) != -1) || $scope.list[j].sn.indexOf($scope.select.add_search) != -1) {
                            filterList.push($scope.list[j]);
                        }
                    }
                }

            }

            return filterList;

        };

        //根据班级id筛选
        $scope.getFilterListByClassId = function () {
            var filterList = [];
            if ( $scope.recordeList &&  $scope.recordeList.length > 0) {
                if (!$scope.select.class_id) {
                    filterList =  $scope.recordeList;
                } else {
                    //console.log($scope.select.class_id);
                    for (var j = 0; j <  $scope.recordeList.length; j++) {
                        if ( $scope.recordeList[j].classNo &&  $scope.recordeList[j].classNo.className &&  $scope.recordeList[j].classNo.id == $scope.select.class_id) {
                            filterList.push( $scope.recordeList[j]);
                        }
                    }
                }

            }

            return filterList;
        };


        //输入框当改变时
        $scope.onChangeText = function () {
            //console.log('change--');
            $scope.filterList = $scope.getFilterList();
            checkIsExitChecked();
        };


        //关闭发布通知
        $scope.hideNoticenModal = function () {
            $scope.noticeModal.hide();
        };


        //获取学校所有班级
        $scope.getSchoolAllClasses = function () {
            Requester.getSchoolAllClasses($scope.schoolId).then(function (resp) {
                $scope.gradeList = resp.data;

               // console.log(resp);
                if ($scope.gradeList && $scope.gradeList.length > 0) {
                    var screenHeight = window.innerHeight * 0.66;
                    $scope.scrollHeight = ($scope.gradeList.length * 55 + 20) < screenHeight ? $scope.gradeList.length * 55 + 20 + 'px' : screenHeight + 'px';
                    $scope.select.grade_id = $scope.gradeList[0].id;
                    $scope.classList = $scope.gradeList[0].classList;
                }
            }, function (error) {
                toaster.warning({
                    body: error.massage,
                    timeout: 3000
                });
            });
        };

        // $scope.loadData = function(){
        //     $scope.getBanPaiList();
        // };

        /***
         * request 获取设备列表
         */
        $scope.getBanPaiList = function () {
            Requester.getBanPaiList($scope.schoolId).then(function (resp) {
                // console.log(resp);
                // console.log('banpai list');
                $scope.list = resp.data;
                $scope.select.selectNum = 0;
                $scope.recordeList = resp.data;
                if($scope.select.class_id){
                    $scope.list = $scope.getFilterListByClassId();
                }
                if ($scope.list && $scope.list.length > 0) {
                    $scope.allCount = $scope.list.length;
                    UserPreference.setObject('BanPaiList', resp.data);
                }

                if (resp.data&&resp.data.length === 0 && $scope.refresh) {
                    clearInterval($scope.refresh);
                }

            }, function (error) {
                toaster.warning({
                    body: error.massage,
                    timeout: 3000
                });
            }).finally(function () {
              
                $scope.$broadcast('scroll.refreshComplete');
            });
        };

        //设置显示模式
        $scope.setDisplayModel = function (adterminalIds,displayKey) {
            Requester.setDisplayModel(adterminalIds,$scope.select).then(function (resp) {
                if (resp.result) {
                    //console.log(resp);
                    if(displayKey===3){
                        $scope.noticeModal.hide();
                    } 
                    toaster.success({
                        body: '设置成功',
                        timeout: 3000
                    });
                    $scope.select.isLight = false;
                    $scope.getBanPaiList();
                }else{
                    toaster.warning({
                        title: '操作失败',
                        body: resp.message,
                        timeout: 3000
                    }); 
                }
            }, function (error) {
                toaster.warning({
                    title: '操作失败',
                    body: error.massage,
                    timeout: 3000
                });
            });
        };

        //设置锁定模式
        $scope.setLockModel = function (adterminalIds, lockset) {
            Requester.setLockModel(adterminalIds, lockset).then(function (resp) {
                if (resp.result) {
                    //console.log(resp); 
                    toaster.success({
                        body: '设置成功',
                        timeout: 3000
                    });
                    $scope.select.isLight = false;
                    $scope.getBanPaiList();
                }else{
                    toaster.warning({
                        title: '操作失败',
                        body: resp.message,
                        timeout: 3000
                    }); 
                }
            }, function (error) {
                toaster.warning({
                    title: '操作失败',
                    body: error.message,
                    timeout: 3000
                });
            });
        };

        //设置关机
        $scope.setEquipmentShutDown = function (adterminalIds) {
            Requester.setEquipmentShutDown(adterminalIds).then(function (resp) {
                if(resp.result){
                    toaster.success({title: '', body: '关机指令已发送，请耐心等待..',timeout:4000});
                    $scope.select.isLight = false;
                    $scope.getBanPaiList();
                    $scope.refresh = setInterval($scope.getBanPaiList, 10000);
                }else{
                    toaster.warning({
                        title: '操作失败',
                        body: resp.message,
                        timeout: 3000
                    }); 
                }
               
            }, function (error) {
                toaster.warning({
                    title: '操作失败',
                    body: error.message,
                    timeout: 3000
                });
            });
        };

        //文件形式上传图片
        $scope.uploadPictureByFileType = function(file){
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
                Requester.uploadPictureByFileType(file).then(function(resp){
               
                   if(resp.result){
                    $ionicLoading.hide();
                      $scope.select.imageUrl = resp.data.url?resp.data.url:resp.data.link?resp.data.link:resp.data.smallUrl;
                   //   console.log(' $scope.select.imageUrl:'+ $scope.select.imageUrl);
                   }else{
                    $ionicLoading.hide();
                    toaster.warning({
                        title: '上传图片失败',
                        body: resp.message,
                        timeout: 3000
                    }); 
                   }

                }).finally(function(){
                    $ionicLoading.hide();
                });
        };

        $scope.$on('$ionicView.leave', function () {
            clearInterval($scope.refresh);
        });
        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function () {
            $scope.noticeModal.remove();
            clearInterval($scope.refresh);
        });
        // Execute action on hide modal
        $scope.$on('modal.hidden', function () {
            // Execute action
        });
        // Execute action on remove modal
        $scope.$on('modal.removed', function () {
            // Execute action
        });

        $scope.getDeviceType = function () {
            var result = 'android';
            $scope.isIos = ionic.Platform.isIOS();
            if ($scope.isIos) {
                if (window.screen.width >= 375 && window.screen.height >= 812) {
                    result = 'iphoneX';
                } else {
                    result = 'iphone';
                }

            } else {
                result = 'android';
            }
            return result;
        };
>>>>>>> wl_admin_1.3.1
    });
