angular.module('app.controllers')
.controller('terminalListCtrl', function ($scope, Constant, UserPreference, $state, $ionicHistory, Requester) {

    $scope.$on("$ionicView.loaded", function (event, data) {
        $scope.selected = {
            area: UserPreference.getObject('user').area.id
        };
        console.log($scope.selected);
        $scope.userRole = UserPreference.getObject('user').role;
        if (String($scope.userRole) === Constant.USER_ROLES.PROVINCE_ADMIN) {
            $scope.cityChoosable = true;
            $scope.areaChoosable = true;
            Requester.getArea($scope.selected.area).then(function (resp) {
                $scope.cities = resp;
            
            });
        } else if (String($scope.userRole) === Constant.USER_ROLES.CITY_ADMIN) {
            $scope.cityChoosable = false;
            $scope.areaChoosable = true;
            Requester.getArea($scope.selected.area).then(function (resp) {
                $scope.areas = resp;
            });
        } else {
            $scope.cityChoosable = false;
            $scope.areaChoosable = false;
            if (String($scope.userRole) === Constant.USER_ROLES.SCHOOL_ADMIN) {
                $scope.schoolAdmin = true;
                $scope.selected.school = UserPreference.getObject('user').school.id;
                $scope.onSchoolSelected(true);
            } else if (String($scope.userRole) === Constant.USER_ROLES.DISTRICT_ADMIN) {
                $scope.schools = UserPreference.getObject('user').schools;
            } else {
                Requester.getSchools($scope.selected.area).then(function (resp) {
                    $scope.schools = resp.data?resp.data.content:[];
                });
            }
        }
    });

    $scope.onSchoolSelected = function (reset) {
        if ($scope.selected.school) {
            if (reset) {
                $scope.page = 1;
                $scope.list = [];
            }
            else if ($scope.listHasMore)
                $scope.page++;
            else
                return;
            $scope.loading = true;
            
            console.log($scope.selected);
            Requester.getTerminalList($scope.page, $scope.selected.school).then(function (resp) {
                if ($scope.list.length === 0)
                    $scope.list = resp.data.content;
                   
                else {
                    Array.prototype.push.apply($scope.list, resp.data.content);
                }
                $scope.listHasMore = !resp.data.last;
            }).finally(function () {
                console.log($scope.list);
                console.log('--list==');
                $scope.loading = false;
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        }
        else
            $scope.$broadcast('scroll.refreshComplete');
    };

    $scope.goDetail = function (id) {
        $state.go('terminalUsageInfo', {id: id});
    };

    $scope.onSelectionChange = function (isCity) {
        if ($scope.cityChoosable && !$scope.selected.city) {
            $scope.selected.area = UserPreference.getObject('user').area.id;
            $scope.areas = [];
            $scope.schools = [];
        }
        else {
            if (isCity) {
                Requester.getArea($scope.selected.city).then(function (resp) {
                    $scope.areas = resp;
                    $scope.schools = [];
                });
            } else if ($scope.selected.area) {
                Requester.getSchools($scope.selected.area).then(function (resp) {
                    $scope.schools = resp.data.content;
                });
            }
        }
        $scope.list = [];
    };

    $scope.goBack = function () {
        if ($ionicHistory.backView())
            $ionicHistory.goBack();
        else
            $state.go('tabsController.mainPage');
    };

});