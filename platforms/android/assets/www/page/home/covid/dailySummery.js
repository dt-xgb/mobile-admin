angular.module('app.controllers')
    .controller('covidDailySummeryCtrl', function ($scope, Constant, $state, Requester, toaster, UserPreference) {

        $scope.$on("$ionicView.loaded", function (event, data) {
            $scope.selectedStatus = '';
            $scope.activeTab = 'all';
            $scope.warningList = [];
        });

        function setProcess(val){
            var process =  document.querySelector("#circle-process");
            var circleLength = Math.floor(2 * Math.PI * 50);
            val = Math.max(0,val);
            val = Math.min(100,val);
            process.setAttribute("stroke-dasharray", "" + circleLength * val + ",10000");
        }

        $scope.$on("$ionicView.enter", function (event, data) {
            setProcess(0);
            $scope.loadData();
        });

        $scope.loadData = function() {
            Requester.getCovidSummary().then(function(res){
                $scope.summary = res.data;
                if($scope.summary.abnormalNum && $scope.summary.abnormalNum != 0)
                    setProcess($scope.summary.normalNum/$scope.summary.abnormalNum);
                    else
                    setProcess(0);
                $scope.warningCounts = [];
                for(var i=0;i<$scope.summary.abnormalNumList.length;i++){
                    var data = $scope.summary.abnormalNumList[i].split('#');
                    $scope.warningCounts.push({
                        day: data[0],
                        count: data[1]
                    });
                }
            });
            Requester.getWarningList().then(function(res){
                if(res.data && res.data.content) {
                    $scope.warningList = res.data.content;
                    Requester.covidWarningRead();
                }
                else
                    $scope.warningList = [];
            });
        };

        $scope.changeFilter = function (arg) {
            if ($scope.activeTab === arg) {
                return;
            }
            $scope.activeTab = arg;
            if (arg === 'clear') {
                $scope.selectedStatus = '2';
            } else if (arg === 'pending') {
                $scope.selectedStatus = '1';
            } else {
                $scope.selectedStatus = '';
            }
        };


        $scope.goFunction = function (where, args) {
            if (where) {
                if (args)
                    $state.go(where, {id: args});
                else
                    $state.go(where);
            }
        };
    });