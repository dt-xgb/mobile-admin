angular.module('app.controllers')
    .controller('cloundLiveListCtrl', function ($scope, Constant, $state, $ionicModal, Requester, toaster, UserPreference, $ionicPopup, BroadCast, CameraHelper, $timeout, ionicDatePicker, $ionicLoading) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            
            var myDate = new Date();
            $scope.currentDate = myDate.getFullYear()+"-"+(myDate.getMonth()+1)+"-"+myDate.getDate(); 
            $scope.select = {
                createDate: $scope.currentDate
            };
            console.log('before enter');
            $scope.page = 1;
            $scope.getCloundLiveList();
           
         });

        // $scope.$on("$ionicView.enter", function (event, data) {
            
        //  });
        $scope.$on("$ionicView.loaded", function (event, data) {
           // $scope.list = [{ status: 1 }, { status: 2 }, { status: 3 }];
            // var myDate = new Date(); //Thu Dec 28 2017 10:</span>07:39 GMT+0800 (中国标准时间)
            // $scope.currentDate = myDate.getFullYear()+"-"+(myDate.getMonth()+1)+"-"+myDate.getDate(); 
           
            // $scope.select = {
            //     createDate: $scope.currentDate
            // };
           
        });

        //选择日期
        $scope.selectDate = function () {
            setTimeout(function () {
                if (window.cordova) cordova.plugins.Keyboard.close();
                var ipObj1 = {
                    callback: function (val) {
                        if (typeof (val) !== 'undefined') {
                            $scope.select.createDate = formatTimeWithoutSecends(val / 1000).substr(0, 10);
                            $scope.page = 1;
                            $scope.getCloundLiveList();
                            // console.log( $scope.select.createDate );
                        }
                    }
                };
                ionicDatePicker.openDatePicker(ipObj1);
            }, 100);
        };

        //添加直播
        $scope.addLive = function () {
            $state.go('addCloundLive');
        };

        //查看详情
        $scope.goToDetail = function (item) {
          
            $state.go('cloundLiveDetail',{
                cloundLiveId:item.id,
                status:item.status,
                mobLiveUrl:item.mobLiveUrl,
                mobLookbackUrl:item.mobLookbackUrl
            });
        };


        //下拉刷新
        $scope.refreshData = function () {
            //$scope.isMoreData = true;
            $scope.page = 1;
            $scope.getCloundLiveList();
        };
        //上拉加载
        $scope.loadMore = function () {
            $scope.page++;
            $scope.getMoreList();
            //$scope.$broadcast('scroll.infiniteScrollComplete');
        };

        //获取列表
        $scope.getCloundLiveList = function () {
            Requester.getCloundLiveList($scope.select.createDate, $scope.page).then(function (rest) {
                if (rest.result) {
                    $scope.list = rest.data.content;
                   // $scope.isMoreData = true;
                } else {
                    $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).finally(function () {
                //$scope.isMoreData = true;
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');

            });
        };
        //加载更多
        $scope.getMoreList = function(){
            Requester.getCloundLiveList($scope.select.createDate, $scope.page).then(function (rest) {
                if (rest.result) {
                    var data = rest.data.content;
                    for (var i = 0; i < data.length; i++) {
                        $scope.list.push(data[i]);
                    }
                    if (!data || (data && data.length < Constant.reqLimit)) {
                        $scope.isMoreData = true;
                    }

                } else {
                    $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).finally(function () {
                $scope.isMoreData = true;
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');

            });
        };
        $scope.getDeviceType = function () {
            var result = 'android';
            $scope.isIos = ionic.Platform.isIOS() ;
            if ($scope.isIos) {
                if (window.screen.width >= 375 && window.screen.height >= 812) {
                    result = 'iphoneX';
                } else {
                    result = 'iphone';
                }
      
            } else {
             result = 'android';
            }
            return result;
        };


    });