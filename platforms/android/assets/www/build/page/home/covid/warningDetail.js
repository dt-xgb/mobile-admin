angular.module('app.controllers')
    .controller('covidWarningDetailCtrl', ['$scope', 'Constant', '$ionicPopup', 'Requester', 'toaster', '$stateParams', function ($scope, Constant, $ionicPopup, Requester, toaster, $stateParams) {


        window.addEventListener('native.keyboardshow', function (keyboardParameters) {
           // console.log('keyboard open warning');
            var tObj = document.getElementById("desc");
            var sPos = tObj.value.length;
            setTimeout(function(){
                if(ionic.Platform.isIOS())
                setCaretPosition(tObj, sPos);
            },100);
           
        });

        // $scope.blur = function(){
        //     var tObj = document.getElementById("desc");
        //     var sPos = tObj.value.length;
        //     setCaretPosition(tObj, sPos);
        // };
        $scope.$on("$ionicView.enter", function (event, data) {
            $scope.warning = $stateParams.id;
            $scope.events = [];
            Requester.getWarningDetail($scope.warning.id).then(function(res){
                $scope.events = res.data;
            });
            $scope.selectedStatus = '1';
            $scope.activeTab = 'pending';
            $scope.formData = {
                desc: ''
            };
        });

        $scope.changeStatus = function (arg) {
            if ($scope.activeTab === arg) {
                return;
            }
            $scope.activeTab = arg;
            if (arg === 'clear') {
                $scope.selectedStatus = '2';
            } else {
                $scope.selectedStatus = '1';
            }
        };

        var UIPath = '';
        if (Constant.debugMode) UIPath = 'page/';
        $scope.openModal = function(){
            $scope.selectModel = $ionicPopup.show({
                templateUrl: UIPath + 'home/covid/handleWarning.html',
                scope: $scope,
                cssClass: 'approval_alert'
            });
        };

        $scope.cancelChoose = function(){
            $scope.selectModel.close();
        };

        $scope.submit = function(){
            Requester.handleWarning($scope.warning.id, $scope.selectedStatus, $scope.formData.desc).then(function(res){
                if(res.result) {
                    toaster.success({title: '跟进成功!', body: '',timeout:'3000'});
                    $scope.cancelChoose();
                    Requester.getWarningDetail($scope.warning.id).then(function(res){
                        $scope.events = res.data;
                    });
                }else{
                    toaster.warning({title: res.message, body: '',timeout:'3000'}); 
                    $scope.selectModel.close();
                }
            });
        };
    }]);