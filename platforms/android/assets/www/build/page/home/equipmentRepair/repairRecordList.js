angular.module('app.controllers')
    .controller('repairRecordListCtrl', ['$scope', '$ionicModal', 'Constant', '$q', 'CameraHelper', '$timeout', '$state', 'Requester', 'UserPreference', '$ionicLoading', 'toaster', 'BroadCast', function ($scope, $ionicModal, Constant, $q, CameraHelper, $timeout, $state, Requester, UserPreference, $ionicLoading, toaster, BroadCast) {

        $scope.$on("$ionicView.enter", function (event, data) {
           // console.log('enter');
            $scope.page = 1; //当前页面
            $scope.repairList = $scope.selectType == 1 ? UserPreference.getArray('WaitFix') ? UserPreference.getArray('WaitFix') : [] : UserPreference.getArray('Closed') ? UserPreference.getArray('Closed') : [];
            $scope.isMoreData = true;
            $scope.repairImgs = [];
            // $scope.repairData = {unclosedSize:0,closedSize:0};
            $scope.selected = {
                terminalAddress: '',
                orderDescribe: '',
                maintainImgs: [],
                assetNumber:''
            };
            $scope.getRepairRecordList($scope.selectType);

        });
        $scope.$on("$ionicView.loaded", function (event, data) {
            $scope.selectType = 1;
        });

        $scope.$on(BroadCast.CONNECT_ERROR, function () {
            $scope.isLoading = false;
            $scope.isMoreData = false;
            $ionicLoading.hide();
        });
        //选择类型
        $scope.selectTypeClick = function (type) {
            $scope.selectType = type;
            $scope.repairList = type == 1 ? UserPreference.getArray('WaitFix') : UserPreference.getArray('Closed');
            $scope.page = 1;
            $scope.isMoreData = true;
            $scope.getRepairRecordList(type);
        };

        //获取报修记录列表
        $scope.getRepairRecordList = function (condition) {
            Requester.getRepairRecordList(condition, $scope.page).then(function (res) {
                if (res.result) {
                    $scope.repairList = res.data.data.content;
                    $scope.repairData = res.data;
                    // UserPreference.setObject('RepairListData',$scope.repairData);
                    if (condition == 1) {
                        UserPreference.setObject('WaitFix', $scope.repairList);
                    } else {
                        UserPreference.setObject('Closed', $scope.repairList);
                    }

                } else {
                    $scope.isMoreData = false;
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };
        //下拉刷新
        $scope.refreshData = function () {
            $scope.page = 0;
            $scope.getRepairRecordList($scope.selectType);
        };

        //上拉加载
        $scope.loadMore = function () {
            $scope.page++;
            Requester.getRepairRecordList($scope.selectType, $scope.page).then(function (res) {
                if (res.result) {
                    res.data.data.content.forEach(function (item) {
                        $scope.repairList.push(item);
                    });
                    //关闭刷新
                    if (!res.data.data.content || res.data.data.content.length < Constant.reqLimit) {
                        $scope.isMoreData = false;
                    }
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        //查看详情
        $scope.goToDetail = function (repairRecordId, readStatus) {

            $state.go('repairDetail', {
                repairRecordId: repairRecordId,
                readStatus: readStatus
            });
        };

        //*** modal */
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'page/';
        $ionicModal.fromTemplateUrl(UIPath + 'home/equipmentRepair/postRepairInfo.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
        });

        $scope.openModal = function () {
            $scope.isLoading = false;
            $scope.requestCount = 0;
            $scope.modal.show();
        };

        $scope.closeModal = function () {
            $scope.modal.hide();
        };

        $scope.selectImg = function () {
            //if(!isWeixin())
            if (window.cordova) {
                CameraHelper.selectMultiImage(function (resp) {
                    console.log(resp);
                    if (!resp)
                        return;
                    if (resp instanceof Array) {

                        Array.prototype.push.apply($scope.repairImgs, resp);
                        $scope.$apply();
                    } else {
                        $scope.repairImgs.push("data:image/jpeg;base64," + resp);
                    }
                }, 9 - $scope.repairImgs.length);
            } else {
                //web 端
                var input = document.getElementById("capture");
                input.click();
            }

        };

        // input形式打开系统系统相册
        $scope.getFile = function (files) {
            var file = files[0];
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (theFile) {
                $timeout(function () {
                    $scope.testImg = theFile.target.result; //设置一个中间值
                    $scope.repairImgs.push($scope.testImg);

                }, 100);
            };
        };

        $scope.removeImg = function (index, $event) {
            $event.stopPropagation();
            $scope.repairImgs.splice(index, 1);
        };

        function onImageResized(resp) {
           
            $scope.selected.maintainImgs.push(resp);
        }

        //立即提交
        $scope.commitRepair = function () {

            var result = checkStringIsIncludeSpecialChar($scope.selected.assetNumber);
            if(!result||($scope.selected.assetNumber&&$scope.selected.assetNumber.length > 30)){
                toaster.warning({
                    title: "提交失败",
                    body: '资产ID不存在',
                    timeout: 3000
                });
             return;
            }
            if (isEmojiCharacter($scope.selected.orderDescribe)) {
                $ionicLoading.show({
                    template: '请不要输入表情字符',
                    duration: 1500
                });
                return;
            }
            $scope.selected.maintainImgs = [];
            var promiseArr = [];
            $scope.repairImgs.forEach(function (img) {
                promiseArr.push(resizeImage(Constant.CAPTURE_IMAGE_RANGE, img).then(onImageResized));
            });
            $scope.isLoading = true;
            //$scope.requestCount++;
            console.log($scope.selected) ;
            $q.all(promiseArr).then(function () {
                // if ($scope.requestCount === 1) {
                    $ionicLoading.show({
                        noBackdrop: true,
                        template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
                    });
                    Requester.addEquipmentRepairRecord($scope.selected).then(function (res) {
                        if (res.result) {
                            toaster.success({
                                title: "提交成功",
                                body: "成功报修，请耐心等待管理员处理",
                                timeout: 3000
                            });

                            $scope.isShow = true;
                            $scope.isLoading = false;
                            $ionicLoading.hide();
                            $scope.closeModal();
                            $scope.repairImgs = [];
                            $scope.selected = {
                                terminalAddress: '',
                                orderDescribe: '',
                                maintainImgs: [],
                                assetNumber:''
                            };

                            return res;
                        }else{
                            $ionicLoading.hide();
                            $scope.isLoading = false;
                            $scope.requestCount = 0;
                          //  $scope.selected =  $scope.selected;
                            toaster.warning({
                                title: "提交失败",
                                body: res.message,
                                timeout: 3000
                            }); 
                        }
                    }).then(function (res) {
                        if (res.result) {
                            //$scope.updateRepairRecordStatus(res.data);
                            $scope.page = 1;
                            $scope.getRepairRecordList($scope.selectType);

                        }
                    }).finally(function(){
                        setTimeout(function(){
                            $scope.isLoading = false;
                        },5000);
                    });
               // }

            });


        };

        $scope.updateRepairRecordStatus = function (repairRecordId) {
            Requester.addRepairProgress(repairRecordId, 2, '').then(function (res) {
                $scope.page = 1;
                $scope.getRepairRecordList($scope.selectType);
            });
        };

        //扫码
        $scope.scanningView = function(event){
            $scope.isIos = ionic.Platform.isIOS();
           if( $scope.isIos){
            $scope.getAssetsIdByScanning();
           }else{
            var permissions = cordova.plugins.permissions;
            permissions.hasPermission(permissions.CAMERA, function (status) {
                if (!status.hasPermission) {
                    var errorCallback = function () {};
                    permissions.requestPermission(
                        permissions.CAMERA,
                        function (status) {
                            if (!status.hasPermission) {
                                errorCallback();
                            } else {
                                $scope.getAssetsIdByScanning();
                            }
                        }, errorCallback);
                } else
                    $scope.getAssetsIdByScanning();
            }, null);
           }
           
        };


        $scope.getAssetsIdByScanning = function () {
            window.scanning.startScanning(function (result) {
                $scope.selected.assetNumber = '';
                Requester.getAssetsIdByScan(result).then(function (res) {
                    if (res.result) {
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.selected.assetNumber = res.data;
                            });

                        }, 200);
                    } else {
                        toaster.warning({
                            title: "获取失败",
                            body: res.message,
                            timeout: 3000
                        });
                    }

                });

            }, function (error) {
                // alert('error');
            }, {});
        };

    }]);