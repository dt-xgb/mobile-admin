angular.module('app.controllers')
  .controller('classAppraisalListCtrl', ['$scope', '$ionicPopup', '$ionicListDelegate', '$state', 'ionicDatePicker', 'toaster', '$ionicLoading', 'Requester', 'Constant', 'UserPreference', '$ionicScrollDelegate', 'BroadCast', function ($scope, $ionicPopup, $ionicListDelegate, $state, ionicDatePicker, toaster, $ionicLoading, Requester, Constant, UserPreference, $ionicScrollDelegate,BroadCast) {

    window.addEventListener('native.keyboardshow', function (keyboardParameters) {
     
      var tObj = document.getElementById("apprailId");
      var sPos = tObj.value.length;
      setTimeout(function(){
        setCaretPosition(tObj, sPos);
    },100);
  });
    //进入视图时
    $scope.$on("$ionicView.enter", function (event, data) {
      //  初始化默认选择时间 
      var time = formatTimeWithoutSecends(new Date().getTime() / 1000, "yyyy-MM-dd ");
      $scope.selected = {
        title: '',
        startDate: '',
        endDate: ''
      };
      $scope.page = 1; //当前页面
      $scope.isMoreData = true;
      $scope.voteList = [];
      var array = UserPreference.getArray('VoteTaskList')?UserPreference.getArray('VoteTaskList'):[];
      $scope.voteList = array;
      $scope.getClassVoteTasktList();


    });

    $scope.$on(BroadCast.CONNECT_ERROR, function () {
     
      $scope.isMoreData = false;
       $ionicLoading.hide();
  });

    //获取评比任务列表
    $scope.getClassVoteTasktList = function () {
      Requester.getClassVoteTasktList2($scope.page).then(function (res) {

        if (res.result) {
          $scope.arr = [];
          $scope.arr = res.data.data.content;
          $scope.startDate  = res.data.startDate.substr(0,10);
          $scope.endDate = res.data.endDate.substr(0,10);
          if ($scope.page && $scope.page > 1) {
            $scope.arr.forEach(function (item) {
              $scope.voteList.push(item);
            });
          } else {
            $scope.voteList = $scope.arr;
            UserPreference.setObject('VoteTaskList', $scope.voteList);
          }
         
          if ($scope.arr.length < Constant.reqLimit && $scope.page > 1) {

            $scope.isMoreData = false;
          }
          //获取成功 存取

        } else {
          $scope.isMoreData = false;
        }

      }, function (error) {
        $ionicLoading.show({
          template: error,
          duration: 1500
        });

      }).finally(function () {
        $scope.isMoreData = false;
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');

      });
    };
    //添加班级评比任务
    $scope.addClassVoteTask = function () {
      Requester.addClassVoteTask($scope.selected).then(function (res) {
        if (res.result) {
          $scope.myPopup.close();
          $scope.page = 1;
          $scope.getClassVoteTasktList();
         
        } else {
          
          $ionicLoading.show({
            template: res.message,
            duration: 1500
          });
          return;
        }

      }, function (error) {
        toaster.warning({
          title: "温馨提示",
          body: error.data.message
        });
      });
    };
    //下拉刷新
    $scope.refreshData = function () {
      $scope.voteList = [];
      $scope.isMoreData = true;
      $scope.page = 1;
      $scope.$broadcast('scroll.refreshComplete');
      $scope.getClassVoteTasktList();
    };

    //上拉加载
    $scope.loadMore = function () {
      // $scope.voteList = [];
      $scope.page++;
      $scope.getClassVoteTasktList();

    };

    //弹出提示框
    var UIPath = '';
    if (Constant.debugMode) UIPath = 'page/';
    $scope.openModal = function () {
      // if(!$scope.data)
      // $scope.data = {};
       $scope.myPopup = $ionicPopup.show({
        // template: '<div   style="font-size:15px;line-height:36px;min-height:36px;"><div style="width:67px;margin-top:8px;padding-right:0px;">评比时段:</div> <div ng-click = "selectStartOrEndDate(0)" ng-style="{\'color\':selected.startDate==\'开始时间\'?\'#999999\':\'#333\'}" style="margin-left:5px; border:1px solid #CFCFCF;font-size:14px;width:32%;line-height:32px;height:32px;position:absolute;left:78px;top:64px;border-radius:5px;">&nbsp;{{selected.startDate}}</div><div style="position:absolute;left:calc(32% + 78px);top:60px;width:32%;">&nbsp;至&nbsp;</div><div  ng-style="{\'color\':selected.endDate==\'结束时间\'?\'#999999\':\'#333\'}" style="border:1px solid #CFCFCF;font-size:14px;line-height:32px;height:32px;width:30%;position:absolute;left:calc(32% + 98px);top:64px;border-radius:5px;"  ng-click = "selectStartOrEndDate(1)">&nbsp;{{selected.endDate}}</div></div>' +
        //   '<div style="margin-top:15px;"><label class="item-input" style="padding-left:0px !important;margin-top:19px;"> <div style="width:67px !important;min-width:67px;text-align:right;font-size:15px;padding-right:0px;">标题:</div><input type="text"  style="border:1px solid #CFCFCF;padding-left:2px;padding-right:5px;height:36px;position:reletive;margin-left:6px;font-size:14px;border-radius:5px;" placeholder="请输入评比标题" ng-model="selected.title"></label></div>',
          templateUrl:UIPath + 'home/classAppraisal/addClassAppraisalTask.html',
          title: '<div> <img src="img/classAppraisal/left.png" /><span>添加班级评比任务</span><img src="img/classAppraisal/right.png" /></div>',
        scope: $scope,
        cssClass: 'dateSelectAlert',
        buttons: [{
            text: '取消',
            type: 'button-cancel'
          },
          {
            text: '<b>确定</b>',
            type: 'button-xgreen',
            onTap: function (e) {
        
              var time1 = $scope.selected.startDate.replace(/\-/g, "/");
              var timestamp1 = Date.parse(new Date(time1)) / 1000;
              var time2 = $scope.selected.endDate.replace(/\-/g, "/");
              var timestamp2 = Date.parse(new Date(time2)) / 1000;
              
              if (!$scope.selected.startDate) {
                $ionicLoading.show({
                  template: '请先选择开始时间',
                  duration: 1500
                });
                e.preventDefault();
                return;
              }
              if (!$scope.selected.endDate) {
                $ionicLoading.show({
                  template: '请选择结束时间',
                  duration: 1500
                });
                e.preventDefault();
                return;
              }
              if (!$scope.selected.title) {
                $ionicLoading.show({
                  template: '请输入标题',
                  duration: 1500
                });
                e.preventDefault();
                return;
              }
                if(timestamp1>timestamp2){
                $ionicLoading.show({
                  template: '结束时间不能小于开始时间',
                  duration: 1500
                });
                e.preventDefault();
                return;
              }
              if ($scope.selected.title.length > 30) {
                $ionicLoading.show({
                  template: '请将标题控制在30字以内',
                  duration: 1500
                });
                e.preventDefault();
                return;
              }

            
              $scope.addClassVoteTask();
              e.preventDefault();
              return e;
            }
          }
        ]
      });

      $scope.myPopup.then(function (res) {

     
      });
    };
     
   
    //侧滑删除
    $scope.deleteItem = function (item, $event) {
      $event.stopPropagation();
      var confirmPopup = $ionicPopup.confirm({
        title: '删除确认',
        template: '确认删除 ' + item.title + ' ?',
        cancelText: '取消',
        okText: '确认',
        okType: 'button-balanced'
      });
      confirmPopup.then(function (res) {
        if (res) {
          Requester.deleteClassVoteTask(item.id).then(function (resp) {
            if (resp.result) {
              //刷新
              console.log('delete succeed');
              // $scope.voteList.splice(item,1);
              $scope.page = 1;
              $scope.getClassVoteTasktList();
              $ionicScrollDelegate.$getByHandle('delegateHandler').resize();
             
            }
          });
          $ionicListDelegate.closeOptionButtons();
        }
      });
    };

    //
    $scope.selectStartOrEndDate = function (arg) {
     
      var ipObj1 = {
        callback: function (val) {
          if (typeof (val) !== 'undefined') {
            if (arg == 0) {
              $scope.selected.startDate = formatTimeWithoutSecends(val / 1000).substr(0, 10);
            } else {

              $scope.selected.endDate = formatTimeWithoutSecends(val / 1000).substr(0, 10);
            }

            if ($scope.selected.startDate && $scope.selected.endDate ) {
              $scope.selected.title = $scope.selected.startDate.substr(5, 5) + '至' + $scope.selected.endDate.substr(5, 5) + '班级评比';
            }
          }
        }
      };
      ionicDatePicker.openDatePicker(ipObj1);
    };


    //查看详情
    $scope.goToDetail = function (voteTaskId, title) {

      $state.go('classAppraisalDetail', {
        voteTaskId: voteTaskId,
        title: title
      });
    };
    
   
  }]);