angular.module('app.controllers')
    .controller('assetsStatisticCtrl', ['$scope', 'Constant', '$state', '$ionicModal', 'Requester', 'toaster', 'UserPreference', '$ionicPopup', function ($scope, Constant, $state, $ionicModal, Requester, toaster, UserPreference,$ionicPopup) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {

            $scope.select = {
                selectIndex1: 0
            };
            $scope.detail = UserPreference.getObject('SchoolAssetsDetail');
            $scope.getSchoolAssetsDetail();
            $scope.emptyContent = false;
            $scope.data = {
                y: [],
                x: [],
                unit: '%' //$scope.unit
            };
            var user = UserPreference.getObject('user');
            $scope.authDeviceManage = user.authDeviceManage && user.authDeviceManage === 1; //设备授权
            $scope.authAssertManage = user.authAssertManage && user.authAssertManage === 1; //资产授权
            if( !$scope.authDeviceManage&&$scope.authAssertManage){
                $scope.checkAuthOrAccountIsOverdue();
            }
        });

        $scope.$on("$ionicView.loaded", function (event, data) {

        });
        $scope.tab1Click = function (index) {
            $scope.select.selectIndex1 = index;
            $scope.data = {
                y: [],
                x: [],
                unit: '' //$scope.unit
            };
            if (index === 0) {
                $scope.getSchoolAssetsRank();
            } else {
                $scope.getFactoryAssetsRank();
            }
        };

        $scope.makeChart = function (y, x) {
            $scope.chartHeight = y.length * 40 + 25;
            $scope.data = {
                y: y,
                x: x,
                unit: '' //$scope.unit
            };
        };

        //request --资产概况
        $scope.getSchoolAssetsDetail = function () {
            Requester.getSchoolAssetsDetail().then(function (rest) {
                if (rest.result) {
                    $scope.detail = rest.data;
                    UserPreference.setObject('SchoolAssetsDetail', rest.data);
                } else {
                    // $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).then(function () {
                $scope.getSchoolAssetsRank();
            });
        };

        //学校资产排行
        $scope.getSchoolAssetsRank = function () {
            Requester.getSchoolAssetsRank().then(function (rest) {
                if (rest.result) {
                    //rest.data = [{name:'阿里一中1',count:240},{name:'阿里一中2',count:180},{name:'阿里一中3',count:60}];
                    if (rest.data && rest.data.length > 0) {
                        $scope.emptyContent = false;
                        var x = [],
                            y = [];
                        var dataArr = rest.data.length>10?rest.data.slice(0,10):rest.data;
                        for (var i = dataArr.length-1; i >=0; i--) {
                           
                            var sd = dataArr[i];
                            y.push(i + 1 + '.' + sd.name);
                            var rate = sd.count;
                           x.push(rate);
                        }
                        $scope.makeChart(y, x);
                    }else{
                        $scope.emptyContent = true;  
                    }
                } else {
                    // $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };

        //厂家资产排行
        $scope.getFactoryAssetsRank = function () {
            Requester.getFactoryAssetsRank().then(function (rest) {
                if (rest.result) {
                   // rest.data = [{name:'阿里一中32',count:20},{name:'阿里一中21',count:18},{name:'阿里一中11',count:15}];
                    if (rest.data && rest.data.length > 0) {
                        $scope.emptyContent = false;
                        var x = [],
                            y = [];
                        var dataArr = rest.data.length>10?rest.data.slice(0,10):rest.data;
                        for (var i = dataArr.length-1; i >=0; i--) {
                            var sd = dataArr[i];
                            y.push(i + 1 + '.' + sd.name);
                            var rate = sd.count;
                           x.push(rate);
                        }
                        $scope.makeChart(y, x);
                    }else{
                        $scope.emptyContent = true;  
                    }
                } else {
                    // $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };

        $scope.checkAuthOrAccountIsOverdue = function () {
            var beforeTamp = UserPreference.get('AreaAssetsAuthAlertDateTamp');
            var currentDate = getNowDate();
            var currentTamp = Date.parse(currentDate.replace(/\-/g, "/")) / 1000;
            var interval = 24 * 60 * 60;
            var flag = currentTamp - beforeTamp >= interval;  
            if (!beforeTamp || beforeTamp==='undefined'||(beforeTamp && flag)) {
                Requester.checkAuthOrAccountIsOverdue().then(function (resp) {
                    if (resp.result) {
                        $scope.authList = resp.data;
                        UserPreference.set('AreaAssetsAuthAlertDateTamp', currentTamp);
                        // console.log('flag:'+flag+'--beforeTamp :'+beforeTamp +'current:'+ currentTamp );
                        if(resp.data){
                            $scope.authAlert = $ionicPopup.show({
                                template: '<div style="color:#666;font-size:15px;padding:10px;width:100%;"><div ng-repeat="item in authList">{{item.authModeName}}功能权限将于<span style="color:#2bcab2;font-weight:bold;">{{item.expireDate.substr(0,10)}}</span>过期</div> <div>过期后对应模块在APP及网页端均不能在使用，续期请联系<span style="color:#2bcab2;font-weight:bold;">QQ客服(1833025389)</span>或小跟班业务员</div><div class="button" ng-click="closeAuthAlert()" style="height:30px;width:calc(100% - 80px);margin-left:40px;margin-top:15px;background:#2bcab2;color:white;">知道了</div></div>',
                                // templateUrl: UIPath + 'common/areaAuthalert.html',
                                title: '<div style="color:white !important;">授权即将到期提醒</div><div class="logo" ><img src="img/icon/logo.png" /></div>',
                                scope: $scope,
                                cssClass: 'auth_alert'
                            }); 
                        }
                                  
                    } else {
                        console.log('未过期');
                    }
                });
            }
           
        };

        //关闭授权提示框
        $scope.closeAuthAlert = function () {
            //UserPreference.set('AreaAuthAlertRead', 'read');
            $scope.authAlert.close();
        };

    }]);