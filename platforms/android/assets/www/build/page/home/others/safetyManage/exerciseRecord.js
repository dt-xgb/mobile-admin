angular.module('app.controllers')
    .controller('exerciseRecordCtrl', ['$scope', 'Constant', '$state', 'CameraHelper', '$ionicModal', 'BroadCast', 'Requester', '$stateParams', '$q', 'toaster', '$ionicLoading', 'ionicDatePicker', 'ionicTimePicker', function ($scope, Constant, $state, CameraHelper,$ionicModal, BroadCast, Requester, $stateParams, $q, toaster, $ionicLoading, ionicDatePicker, ionicTimePicker) {
        $scope.$on("$ionicView.beforeEnter", function (event, data) {

            // $scope.title = $stateParams.title;
            //    $scope.getDrillRedordDetail(); 

        });

        $scope.$on("$ionicView.loaded", function (event, data) {

            $scope.startTime = String($stateParams.startTime);
            $scope.endTime = $stateParams.endTime;
            $scope.recordStartTime = $scope.startTime; //用于记录修改前的开始时间
            $scope.recordEndTime = $scope.endTime;
            $scope.intervalTime = $scope.getTimeDistant($scope.startTime, $scope.endTime);
            // console.log('start:'+startTime);
            $scope.selected = {
                remarks: '',
                picdatas: []
            };

            $scope.drillId = $stateParams.itemId; //演练id

            $scope.reportImgs = [];

            $scope.page = 1;

            $scope.getDrillRedordDetail();
        });

        //添加记录
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'page/';
        $ionicModal.fromTemplateUrl(UIPath + 'home/others/safetyManage/addExerciseRecord.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.recordModal = modal;
        });
        $scope.addRecordModal = function () {
            $scope.recordModal.show();
        };

        $scope.hideModal = function () {
            $scope.recordModal.hide();
        };


        //选择图片
        $scope.selectImg = function () {
            if (window.cordova) {
                CameraHelper.selectMultiImage(function (resp) {
                    if (!resp)
                        return;
                    if (resp instanceof Array) {
                        Array.prototype.push.apply($scope.reportImgs, resp);
                        $scope.$apply();
                    } else {
                        $scope.reportImgs.push(
                            "data:image/jpeg;base64," + resp
                        );
                    }
                }, 9 - $scope.reportImgs.length);
            } else {
                //web 端
                var input = document.getElementById("capture");
                input.click();
            }
        };
        // input形式打开系统系统相册
        $scope.getFile = function (files) {
            var file = files[0];
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (theFile) {
                setTimeout(function () {
                    $scope.testImg = theFile.target.result; //设置一个中间值
                    $scope.reportImgs.push($scope.testImg);

                }, 100);
            };
        };

        $scope.removeImg = function (index, $event) {
            $event.stopPropagation();
            $scope.reportImgs.splice(index, 1);
        };
        //选择日期
        $scope.selectTime = function (arg) {
            var ipObj1 = {
                callback: function (val) {
                    if (typeof (val) !== 'undefined') {
                        var nowTime = '';

                        if (arg === 'start') {
                            nowTime = formatTimeWithoutSecends(val / 1000).substr(0, 10);
                        } else if (arg === 'end') {
                            nowTime = formatTimeWithoutSecends(val / 1000).substr(0, 10);
                        }
                        $scope.showTimePicker(arg, nowTime);
                    }
                }
            };
            ionicDatePicker.openDatePicker(ipObj1);
        };

        $scope.showTimePicker = function (arg, nowTime) {
            var ipObj1 = {
                inputTime: (((new Date()).getHours() * 60 * 60) + ((new Date()).getMinutes() * 60)),
                callback: function (val) {
                    if (typeof (val) === 'undefined') {} else {
                        var showTime = new Date(val * 1000);
                        var h = showTime.getUTCHours();
                        var m = showTime.getUTCMinutes();
                        var time = (h < 10 ? '0' + h : h) + ':' + (m < 10 ? '0' + m : m);
                        // $scope.timeBtnTxt = time;
                        if (arg === 'start') {
                            $scope.recordStartTime = nowTime + ' ' + time;
                            var startTamp1 = Date.parse(new Date( $scope.recordStartTime.replace(/\-/g, "/"))) / 1000;
                            var endtamp1 = '';
                            if($scope.endTime)
                             endtamp1 = Date.parse(new Date( $scope.endTime.replace(/\-/g, "/"))) / 1000;
                            if($scope.endTime&&endtamp1<startTamp1){
                                toaster.warning({
                                   // title: "温馨提示",
                                    body: '结束时间不能早于开始时间'
                                });
                                return;
                            }
                            $scope.fixedDrillStartDateOrEndDate($scope.recordStartTime, $scope.recordEndTime);
                        } else {
                            $scope.recordEndTime = nowTime + ' ' + time;
                            var startTamp = Date.parse(new Date( $scope.startTime.replace(/\-/g, "/"))) / 1000;
                            var endtamp = Date.parse(new Date( $scope.recordEndTime.replace(/\-/g, "/"))) / 1000;
                            if(endtamp<startTamp){
                                toaster.warning({
                                   // title: "温馨提示",
                                    body: '结束时间不能早于开始时间'
                                });
                                return;
                            }
                            $scope.fixedDrillStartDateOrEndDate($scope.recordStartTime, $scope.recordEndTime);
                        }

                    }
                }

            };
            ionicTimePicker.openTimePicker(ipObj1);
        };

        function onImageResized(resp) {
            $scope.selected.picdatas.push(resp);
        }
        //提交演练记录
        $scope.commitAddExerciseRecord = function () {
           
            $scope.selected.picdatas = [];
            var promiseArr = [];
            $scope.reportImgs.forEach(function (img) {
                promiseArr.push(resizeImage(Constant.CAPTURE_IMAGE_RANGE, img).then(onImageResized));
            });
            $scope.isLoading = true;
            $q.all(promiseArr).then(function () {
                $ionicLoading.show({
                    noBackdrop: true,
                    template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
                });
                $scope.addDrillRecord();

            });

        };
        $scope.$on(BroadCast.CONNECT_ERROR, function () {
            $scope.isLoading = false;
            $scope.isMoreData = true;
            $ionicLoading.hide();
        });

        //加载更多
        $scope.refreshData = function () {
            $scope.page = 1;
            $scope.getDrillRedordDetail();
        };

        //加载更多
        $scope.loadMore = function () {
            $scope.page++;
            $scope.getMoreData();
        };

        // 计算时长的
        $scope.getTimeDistant = function (start, end) {
            var result = '--';
            if (!end || !start) {
                return result;
            }
            //获取当前时间的时间戳
            var startTamp = Date.parse(new Date(start.replace(/\-/g, "/"))) / 1000;
            var endtamp = Date.parse(new Date(end.replace(/\-/g, "/"))) / 1000;
            var interval = (endtamp - startTamp) / 60;
            // if (interval > 60) {
            //     result = (interval - interval % 60) / 60 + '小时' + interval % 60 + '分钟';
            // } else {
            //     result = interval + '分钟';
            // }
            result = interval + '分钟';
            return result;
        };

        //request ---演练记录详情
        $scope.getDrillRedordDetail = function () {

            Requester.getDrillRedordDetail($scope.drillId, $scope.page).then(function (rest) {
                if (rest.result) {
                    $scope.list = rest.data.content;

                    $scope.list.forEach(function (item) {
                        var picUrls = [];
                        item.picUrls = [];
                        picUrls = item.strImageUrls ? item.strImageUrls.split(',') : [];
                        picUrls.forEach(function (url) {
                            item.picUrls.push({
                                thumb: url,
                                src: url
                            });
                        });
                    });
                } else {
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });

        };

        //request --加载更多
        $scope.getMoreData = function () {
            Requester.getDrillRedordDetail($scope.drillId, $scope.page).then(function (rest) {
                if (rest.result) {
                    var data = rest.data.content;
                    for (var i = 0; i < data.length; i++) {
                        //var item = data[i];
                        $scope.list.push(data[i]);
                    }
                    $scope.list.forEach(function (item) {
                        var picUrls = [];
                        item.picUrls = [];
                        picUrls = item.strImageUrls ? item.strImageUrls.split(',') : [];
                        picUrls.forEach(function (url) {
                            item.picUrls.push({
                                thumb: url,
                                src: url
                            });
                        });
                    });
                    if (!data || (data && data.length < Constant.reqLimit)) {
                        $scope.isMoreData = true;
                    }
                } else {
                    $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        //request --添加演练记录
        $scope.addDrillRecord = function () {
            Requester.addDrillRecord($scope.drillId, $scope.selected).then(function (rest) {
                if (rest.result) {
                    $ionicLoading.hide();
                    $scope.isLoading = false;
                    $scope.recordModal.hide();
                    toaster.success({
                        title: "",
                        body: "添加成功",
                        timeout: 3000
                    });

                    $scope.selected = {
                        remarks: '',
                        picdatas: []
                    };
                    $scope.reportImgs = [];
                    $scope.page = 1;
                    $scope.getDrillRedordDetail();
                } else {
                    $ionicLoading.hide();
                    $scope.isLoading = false;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).finally(function () {
                setTimeout(function () {
                    $ionicLoading.hide();
                    $scope.isLoading = false;
                }, 5000);
            });
        };

        //修改演练时间
        $scope.fixedDrillStartDateOrEndDate = function (startTime, endTime) {
            Requester.fixedDrillStartDateOrEndDate($scope.drillId, startTime, endTime).then(function (rest) {
                if (rest.result) {
                    toaster.success({
                        title: "",
                        body: "修改成功",
                        timeout: 3000
                    });
                    $scope.startTime = startTime;
                    $scope.endTime = endTime;
                    $scope.intervalTime = $scope.getTimeDistant($scope.startTime, $scope.endTime);
                    // console.log('修改成功');
                    // console.log(rest);

                } else {
                    // $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };
    }]);