angular.module('app.controllers')
    .controller('generalCtrl', ['$scope', 'Constant', 'UserPreference', '$ionicHistory', 'Requester', '$state', '$ionicPopup', function ($scope, Constant, UserPreference, $ionicHistory, Requester, $state, $ionicPopup) {
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'page/';
        $scope.userRole = UserPreference.getObject('user').role;
     
        $scope.selectType = 1;

        if (String($scope.userRole) === Constant.USER_ROLES.SCHOOL_ADMIN) {
            $scope.schoolAdmin = true;
            $scope.pageTitle = '设备概况';
        } else if (String($scope.userRole) === Constant.USER_ROLES.DISTRICT_ADMIN) {
            $scope.pageTitle = UserPreference.getObject('user').districtName;
        } else {
            $scope.pageTitle = UserPreference.getObject('user').area.areaName;
        }

        $scope.goBack = function () {
            if ($ionicHistory.backView())
                $ionicHistory.goBack();
            else
                $state.go('tabsController.mainPage');
        };

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            if (String($scope.userRole) != Constant.USER_ROLES.SCHOOL_ADMIN) {
                $scope.checkAuthOrAccountIsOverdue();
            }
            Requester.getGeneralInfo().then(function (resp) {
                if (resp.result) {
                    $scope.classNum = resp.data.classNum;
                    $scope.schlJoinNum = resp.data.schlJoinNum;
                    $scope.schlJoinRate = resp.data.schlJoinRate;
                    $scope.studentNum = resp.data.studentNum;
                    $scope.teacherAvgTime = resp.data.teacherAvgTime;
                    $scope.teacherNum = resp.data.teacherNum;
                    $scope.teacherUseNum = resp.data.teacherUseNum;
                    $scope.teacherUseRate = resp.data.teacherUseRate;
                    $scope.terminalAvgTime = resp.data.terminalAvgTime;
                    $scope.terminalNum = resp.data.terminalNum;
                    $scope.terminalOnlineNum = resp.data.terminalOnlineNum;
                    $scope.terminalOnlineRate = resp.data.terminalOnlineRate;
                    $scope.terminalUseNum = resp.data.terminalUseNum;
                    $scope.terminalUseRate = resp.data.terminalUseRate;
                }
            });



        });

        $scope.checkAuthOrAccountIsOverdue = function () {
            var beforeTamp = UserPreference.get('AreaAuthAlertDateTamp');
            var currentDate = getNowDate();
            var currentTamp = Date.parse(currentDate.replace(/\-/g, "/")) / 1000;
            var interval = 24 * 60 * 60;
            var flag = currentTamp - beforeTamp >= interval;
            if (!beforeTamp ||beforeTamp==='undefined'|| (beforeTamp && flag)) {
                Requester.checkAuthOrAccountIsOverdue().then(function (resp) {
                    if (resp.result) {
                        $scope.authList = resp.data;
                        UserPreference.set('AreaAuthAlertDateTamp', currentTamp);
                        //检查授权到期提醒  
                        if (resp.data) {
                            $scope.authAlert = $ionicPopup.show({
                                template: '<div style="color:#666;font-size:15px;padding:10px;width:100%;"><div ng-repeat="item in authList">{{item.authModeName}}功能权限将于<span style="color:#2bcab2;font-weight:bold;">{{item.expireDate.substr(0,10)}}</span>过期</div> <div>过期后对应模块在APP及网页端均不能在使用，续期请联系<span style="color:#2bcab2;font-weight:bold;">QQ客服(1833025389)</span>或小跟班业务员</div><div class="button" ng-click="closeAuthAlert()" style="height:30px;width:calc(100% - 80px);margin-left:40px;margin-top:15px;background:#2bcab2;color:white;">知道了</div></div>',
                                // templateUrl: UIPath + 'common/areaAuthalert.html',
                                title: '<div style="color:white !important;">授权即将到期提醒</div><div class="logo" ><img src="img/icon/logo.png" /></div>',
                                scope: $scope,
                                cssClass: 'auth_alert'
                            });
                        }

                    } else {
                        console.log('未过期');
                    }
                });
            }

        };

        //关闭授权提示框
        $scope.closeAuthAlert = function () {
            //UserPreference.set('AreaAuthAlertRead', 'read');
            $scope.authAlert.close();
        };

        $scope.selectTypeClick = function (type) {
            $scope.selectType = type;
            if (type == 1) {

            } else {
                //使用分析
                $scope.data = {
                    date: [],
                    data: []
                };
                $scope.refreshData();
            }
        };
        
        // 使用分析

        $scope.refreshData = function () {
            Requester.getUsedTerminalsRecently().then(function (resp) {
                var date = [];
                var data = [];
                if (resp.result) {
                    for (var i = 0; i < resp.data.length; i++) {
                        if (resp.data[i].day.length === 10)
                            date.push(resp.data[i].day.substr(5, 5));
                        else
                            date.push(resp.data[i].day);
                        data.push(resp.data[i].count);
                    }
                    $scope.data = {
                        date: date,
                        data: data
                    };
                }
            });
        };

        //设备在线
        $scope.goToOnlineDevice = function(){
            $state.go('devicesOnline');
        };
        $scope.getDeviceType = function () {
            var result = 'android';
            $scope.isIos = ionic.Platform.isIOS() ;
            if ($scope.isIos) {
                if (window.screen.width >= 375 && window.screen.height >= 812) {
                    result = 'iphoneX';
                } else {
                    result = 'iphone';
                }

            } else {
             result = 'android';
            }
            return result;
        };

    }]);