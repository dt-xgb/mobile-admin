/*jshint scripturl:true*/
angular.module('app.directives', [])
    .directive('resizeFootBar', ['$ionicScrollDelegate', '$rootScope', function ($ionicScrollDelegate, $rootScope) {
        return {
            replace: false,
            link: function (scope, iElm, iAttrs, controller) {
                scope.$on("taResize", function (e, ta) {
                    if (!ta) return;
                    var scroller = document.body.querySelector('#userMessagesView .scroll-content');
                    var viewScroll = $ionicScrollDelegate.$getByHandle('userMessageScroll');

                    var taHeight = ta[0].offsetHeight;
                    var newFooterHeight = taHeight + 10;
                    newFooterHeight = (newFooterHeight > 44) ? newFooterHeight : 44;
                    iElm[0].style.height = newFooterHeight + 'px';
                    scroller.style.bottom = newFooterHeight + 'px';
                    viewScroll.scrollBottom();
                });
            }
        };
    }])
    .directive('ngFocus', [function () {
        var FOCUS_CLASS = "ng-focused";
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ctrl) {
                ctrl.$focused = false;
                element.bind('focus', function (evt) {
                    element.addClass(FOCUS_CLASS);
                    scope.$apply(function () {
                        ctrl.$focused = true;
                    });
                }).bind('blur', function (evt) {
                    element.removeClass(FOCUS_CLASS);
                    scope.$apply(function () {
                        ctrl.$focused = false;
                    });
                });
            }
        };
    }])
    .directive('compile', ['$compile', function ($compile) {
        return function (scope, element, attrs) {
            scope.$watch(function (scope) {
                    return scope.$eval(attrs.compile);
                },
                function (value) {
                    element.html(value);
                    $compile(element.contents())(scope);
                }
            );
        };
    }])
    .directive('backImg', function () {
        return function (scope, element, attrs) {
            element.css({
                'background-image': 'url(' + attrs.backImg + ')',
                'background-size': 'cover'
            });
        };
    })
    .directive('pwCheck', [function () {
        return {
            require: 'ngModel',
            link: function (scope, elem, attrs, ctrl) {
                var firstPassword = '#' + attrs.pwCheck;
                elem.add(firstPassword).on('keyup', function () {
                    scope.$apply(function () {
                        var v = elem.val() === $(firstPassword).val();
                        ctrl.$setValidity('pwmatch', v);
                    });
                });
            }
        };
    }])
    .directive('imageonload', ['$ionicLoading', function ($ionicLoading) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs, ctrl) {
                element.bind('load', function () {
                    $ionicLoading.hide();
                });
                element.bind('error', function () {
                    $ionicLoading.hide();
                });
            }
        };
    }])
    .directive('backBtn', function () {
        return {
            restrict: 'EA',
            replace: true,
            scope: {
                nav: '@navTo'
            },
            template: [
                '<button class="button button-icon icon " ng-click="goBack()">' +
                '<img src="img/icon/icon-arrow_left.png" style="max-width: 25px;max-height:25px;padding-top:7px;margin-bottom:7px;" alt=""/>' +
                '</button>'
            ].join(''),
            controller: ['$scope', '$ionicHistory', '$state', function ($scope, $ionicHistory, $state) {

                $scope.goBack = function() {
                    if ($scope.nav) {
                        $ionicHistory.nextViewOptions({
                            disableBack: true
                        });
                        $state.go($scope.nav);
                    } else {
                        if ($ionicHistory.backView())
                            $ionicHistory.goBack();
                        else {
                            console.log('back view is null, go mainPage');
                            $state.go('tabsController.mainPage');
                        }
                    }
                };
            }]
        };
    })
    .directive('rankChart', function () {
        return {
            restrict: 'A',
            link: function ($scope, element, attrs) {
                var myChart = echarts.init(element[0]);
                $scope.$watch(attrs.eData, function (newValue, oldValue, scope) {
                    var option = {
                        color: ['#3398DB'],
                        grid: {
                            left: '5%',
                            right: '4%',
                            top: '20',
                            bottom: '5',
                            containLabel: true
                        },
                        tooltip: {
                            trigger: 'axis',
                            axisPointer: {
                                type: 'shadow',
                                shadowStyle: {
                                    opacity: 0.5
                                }
                            },
                            confine: true
                        },
                        yAxis : [
                            {
                                type : 'category',
                                data : newValue.y,
                                axisLine: {
                                    show: false
                                },
                                splitLine: {show: false},
                                axisTick:false,
                                axisLabel: {
                                    align:'right',
                                    formatter: function (value, index) {
                                        if (value.length > 9) {
                                            return value.substr(0, 9);
                                        }
                                        return value;
                                    }

                                }
                            }
                        ],
                        xAxis : [
                            {
                                type : 'value',
                                show : false
                            }
                        ],
                        series : [
                            {
                                type:'bar',
                                smooth: true,
                                data: newValue.x,
                                label: {
                                    normal: {
                                        show: true,
                                        position: 'inside',
                                        textStyle: {
                                            color: '#fff',
                                            fontSize: 14
                                        },
                                        formatter: '{c}' + newValue.unit
                                    }

                                },
                                barWidth: 18,
                                barMinHeight: 50,
                                itemStyle: {
                                    emphasis: {
                                        barBorderRadius: 25
                                    },
                                    normal: {
                                        barBorderRadius: 25,
                                        color: new echarts.graphic.LinearGradient(
                                            0, 0, 1, 0,
                                            [
                                                {offset: 0, color: '#096fcf'},
                                                {offset: 1, color: '#47fbb9'}
                                            ]
                                        )
                                    }
                                }
                            }
                        ]
                    };
                    setTimeout(function () {
                        myChart.setOption(option);
                        myChart.resize();
                    }, 100);
                }, true);
            }
        };
    })
    .directive('pieChart', ['$state', function ($state) {
        return {
            restrict: 'A',
            link: function ($scope, element, attrs) {
                var myChart = echarts.init(element[0]);
                var textStyle = {
                    color: 'white',
                    rich: {
                        hh: {
                            fontSize: 15
                        },
                        t1: {
                            fontSize: 20,
                            align: 'center',
                            lineHeight: 30,
                            fontWeight: 'bold'
                        },
                        t2: {
                            fontSize: 20,
                            align: 'center',
                            lineHeight: 30,
                            fontWeight: 'bold'
                        }
                    }
                };
                $scope.$watch(attrs.eData, function (newValue, oldValue, scope) {
                    var option = {
                        title: {
                            right: '15%',
                            top: 50,
                            text: newValue.title,
                            subtext: newValue.subtitle,
                            sublink: "javascript: var state = angular.element(document.querySelector('[ng-app]')).injector().get('$state');state.go('devicesOnline');",
                            subtarget: 'self',
                            textStyle: textStyle,
                            subtextStyle: textStyle
                        },
                        color: ["rgba(255,255,255,.2)", "#fff"],
                        series: [{
                            type: 'pie',
                            radius: ['40%', '70%'],
                            data: newValue.data,
                            avoidLabelOverlap: true,
                            label: {
                                normal: {
                                    show: false,
                                    position: 'center'
                                },
                                emphasis: {
                                    show: false
                                }
                            },
                            labelLine: {
                                normal: {
                                    show: false
                                }
                            }
                        }]
                    };
                    setTimeout(function () {
                        myChart.setOption(option);
                        myChart.resize();
                    }, 100);
                }, true);
            }
        };
    }])
    .directive('terminalUseChart', function () {
        return {
            restrict: 'A',
            link: function ($scope, element, attrs) {
                var myChart = echarts.init(element[0]);
                $scope.$watch(attrs.eData, function (newValue, oldValue, scope) {
                    var option = {
                        grid: {
                            top: '40',
                            bottom: '50',
                            left: '40',
                            right: '30'
                        },
                        xAxis: {
                            type: 'category',
                            boundaryGap: false,
                            data: newValue.x
                        },
                        yAxis: {
                            type: 'value',
                            axisLabel: {
                                formatter: '{value}h'
                            }
                        },
                        series: [
                            {
                                type: 'line',
                                data: newValue.y
                            }
                        ]
                    };
                    myChart.setOption(option);
                }, true);
            }
        };
    })
    .directive('lineChart', function () {
        return {
            restrict: 'A',
            link: function ($scope, element, attrs) {
                var myChart = echarts.init(element[0]);
                $scope.$watch(attrs.eData, function (newValue, oldValue, scope) {
                    var option = {
                        title: {
                            top: 7,
                            left: 7,
                            text: '最近7天设备使用数',
                            textStyle: {
                                fontWeight: 'normal',
                                fontSize: 16
                            }
                        },
                        grid: {
                            top: '50',
                            bottom: '30'
                        },
                        xAxis: {
                            type: 'category',
                            boundaryGap: false,
                            data: newValue.date
                        },
                        yAxis: {
                            type: 'value',
                            boundaryGap: [0, '100%'],
                            minInterval: 1
                        },
                        series: [
                            {
                                type: 'line',
                                smooth: true,
                                symbol: 'none',
                                sampling: 'average',
                                itemStyle: {
                                    normal: {
                                        color: 'rgb(255, 70, 131)'
                                    }
                                },
                                areaStyle: {
                                    normal: {
                                        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                                            offset: 0,
                                            color: 'rgb(255, 158, 68)'
                                        }, {
                                            offset: 1,
                                            color: 'rgb(255, 70, 131)'
                                        }])
                                    }
                                },
                                markPoint: {
                                    symbolSize: [30, 20],
                                    data: [
                                        {type: 'max', name: '最大值'},
                                        {type: 'min', name: '最小值'}
                                    ]
                                },
                                data: newValue.data
                            }
                        ]
                    };
                    myChart.setOption(option);
                }, true);
            }
        };
    })
    .directive('itemEnd',function () {
        //用于将某些地方将元素放在容器的右边
        return {
            restrict: 'A',
            link:function (scope,element,attrs,ctrl) {
                element.css({
                    'float': 'right'
                });
            }
        };
    })
    .directive('emotionPicker',function(){
        return {
            restrict :'EA',
            scope:false,
            template:'<div style="height:195px;border-top: 1px solid #cfcece;width:100%;">'+
            '<div style="padding:10px; height:195px;width:100%;">'+
            '<ion-slides >'+
            '<ion-slide-page ng-repeat="item in items" style="height:195px;width:100%;">'+
            '<span ng-repeat="emotion in item"  ng-click="setValue(emotion)" style="display: block;float: left;width: 12.5%; height: 42px; font-size: 1.2em;line-height: 42px;text-align: center;margin-bottom: 10px;">'+
            '{{emotion}}'+
            '</span>'+
            '</ion-slide-page>'+
            '</ion-slides>'+
            '</div>'+
            '</div>',
            link: function (scope, element, attrs) {
                var EMOJIS =
                    "😀 😃 😄 😁 😆 😅 😂 🤣 😊 😇 🙂 😉 😌 😭 😍 😘 😗 😙 😚 😋 😜 😝 😛 🤑 🤗 🤓 😎 🤡 🤠 😏 😒 😞 😔 😟 😕 🙁 ☹️" +
                    " 😣 😖 😫 😩 😤 😠 😡 😶 😐 😑 😯 😦 😧 😮 😲 😵 😳 😱 😨 😰 😢 😥 🤤 😓 😪 😴 🙄 🤔 🤥 😬 🤐 🤢 🤧 😷 🤒 🤕 😈 👿 👹 👺" +
                    " 💩 👻 💀 ☠️ 👽 👾 🤖 🎃 😺 😸 😹 😻 😼 😽 🙀 😿 😾 👐 🙌 👏 🙏 🤝 👍 👎 👊 ✊ 🤛 🤜 🤞 ✌️ 🤘 👌 👈 👉 👆 👇 ☝️ ✋ 🤚 🖐 🖖" +
                    " 👋 🤙 💪 🖕 ✍️ 🤳 💅 🖖 💄 💋 👄 👅 👂 👃 👣 👁 👀 🗣 👤 👥 🕶 🌂 ☂️";
                var EmojiArr = EMOJIS.split(" ");
                var groupNum = Math.ceil(EmojiArr.length / 24);
                scope.items = [];

                for (var i = 0; i < groupNum; i++) {
                    scope.items.push(EmojiArr.slice(i * 24, (i + 1) * 24));
                }
            }
        };
    })

    .directive('popupKeyBoardShow', [function ($rootScope, $ionicPlatform, $timeout, $ionicHistory, $cordovaKeyboard) {
           return {
             link: function (scope, element, attributes) {
             window.addEventListener('native.keyboardshow', function (e) {
               angular.element(element).parent().parent().css({
                 'margin-top': '-' + 260 + 'px'   //这里80可以根据页面设计，自行修改
               });
           });
              window.addEventListener('native.keyboardhide', function (e) {
               angular.element(element).parent().parent().css({
                  'margin-top': 0
                });
              });
            }
        };
     }]);
    

