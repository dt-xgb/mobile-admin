/**
 * Created by hewz on 2017/5/16.
 */
angular.module('app.requester', ['ionic'])
    .factory('Requester', ['$http', 'Constant', '$q', 'UserPreference', 'toaster', 'SavePhotoTool', '$timeout', function ($http, Constant, $q, UserPreference, toaster,SavePhotoTool,$timeout) {
        var req = {};


        /**
         * 设置推送消息已读
         * @param noticeId
         * @returns {*}
         */
        req.setNoticeRead = function (noticeId) {
            return $http({
                method: "post",
                url: Constant.ServerUrl + "/notice/readSign",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: CustomParam({
                    noticeId: noticeId
                })
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };


        /**
         * 获取设备概况
         * @returns {*}
         */
        req.getGeneralInfo = function () {
            if(iOSDevice()){
                var defer = $q.defer();
                // var tool = $injector.get('SavePhotoTool');
                cordova.plugin.http.setDataSerializer('json');
                var options = {};
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/terminal/general",
                    options, header, function (response) {  
                        var data = JSON.parse(response.data);
                        try { 
                            $timeout(function () {
                                 defer.resolve(data);
                            });
                        } catch (error) {                
                            console.error("JSON parsing error");
                        }
                    }, function (error) {
                        if (error.status) {
                            SavePhotoTool.interceptors(error.status);
                        }
                        $timeout(function () {
                            defer.reject(error);
                        });
                    });            
                return defer.promise;
            }else{
                return $http.get(Constant.ServerUrl + "/terminal/general")
                .then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }      
        };

        /**
         * 查询最近设备使用数
         * @returns {*}
         */
        req.getUsedTerminalsRecently = function () {
            return $http.get(Constant.ServerUrl + "/terminal/terminalUsedCountDayly")
                .then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
        };


        /**
         * 获取地区设备分布
         * @param area
         * @returns {*}
         */
        req.getTerminalsLocation = function (area) {
            return $http.get(Constant.ServerUrl + "/terminal/info/distribution", {
                params: {
                    areaCode: area,
                    page: 1,
                    rows: 999
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 学校教师使用时长排行
         * @param area
         * @param lastDays
         * @returns {*}
         */
        req.getTeacherUseTimeRank = function (area, lastDays) {
            return $http.get(Constant.ServerUrl + "/terminal/rank/teacherAvgTime", {
                params: {
                    areaCode: area,
                    lastDays: lastDays,
                    page: 1,
                    rows: 50
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };


        /**
         * 学校设备使用时长排行
         * @param area
         * @param lastDays
         * @returns {*}
         */
        req.getTerminalUseTimeRank = function (area, lastDays) {
            return $http.get(Constant.ServerUrl + "/terminal/rank/schoolAvgTime", {
                params: {
                    areaCode: area,
                    lastDays: lastDays,
                    page: 1,
                    rows: 50
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };


        /**
         * 学校设备使用率排行
         * @param type
         * @param area
         * @param lastDays
         * @returns {*}
         */
        req.getTerminalUsageRank = function (type, area, lastDays) {
            return $http.get(Constant.ServerUrl + "/terminal/rank/schoolUseRate", {
                params: {
                    rankCondition: type,
                    areaCode: area,
                    lastDays: lastDays,
                    page: 1,
                    rows: 50
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };


        /**
         * 获取学校列表
         * @param county
         * @returns {*}
         */
        req.getSchools = function (county) {
            return $http.get(Constant.ServerUrl + "/school", {
                params: {
                    county: county,
                    page: 1,
                    rows: 999
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 获取省市区列表
         * @param code
         * @returns {*}
         */
        req.getArea = function (code) {
            return $http.get(Constant.ServerUrl + "/area/get", {
                params: {
                    code: code
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 教师使用统计
         * @param statCondition
         * @returns {*}
         */
        req.getTeacherUseRank = function (statCondition) {
            return $http.get(Constant.ServerUrl + "/terminal/teacherTimeRank", {
                params: {
                    statCondition: statCondition
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 班级使用统计
         * @param statCondition
         * @returns {*}
         */
        req.getClassUseRank = function (statCondition) {
            return $http.get(Constant.ServerUrl + "/terminal/classTimeRank", {
                params: {
                    statCondition: statCondition
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 设备使用明细-查看设备列表
         * @param page
         * @param schoolId
         * @returns {*}
         */
        req.getTerminalList = function (page, schoolId) {
            return $http.get(Constant.ServerUrl + "/terminal/UseDetail/terminal/info", {
                params: {
                    page: page,
                    row: Constant.reqLimit,
                    schoolId: schoolId
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 设备使用明细-查看设备使用时长
         * @param days
         * @param sn
         * @returns {*}
         */
        req.getTerminalUseTime = function (days, sn) {
            return $http.get(Constant.ServerUrl + "/terminal/UseDetail/times-every-day", {
                params: {
                    days: days,
                    sn: sn
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 设备使用明细-查看设备截图
         * @param page
         * @param day
         * @param sn
         * @returns {*}
         */
        req.getTerminalScreenShot = function (page, day, sn) {
            return $http.get(Constant.ServerUrl + "/terminal/UseDetail/snapshot", {
                params: {
                    day: day,
                    sn: sn,
                    page: page,
                    row: Constant.reqLimit
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return error.data;
            });
        };

        /**
         * 获取在线设备列表
         * @returns {*}
         */
        req.getOnlineTerminalList = function () {
            return $http.get(Constant.ServerUrl + "/terminal/?state=RUNNING" + '&i=' + new Date()).then(function (response) {
                return response.data;
            }, function (error) {
                return error.data;
            });
        };

        /**
         * 将指定设备关机
         * @param tid
         * @returns {*}
         */
        req.turnOffTerminals = function (tid) {
            return $http({
                method: "post",
                url: Constant.ServerUrl + "/terminal/shutdown",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: CustomParam({
                    terminalIds: tid
                })
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };
        /**
         * 获取消息通知未读数
         * @param newsId
         */
        req.getNoticeNotReadNumber = function () {
            if(iOSDevice()){
                var defer = $q.defer();
                // var tool = $injector.get('SavePhotoTool');
                cordova.plugin.http.setDataSerializer('json');
                var options = {};
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/notice/unreadTotal/byAppAdmin",
                    options, header, function (response) { 
                        var data = JSON.parse(response.data); 
                        try { 
                            $timeout(function () {
                                 defer.resolve(data);
                            });
                        } catch (error) {                
                            console.error("JSON parsing error");
                        }
                    }, function (error) {
                        console.log("====error=====");
                        console.log(error);
                        if (error.status) {
                            SavePhotoTool.interceptors(error.status);
                        }
                        $timeout(function () {
                            defer.reject(error);
                        });
                    });            
                return defer.promise;
            }else{
                return $http.get(Constant.ServerUrl + "/notice/unreadTotal/byAppAdmin").then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };

        /**
         * 获取校园风采未读
         * @returns {*}
         */
        req.getCampusUnread = function () {
            var params = {};
            if (String(UserPreference.getObject('user').rolename) === Constant.USER_ROLES.PARENT) {
                params.stuId = UserPreference.get('DefaultChildID');
            }
            if(iOSDevice()){
                var defer = $q.defer();
                // var tool = $injector.get('SavePhotoTool');
                cordova.plugin.http.setDataSerializer('json');
                var options = params;
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/campusview/unreadTotalByList",
                    options, header, function (response) {  
                        var data = JSON.parse(response.data);
                        try { 
                            $timeout(function () {
                                 defer.resolve(data);
                            });
                        } catch (error) {       
                            console.error("JSON parsing error");
                        }
                    }, function (error) {
                        if (error.status) {
                            SavePhotoTool.interceptors(error.status);
                        }
                        $timeout(function () {
                            defer.reject(error);
                        });
                    });
               
                return defer.promise;

            }else{
                return $http.get(Constant.ServerUrl + "/campusview/unreadTotalByList", {
                    params: params
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };

        /**
         * 校园风采添加评论
         * @param newsId
         * @param comment
         * @returns {*}
         */
        req.addCampusComment = function (newsId, comment) {
            return $http.post(Constant.ServerUrl + "/campus/add-comments", {
                newsId: newsId,
                comments: comment
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 校园风采删除评论
         * @param commentId
         * @returns {*}
         */
        req.deleteCampusComment = function (commentId) {
            return $http.get(Constant.ServerUrl + "/campus/deleteComments", {
                params: {
                    id: commentId
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 校园风采点赞
         */
        req.favorCampus = function (newsId) {
            return $http.post(Constant.ServerUrl + "/campus/add-praise", {
                newsId: newsId
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 校园风采取消点赞
         */
        req.unfavorCampus = function (newsId) {
            return $http.get(Constant.ServerUrl + "/campus/deletePraise", {
                params: {
                    id: newsId
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 校园风采获取点赞列表
         * @param newsId
         */
        req.getCampusNewsFavorList = function (newsId) {
            return $http.get(Constant.ServerUrl + "/campus/getPraiseList", {
                params: {
                    newsId: newsId
                }

            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };


        /**
         * 校园风采获取评论列表
         * @param newsId
         * @param page
         * @returns {*}
         */
        req.getCampusNewsCommentList = function (newsId, page) {
            var params = {
                newsId: newsId,
                page: page,
                rows: Constant.reqLimit
            };
            return $http.get(Constant.ServerUrl + "/campus/getCommentsList", {
                params: params
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * by wl
         * 发表用户反馈
         * content 发表的内容  若为图片类型 则为base 64字符串形式
         * contentType : 消息类型
         */
        req.publishUserFeedback = function (content, contentType) {
            var phoneSy = ionic.Platform.isIOS() ? 'iOS' : 'android';
            return $http.post(Constant.ServerUrl + "/suggestions/public", {
                appVersion: Constant.version,
                content: content,
                phoneOs: phoneSy,
                contentType: contentType
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 标记消息为已读 用户反馈
         * newsId 消息Id
         */
        req.fixNewsReadStatus = function (newsId) {
            return $http.post(Constant.ServerUrl + "/suggestions/signRead", {
                id: newsId
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 获取用户反馈消息列表
         */
        req.getFeedbackDetailList = function () {
            return $http.get(Constant.ServerUrl + "/suggestions/detail", {
                params: {
                    endDate: '',
                    startDate: ''
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 获取用户反馈 未读数
         */
        req.getNoreadNewsNumber = function () {
            return $http.get(Constant.ServerUrl + "/suggestions/getUnreadReplySize", {
                params: {}

            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 班级评比任务列表
         * @param endDate 结束时间
         * @param startDate 开始时间
         * @param page 页面
         */
        req.getClassVoteTasktList = function (page, startDate, endDate) {
            return $http.get(Constant.ServerUrl + "/classvote/list", {
                params: {
                    page: page,
                    rows: Constant.reqLimit,
                    startDate: startDate,
                    endDate: endDate
                }

            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };
        /**
         * 班级评比任务列表 new(1.1.6)
         * @param endDate 结束时间
         * @param startDate 开始时间
         * @param page 页面
         */
        req.getClassVoteTasktList2 = function (page, startDate, endDate) {
            return $http.get(Constant.ServerUrl + "/classvote/list/byTerm", {
                params: {
                    page: page,
                    rows: Constant.reqLimit,
                    startDate: startDate,
                    endDate: endDate
                }

            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };
        /**
         * 添加班级评比任务
         * @param title 任务标题
         * @param startDate 开始时间
         * @param endDate 结束时间
         */
        req.addClassVoteTask = function (params) {
            return $http.post(Constant.ServerUrl + "/classvote/add", params)
                .then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
        };

        /**
         * 删除班级评比任务
         * @param classvoteId 评比任务id
         */
        req.deleteClassVoteTask = function (classvoteId) {
            return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/classvote/delete",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: CustomParam({
                        id: classvoteId
                    })
                })
                .then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
        };

        /**
         * 获取当前学期班级评比任务总数
         *
         */
        req.getCurrentSemesterVoteTaskCount = function () {
            if(iOSDevice()){
                var defer = $q.defer();
                cordova.plugin.http.setDataSerializer('json');
                var options = {};
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/classvote/countByTerm",
                    options, header, function (response) { 
                        var data = JSON.parse(response.data); 
                        try { 
                            $timeout(function () {
                                 defer.resolve(data);
                            });
                        } catch (error) {
                            
                            console.error("JSON parsing error");
                        }
                    }, function (error) {
                        if (error.status) {
                            SavePhotoTool.interceptors(error.status);
                        }
                        $timeout(function () {
                            defer.reject(error);
                        });
                    });
               
                return defer.promise;
            }else{
                console.log('cookies ;;;');
                return $http.get(Constant.ServerUrl + "/classvote/countByTerm", {
                    params: {
    
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
          
        };

        /**
         * 获取年级列表
         */
        req.getGradeList = function () {
            return $http.get(Constant.ServerUrl + "/classvote/grade/list", {
                params: {

                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };


        /**
         * 班级评比结果列表
         * @param classvoteId 评比任务id
         * @param gradeId 年级id 为空时默认选择全部年级
         */
        req.getClassVoteReultList = function (classvoteId, gradeName, page) {
            return $http.get(Constant.ServerUrl + "/classvote/detail/" + classvoteId, {
                params: {
                    gradeName: gradeName,
                    page: page,
                    rows: 20
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };


        /**
         * 添加评分结果
         * @param classId 班级id
         * @param classvoteDetailId 评比详情id
         * @param classvoteItem 评比维度 数组[{id: '评比维度id',score:'分数'}]
         */
        req.addClassScoreResult = function (classId, classvoteDetailId, classvoteItem) {
            return $http({
                method: 'post',
                url: Constant.ServerUrl + "/classvote/result/add",
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    classId: classId,
                    classvoteDetailId: classvoteDetailId,
                    classvoteItem: classvoteItem
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 修改班级评分结果
         *  @param classId 班级id
         *  @param classvoteDetailId 评比详情id
         *   @param classvoteItem 评比维度 数组[{id: '评比维度id',score:'分数'}]
         */
        req.fixClassScoreResult = function (classId, classvoteDetailId, classvoteItem) {
            return $http({
                method: 'post',
                url: Constant.ServerUrl + "/classvote/result/alter",
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    classId: classId,
                    classvoteDetailId: classvoteDetailId,
                    classvoteItem: classvoteItem
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 查询评比维度
         */
        req.selectVoteItem = function () {
            return $http.get(Constant.ServerUrl + "/classvote/item/list").then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 版本说明
         * @param appType 版本类型 0用户app 1管理app
         * @param version 版本号
         */
        req.getCurrentVersionIntroduce = function () {
            var os = ionic.Platform.isIOS() ? 'ios' : 'android';
            return $http.get(Constant.ServerUrl + "/checkversion/describe", {
                params: {
                    appType: 1,
                    version: Constant.version,
                    osType: os
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };


        /**
         * 报修记录列表
         * @param condition 0,全部；1，待解决；2，已关闭
         * @param isManage true管理列表；false,我的列表
         * @param page 当前页面
         */

        req.getRepairRecordList = function (condition, page) {
            return $http.get(Constant.ServerUrl + "/maintainOrders", {
                params: {
                    condition: condition,
                    isManage: true,
                    row: Constant.reqLimit,
                    page: page
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 报修记录详情
         * @param repairRecordId  报修记录id
         */
        req.getRepairRecordDetail = function (repairRecordId) {
            return $http.get(Constant.ServerUrl + "/maintainOrders/" + repairRecordId, {
                params: {

                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 添加进度处理
         * @param repairRecordId 报修记录id
         * @param orderProgress 1:已提交;2:管理员已查看;4:正在处理;2^30:已关闭;maxinteger:已取消
         * @param progressDescribe 进度描述
         * @param manufacturersContactId 厂家id
         */

        req.addRepairProgress = function (repairRecordId, orderProgress, progressDescribe, manufacturersContactId) {
            return $http({
                method: 'post',
                url: Constant.ServerUrl + "/maintainOrders/" + repairRecordId + "/progress",
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    orderProgress: orderProgress,
                    progressDescribe: progressDescribe,
                    manufacturersContactId: manufacturersContactId
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 获取厂家列表
         */
        req.getManufacturesList = function () {
            return $http.get(Constant.ServerUrl + "/manufacturers/getList", {

            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 获取售后人员列表
         * @param factoryId
         */
        req.getManufacturesMermberList = function (factoryId) {
            return $http.get(Constant.ServerUrl + "/manufacturers/getContactByManufacturerId/" + factoryId, {

            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 添加报修记录
         * @param maintainImgs 故障图片 array
         * @param orderDescribe 故障描述
         * @param terminalAddress	设备所在地址
         */
        req.addEquipmentRepairRecord = function (params) {
            return $http({
                method: 'post',
                url: Constant.ServerUrl + "/maintainOrders",
                headers: {
                    'Content-Type': 'application/json'
                },
                data: params
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 二维码 地址打开
         */
        req.getAssetsIdByScan = function (url) {
            return $http({
                method: 'get',
                url: url,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 添加资产申领进度（管理端）
         * @param orderId 资产申领单id
         * @param orderProgress 进度
         * @param progressDescribe 原因
         */
        req.addAssetsApplyProgress = function (orderId, orderProgress, fixApplyCount, progressDescribe) {
            return $http({
                method: 'post',
                url: Constant.ServerUrl + "/asset/applyProgress",
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    orderId: orderId,
                    orderProgress: orderProgress,
                    param1: fixApplyCount,
                    progressDescribe: progressDescribe
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 资产申领详情
         * @param id 资产id
         */
        req.assetsApplyDetail = function (id) {
            return $http.get(Constant.ServerUrl + "/assets/" + id, {
                params: {

                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 资产审批列表 （管理端）
         */
        req.manageAssetsApplyList = function (page) {
            return $http.get(Constant.ServerUrl + "/manage/assets", {
                params: {
                    row: Constant.reqLimit,
                    page: page
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 获取资产未读数
         *
         */
        req.getAssetsUnread = function () {
            if(iOSDevice()){
                var defer = $q.defer();
                cordova.plugin.http.setDataSerializer('json');
                var options = {};
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/manage/assets/getUnreadSize",
                    options, header, function (response) {  
                        var data = JSON.parse(response.data);
                        try { 
                            $timeout(function () {
                                 defer.resolve(data);
                            });
                        } catch (error) {
                            console.error("JSON parsing error");
                        }
                    }, function (error) {
                        if (error.status) {
                            SavePhotoTool.interceptors(error.status);
                        }
                        $timeout(function () {
                            defer.reject(error);
                        });
                    });
               
                return defer.promise;
            }else{
                return $http.get(Constant.ServerUrl + "/manage/assets/getUnreadSize", {
                    params: {
    
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
           
        };
        /**
         * 查询资产申领的id
         * @param assetsId 领单id
         */
        req.queryAssetsApplyIds = function (assetsId) {
            return $http.get(Constant.ServerUrl + "/asset/getApplyIssue", {
                params: {
                    id: assetsId
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 获取授权模块列表
         * @returns {*}
         */
        req.getToolkitList = function () {
            return $http.get(Constant.ServerUrl + "/toolkit/list").then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        req.requestToolkit = function (phone, appId) {
            return $http({
                method: 'post',
                url: Constant.ServerUrl + "/toolkit/apply",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: CustomParam({
                    phone: phone,
                    appId: appId
                })
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };
        /** 获取审批管理列表
         * @param schoolId 学校Id
         * @param progress 状态: 1通过 2审批中 3不通过 全部是传空
         */
        req.getApprovalManageList = function (page, schoolId, progress) {
            return $http.get(Constant.ServerUrl + "/archive/admin/list", {
                params: {
                    rows: Constant.reqLimit,
                    page: page,
                    progress: progress,
                    schoolId: schoolId
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };



        /**
         * 获取申请的详细信息
         * @param archiveId 申请的Id
         */
        req.getApprovalManageDetail = function (archiveId) {
            return $http.get(Constant.ServerUrl + "/archive/admin/detail", {
                params: {
                    archiveId: archiveId
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 检测是否过期 fasle为未过期 true过期
         */
        req.checkAuthOrAccountIsOverdue = function () {
            if(iOSDevice()){
                var defer = $q.defer();
                cordova.plugin.http.setDataSerializer('json');
                var options = {};
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/checkAuthExpireNew",
                    options, header, function (response) {  
                        var data = JSON.parse(response.data);
                        try { 
                            $timeout(function () {
                                 defer.resolve(data);
                            });
                        } catch (error) {
                            
                            console.error("JSON parsing error");
                        }
                    }, function (error) {
                        console.log('=====error2=====');
                        if (error.status) {
                            SavePhotoTool.interceptors(error.status);
                        }
                        $timeout(function () {
                            defer.reject(error);
                        });
                    });
               
                return defer.promise;
            }else{
                return $http.get(Constant.ServerUrl + "/checkAuthExpireNew", {
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
            
        };



        //应急演练
        /**
         * 演练记录列表
         * @param 
         */
        req.getDrillRecordList = function (page) {
            return $http.get(Constant.ServerUrl + "/drillRecords/list/byDrillDefine", {
                params: {
                    page: page,
                    rows: 10
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 演练详情 -查看详情
         * @param itemId
         */
        req.getDrillDetail = function (itemId) {
            return $http.get(Constant.ServerUrl + "/drillRecords/detail", {
                params: {
                    id: itemId
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 新增记录
         * @param tempDrillDefineId 演练记录id
         * @param remarks 演练记录说明
         * @param strImageUrls 图片数组
         */

        req.addDrillRecord = function (drillId, selected) {
            return $http({
                method: 'post',
                url: Constant.ServerUrl + "/drillRecords/save/byDrillRecords",
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    tempDrillDefineId: drillId,
                    remarks: selected.remarks,
                    picdatas: selected.picdatas,
                    strImageUrls: ''
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /***
         * 演练记录详情
         * @param drillDefineId 演练id
         * @param page  分页
         */

        req.getDrillRedordDetail = function (drillDefineId, page) {
            return $http.get(Constant.ServerUrl + "/drillRecords/list/byDrilRecords", {
                params: {
                    drillDefineId: drillDefineId,
                    page: page,
                    rows: 5,
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 查看演练方案
         * 
         */
        req.getDrillProgramList = function () {
            return $http.get(Constant.ServerUrl + "/drillRecords/drillPlanList", {
                params: {

                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 查看方案详情
         * @param id 方案id
         */
        req.getDrillProgramDetail = function (programId) {
            return $http.get(Constant.ServerUrl + "/drillRecords/drillPlan/detail", {
                params: {
                    id: programId
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 发起演练
         * @param tempDrillPlanId 演练方案编号
         * @param drillTime 演练时间
         * @param address 演练地址
         * @param remarks 演练说明 
         */
        req.sendDrillRecord = function (tempDrillPlanId, selected) {
            return $http({
                method: 'post',
                url: Constant.ServerUrl + "/drillRecords/save/byDrillDefine",
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    tempDrillPlanId: tempDrillPlanId,
                    drillTime: selected.exerciseTime,
                    address: selected.address,
                    remarks: selected.content,
                    title: selected.title
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 修改演练时间（开始时间和结束时间）
         * @param drillDefineId 演练id
         * @param drillStartTime 开始时间
         * @param  drillEndTime 结束时间
         */
        req.fixedDrillStartDateOrEndDate = function (drillDefineId, drillStartTime, drillEndTime) {
            return $http.get(Constant.ServerUrl + "/drillRecords/saveDrillTime", {
                params: {
                    drillDefineId: drillDefineId,
                    drillStartTime: drillStartTime,
                    drillEndTime: drillEndTime
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        //隐患管理
        /**
         * 获取 隐患列表
         * @param status 状态
         */
        req.getHidenTroubleList = function (status, schoolId, page) {
            return $http.get(Constant.ServerUrl + "/teacher/hidden-danger/admin/list", {
                params: {
                    status: status,
                    schoolId: schoolId,
                    page: page,
                    rows: 10
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 获取隐患详情
         */
        req.getHidenTroubleDetail = function (hiddenDangerId) {
            return $http.get(Constant.ServerUrl + "/teacher/hidden-danger/detail", {
                params: {
                    hiddenDangerId: hiddenDangerId
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 获取学校老师列表
         * @param schoolId 学校id
         */

        req.getSchoolTeachersList = function (schoolId) {
            return $http.get(Constant.ServerUrl + "/teacher/hidden-danger/get-teachers", {
                params: {
                    schoolId: schoolId,
                    teacherName: ''
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 添加隐患
         * @param location 隐患位置
         * @param title 隐患标题
         * @param message 隐患信息
         * @param imagesBase64 图片数组
         */
        req.addHidenTrouble = function (selected, schoolId) {
            return $http({
                method: 'post',
                url: Constant.ServerUrl + "/teacher/hidden-danger/admin/upload/" + selected.selectManId,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    location: selected.location,
                    title: selected.title,
                    message: selected.message,
                    imagesBase64: selected.picdatas,
                    schoolId: schoolId
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 增加隐患处理进度
         * @param assignedPersonId 指派人
         * @param description //描述
         * @param hiddenDangerId //隐患id
         * @param status 状态
         */
        req.addHidenTroubleProgress = function (selected, itemId) {
            return $http({
                method: 'post',
                url: Constant.ServerUrl + "/teacher/hidden-danger/add-progress/" + selected.status,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    assignedPersonId: selected.selectManId,
                    description: selected.content,
                    hiddenDangerId: itemId
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };
        /**
         * 获取隐患数量
         */
        req.getHidenTroubleCount = function (schoolId) {
            return $http.get(Constant.ServerUrl + "/teacher/hidden-danger/admin/list/count", {
                params: {
                    schoolId: schoolId
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };


        //资产概况
        /**
         * 
         */
        req.getSchoolAssetsDetail = function () {
            return $http.get(Constant.ServerUrl + "/asset/stat/general", {
                params: {

                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 学校资产排行
         */
        req.getSchoolAssetsRank = function () {
            return $http.get(Constant.ServerUrl + "/asset/stat/getRankBySchl", {
                params: {

                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 厂家资产排行
         */
        req.getFactoryAssetsRank = function () {
            return $http.get(Constant.ServerUrl + "/asset/stat/getRankByManufacturer", {
                params: {

                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };


        //查询疫情 今日概况
        req.getCovidSummary = function () {
            return $http.get(Constant.ServerUrl + "/epidemic/record/count/class_level", {
                params: {}
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        //查询疫情 体温异常名单
        req.getWarningList = function () {
            return $http.get(Constant.ServerUrl + "/epidemic/alarm/list", {
                params: {
                    page: 1,
                    rows: 9999
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };


        //查询疫情 告警详情
        req.getWarningDetail = function (alarmId) {
            return $http.get(Constant.ServerUrl + "/epidemic/alarm/details", {
                params: {
                    alarmId: alarmId
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 疫情跟进
         */
        req.handleWarning = function (alarmId, status, description) {
            return $http({
                method: 'post',
                url: Constant.ServerUrl + "/epidemic/alarm/save/progress",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: CustomParam({
                    alarmId: alarmId,
                    status: status,
                    description: description

                })
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 设置疫情告警已读
         */
        req.covidWarningRead = function () {
            return $http.get(Constant.ServerUrl + "/epidemicAlarm/readSign", {
                params: {}
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 获取学校所有班级列表
         */
        req.getSchoolAllClasses = function (schoolId) {
            var url = Constant.ServerUrl.substr(0, Constant.ServerUrl.length - 3) + 'api';
            return $http.get(url + "/class", {
                params: {
                    schoolId:schoolId
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };
         /**
         * 获取班级同学人像列表
         * @param classId 班级id
         * @param name 学生姓名
         */
        req.getFaceRecognitionClassList = function (classId, name) {
            return $http.get(Constant.ServerUrl + "/faceRecognition/getOfClass", {
                params: {
                    classId: classId,
                    name: name
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };
        /**
         * 获取老师的个人人像
         * @param userId
         */
        req.getTeacherFaceRecognition = function (userId) {
            return $http.get(Constant.ServerUrl + "/faceRecognition/getOne", {
                params: {
                    userId: userId,
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 获取学校老师人像采集接口列表
         */
        req.getTeacherPortraitList = function(schoolId,page) {
            return $http.get(Constant.ServerUrl + "/faceRecognition/getRecognitonTeacherList", {
                params: {
                    teacherName: '',
                    page:1,
                    rows:500
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            }); 
        };

        /**
         * 上传人像
         */
        req.uploadUserPortrait = function (userId, base64Str) {
            return $http({
                method: 'post',
                url: Constant.ServerUrl + "/faceRecognition/uploadImg",
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    userId: userId,
                    imgData: base64Str
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
        };

        /**
         * 获取
         */


        return req;
    }]);
