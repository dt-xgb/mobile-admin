angular.module('app.controllers')
.controller('userProfileCtrl', ['$scope', '$ionicHistory', '$ionicActionSheet', '$ionicPopup', 'CameraHelper', '$state', 'BroadCast', 'UserPreference', 'SettingService', 'MESSAGES', '$ionicLoading', 'toaster', function ($scope, $ionicHistory, $ionicActionSheet, $ionicPopup, CameraHelper, $state, BroadCast, UserPreference, SettingService, MESSAGES, $ionicLoading, toaster) {

    $scope.$on("$ionicView.beforeEnter", function (event, data) {
        $scope.user = UserPreference.getObject("user");
        if (!$scope.user.logo || $scope.user.logo.trim() === '')
            $scope.user.logo = 'img/icon/person.png';
    });

    $scope.openEditView = function (content) {
        $state.go('edit_profile', {content: content});
    };

    $scope.goBack = function () {
        $ionicHistory.nextViewOptions({
            disableBack: true
        });
        $state.go('tabsController.settingPage');
    };

    $scope.$on(BroadCast.IMAGE_SELECTED, function (a, rst) {
        if (rst && rst.which === 'av') {
            $scope.user.logo = rst.source;
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
            SettingService.editInfo('logo', rst.source.substr(rst.source.indexOf('base64,') + 7));
        }
    });

    $scope.$on(BroadCast.EDIT_INFO, function (a, rst) {
        if (rst && rst.result)
            toaster.success({title: MESSAGES.SAVE_SUCCESS, body: MESSAGES.INFO_UPDATED});
        else
            toaster.error({title: MESSAGES.REMIND, body: rst.message});
        $ionicLoading.hide();
    });

    $scope.$on(BroadCast.CONNECT_ERROR, function (arg, data) {
        $ionicLoading.hide();
    });

    $scope.selectImage = function () {
        CameraHelper.selectImage('av', {allowEdit: true});
    };


    $scope.onSexChange = function (sex) {
        SettingService.editInfo('sex', sex);
    };
}]);