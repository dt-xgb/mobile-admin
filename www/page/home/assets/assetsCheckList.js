angular.module('app.controllers')
    .controller('assetsCheckListCtrl', function ($scope, $ionicModal, Constant, $state, Requester,BroadCast,$ionicLoading,UserPreference) {

        //即将进入
        $scope.$on("$ionicView.enter", function (event, data) {

            $scope.isMoreData = true;
            $scope.page = 1;
            $scope.hasGot = Math.pow(2, 30);
            $scope.assetsList = UserPreference.getArray('AssetsApplyList');
            $scope.getAssetsApplyList();
        });


        $scope.$on(BroadCast.CONNECT_ERROR, function () {
             $ionicLoading.hide();
            $scope.isMoreData = false;
        });

        //下拉刷新
        $scope.refreshData = function () {
            $scope.page = 1;
             $scope.getAssetsApplyList();
        };

        //上拉加载
        $scope.loadMore = function () {
            $scope.page++;
            $scope.loading = true;
            Requester.manageAssetsApplyList($scope.page).then(function (res) {
                if (res.result) {
                    $scope.loading = false;
                    if (res.data.content && res.data.content.length > 0) {
                        for (var i = 0; i < res.data.content.length; i++) {
                            $scope.assetsList.push(res.data.content(i));
                        }
                    }
                    //关闭刷新
                    if (!res.data.content || res.data.content.length < Constant.reqLimit) {
                        $scope.isMoreData = false;
                    }
                } else {
                    $scope.isMoreData = false;
                }

            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };

        //获取资产申领的列表
        //审核状态 1:已提交申请;2:管理员已查;4:已拒绝;8:待领取;2^30:已领取;maxinteger:已取消;
        $scope.getAssetsApplyList = function () {
            $scope.loading = true;
            Requester.manageAssetsApplyList($scope.page).then(function (res) {
                if (res.result) {
                    $scope.loading = false;
                    $scope.assetsList = res.data.content;
                    UserPreference.setObject('AssetsApplyList',res.data.content);
                } else {
                    $scope.isMoreData = false;
                }

            }).finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        };



       

        //查看申领详情
        $scope.goToDetail = function (assets) {
        
            $state.go('assetsCheckDetail',{assetsId:assets.id,progressStatus:assets.orderProgress});
        };



    });
