angular.module('app.controllers')
    .controller('hidenTroubleReportCtrl', function ($scope, Constant, $state, $ionicModal, Requester, toaster,$timeout, UserPreference,CameraHelper, $q, $ionicHistory, $ionicLoading, $rootScope) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.select = {
                selectManName: '',
                selectManId: 0,
                location: '',
                title: '',
                message: '',
                picdatas: []
            };
            $scope.schoolId = UserPreference.get('DefaultSchoolID');
            $scope.reportImgs = [];
            $scope.getSchoolTeachersList();
        });

        $scope.$on("$ionicView.loaded", function (event, data) {
           
        });

        function onImageResized(resp) {
            $scope.select.picdatas.push(resp);
        }
         //提交隐患
         $scope.commitTrouble = function () {
             if( $scope.select.selectManId ===0|| !$scope.select.selectManId ){
                toaster.warning({
                    // title: "温馨提示",
                    body:'请选择要指派的老师'
                });
                return;
             }
            $scope.select.picdatas = [];
            var promiseArr = [];
          
            for(var i=0;i< $scope.reportImgs.length;i++ ){
                var img = $scope.reportImgs[i];
                promiseArr.push(resizeImage(Constant.CAPTURE_IMAGE_RANGE, img).then(onImageResized)); 
            }
            $scope.isLoading = true;
            $q.all(promiseArr).then(function () {
                $ionicLoading.show({
                    noBackdrop: true,
                    template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
                });
                $scope.addHidenTrouble();

            });
        };

        //选择图片
        $scope.selectImg = function () {
           
            if (window.cordova) {
                CameraHelper.selectMultiImage(function (resp) {
                    if (!resp)
                        return;
                    if (resp instanceof Array) {
                        Array.prototype.push.apply($scope.reportImgs, resp);
                        $scope.$apply();
                    } else {
                        $scope.reportImgs.push(
                            "data:image/jpeg;base64," + resp
                        );
                    }
                }, 9 - $scope.reportImgs.length);
            } else {
                
            //web 端
                var input = document.getElementById("capture101");
                input.click();
            }
        };

         // input形式打开系统系统相册
         $scope.getFile = function (files) {
            var file = files[0];
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (theFile) {
                $timeout(function () {
                    $scope.testImg = theFile.target.result; //设置一个中间值
                    $scope.reportImgs.push($scope.testImg);

                }, 100);
            };
        };
        $scope.removeImg = function (index, $event) {
            $event.stopPropagation();
            $scope.reportImgs.splice(index, 1);
        };

        //选择指派人
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'page/';
        $ionicModal.fromTemplateUrl(UIPath + 'home/others/safetyManage/selectDealHidenTroubleMan.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.selectManModal = modal;
        });
        $scope.selectMan = function () {
            $scope.selectManModal.show();
        };
        //关闭选择指派人弹窗
        $scope.hideSelectManModal = function () {
            $scope.selectManModal.hide();
        };

        //删除指派人
        $scope.removeMan = function () {
            $scope.select.selectManId = 0;
            $scope.select.selectManName = '';
            $scope.select.isChecked = 0;
            // for (var i = 0; i < $scope.teacherList.length; i++) {
            //     var man = $scope.teacherList[i];
            //     man.checked = false;
            // }
        };

        //确定选择的人
        $scope.saveSelectMan = function () {
            for (var i = 0; i < $scope.teacherList.length; i++) {
                var man = $scope.teacherList[i];
                if (man.id == $scope.select.isChecked) {
                    $scope.select.selectManId = man.id;
                    $scope.select.selectManName = man.chname;
                    break;
                }
            }
            $scope.selectManModal.hide();
        };
        $scope.chooseMan = function (man) {
            // $scope.select.selectManId = man.id;
            // $scope.select.selectManName = man.chname;
        };

        //request --隐患上报
        $scope.addHidenTrouble = function () {
            Requester.addHidenTrouble($scope.select, $scope.schoolId ).then(function (rest) {
                if (rest.result) {
                    $ionicLoading.hide();
                    $scope.isLoading = false;
                    $rootScope.$broadcast('ADD_HidenTrouble_SUCCEED', 'succeed');
                    $ionicHistory.goBack();
                    toaster.success({
                        title: "",
                        body: "添加成功",
                        timeout: 3000
                    });
                   
                    $scope.reportImgs = [];
                } else {
                    $ionicLoading.hide();
                    $scope.isLoading = false;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).finally(function () {
                setTimeout(function () {
                    $ionicLoading.hide();
                    $scope.isLoading = false;
                }, 5000);
            });
        };

        //request --获取学校教师列表
        $scope.getSchoolTeachersList = function () {
            Requester.getSchoolTeachersList($scope.schoolId).then(function (rest) {
                if (rest.result) {
                    $scope.teacherList = rest.data;
                } else {

                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });

        };



    });