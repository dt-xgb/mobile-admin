angular.module('app.controllers')
    .controller('hidenTroubleDetailCtrl', function ($scope, Constant, $state, $ionicModal, Requester, toaster, UserPreference, $stateParams) {
        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.select = {
                selectManName: null,
                selectManId: null,
                content: '',
                status: 2
            };
            $scope.detail = {};

            $scope.list = [1, 2, 3];
            $scope.picUrls = [];

            $scope.schoolId = UserPreference.get('DefaultSchoolID');
           $scope.getSchoolTeachersList(); //获取学校教师列表

        });
        $scope.$on("$ionicView.loaded", function (event, data) {
            $scope.itemId = $stateParams.itemId;
            // console.log('item id:'+$scope.itemId);
            $scope.getHidenTroubleDetail();
        });
        //增加进度
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'page/';
        $ionicModal.fromTemplateUrl(UIPath + 'home/others/safetyManage/addDealHidenTroubleProgress.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.progressModal = modal;
        });
        $scope.addProgress = function () {
            $scope.requestCount = 0;
            $scope.progressModal.show();
        };

        $scope.hideAddProgressModal = function () {
            $scope.progressModal.hide();
        };

        //保存增加进度
        $scope.saveProgress = function () {
            if ($scope.select.status === 3) $scope.select.selectManId = 0;
            if($scope.requestCount===0) $scope.addHidenTroubleProgress();
            $scope.requestCount ++;
            
        };

        //选择指派人
        $ionicModal.fromTemplateUrl(UIPath + 'home/others/safetyManage/selectDealHidenTroubleMan.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.selectManModal = modal;
        });
        $scope.selectMan = function () {
            $scope.selectManModal.show();
        };
        //关闭选择指派人弹窗
        $scope.hideSelectManModal = function () {
            $scope.selectManModal.hide();
        };

        //删除指派人
        $scope.removeMan = function () {
            $scope.select.selectManId = 0;
            $scope.select.selectManName = '';
            $scope.select.isChecked = 0;
            // for (var i = 0; i < $scope.teacherList.length; i++) {
            //     var man = $scope.teacherList[i];
            //     man.checked = false;
            // }
        };

        //确定选择的人
        $scope.saveSelectMan = function () {
            //  console.log('select man :' + $scope.select.selectManId);
            //  console.log($scope.teacherList);
            for (var i = 0; i < $scope.teacherList.length; i++) {
                var man = $scope.teacherList[i];
                if (man.id == $scope.select.isChecked) {
                    $scope.select.selectManId = man.id;
                    $scope.select.selectManName = man.chname;
                    break;
                }
            }
            $scope.selectManModal.hide();
        };
        $scope.chooseMan = function (man) {
         
            // $scope.select.selectManId = man.id;
            // $scope.select.selectManName = man.chname;
            // console.log('select man :' + $scope.select.selectManId);
        };

        //request --隐患详情
        $scope.getHidenTroubleDetail = function () {
            Requester.getHidenTroubleDetail($scope.itemId).then(function (rest) {
                if (rest.result) {
                    $scope.detail = rest.data;
                    $scope.picUrls = [];
                    $scope.events = []; //处理进度
                   
                    if (rest.data.imgUrlList && rest.data.imgUrlList.length > 0) {
                        rest.data.imgUrlList.forEach(function (img) {
                            $scope.picUrls.push({
                                src: img
                            });

                        });
                    }
                    $scope.events = $scope.detail.processResList;

                } else {
                    // $scope.isMoreData = true;
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            }).then(function () {

            });

        };


        //request --获取学校教师列表
        $scope.getSchoolTeachersList = function () {
            Requester.getSchoolTeachersList($scope.schoolId).then(function (rest) {
                if (rest.result) {
                    $scope.teacherList = rest.data;
                    for(var i=0 ;i<$scope.teacherList.length;i++){
                        $scope.teacherList[i].checked = false;
                    }
                } else {

                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });

        };

        //request --增加隐患进度
        $scope.addHidenTroubleProgress = function () {
            Requester.addHidenTroubleProgress($scope.select, $scope.itemId).then(function (rest) {
                $scope.requestCount = 0;
                if (rest.result) {
                    $scope.progressModal.hide();
                    toaster.success({
                        title: "",
                        body: "添加成功",
                        timeout: 3000
                    });

                    $scope.select = {
                        selectManName: null,
                        selectManId: 0,
                        content: '',
                        status: 2
                    };
                    $scope.getHidenTroubleDetail();
                } else {
                    toaster.warning({
                        title: "温馨提示",
                        body: rest.message
                    });
                }
            });
        };

    });