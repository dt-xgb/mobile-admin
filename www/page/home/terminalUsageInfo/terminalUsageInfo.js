angular.module('app.controllers')
.controller('terminalUsageInfoCtrl', function ($scope, Constant, UserPreference,ionicImageView, $state, $ionicHistory, Requester, $stateParams, ionicDatePicker, toaster, $ionicModal) {

    $scope.goBack = function () {
        if ($ionicHistory.backView())
            $ionicHistory.goBack();
        else
            $state.go('tabsController.mainPage');
    };

    $scope.$on('$destroy', function () {
        $scope.imgModal.remove();
    });

    var UIPath = '';
    if (Constant.debugMode) UIPath = 'page/';
    $ionicModal.fromTemplateUrl(UIPath + 'common/ionic-image-view.html', {
        scope: $scope
    }).then(function (modal) {
        $scope.imgModal = modal;
    });

    $scope.closeImageView = function () {
        $scope.imgModal.hide();
    };

    $scope.viewImg = function (url) {
        
        var imgArr = [];
        imgArr.push(url);
        ionicImageView.showViewModal({
            allowSave: true
        }, imgArr, 0);
        //$scope.imgModal.show();
        //$scope.largeImgUrl = url;
    };

    $scope.$on("$ionicView.loaded", function (event, data) {
        $scope.selected = {
            days: 7,
            sn: $stateParams.id,
            date: formatTimeWithoutSecends((new Date()).getTime() / 1000, "yyyy-MM-dd")
        };
        $scope.data = {};
        $scope.chartView = true;
        $scope.getChartData();
        $scope.getScreenShots(true, true);
    });

    $scope.switchView = function (p) {
        $scope.chartView = p;
    };

    $scope.onSelectedDayChange = function (d) {
        if ($scope.selected.days != d) {
            $scope.selected.days = d;
            $scope.getChartData();
        }
    };

    $scope.showDatePicker = function () {
        var ipObj1 = {
            callback: function (val) {
                if (new Date().getTime() < val) {
                    toaster.warning({title: '所选日期不能晚于今天！', body: ''});
                    return;
                }
                $scope.selected.date = formatTimeWithoutSecends(val / 1000, "yyyy-MM-dd");
                $scope.getScreenShots(true);
            }
        };
        ionicDatePicker.openDatePicker(ipObj1);
    };

    $scope.getChartData = function () {
        $scope.totalUseTime = 0;
        Requester.getTerminalUseTime($scope.selected.days, $scope.selected.sn).then(function (resp) {
            if (resp.data) {
                var x = [], y = [];
                for (var i in resp.data) {
                    x.push(i.substr(5, 5));
                    var uses = resp.data[i] / 1000;
                    y.push(uses / 3600);
                    $scope.totalUseTime += uses;
                }
                $scope.data = {
                    x: x,
                    y: y
                };

                $scope.totalTime = getFormattedTimeLabel($scope.totalUseTime);
                $scope.avaTime = getFormattedTimeLabel(Math.floor($scope.totalUseTime / $scope.selected.days));
            }
        });
    };

    function getFormattedTimeLabel(s) {
        var rst = '';
        var hour = Math.floor(s / 3600);
        var min = Math.floor(s / 60) % 60;
        var sec = s % 60;
        if (hour > 0)
            rst += (hour + '小时');
        if (min > 0)
            rst += (min + '分');
        return rst + sec + '秒';
    }

    $scope.getScreenShots = function (reset, ignore) {
        if ($scope.chartView && !ignore) {
            $scope.$broadcast('scroll.refreshComplete');
            return;
        }
        if (reset) {
            $scope.page = 1;
            $scope.list = [];
        }
        else if ($scope.listHasMore)
            $scope.page++;
        else
            return;
        $scope.loading = true;
        Requester.getTerminalScreenShot($scope.page, $scope.selected.date, $scope.selected.sn).then(function (resp) {
            if (resp.result) {
                if ($scope.list.length === 0)
                    $scope.list = resp.data.content;
                else {
                    Array.prototype.push.apply($scope.list, resp.data.content);
                }
                $scope.listHasMore = !resp.data.last;
            } else
                $scope.listHasMore = false;
        }).finally(function () {
            $scope.loading = false;
            $scope.$broadcast('scroll.refreshComplete');
            $scope.$broadcast('scroll.infiniteScrollComplete');
        });
    };

});