angular.module('app.controllers')
    .controller('addCloundLiveCtrl', function ($scope, Constant, $state, $stateParams, Requester, toaster, UserPreference, $ionicPopup, $ionicHistory, $timeout, ionicDatePicker, ionicTimePicker, $ionicLoading) {

        $scope.$on("$ionicView.enter", function (event, data) {
            $scope.cloundLiveId = $stateParams.id;
            $scope.pageTitle = $stateParams.id?'编辑直播':'添加直播';
            $scope.select = {
                title:$stateParams.title,
                status:$stateParams.status,
                teacherName:$stateParams.speaker,
                createDate:$stateParams.liveDay,
                startTime:$stateParams.startTime,
                endTime:$stateParams.endTime,
                isReview:$stateParams.islookBack===1
            };
        });
        //请选择日期
        $scope.selectDate = function () {
            if($scope.select.status===1||$scope.select.status===2) return;
            setTimeout(function () {
                if (window.cordova) cordova.plugins.Keyboard.close();
                var ipObj1 = {
                    callback: function (val) {
                        if (typeof (val) !== 'undefined') {
                            $scope.select.createDate = formatTimeWithoutSecends(val / 1000).substr(0, 10);
                            // console.log( $scope.select.createDate );
                        }
                    }
                };
                ionicDatePicker.openDatePicker(ipObj1);
            }, 100);
        };
        //请选择时间
        $scope.selectTime = function (type) {
            if($scope.select.status===1||$scope.select.status===2) return;
            var ipObj1 = {
                inputTime: (((new Date()).getHours() * 60 * 60) + ((new Date()).getMinutes() * 60)),
                callback: function (val) {
                    if (typeof (val) === 'undefined') { } else {
                        var showTime = new Date(val * 1000);
                        var h = showTime.getUTCHours();
                        var m = showTime.getUTCMinutes();
                        var time = (h < 10 ? '0' + h : h) + ':' + (m < 10 ? '0' + m : m);
                        // $scope.timeBtnTxt = time;
                        // console.log('time :'+time);
                        // $scope.select.crateTime =  $scope.select.crateTime + ' ' + time + ':00';
                        if (type === 'start') {
                            $scope.select.startTime = time;
                        } else {
                            $scope.select.endTime = time;
                        }
                    }
                }

            };
            ionicTimePicker.openTimePicker(ipObj1);

        };

        //提交
        $scope.commit = function () {
            if (!$scope.select.createDate) {
                $ionicLoading.show({
                    template: '请选择直播日期',
                    duration: 2000
                });
                return;
            }
            if (!$scope.select.startTime) {
                $ionicLoading.show({
                    template: '请选择直播开始时间',
                    duration: 2000
                });
                return;
            }
            if (!$scope.select.endTime) {
                $ionicLoading.show({
                    template: '请选择直播结束时间',
                    duration: 2000
                });
                return;
            }
            var selectStartTime = $scope.select.createDate + ' ' + $scope.select.startTime;
            var selectEndTime = $scope.select.createDate + ' ' + $scope.select.endTime;
            var timestamp1 = Date.parse(new Date(selectStartTime.replace(/\-/g, "/"))) / 1000;
            var timestamp2 = Date.parse(new Date(selectEndTime.replace(/\-/g, "/"))) / 1000;
            $scope.my_date = new Date();
            $scope.currentTamp = Date.parse($scope.my_date) / 1000;
           
            if ((!$scope.select.status||$scope.select.status===0)&&timestamp1 - $scope.currentTamp < 0) {
                $ionicLoading.show({
                    template: '直播时间不能早于当前时间',
                    duration: 2000
                });
                return;
            }
            if (timestamp1 > timestamp2) {
                $ionicLoading.show({
                    template: '结束时间不能小于开始时间',
                    duration: 2000
                });
                return;
            }
            $scope.addCloundLive();

        };
        //取消
        $scope.cancel = function () {
            $scope.select = {};
            $ionicHistory.goBack();
        };

        //requester 
        $scope.addCloundLive = function(){
          Requester.cloundLiveEdit($scope.select,$scope.cloundLiveId).then(function(rest){
            if (rest.result) {
                toaster.success({
                    body: '操作成功',
                    timeout: 3000
                });
                $ionicHistory.goBack();
            } else {
                
                toaster.warning({
                    title: "温馨提示",
                    body: rest.message
                });
            }
          });
        };
    });