angular.module('app.controllers')
.controller('loginCtrl', function ($scope, $state, $ionicLoading, AuthorizeService, BroadCast, UserPreference, toaster, MESSAGES) {

    $scope.$on("$ionicView.enter", function (event, data) {
        $scope.isLogin = false;
        $scope.loginModel = {
            remember: true,
            username: UserPreference.get('username', ''),
            password: UserPreference.get('password', '')
        };
        if ($scope.loginModel.username !== '' && $scope.loginModel.password !== '') {
            if(iOSDevice()){
                $state.go('tabsController.mainPage');
                AuthorizeService.logining = false;
                AuthorizeService.login($scope.loginModel,function(res){
                    //console.log('默认登录成功');
                },function(error){
                   // console.log('默认登录失败');
                });
            }else{
                AuthorizeService.login($scope.loginModel);
                $state.go('tabsController.mainPage');
            }
           
          
        }
    });

    $scope.isIOS = ionic.Platform.isIOS();

    $scope.login = function () {
        if (this.loginForm.$valid) {
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
            
           // if (rst) {
               if(iOSDevice()){
                AuthorizeService.logining = false;
                AuthorizeService.login($scope.loginModel,function(rst){
                    $ionicLoading.hide();
                    var data =   rst;//iOSDevice()?JSON.parse(rst):rst;
                    $scope.isLogin = true;
                        if (data.result) {
                           
                            UserPreference.set("username", $scope.loginModel.username);
                            if ($scope.loginModel.remember)
                                UserPreference.set("password", $scope.loginModel.password);
                            $state.go('tabsController.mainPage');
                        }
                        else{
                            $ionicLoading.hide();
                            toaster.error({title: MESSAGES.LOGIN_ERROR, body: data.message,timeout:5000});
                        }
                },function(error){
                    $scope.isLogin = true;
                    $ionicLoading.hide();
                });
                setTimeout(function(){
                    if(!$scope.isLogin&& AuthorizeService.logining===false)
                    //toaster.warning({title: MESSAGES.LOGIN_ERROR, body: '登录超时'});
                    AuthorizeService.logining = false;
                    $ionicLoading.hide();
                },20000);
               }else{
                AuthorizeService.login($scope.loginModel).then(function (rst) {
                    $ionicLoading.hide();
                    var data =   rst;//iOSDevice()?JSON.parse(rst):rst;
                        if (data.result) {
                            UserPreference.set("username", $scope.loginModel.username);
                            if ($scope.loginModel.remember)
                                UserPreference.set("password", $scope.loginModel.password);
                            $state.go('tabsController.mainPage');
                        }
                        else{
                            $ionicLoading.hide();
                            toaster.error({title: MESSAGES.LOGIN_ERROR, body: data.message,timeout:5000});
                        }
                           
                    }).finally(function () {
                        // console.log('request end');
                        $ionicLoading.hide();
                    });
               }
               
               
             
        } else {
            if (this.loginForm.input_user.$error.required) {
                toaster.warning({title: MESSAGES.LOGIN_ERROR, body: MESSAGES.LOGIN_NO_USERNAME});
            } else if (this.loginForm.input_user.$error.pattern) {
                toaster.warning({title: MESSAGES.LOGIN_ERROR, body: MESSAGES.LOGIN_USERNAME_PATTERN});
            } else if (this.loginForm.input_pwd.$error.required) {
                toaster.warning({title: MESSAGES.LOGIN_ERROR, body: MESSAGES.LOGIN_NO_PASSWORD});
            } else if (this.loginForm.input_pwd.$error.pattern) {
                toaster.warning({title: MESSAGES.LOGIN_ERROR, body: MESSAGES.LOGIN_PASSWORD_PATTERN});
            }
            else
                toaster.error({title: MESSAGES.LOGIN_ERROR, body: 'Unknown Error'});
         }
    };


    $scope.hideKeyBoard = function(){
       if(window.cordova){
        //cordova.plugins.Keyboard.close();
        //Keyboard.hide();
       }
    };

    // $scope.test = function(){
    //  $state.go('abnormalHandle',{notice:{extras:{createTime:'2018-07-17 10:47',text:'哈哈哈nishi',title:'标题'}}});
    // };
});