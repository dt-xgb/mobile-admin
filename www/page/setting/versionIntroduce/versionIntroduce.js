angular.module('app.controllers')
.controller('versionIntroduceCtrl',function($scope,Requester,UserPreference){
    UserPreference.set('VersionIntroduce','READ');

    Requester.getCurrentVersionIntroduce().then(function(res){
        if(res.result){
            console.log("版本说明 succeed");
            console.log(res);   
            $scope.text = res.data.versionDescribe;
        }else{
            console.log("版本说明 failure");
        }

    });
});