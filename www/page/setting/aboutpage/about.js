angular.module('app.controllers')
.controller('aboutCtrl', function ($scope, Constant, $ionicHistory, $state) {
    $scope.version = Constant.version;
    $scope.versionCode = Constant.versionCode;
    $scope.goBack = function () {
        $ionicHistory.nextViewOptions({
            disableBack: true
        });
        $state.go('tabsController.settingPage');
    };
});