angular.module('app.controllers')
.controller('abnormalHandleCtrl',function($scope,$state,$stateParams){
    $scope.isIos = ionic.Platform.isIOS();
    $scope.news = $scope.isIos? $stateParams.notice.extras: $stateParams.notice;
    
    $scope.noticeContent = $scope.news.text;
  
});