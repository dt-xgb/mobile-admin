/**
 * Created by hewz on 2017/5/16.
 */
angular.module('app.requester', ['ionic'])
    .factory('Requester', function ($http, Constant, $q, UserPreference, toaster, SavePhotoTool, $timeout,AuthorizeService) {
        var req = {};

        function getRequest(url, headerStatus, params) {
            var defer = $q.defer();
            var cookie = UserPreference.get('IOSCookies');

            cordova.plugin.http.setCookie(url, cookie);
            var reqStr = headerStatus === 2 ? 'urlencoded' : 'json';
            cordova.plugin.http.setDataSerializer(reqStr);
             cordova.plugin.http.setRequestTimeout(10.0);
            var options = params ?getStringJson(params)  : {};
           // console.log(options);
            
            var header = headerStatus === 2 ? { "Content-Type": 'application/x-www-form-urlencoded' } : { "Content-Type": 'application/json' };
            cordova.plugin.http.get(url,
                options, header, function (response) {
                    // console.log('url:'+url);
                    //   console.log(response);
                    try {
                       // var data = JSON.parse(response.data);
                      var data =  JSON.parse(response.data);
                      console.log(data);
                        var loginModel = {
                            //remember: true,
                            username: UserPreference.get('username', '')?UserPreference.get('username', ''):UserPreference.get('account', ''),
                            password: UserPreference.get('password', '')
                        };
                        if(data.result==false&&d(data.code==-1||code==-100)&&loginModel.password&&loginModel.username){ 
                            AuthorizeService.logining = false; 
                            AuthorizeService.login(loginModel,function(){},function(){});
                        }
                        $timeout(function () {
                            defer.resolve(data);
                        });
                    } catch (error) {
                        console.error("JSON parsing error");
                    }
                }, function (error) {
                    console.log('=======get error======='+url);
                    console.log(error);
                    var status = JSON.parse(error.status);
                    if (status) {
                        
                        SavePhotoTool.interceptors(status);
                    }
                    $timeout(function () {
                        defer.reject(error);
                    });
                });
            return defer.promise;
        }

        function postRequest(url, headerStatus, params) {
            var defer = $q.defer();
            var cookie = UserPreference.get('IOSCookies');
            cordova.plugin.http.setCookie(url, cookie);
            cordova.plugin.http.setRequestTimeout(10.0);
            var reqStr = headerStatus == 2 ? 'urlencoded' : 'utf8';
            cordova.plugin.http.setDataSerializer(reqStr);
            var options = params?headerStatus == 2? getStringJson(params):JSON.stringify(params) :{};
            var header = headerStatus == 2 ? { "Content-Type": 'application/x-www-form-urlencoded' } : { "Content-Type": 'application/json' };
            cordova.plugin.http.post(url,
                options, header, function (response) {

                    try {
                        var data = JSON.parse(response.data);
                        var loginModel = {
                            //remember: true,
                            username: UserPreference.get('username', '')?UserPreference.get('username', ''):UserPreference.get('account', ''),
                            password: UserPreference.get('password', '')
                        };
                        
                        if(data.result==false&&(data.code==-1||code==-100)&&loginModel.password&&loginModel.username){ 
                            AuthorizeService.logining = false;
                            AuthorizeService.login(loginModel,function(){},function(){});
                        }
                        $timeout(function () {
                            defer.resolve(data);
                        });
                    } catch (error) {
                        //console.error("JSON parsing error");
                    }
                }, function (error) {
                    // console.log('=======post error=======');
                    // console.log(error);
                    var status = JSON.parse(error.status);
                    if (status) {
                        SavePhotoTool.interceptors(status);
                    }
                    $timeout(function () {
                        defer.reject(error);
                    });
                });
            return defer.promise;
        }

         /**
         * 文件形式上传图片 单个
         */
        req.uploadPictureByFileType = function (file,index) {
            var fd = new FormData();
            if(!index) index=0;
            fd.append('file', file);
            fd.append('filename', 'image'+index*3+'.jpeg');
            
            var url = Constant.ServerUrl.substr(0, Constant.ServerUrl.length - 4) + '/admin/fw/common/upload';
            if(iOSDevice()){
             var defer = $q.defer();
            cordova.plugin.http.setDataSerializer('multipart');
            cordova.plugin.http.setRequestTimeout(12.0);
           
            var header =  { "Content-Type": 'multipart/form-data' } ;
            var options  = {
                method:'post',
                data:fd,
                headers:header,
                responseType:'json',
                timeout:12
              }; 
            
            cordova.plugin.http.sendRequest(url,options,function(response){
                try {
                    var data =  response.data;//JSON.parse(response.data);   
                    $timeout(function () {
                        defer.resolve(data);
                    });
                } catch (error) {
                    console.error("JSON parsing error");
                }
            },function(error){
                
                var status = JSON.parse(error.status);
                if (status) {

                    SavePhotoTool.interceptors(status);
                }
                $rootScope.$broadcast(BroadCast.CONNECT_ERROR, undefined);
                $timeout(function () {
                    defer.reject(error);
                });
            });
            return defer.promise;
              

            }else{
                return $http({
                    method: 'POST',
                    url: url,
                    data: fd,
                    headers: {
                        'Content-Type': undefined, //' multipart/form-data',
                        // 'Content-Disposition' :"inline;filename=hello.jpg "
                    },
                    transformRequest: angular.identity
                }).then(function (response) {
                    //上传成功的操作
                    return response.data;
                }, function (error) {
                    console.log('failure');
                    return $q.reject(error);
                });
            }
           
        };



        /**
         * 设置推送消息已读
         * @param noticeId
         * @returns {*}
         */
        req.setNoticeRead = function (noticeId) {
            if (iOSDevice()) {
                var options = {
                    noticeId: noticeId
                };
                return postRequest(Constant.ServerUrl + "/notice/readSign", 2, options);
            } else {
                return $http({
                    method: "post",
                    url: Constant.ServerUrl + "/notice/readSign",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: CustomParam({
                        noticeId: noticeId
                    })
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };


        /**
         * 获取设备概况
         * @returns {*}
         */
        req.getGeneralInfo = function () {
            if (iOSDevice()) {
                return getRequest(Constant.ServerUrl + "/terminal/general", 1);
            } else {
                return $http.get(Constant.ServerUrl + "/terminal/general")
                    .then(function (response) {
                        return response.data;
                    }, function (error) {
                        return $q.reject(error);
                    });
            }
        };

        /**
         * 查询最近设备使用数
         * @returns {*}
         */
        req.getUsedTerminalsRecently = function () {
            if (iOSDevice()) {
               return getRequest(Constant.ServerUrl + "/terminal/terminalUsedCountDayly",1);
            } else {
                return $http.get(Constant.ServerUrl + "/terminal/terminalUsedCountDayly")
                    .then(function (response) {
                        return response.data;
                    }, function (error) {
                        return $q.reject(error);
                    });
            }

        };


        /**
         * 获取地区设备分布
         * @param area
         * @returns {*}
         */
        req.getTerminalsLocation = function (area) {
            if (iOSDevice()) {
                var options = {
                    areaCode: area,
                    page: 1,
                    rows: 999
                };
                return getRequest(Constant.ServerUrl + "/terminal/info/distribution", 1, options);
            } else {
                return $http.get(Constant.ServerUrl + "/terminal/info/distribution", {
                    params: {
                        areaCode: area,
                        page: 1,
                        rows: 999
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 学校教师使用时长排行
         * @param area
         * @param lastDays
         * @returns {*}
         */
        req.getTeacherUseTimeRank = function (area, lastDays) {
            if (iOSDevice()) {
                var options = {
                    areaCode: area,
                    lastDays: lastDays,
                    page: 1,
                    rows: 50
                };
                return getRequest(Constant.ServerUrl + "/terminal/rank/teacherAvgTime", 1, options);

            } else {
                return $http.get(Constant.ServerUrl + "/terminal/rank/teacherAvgTime", {
                    params: {
                        areaCode: area,
                        lastDays: lastDays,
                        page: 1,
                        rows: 50
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };


        /**
         * 学校设备使用时长排行
         * @param area
         * @param lastDays
         * @returns {*}
         */
        req.getTerminalUseTimeRank = function (area, lastDays) {
            if (iOSDevice()) {
                var options = {
                    areaCode: area,
                    lastDays: lastDays,
                    page: 1,
                    rows: 50
                };
                return getRequest(Constant.ServerUrl + "/terminal/rank/schoolAvgTime", 1, options);

            } else {
                return $http.get(Constant.ServerUrl + "/terminal/rank/schoolAvgTime", {
                    params: {
                        areaCode: area,
                        lastDays: lastDays,
                        page: 1,
                        rows: 50
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };


        /**
         * 学校设备使用率排行
         * @param type
         * @param area
         * @param lastDays
         * @returns {*}
         */
        req.getTerminalUsageRank = function (type, area, lastDays) {
            if (iOSDevice()) {
                var options = {
                    rankCondition: type,
                    areaCode: area,
                    lastDays: lastDays,
                    page: 1,
                    rows: 50
                };
                return getRequest(Constant.ServerUrl + "/terminal/rank/schoolUseRate", 1, options);
            } else {
                return $http.get(Constant.ServerUrl + "/terminal/rank/schoolUseRate", {
                    params: {
                        rankCondition: type,
                        areaCode: area,
                        lastDays: lastDays,
                        page: 1,
                        rows: 50
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };


        /**
         * 获取学校列表
         * @param county
         * @returns {*}
         */
        req.getSchools = function (county) {
            if (iOSDevice()) {
                var options = {
                    county: county,
                    page: 1,
                    rows: 999
                };
                return getRequest(Constant.ServerUrl + "/school", 1, options);
            } else {
                return $http.get(Constant.ServerUrl + "/school", {
                    params: {
                        county: county,
                        page: 1,
                        rows: 999
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 获取省市区列表
         * @param code
         * @returns {*}
         */
        req.getArea = function (code) {
            if (iOSDevice()) {
                var options = {
                    code: code
                };
                return getRequest(Constant.ServerUrl + "/area/get", 1, options);

            } else {
                return $http.get(Constant.ServerUrl + "/area/get", {
                    params: {
                        code: code
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 教师使用统计
         * @param statCondition
         * @returns {*}
         */
        req.getTeacherUseRank = function (statCondition) {
            if (iOSDevice()) {
                var options = {
                    statCondition: statCondition
                };
                return getRequest(Constant.ServerUrl + "/terminal/teacherTimeRank", 1, options);

            } else {
                return $http.get(Constant.ServerUrl + "/terminal/teacherTimeRank", {
                    params: {
                        statCondition: statCondition
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 班级使用统计
         * @param statCondition
         * @returns {*}
         */
        req.getClassUseRank = function (statCondition) {
            if (iOSDevice()) {
                var options = {
                    statCondition: statCondition
                };
                return getRequest(Constant.ServerUrl + "/terminal/classTimeRank", 1, options);

            } else {
                return $http.get(Constant.ServerUrl + "/terminal/classTimeRank", {
                    params: {
                        statCondition: statCondition
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 设备使用明细-查看设备列表
         * @param page
         * @param schoolId
         * @returns {*}
         */
        req.getTerminalList = function (page, schoolId) {

            if (iOSDevice()) {
                var options = {
                    page: page,
                    row: Constant.reqLimit,
                    schoolId: schoolId
                };
                return getRequest(Constant.ServerUrl + "/terminal/UseDetail/terminal/info",1, options);
            } else {
                return $http.get(Constant.ServerUrl + "/terminal/UseDetail/terminal/info", {
                    params: {
                        page: page,
                        row: Constant.reqLimit,
                        schoolId: schoolId
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 设备使用明细-查看设备使用时长
         * @param days
         * @param sn
         * @returns {*}
         */
        req.getTerminalUseTime = function (days, sn) {
            if (iOSDevice()) {
                var options = {
                    days: days,
                    sn: sn
                };
                return getRequest(Constant.ServerUrl + "/terminal/UseDetail/times-every-day", 1, options);

            } else {
                return $http.get(Constant.ServerUrl + "/terminal/UseDetail/times-every-day", {
                    params: {
                        days: days,
                        sn: sn
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 设备使用明细-查看设备截图
         * @param page
         * @param day
         * @param sn
         * @returns {*}
         */
        req.getTerminalScreenShot = function (page, day, sn) {
            if (iOSDevice()) {
                var options = {
                    day: day,
                    sn: sn,
                    page: page,
                    row: Constant.reqLimit
                };
                return getRequest(Constant.ServerUrl + "/terminal/UseDetail/snapshot", 1, options);
            } else {
                return $http.get(Constant.ServerUrl + "/terminal/UseDetail/snapshot", {
                    params: {
                        day: day,
                        sn: sn,
                        page: page,
                        row: Constant.reqLimit
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return error.data;
                });
            }

        };

        /**
         * 获取在线设备列表
         * @returns {*}
         */
        req.getOnlineTerminalList = function () {
            if (iOSDevice()) {
                var newDate = getCurrentTimeOfSecond();
                var params = {
                    state:'RUNNING',
                    i:newDate,
                    page:'1',
                    rows:'500'
                };
                return getRequest(Constant.ServerUrl + "/terminal", 1,params);
            } else {
                return $http.get(Constant.ServerUrl + "/terminal/?state=RUNNING" + '&i=' + new Date()).then(function (response) {
                    return response.data;
                }, function (error) {
                    return error.data;
                });
            }

        };

        /**
         * 将指定设备关机
         * @param tid
         * @returns {*}
         */
        req.turnOffTerminals = function (tid) {
            if (iOSDevice()) {
                var options = {
                    terminalIds: tid
                };
                return postRequest(Constant.ServerUrl + "/terminal/shutdown", 2, options);
            } else {
                return $http({
                    method: "post",
                    url: Constant.ServerUrl + "/terminal/shutdown",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: CustomParam({
                        terminalIds: tid
                    })
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };
        /**
         * 获取消息通知未读数
         * @param newsId
         */
        req.getNoticeNotReadNumber = function () {
            if (iOSDevice()) {
                return getRequest(Constant.ServerUrl + "/notice/unreadTotal/byAppAdmin", 1);
                // var defer = $q.defer();
                // var cookie = UserPreference.get('IOSCookies');
                // cordova.plugin.http.setCookie(Constant.ServerUrl + "/notice/unreadTotal/byAppAdmin", cookie);
                // cordova.plugin.http.setDataSerializer('json');
                // var options = {};
                // var header = { "Content-Type": 'application/json' };
                // cordova.plugin.http.get(Constant.ServerUrl + "/notice/unreadTotal/byAppAdmin",
                //     options, header, function (response) { 

                //         try { 
                //             var data = JSON.parse(response.data); 
                //             $timeout(function () {
                //                  defer.resolve(data);
                //             });
                //         } catch (error) {                
                //             console.error("JSON parsing error");
                //         }
                //     }, function (error) {

                //         var status = JSON.parse(error.status);
                //         if (status) {
                //             SavePhotoTool.interceptors(status);
                //         }
                //         $timeout(function () {
                //             defer.reject(error);
                //         });
                //     });            
                // return defer.promise;
            } else {
                return $http.get(Constant.ServerUrl + "/notice/unreadTotal/byAppAdmin").then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 获取校园风采未读
         * @returns {*}
         */
        req.getCampusUnread = function () {
            var params = {};
            if (String(UserPreference.getObject('user').rolename) === Constant.USER_ROLES.PARENT) {
                params.stuId = UserPreference.get('DefaultChildID');
            }
            if (iOSDevice()) {
                var defer = $q.defer();
                // var tool = $injector.get('SavePhotoTool');
                var cookie = UserPreference.get('IOSCookies');
               // cordova.plugin.http.setCookie(Constant.ServerUrl + "/campusview/unreadTotalByList", cookie);
                cordova.plugin.http.setDataSerializer('json');
                var options = getStringJson(params);
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/campusview/unreadTotalByList",
                    options, header, function (response) {
                        try {
                            var data = JSON.parse(response.data);
                            $timeout(function () {
                                defer.resolve(data);
                            });
                        } catch (error) {
                            console.error("JSON parsing error");
                        }
                    }, function (error) {
                        var status = JSON.parse(error.status);
                        if (status) {
                            SavePhotoTool.interceptors(status);
                        }
                        $timeout(function () {
                            defer.reject(error);
                        });
                    });

                return defer.promise;

            } else {
                return $http.get(Constant.ServerUrl + "/campusview/unreadTotalByList", {
                    params: params
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 校园风采添加评论
         * @param newsId
         * @param comment
         * @returns {*}
         */
        req.addCampusComment = function (newsId, comment) {
            if (iOSDevice()) {
                var options = {
                    newsId: newsId,
                    comments: comment
                };
                return postRequest(Constant.ServerUrl + "/campus/add-comments", 1, options);

            } else {
                return $http.post(Constant.ServerUrl + "/campus/add-comments", {
                    newsId: newsId,
                    comments: comment
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 校园风采删除评论
         * @param commentId
         * @returns {*}
         */
        req.deleteCampusComment = function (commentId) {
            if (iOSDevice()) {
                var options = {
                    id: commentId
                };
                return getRequest(Constant.ServerUrl + "/campus/deleteComments", 1, options);
            } else {
                return $http.get(Constant.ServerUrl + "/campus/deleteComments", {
                    params: {
                        id: commentId
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 校园风采点赞
         */
        req.favorCampus = function (newsId) {
            if (iOSDevice()) {
                var options = {
                    newsId: newsId
                };
                return postRequest(Constant.ServerUrl + "/campus/add-praise", 1, options);

            } else {
                return $http.post(Constant.ServerUrl + "/campus/add-praise", {
                    newsId: newsId
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 校园风采取消点赞
         */
        req.unfavorCampus = function (newsId) {
            if (iOSDevice()) {
                var options = { id: newsId };
                return getRequest(Constant.ServerUrl + "/campus/deletePraise", 1, options);
            } else {
                return $http.get(Constant.ServerUrl + "/campus/deletePraise", {
                    params: {
                        id: newsId
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 校园风采获取点赞列表
         * @param newsId
         */
        req.getCampusNewsFavorList = function (newsId) {
            if (iOSDevice()) {
                var options = { newsId: newsId };
                return getRequest(Constant.ServerUrl + "/campus/getPraiseList", 1, options);

            } else {
                return $http.get(Constant.ServerUrl + "/campus/getPraiseList", {
                    params: {
                        newsId: newsId
                    }

                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };


        /**
         * 校园风采获取评论列表
         * @param newsId
         * @param page
         * @returns {*}
         */
        req.getCampusNewsCommentList = function (newsId, page) {
            var params = {
                newsId: newsId,
                page: page,
                rows: Constant.reqLimit
            };
            if (iOSDevice()) {
                return getRequest(Constant.ServerUrl + "/campus/getCommentsList", 1, params);
            } else {
                return $http.get(Constant.ServerUrl + "/campus/getCommentsList", {
                    params: params
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * by wl
         * 发表用户反馈
         * content 发表的内容  若为图片类型 则为base 64字符串形式
         * contentType : 消息类型
         */
        req.publishUserFeedback = function (content, contentType) {
            var phoneSy = ionic.Platform.isIOS() ? 'iOS' : 'android';
            if (iOSDevice()) {
                var options = {
                    appVersion: Constant.version,
                    content: content,
                    phoneOs: phoneSy,
                    contentType: contentType
                };
                return postRequest(Constant.ServerUrl + "/suggestions/public", 1, options);
            } else {
                return $http.post(Constant.ServerUrl + "/suggestions/public", {
                    appVersion: Constant.version,
                    content: content,
                    phoneOs: phoneSy,
                    contentType: contentType
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 标记消息为已读 用户反馈
         * newsId 消息Id
         */
        req.fixNewsReadStatus = function (newsId) {
            if (iOSDevice()) {
                var options = { id: newsId };
                return postRequest(Constant.ServerUrl + "/suggestions/signRead", 1, options);
            } else {
                return $http.post(Constant.ServerUrl + "/suggestions/signRead", {
                    id: newsId
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 获取用户反馈消息列表
         */
        req.getFeedbackDetailList = function () {
            if (iOSDevice()) {
                var options = {
                    endDate: '',
                    startDate: ''
                };
                return getRequest(Constant.ServerUrl + "/suggestions/detail", 1, options);

            } else {
                return $http.get(Constant.ServerUrl + "/suggestions/detail", {
                    params: {
                        endDate: '',
                        startDate: ''
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 获取用户反馈 未读数
         */
        req.getNoreadNewsNumber = function () {
            if (iOSDevice()) {
                return getRequest(Constant.ServerUrl + "/suggestions/getUnreadReplySize", 1);

            } else {
                return $http.get(Constant.ServerUrl + "/suggestions/getUnreadReplySize", {
                    params: {}

                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 班级评比任务列表
         * @param endDate 结束时间
         * @param startDate 开始时间
         * @param page 页面
         */
        req.getClassVoteTasktList = function (page, startDate, endDate) {
            if (iOSDevice()) {
                var options = {
                    page: page,
                    rows: Constant.reqLimit,
                    startDate: startDate,
                    endDate: endDate
                };
                return getRequest(Constant.ServerUrl + "/classvote/list", 1, options);

            } else {
                return $http.get(Constant.ServerUrl + "/classvote/list", {
                    params: {
                        page: page,
                        rows: Constant.reqLimit,
                        startDate: startDate,
                        endDate: endDate
                    }

                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };
        /**
         * 班级评比任务列表 new(1.1.6)
         * @param endDate 结束时间
         * @param startDate 开始时间
         * @param page 页面
         */
        req.getClassVoteTasktList2 = function (page, startDate, endDate) {
            if (iOSDevice()) {
                var options = {
                    page: page,
                    rows: Constant.reqLimit,
                    startDate: startDate,
                    endDate: endDate
                };
                return getRequest(Constant.ServerUrl + "/classvote/list/byTerm", 1, options);

            } else {
                return $http.get(Constant.ServerUrl + "/classvote/list/byTerm", {
                    params: {
                        page: page,
                        rows: Constant.reqLimit,
                        startDate: startDate,
                        endDate: endDate
                    }

                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };
        /**
         * 添加班级评比任务
         * @param title 任务标题
         * @param startDate 开始时间
         * @param endDate 结束时间
         */
        req.addClassVoteTask = function (params) {
            if (iOSDevice()) {
                return postRequest(Constant.ServerUrl + "/classvote/add", 1, params);
            } else {
                return $http.post(Constant.ServerUrl + "/classvote/add", params)
                    .then(function (response) {
                        return response.data;
                    }, function (error) {
                        return $q.reject(error);
                    });
            }

        };

        /**
         * 删除班级评比任务
         * @param classvoteId 评比任务id
         */
        req.deleteClassVoteTask = function (classvoteId) {
            if (iOSDevice()) {
                var options = {
                    id: classvoteId
                };
                return postRequest(Constant.ServerUrl + "/classvote/delete", 2, options);

            } else {
                return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/classvote/delete",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: CustomParam({
                        id: classvoteId
                    })
                })
                    .then(function (response) {
                        return response.data;
                    }, function (error) {
                        return $q.reject(error);
                    });
            }

        };

        /**
         * 获取当前学期班级评比任务总数
         *
         */
        req.getCurrentSemesterVoteTaskCount = function () {
            if (iOSDevice()) {
                var defer = $q.defer();
                var cookie = UserPreference.get('IOSCookies');
              //  cordova.plugin.http.setCookie(Constant.ServerUrl + "/classvote/countByTerm", cookie);
                cordova.plugin.http.setDataSerializer('json');
                var options = {};
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/classvote/countByTerm",
                    options, header, function (response) {
                        var data = JSON.parse(response.data);
                        try {
                            $timeout(function () {
                                defer.resolve(data);
                            });
                        } catch (error) {

                            console.error("JSON parsing error");
                        }
                    }, function (error) {
                        var status = JSON.parse(error.status);
                        if (status) {
                            SavePhotoTool.interceptors(status);
                        }
                        $timeout(function () {
                            defer.reject(error);
                        });
                    });

                return defer.promise;
            } else {
               
                return $http.get(Constant.ServerUrl + "/classvote/countByTerm", {
                    params: {

                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 获取年级列表
         */
        req.getGradeList = function () {
            if (iOSDevice()) {
                return getRequest(Constant.ServerUrl + "/classvote/grade/list", 1);
            } else {
                return $http.get(Constant.ServerUrl + "/classvote/grade/list", {
                    params: {

                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };


        /**
         * 班级评比结果列表
         * @param classvoteId 评比任务id
         * @param gradeId 年级id 为空时默认选择全部年级
         */
        req.getClassVoteReultList = function (classvoteId, gradeName, page) {
            if (iOSDevice()) {
                var options = {
                    gradeName: gradeName,
                    page: page,
                    rows: 20
                };
                return getRequest(Constant.ServerUrl + "/classvote/detail/" + classvoteId, 1, options);
            } else {
                return $http.get(Constant.ServerUrl + "/classvote/detail/" + classvoteId, {
                    params: {
                        gradeName: gradeName,
                        page: page,
                        rows: 20
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };


        /**
         * 添加评分结果
         * @param classId 班级id
         * @param classvoteDetailId 评比详情id
         * @param classvoteItem 评比维度 数组[{id: '评比维度id',score:'分数'}]
         */
        req.addClassScoreResult = function (classId, classvoteDetailId, classvoteItem) {
            if (iOSDevice()) {
                var options = {
                    classId: classId,
                    classvoteDetailId: classvoteDetailId,
                    classvoteItem: classvoteItem
                };
                return postRequest(Constant.ServerUrl + "/classvote/result/add", 1, options);

            } else {
                return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/classvote/result/add",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        classId: classId,
                        classvoteDetailId: classvoteDetailId,
                        classvoteItem: classvoteItem
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 修改班级评分结果
         *  @param classId 班级id
         *  @param classvoteDetailId 评比详情id
         *   @param classvoteItem 评比维度 数组[{id: '评比维度id',score:'分数'}]
         */
        req.fixClassScoreResult = function (classId, classvoteDetailId, classvoteItem) {
            if (iOSDevice()) {
                var options = {
                    classId: classId,
                    classvoteDetailId: classvoteDetailId,
                    classvoteItem: classvoteItem
                };
                return postRequest(Constant.ServerUrl + "/classvote/result/alter", 1, options);

            } else {
                return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/classvote/result/alter",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        classId: classId,
                        classvoteDetailId: classvoteDetailId,
                        classvoteItem: classvoteItem
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 查询评比维度
         */
        req.selectVoteItem = function () {
            if (iOSDevice()) {
                return getRequest(Constant.ServerUrl + "/classvote/item/list", 1);
            } else {
                return $http.get(Constant.ServerUrl + "/classvote/item/list").then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 版本说明
         * @param appType 版本类型 0用户app 1管理app
         * @param version 版本号
         */
        req.getCurrentVersionIntroduce = function () {
            var os = ionic.Platform.isIOS() ? 'ios' : 'android';
            if (iOSDevice()) {
                var options = {
                    appType: 1,
                    version: Constant.version,
                    osType: os
                };
                return getRequest(Constant.ServerUrl + "/checkversion/describe", 1, options);
            } else {
                return $http.get(Constant.ServerUrl + "/checkversion/describe", {
                    params: {
                        appType: 1,
                        version: Constant.version,
                        osType: os
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };


        /**
         * 报修记录列表
         * @param condition 0,全部；1，待解决；2，已关闭
         * @param isManage true管理列表；false,我的列表
         * @param page 当前页面
         */

        req.getRepairRecordList = function (condition, page) {
            if (iOSDevice()) {
                var options = {
                    condition:"1",
                    isManage: true,
                    row:String(Constant.reqLimit) ,
                    page: String(page)
                };
                return getRequest(Constant.ServerUrl + "/maintainOrders", 1, options);
            } else {
                return $http.get(Constant.ServerUrl + "/maintainOrders", {
                    params: {
                        condition: condition,
                        isManage: true,
                        row: Constant.reqLimit,
                        page: page
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 报修记录详情
         * @param repairRecordId  报修记录id
         */
        req.getRepairRecordDetail = function (repairRecordId) {
            if (iOSDevice()) {
                return getRequest(Constant.ServerUrl + "/maintainOrders/" + repairRecordId, 1);

            } else {
                return $http.get(Constant.ServerUrl + "/maintainOrders/" + repairRecordId, {
                    params: {

                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 添加进度处理
         * @param repairRecordId 报修记录id
         * @param orderProgress 1:已提交;2:管理员已查看;4:正在处理;2^30:已关闭;maxinteger:已取消
         * @param progressDescribe 进度描述
         * @param manufacturersContactId 厂家id
         */

        req.addRepairProgress = function (repairRecordId, orderProgress, progressDescribe, manufacturersContactId) {
            if (iOSDevice()) {
                var options = {
                    orderProgress: orderProgress,
                    progressDescribe: progressDescribe,
                    manufacturersContactId: manufacturersContactId
                };
                return postRequest(Constant.ServerUrl + "/maintainOrders/" + repairRecordId + "/progress", 1, options);
            } else {
                return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/maintainOrders/" + repairRecordId + "/progress",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        orderProgress: orderProgress,
                        progressDescribe: progressDescribe,
                        manufacturersContactId: manufacturersContactId
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 获取厂家列表
         */
        req.getManufacturesList = function () {
            if (iOSDevice()) {
                return getRequest(Constant.ServerUrl + "/manufacturers/getList", 1);
            } else {
                return $http.get(Constant.ServerUrl + "/manufacturers/getList", {

                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 获取售后人员列表
         * @param factoryId
         */
        req.getManufacturesMermberList = function (factoryId) {
            if (iOSDevice()) {
                return getRequest(Constant.ServerUrl + "/manufacturers/getContactByManufacturerId/" + factoryId, 1);

            } else {
                return $http.get(Constant.ServerUrl + "/manufacturers/getContactByManufacturerId/" + factoryId, {

                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 添加报修记录
         * @param maintainImgs 故障图片 array
         * @param orderDescribe 故障描述
         * @param terminalAddress	设备所在地址
         */
        req.addEquipmentRepairRecord = function (params) {
            if (iOSDevice()) {
                return postRequest(Constant.ServerUrl + "/maintainOrders", 1, params);
            } else {
                return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/maintainOrders",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: params
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 二维码 地址打开
         */
        req.getAssetsIdByScan = function (url) {
            if (iOSDevice()) {
                return getRequest(url, 1);

            } else {
                return $http({
                    method: 'get',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 添加资产申领进度（管理端）
         * @param orderId 资产申领单id
         * @param orderProgress 进度
         * @param progressDescribe 原因
         */
        req.addAssetsApplyProgress = function (orderId, orderProgress, fixApplyCount, progressDescribe) {
            if (iOSDevice()) {
                var options = {
                    orderId: orderId,
                    orderProgress: orderProgress,
                    param1: fixApplyCount,
                    progressDescribe: progressDescribe
                };
                return postRequest(Constant.ServerUrl + "/asset/applyProgress", 1, options);

            } else {
                return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/asset/applyProgress",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        orderId: orderId,
                        orderProgress: orderProgress,
                        param1: fixApplyCount,
                        progressDescribe: progressDescribe
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 资产申领详情
         * @param id 资产id
         */
        req.assetsApplyDetail = function (id) {
            if (iOSDevice()) {
                return getRequest(Constant.ServerUrl + "/assets/" + id, 1);
            } else {
                return $http.get(Constant.ServerUrl + "/assets/" + id, {
                    params: {

                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 资产审批列表 （管理端）
         */
        req.manageAssetsApplyList = function (page) {
            if (iOSDevice()) {
                var options = {
                    row: Constant.reqLimit,
                    page: page
                };
                return getRequest(Constant.ServerUrl + "/manage/assets", 1, options);

            } else {
                return $http.get(Constant.ServerUrl + "/manage/assets", {
                    params: {
                        row: Constant.reqLimit,
                        page: page
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 获取资产未读数
         *
         */
        req.getAssetsUnread = function () {
            if (iOSDevice()) {
               return getRequest(Constant.ServerUrl + "/manage/assets/getUnreadSize");
            } else {
                return $http.get(Constant.ServerUrl + "/manage/assets/getUnreadSize", {
                    params: {

                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };
        /**
         * 查询资产申领的id
         * @param assetsId 领单id
         */
        req.queryAssetsApplyIds = function (assetsId) {
            if (iOSDevice()) {
                return getRequest(Constant.ServerUrl + "/asset/getApplyIssue", 1, {
                    id: assetsId
                });
            } else {
                return $http.get(Constant.ServerUrl + "/asset/getApplyIssue", {
                    params: {
                        id: assetsId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 获取授权模块列表
         * @returns {*}
         */
        req.getToolkitList = function () {
            if (iOSDevice()) {
                return getRequest(Constant.ServerUrl + "/toolkit/list", 1);

            } else {
                return $http.get(Constant.ServerUrl + "/toolkit/list").then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        req.requestToolkit = function (phone, appId) {
            if (iOSDevice()) {
                return postRequest(Constant.ServerUrl + "/toolkit/apply", 2,{
                    phone: phone,
                    appId: String(appId)
                });

            } else {
                return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/toolkit/apply",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: CustomParam({
                        phone: phone,
                        appId: appId
                    })
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };
        /** 获取审批管理列表
         * @param schoolId 学校Id
         * @param progress 状态: 1通过 2审批中 3不通过 全部是传空
         */
        req.getApprovalManageList = function (page, schoolId, progress) {
            if (iOSDevice()) {
                var options = {
                    rows: Constant.reqLimit,
                    page: page,
                    progress: progress,
                    schoolId: schoolId
                };
                return getRequest(Constant.ServerUrl + "/archive/admin/list", 1, options);
            } else {
                return $http.get(Constant.ServerUrl + "/archive/admin/list", {
                    params: {
                        rows: Constant.reqLimit,
                        page: page,
                        progress: progress,
                        schoolId: schoolId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };



        /**
         * 获取申请的详细信息
         * @param archiveId 申请的Id
         */
        req.getApprovalManageDetail = function (archiveId) {
            if (iOSDevice()) {
                return getRequest(Constant.ServerUrl + "/archive/admin/detail", 1, {
                    archiveId: archiveId
                });

            } else {
                return $http.get(Constant.ServerUrl + "/archive/admin/detail", {
                    params: {
                        archiveId: archiveId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 检测是否过期 fasle为未过期 true过期
         */
        req.checkAuthOrAccountIsOverdue = function () {
            if (iOSDevice()) {
                var defer = $q.defer();
                var cookie = UserPreference.get('IOSCookies');
              //  cordova.plugin.http.setCookie(Constant.ServerUrl + "/checkAuthExpireNew", cookie);
                cordova.plugin.http.setDataSerializer('json');
                var options = {};
                var header = { "Content-Type": 'application/json' };
                cordova.plugin.http.get(Constant.ServerUrl + "/checkAuthExpireNew",
                    options, header, function (response) {
                        var data = JSON.parse(response.data);
                        try {
                            $timeout(function () {
                                defer.resolve(data);
                            });
                        } catch (error) {

                            console.error("JSON parsing error");
                        }
                    }, function (error) {

                        var status = JSON.parse(error.status);
                        console.log('check auth error');
                        console.log(error);
                        if (status) {
                            SavePhotoTool.interceptors(status);
                        }
                        $timeout(function () {
                            defer.reject(error);
                        });
                    });

                return defer.promise;
            } else {
                return $http.get(Constant.ServerUrl + "/checkAuthExpireNew", {
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };



        //应急演练
        /**
         * 演练记录列表
         * @param 
         */
        req.getDrillRecordList = function (page) {
            if (iOSDevice()) {
                return getRequest(Constant.ServerUrl + "/drillRecords/list/byDrillDefine", 1, {
                    page: page,
                    rows: 10
                });
            } else {
                return $http.get(Constant.ServerUrl + "/drillRecords/list/byDrillDefine", {
                    params: {
                        page: page,
                        rows: 10
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 演练详情 -查看详情
         * @param itemId
         */
        req.getDrillDetail = function (itemId) {
            if (iOSDevice()) {
                return getRequest(Constant.ServerUrl + "/drillRecords/detail", 1, {
                    id: itemId
                });

            } else {
                return $http.get(Constant.ServerUrl + "/drillRecords/detail", {
                    params: {
                        id: itemId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 新增记录
         * @param tempDrillDefineId 演练记录id
         * @param remarks 演练记录说明
         * @param strImageUrls 图片数组
         */

        req.addDrillRecord = function (drillId, selected) {
            if (iOSDevice()) {
                var options = {
                    tempDrillDefineId: drillId,
                    remarks: selected.remarks,
                    picdatas: selected.picdatas,
                    strImageUrls: ''
                };
                return postRequest(Constant.ServerUrl + "/drillRecords/save/byDrillRecords", 1, options);

            } else {
                return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/drillRecords/save/byDrillRecords",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        tempDrillDefineId: drillId,
                        remarks: selected.remarks,
                        picdatas: selected.picdatas,
                        strImageUrls: ''
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /***
         * 演练记录详情
         * @param drillDefineId 演练id
         * @param page  分页
         */

        req.getDrillRedordDetail = function (drillDefineId, page) {
            if (iOSDevice()) {
                var options = {
                    drillDefineId: drillDefineId,
                    page: page,
                    rows: 5,
                };
                return getRequest(Constant.ServerUrl + "/drillRecords/list/byDrilRecords", 1, options);
            } else {
                return $http.get(Constant.ServerUrl + "/drillRecords/list/byDrilRecords", {
                    params: {
                        drillDefineId: drillDefineId,
                        page: page,
                        rows: 5,
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 查看演练方案
         * 
         */
        req.getDrillProgramList = function () {
            if (iOSDevice()) {
                return getRequest(Constant.ServerUrl + "/drillRecords/drillPlanList", 1);
            } else {
                return $http.get(Constant.ServerUrl + "/drillRecords/drillPlanList", {
                    params: {

                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 查看方案详情
         * @param id 方案id
         */
        req.getDrillProgramDetail = function (programId) {
            if (iOSDevice()) {
                return getRequest(Constant.ServerUrl + "/drillRecords/drillPlan/detail", 1, {
                    id: programId
                });
            } else {
                return $http.get(Constant.ServerUrl + "/drillRecords/drillPlan/detail", {
                    params: {
                        id: programId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 发起演练
         * @param tempDrillPlanId 演练方案编号
         * @param drillTime 演练时间
         * @param address 演练地址
         * @param remarks 演练说明 
         */
        req.sendDrillRecord = function (tempDrillPlanId, selected) {
            if (iOSDevice()) {
                var options = {
                    tempDrillPlanId: tempDrillPlanId,
                    drillTime: selected.exerciseTime,
                    address: selected.address,
                    remarks: selected.content,
                    title: selected.title
                };
                return postRequest(Constant.ServerUrl + "/drillRecords/save/byDrillDefine", 1, options);
            } else {
                return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/drillRecords/save/byDrillDefine",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        tempDrillPlanId: tempDrillPlanId,
                        drillTime: selected.exerciseTime,
                        address: selected.address,
                        remarks: selected.content,
                        title: selected.title
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 修改演练时间（开始时间和结束时间）
         * @param drillDefineId 演练id
         * @param drillStartTime 开始时间
         * @param  drillEndTime 结束时间
         */
        req.fixedDrillStartDateOrEndDate = function (drillDefineId, drillStartTime, drillEndTime) {
            if (iOSDevice()) {
                var options = {
                    drillDefineId: drillDefineId,
                    drillStartTime: drillStartTime,
                    drillEndTime: drillEndTime
                };
                return getRequest(Constant.ServerUrl + "/drillRecords/saveDrillTime", 1, options);
            } else {
                return $http.get(Constant.ServerUrl + "/drillRecords/saveDrillTime", {
                    params: {
                        drillDefineId: drillDefineId,
                        drillStartTime: drillStartTime,
                        drillEndTime: drillEndTime
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        //隐患管理
        /**
         * 获取 隐患列表
         * @param status 状态
         */
        req.getHidenTroubleList = function (status, schoolId, page) {
            if (iOSDevice()) {
                var options = {
                    status: status,
                    schoolId: schoolId,
                    page: page,
                    rows: 10
                };
                return getRequest(Constant.ServerUrl + "/teacher/hidden-danger/admin/list", 1, options);

            } else {
                return $http.get(Constant.ServerUrl + "/teacher/hidden-danger/admin/list", {
                    params: {
                        status: status,
                        schoolId: schoolId,
                        page: page,
                        rows: 10
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 获取隐患详情
         */
        req.getHidenTroubleDetail = function (hiddenDangerId) {
            if (iOSDevice()) {
                return getRequest(Constant.ServerUrl + "/teacher/hidden-danger/detail", 1, {
                    hiddenDangerId: hiddenDangerId
                });

            } else {
                return $http.get(Constant.ServerUrl + "/teacher/hidden-danger/detail", {
                    params: {
                        hiddenDangerId: hiddenDangerId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 获取学校老师列表
         * @param schoolId 学校id
         */

        req.getSchoolTeachersList = function (schoolId) {
            if (iOSDevice()) {
                return getRequest(Constant.ServerUrl + "/teacher/hidden-danger/get-teachers", 1, {
                    schoolId: schoolId,
                    teacherName: ''
                });
            } else {
                return $http.get(Constant.ServerUrl + "/teacher/hidden-danger/get-teachers", {
                    params: {
                        schoolId: schoolId,
                        teacherName: ''
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 添加隐患
         * @param location 隐患位置
         * @param title 隐患标题
         * @param message 隐患信息
         * @param imagesBase64 图片数组
         */
        req.addHidenTrouble = function (selected, schoolId) {
            if (iOSDevice()) {
                var options = {
                    location: selected.location,
                    title: selected.title,
                    message: selected.message,
                    imagesBase64: selected.picdatas,
                    schoolId: schoolId
                };
                return postRequest(Constant.ServerUrl + "/teacher/hidden-danger/admin/upload/" + selected.selectManId, 1, options);
            } else {
                return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/teacher/hidden-danger/admin/upload/" + selected.selectManId,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        location: selected.location,
                        title: selected.title,
                        message: selected.message,
                        imagesBase64: selected.picdatas,
                        schoolId: schoolId
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 增加隐患处理进度
         * @param assignedPersonId 指派人
         * @param description //描述
         * @param hiddenDangerId //隐患id
         * @param status 状态
         */
        req.addHidenTroubleProgress = function (selected, itemId) {
            if (iOSDevice()) {
                var options = {
                    assignedPersonId: selected.selectManId,
                    description: selected.content,
                    hiddenDangerId: itemId
                };
                return postRequest(Constant.ServerUrl + "/teacher/hidden-danger/add-progress/" + selected.status, 1, options);
            } else {
                return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/teacher/hidden-danger/add-progress/" + selected.status,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        assignedPersonId: selected.selectManId,
                        description: selected.content,
                        hiddenDangerId: itemId
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };
        /**
         * 获取隐患数量
         */
        req.getHidenTroubleCount = function (schoolId) {
            if (iOSDevice()) {
                return getRequest(Constant.ServerUrl + "/teacher/hidden-danger/admin/list/count", 1, {
                    schoolId: schoolId
                });
            } else {
                return $http.get(Constant.ServerUrl + "/teacher/hidden-danger/admin/list/count", {
                    params: {
                        schoolId: schoolId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };


        //资产概况
        /**
         * 
         */
        req.getSchoolAssetsDetail = function () {
            if (iOSDevice()) {
                return getRequest(Constant.ServerUrl + "/asset/stat/general", 1);
            } else {
                return $http.get(Constant.ServerUrl + "/asset/stat/general", {
                    params: {

                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 学校资产排行
         */
        req.getSchoolAssetsRank = function () {
            if (iOSDevice()) {
                return getRequest(Constant.ServerUrl + "/asset/stat/getRankBySchl", 1);
            } else {
                return $http.get(Constant.ServerUrl + "/asset/stat/getRankBySchl", {
                    params: {

                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 厂家资产排行
         */
        req.getFactoryAssetsRank = function () {
            if (iOSDevice()) {
                return getRequest(Constant.ServerUrl + "/asset/stat/getRankByManufacturer", 1);
            } else {
                return $http.get(Constant.ServerUrl + "/asset/stat/getRankByManufacturer", {
                    params: {

                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };


        //查询疫情 今日概况
        req.getCovidSummary = function () {
            if (iOSDevice()) {
                return getRequest(Constant.ServerUrl + "/epidemic/record/count/class_level", 1);
            } else {
                return $http.get(Constant.ServerUrl + "/epidemic/record/count/class_level", {
                    params: {}
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        //查询疫情 体温异常名单
        req.getWarningList = function () {
            if (iOSDevice()) {
                return getRequest(Constant.ServerUrl + "/epidemic/alarm/list", 1, {
                    page: 1,
                    rows: 9999
                });
            } else {
                return $http.get(Constant.ServerUrl + "/epidemic/alarm/list", {
                    params: {
                        page: 1,
                        rows: 9999
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };


        //查询疫情 告警详情
        req.getWarningDetail = function (alarmId) {
            if (iOSDevice()) {
                return getRequest(Constant.ServerUrl + "/epidemic/alarm/details", 1, {
                    alarmId: alarmId
                });
            } else {
                return $http.get(Constant.ServerUrl + "/epidemic/alarm/details", {
                    params: {
                        alarmId: alarmId
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 疫情跟进
         */
        req.handleWarning = function (alarmId, status, description) {
            if (iOSDevice()) {
                return postRequest(Constant.ServerUrl + "/epidemic/alarm/save/progress", 2, {
                    alarmId: alarmId,
                    status: status,
                    description: description

                });
            } else {
                return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/epidemic/alarm/save/progress",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: CustomParam({
                        alarmId: alarmId,
                        status: status,
                        description: description

                    })
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 设置疫情告警已读
         */
        req.covidWarningRead = function () {
            if (iOSDevice()) {
                return getRequest(Constant.ServerUrl + "/epidemicAlarm/readSign", 1);
            } else {
                return $http.get(Constant.ServerUrl + "/epidemicAlarm/readSign", {
                    params: {}
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 获取学校所有班级列表
         */
        req.getSchoolAllClasses = function (schoolId) {
            var url = Constant.ServerUrl.substr(0, Constant.ServerUrl.length - 3) + 'api';
            if (iOSDevice()) {
                return getRequest(url + "/class", 1, {
                    schoolId: schoolId
                });
            } else {
                return $http.get(url + "/class", {
                    params: {
                        schoolId: schoolId
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };
        /**
        * 获取班级同学人像列表
        * @param classId 班级id
        * @param name 学生姓名
        */
        req.getFaceRecognitionClassList = function (classId, name) {
            if (iOSDevice()) {
                return getRequest(Constant.ServerUrl + "/faceRecognition/getOfClass", 1, {
                    classId: classId,
                    name: name
                });
            } else {
                return $http.get(Constant.ServerUrl + "/faceRecognition/getOfClass", {
                    params: {
                        classId: classId,
                        name: name
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };
        /**
         * 获取老师的个人人像
         * @param userId
         */
        req.getTeacherFaceRecognition = function (userId) {
            if (iOSDevice()) {
                return getRequest(Constant.ServerUrl + "/faceRecognition/getOne", 1, {
                    userId: userId,
                });
            } else {
                return $http.get(Constant.ServerUrl + "/faceRecognition/getOne", {
                    params: {
                        userId: userId,
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 获取学校老师人像采集接口列表
         */
        req.getTeacherPortraitList = function (schoolId, page) {
            if (iOSDevice()) {
                var options = {
                    teacherName: '',
                    page: 1,
                    rows: 500
                };
                return getRequest(Constant.ServerUrl + "/faceRecognition/getRecognitonTeacherList", 1, options);
            } else {
                return $http.get(Constant.ServerUrl + "/faceRecognition/getRecognitonTeacherList", {
                    params: {
                        teacherName: '',
                        page: 1,
                        rows: 500
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 上传人像
         */
        req.uploadUserPortrait = function (userId, base64Str) {
            if (iOSDevice()) {
                return postRequest(Constant.ServerUrl + "/faceRecognition/uploadImg", 1, {
                    userId: userId,
                    imgData: base64Str
                });
            } else {
                return $http({
                    method: 'post',
                    url: Constant.ServerUrl + "/faceRecognition/uploadImg",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        userId: userId,
                        imgData: base64Str
                    }
                }).then(function (response) {
                    return response.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }

        };

        /**
         * 获取电子班牌列表
         */
        req.getBanPaiList = function(schoolId,classId,sn){
            if (iOSDevice()) {
                var options =  {
                    schoolId: schoolId,
                    classId:classId?classId:'',
                    sn:sn?sn:''
                    
                };
                return getRequest(Constant.ServerUrl + "/adterminal/list", 1, options);
            } else {
                return $http.get(Constant.ServerUrl + "/adterminal/list", {
                    params: {
                        schoolId: schoolId,
                        classId:classId,
                        sn:sn,
                        page:1,
                        rows:1000
                        
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
            }
        };

        /**
         * 显示模式设置
         * @param adterminalIds 设备id字符串 逗号隔开
         * @param mode 1.班牌模式 2.校牌模式 3.通知模式 5.上课模式 6.场所模式 7.会议模式 8.考试模式 
         * @param contentType 0:文本 1图片
         */
        req.setDisplayModel = function(adterminalIds,selected){
            if(iOSDevice()){
                //console.log('adterminalIds:'+adterminalIds);
                return getRequest(Constant.ServerUrl + "/adterminal/a/set-sm", 1, {
                    adterminalIds:adterminalIds,
                    mode:selected.displayKey,
                    termTime:selected.noticeTime,
                    title:selected.title,
                    text:selected.content,
                    contentType:selected.noticeType,
                    imgUrl:selected.imageUrl
                });
             }else{
                return $http.get(Constant.ServerUrl + "/adterminal/a/set-sm", {
                    params: {
                        adterminalIds:adterminalIds,
                        mode:selected.displayKey,
                        termTime:selected.noticeTime,
                        title:selected.noticeType===0?selected.title:'',
                        text:selected.noticeType===0?selected.content:'',
                        contentType:selected.noticeType,
                        imgUrl:selected.noticeType===0?'':selected.imageUrl
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
               
             } 
        };

        /**
         * 锁定设置 二进制拼接 0表示不锁定不隐藏 1表示锁定隐藏
         * @param lockset 第一位表示学校班级信息 第二位表设计显示风格 第三位表示显示模式 第四位表示更多设置 第五位表示系统按钮
         * 例如 10001 表示学校班级信息和系统按钮隐藏 其余三个不隐藏
         */
        req.setLockModel = function(adterminalIds,lockset){
            if(iOSDevice()){
                return getRequest(Constant.ServerUrl + "/adterminal/lock", 1, {
                    adterminalIds:adterminalIds,
                    lockset:lockset
                });
             }else{
                return $http.get(Constant.ServerUrl + "/adterminal/lock", {
                    params: {
                      adterminalIds:adterminalIds,
                      lockset:lockset
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
              
             }
        };

        /**
         * 设备关机
         * @param adterminalIds  班牌ids 数组字符串
         */
        req.setEquipmentShutDown = function(adterminalIds){
             if(iOSDevice()){
                return getRequest(Constant.ServerUrl + "/adterminal/shutdown", 1, {
                    adterminalIds:adterminalIds
                });
             }else{
                return $http.get(Constant.ServerUrl + "/adterminal/shutdown", {
                    params: {
                      adterminalIds:adterminalIds,
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
             }
        };

        /**
         * 直播列表
         * @param liveDay 直播日期
         */
        req.getCloundLiveList = function(liveDay,page){
           if(iOSDevice()){
                return getRequest(Constant.ServerUrl + "/livevideo/course/list",1,{
                    liveDay:liveDay,
                    page:page,
                    rows:10
                  });
           }else{
            return $http.get(Constant.ServerUrl + "/livevideo/course/list", {
                params: {
                  liveDay:liveDay,
                  page:page,
                  rows:10
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
           }
        };

        /**
         * 保存编辑 添加
         * @param teacherName 演讲人
         * @param createDate 直播日期
         * @param startTime 开始时间
         * @param isReview 是否支持回放 1 支持 0 不支持
         * @param id 非必填 id存在时为编辑 反之为新增
         */
        req.cloundLiveEdit = function(selected,id){
           if(iOSDevice()){
            return postRequest(Constant.ServerUrl + "/livevideo/course/save", 1, {
                title: selected.title,
                speaker: selected.teacherName,
                liveDayStr:selected.createDate,
                startTimeStr:selected.startTime,
                endTimeStr:selected.endTime,
                islookBack:selected.isReview?'1':'0',
                //id:id
            });
           }else{
            return $http({
                method: 'post',
                url: Constant.ServerUrl + "/livevideo/course/save",
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    title: selected.title,
                    speaker: selected.teacherName,
                    liveDayStr:selected.createDate,
                    startTimeStr:selected.startTime,
                    endTimeStr:selected.endTime,
                    islookBack:selected.isReview?'1':'0',
                    id:id
                }
            }).then(function (response) {
                return response.data;
            }, function (error) {
                return $q.reject(error);
            });
           }
        };

        /**
         * 删除操作
         */
        req.cloundLiveDelete = function(id){
           if(iOSDevice()){
            return getRequest(Constant.ServerUrl + "/livevideo/course/delete",1,{
                id:id
              });
           }else{
            return $http.get(Constant.ServerUrl + "/livevideo/course/delete", {
                params: {
                  id:id
                }
            }).then(function (res) {
                return res.data;
            }, function (error) {
                return $q.reject(error);
            });
           }
        };
        /**
         * 查看直播详情
         */
        req.cloundLiveDetail = function(id){
              if(iOSDevice()){
                return getRequest(Constant.ServerUrl + "/livevideo/course/info",1,{
                    id:id
                  }); 
              }else{
                  console.log('id:'+id);
                return $http.get(Constant.ServerUrl + "/livevideo/course/info", {
                    params: {
                      id:id
                    }
                }).then(function (res) {
                    return res.data;
                }, function (error) {
                    return $q.reject(error);
                });
              }
        };


        return req;
    });
