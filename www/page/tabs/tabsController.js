angular.module('app.controllers')
    .controller('tabsCtrl', function ($scope, BroadCast, Constant, UserPreference, ChatService) {
        $scope.badge = 0;

        $scope.$on(BroadCast.BADGE_UPDATE, function (event, data) {
            if (data.type === 'im') {
                $scope.badge = data.count;
                $scope.$digest();
            } else {
                $scope.campus = data.count;
            }
        });


        var user = UserPreference.getObject('user');
        $scope.userRole = user.role;
        if (String($scope.userRole) === Constant.USER_ROLES.SCHOOL_ADMIN) {
            ChatService.init();
            $scope.schoolAdmin = true;
        } else {
            $scope.authAssetsManage = '';
        }

        $scope.authDeviceManage = user.authDeviceManage && user.authDeviceManage === 1; //设备授权
        $scope.authAssertManage = user.authAssertManage && user.authAssertManage === 1; //资产授权

        $scope.isCityManage = false;
        // $scope.$on("$ionicView.beforeEnter", function (event, data) {

        // });


    });