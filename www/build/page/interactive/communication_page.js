angular.module('app.controllers')
.controller('communicatePageCtrl', ['UserPreference', '$scope', '$ionicScrollDelegate', '$state', 'ChatService', 'BroadCast', 'Constant', '$ionicModal', '$ionicPopup', 'toaster', 'MESSAGES', '$rootScope', 'closePopupService', function (UserPreference, $scope, $ionicScrollDelegate, $state, ChatService, BroadCast, Constant, $ionicModal, $ionicPopup, toaster, MESSAGES, $rootScope, closePopupService) {
    $scope.contactView = false;
    $scope.switchView = function (contactV) {
        $scope.contactView = contactV;
        $ionicScrollDelegate.resize();
    };

    $scope.toggle = function (group, length) {
        if (length > 0) {
            group.show = !group.show;
            $ionicScrollDelegate.resize();
        }
    };
    $scope.sel = {};
    var UIPath = '';
    if (Constant.debugMode) UIPath = 'page/';
    $ionicModal.fromTemplateUrl(UIPath + 'interactive/add_conversation.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.modal = modal;
    });

    function initAddView() {
        $scope.selCount = 0;
        $scope.modalTitle = '发起聊天';
        for (var i = 0; i < $scope.addModeContactTree.length; i++) {
            for (var j = 0; j < $scope.addModeContactTree[i].list.length; j++) {
                $scope.addModeContactTree[i].list[j].isChecked = false;
            }
        }
        $scope.modal.show();
    }

    $scope.openModal = function () {
        $scope.addModeContactTree = $scope.ContactTree;
        initAddView();
    };
    $scope.closeModal = function () {
        $scope.modal.hide();
    };
    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function () {
        $scope.modal.remove();
    });

    $scope.saveConversation = function () {
        var arr = [];
        var name = '群聊:';
        for (var i = 0; i < $scope.addModeContactTree.length; i++) {
            for (var j = 0; j < $scope.addModeContactTree[i].list.length; j++) {
                var p = $scope.addModeContactTree[i].list[j];
                if (p.isChecked) {
                    arr.push(p.Member_Account);
                    if (arr.length < 2) {
                        name += p.Name;
                        name += ',';
                    }
                    else if (arr.length === 2)
                        name += p.Name;
                }
            }
        }
        if (arr.length === 1) {
            var info = ChatService.getMemberInfoFromMap(webim.SESSION_TYPE.C2C, arr[0], $scope.sel.class.key);
            var obj = {
                SessionType: webim.SESSION_TYPE.C2C,
                SessionId: arr[0],
                SessionNick: info.name,
                SessionImage: info.image
            };
            $scope.modal.hide();
            $state.go('communicate_detail', {obj: obj});
        }
        else {
            if (name.length > 10) {
                name = name.substr(0, 10);
                name += '..';
            }

            ChatService.createCustomGroup(name, $scope.sel.class, arr).then(function (resp) {
                //console.log(resp);
                if (resp.data.result) {
                    ChatService.getContacts();
                    $scope.modal.hide();

                    var rsp = JSON.parse(resp.data.data);
                    var obj = {
                        GroupId: rsp.groupId,
                        TypeEn: 'Private',
                        Name: name
                    };
                    $scope.conversationDetail(obj);
                    if (window.cordova) MobclickAgent.onEvent('app_private_group');
                }
            });
        }
    };

    $scope.onAddModeClassSelected = function (key) {
        if (key && $scope.classChooser) {
            $scope.sel.class = key;
            $scope.classChooser.close();
            getContactTree(key.key, true);
            initAddView();
        }
    };

    $scope.selectionChanged = function (contact) {
        if (contact.isChecked)
            $scope.selCount++;
        else
            $scope.selCount--;
        if ($scope.selCount > 0)
            $scope.modalTitle = '已选择' + $scope.selCount + '人';
        else
            $scope.modalTitle = '发起聊天';
    };

    function getContactTree(classid, addMode) {
        $scope.cachedCID = angular.copy(classid);
        var friends = ChatService.friendsMap[classid];
        var groups = ChatService.groups;
        var filterGroups = [];
        for (var j in groups) {
            if (groups[j].ClassID == classid)
                filterGroups.push(groups[j]);
        }
        if (friends && friends.length > 0) {
            $scope.emptyContacts = false;
            var teachers = [];
            for (var i in friends) {
                var p = friends[i];
                switch (p.Category) {
                    case 't':
                        teachers.push(p);
                        break;
                }
            }
            var contactTree = [
                {
                    name: '老师',
                    list: teachers,
                    num: teachers.length,
                    icon: 'img/icon/chat_teacher.png',
                    isGroup: false
                },
                {
                    name: '我的群组',
                    list: filterGroups,
                    num: filterGroups.length,
                    icon: 'img/icon/chat_group.png',
                    isGroup: true
                }
            ];
            if (addMode)
                $scope.addModeContactTree = contactTree;
            else
                $scope.ContactTree = contactTree;
        }
        else {
            $scope.emptyContacts = true;
            if (addMode)
                $scope.addModeContactTree = [];
            else
                $scope.ContactTree = [];
        }
    }

    $scope.$on('$ionicView.beforeEnter', function () {
      
        if (window.cordova&&ionic.Platform.isIOS()) {
            cordova.plugins.Keyboard.disableScroll(true);
        }
        $scope.userID = UserPreference.getObject('user').id;
        $scope.sel.class = {
            key: 'schoolTeacher'
        };
    });

    $scope.refreshContacts = function (contactView) {
        if (contactView)
            ChatService.getContacts();
        else
            ChatService.getRecentContacts($scope.sel.class.key);
    };

    $scope.$on('$ionicView.enter', function () {
        if ($rootScope.ContactProfileChanged === true)
            ChatService.getContacts();
        else if ($scope.sel.class.key != $scope.cachedCID) {
            getContactTree($scope.sel.class.key);
        }
        ChatService.getRecentContacts($scope.sel.class.key);
    });

    $scope.loading = true;
    setTimeout(function () {
        ChatService.getRecentContacts($scope.sel.class.key);
    }, 5000);

    $scope.onSelectionChange = function () {
        if ($scope.sel.class) {
            if ($scope.sel.class.key !== 'schoolTeacher')
                UserPreference.set("DefaultClassID", $scope.sel.class.key);
            getContactTree($scope.sel.class.key);
        }
    };

    $scope.conversationDetail = function (item, isConversation) {
        if (isConversation) {
            $state.go('communicate_detail', {obj: item});
        }
        else {
            if (item.Member_Account) {
                var obj = {
                    SessionId: item.Member_Account
                };
                $state.go('contact_profile', {obj: obj});
            }
            else
                $state.go('communicate_detail', {obj: item});
        }
    };

    $scope.$on(BroadCast.IM_RECENT_CONTACTS, function (event, resp) {
        if (resp) {
            $scope.conversations = resp;
            $scope.$digest();
        }
        $scope.$broadcast('scroll.refreshComplete');
        $scope.loading = false;
    });

    $scope.$on(BroadCast.IM_REV_CONTACTS, function (event, data) {
        if (data)
            getContactTree($scope.sel.class.key);
        else
            toaster.error({title: MESSAGES.REMIND, body: MESSAGES.CONTACT_FETCH_FAIL});
        $scope.$broadcast('scroll.refreshComplete');
    });

    $scope.$on(BroadCast.IM_NEW_MESSAGE, function (event, newMsgList) {
        $scope.conversations = ChatService.conversations;
        $scope.$digest();
    });
}]);