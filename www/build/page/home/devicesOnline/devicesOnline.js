angular.module('app.controllers')
.controller('devicesOnlineCtrl', ['$scope', 'Constant', 'Requester', '$state', '$ionicHistory', '$ionicPopup', 'toaster', function ($scope, Constant, Requester, $state, $ionicHistory, $ionicPopup, toaster) {

    $scope.$on("$ionicView.enter", function (event, data) {
        $scope.loadData();
        $scope.shutdown = '';
    });

    $scope.$on('$ionicView.leave', function () {
        clearInterval($scope.refresh);
    });

    $scope.loadData = function () {
        $scope.loading = true;
        $scope.selected = {
            all: false,
            num: 0
        };
        Requester.getOnlineTerminalList().then(function (resp) {
            $scope.list = resp.data;
            for (var i = 0; $scope.shutdown !== '' && i < $scope.list.length; i++) {
                if ($scope.shutdown.indexOf($scope.list[i].id) >= 0)
                    $scope.list[i].processing = true;
            }
            if (resp.data.length === 0 && $scope.refresh) {
                clearInterval($scope.refresh);
            }
        }).finally(function () {
            $scope.loading = false;
            $scope.$broadcast('scroll.refreshComplete');
        });
    };

    function setListCheckedStatus(sel) {
        for (var i = 0; i < $scope.list.length; i++) {
            $scope.list[i].isChecked = sel;
        }
    }

    $scope.selectAll = function () {
        $scope.selected.all = $scope.selected.all === true;
        setListCheckedStatus($scope.selected.all);
        if ($scope.selected.all === true)
            $scope.selected.num = $scope.list.length;
        else
            $scope.selected.num = 0;
    };

    $scope.confirmShutdown = function () {
        var confirmPopup = $ionicPopup.confirm({
            title: '温馨提示',
            template: '该操作后，选中的设备将被远程关机，只有手动启动后才能继续使用。<p style="color: orange;font-size: small">由于网络环境不同，设备关机时间会有不同程度的延迟，请耐心等待。</p>',
            cancelText: '取消',
            okText: '确认关机',
            okType: 'button-balanced'
        });
        confirmPopup.then(function (res) {
            if (res) {
                var ids = '';
                for (var i = 0; i < $scope.list.length; i++) {
                    if ($scope.list[i].isChecked)
                        ids += ($scope.list[i].id + ',');
                }
                Requester.turnOffTerminals(ids.substr(0, ids.length - 1)).then(function (resp) {
                    toaster.success({title: '', body: '关机指令已发送，请耐心等待..'});
                    $scope.shutdown += ids;
                    $scope.loadData();
                    $scope.refresh = setInterval($scope.loadData, 10000);
                });
            }
        });
    };

    $scope.onCheckedChange = function (item) {
        if (item.isChecked === true) {
            $scope.selected.num++;
            $scope.selected.all = $scope.selected.num === $scope.list.length;
        }
        else {
            $scope.selected.num--;
            $scope.selected.all = false;
        }
    };

    $scope.goBack = function () {
        if ($ionicHistory.backView())
            $ionicHistory.goBack();
        else
            $state.go('tabsController.mainPage');
    };

}]);