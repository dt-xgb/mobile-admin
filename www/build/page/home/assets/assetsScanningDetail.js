angular.module('app.controllers')
    .controller('assetsScanningDetailCtrl', ['$q', '$scope', '$ionicModal', '$state', 'CameraHelper', 'BroadCast', 'NoticeService', 'Constant', 'UserPreference', 'toaster', 'MESSAGES', '$ionicLoading', '$ionicPopup', '$ionicListDelegate', 'Requester', '$rootScope', 'ionicImageView', function ($q, $scope, $ionicModal, $state, CameraHelper, BroadCast, NoticeService, Constant, UserPreference, toaster, MESSAGES, $ionicLoading, $ionicPopup, $ionicListDelegate, Requester, $rootScope, ionicImageView) {
        $scope.$on("$ionicView.enter", function (event, data) {
            $scope.selectType = 1;
        });

        $scope.selectTypeClick = function(type){
            $scope.selectType = type;
            $scope.events = [{
                createTime:'2019-10-28',
                title:'教育入库',
                resignMen:'张三',
                useMan:'哈哈哈',
                addType:'上级发放',
                statue:0
            },
            {
                createTime:'2019-10-29',
                title:'发放',
                resignMen:'李四',
                usePart:'DT',
                useMan:'哈哈哈',
                addType:'上级发放',
                statue:1
            },
            {
                createTime:'2019-10-30',
                title:'发放',
                resignMen:'王五',
                usePart:'DT',
                useMan:'李思思',
                addType:'上级发放',
                statue:2
            }
        ];

        };

        $scope.getDeviceType = function () {
            var result = 'android';
            $scope.isIos = ionic.Platform.isIOS() ;
            if ($scope.isIos) {
                if (window.screen.width >= 375 && window.screen.height >= 812) {
                    result = 'iphoneX';
                } else {
                    result = 'iphone';
                }

            } else {
             result = 'android';
            }
            return result;
        };
    }]);