angular.module('app.controllers')
  .controller('assetsCheckDetailCtrl', ['$scope', '$ionicModal', 'Constant', '$stateParams', '$state', 'toaster', '$ionicPopup', 'Requester', '$ionicLoading', function ($scope, $ionicModal, Constant, $stateParams, $state, toaster, $ionicPopup, Requester, $ionicLoading) {

    $scope.$on("$ionicView.beforeEnter", function (event, data) {
      $scope.assetsId = $stateParams.assetsId;
      $scope.progressStatus = $stateParams.progressStatus;
      $scope.select = {};

      $scope.hasGot = Math.pow(2, 30);
      $scope.cancelStatus = Math.pow(2, 31) - 1;

      if ($scope.progressStatus == 1) {
        $scope.fixAssetsApplyStatus(2);
      } else {
        $scope.fixAssetsApplyStatus(2);
      }
    });
    //即将进入
    $scope.$on("$ionicView.enter", function (event, data) {

    });



    //资产详情Request
    $scope.getAssetsDetail = function () {
      $scope.events = [];
      Requester.assetsApplyDetail($scope.assetsId).then(function (res) {
        if (res.result) {
          $scope.assetsDetail = res.data;
          $scope.progressStatus = res.data.orderProgress;
          if (res.data.progress && res.data.progress.length > 0) {
            for (var i = 0; i < res.data.progress.length; i++) {
              var assetsProgress = res.data.progress[i];
              var checkStatus = assetsProgress.orderProgress;
              var progressName = checkStatus == 1 ? '提交申请' : checkStatus == 2 ? '正在审核' : checkStatus == 4 ? '拒绝申请' : checkStatus == 8 ? '审核通过' : checkStatus == $scope.hasGot ? '已领取' : checkStatus == $scope.cancelStatus ? '已取消' : '审核通过';
              var prefix = checkStatus == 1 ? '申请人:' : checkStatus == 4 ? '审批人:' : checkStatus == 8 ? '审批人:' : checkStatus == $scope.hasGot ? '发放人:' : '审批人:';
              var afterFix = assetsProgress.userDepartment ? '(' + assetsProgress.userDepartment + '）' : '';
              if (checkStatus != 2) {
                $scope.events.push({
                  badgeIconClass: 'tm-icon',
                  title: progressName,
                  operateMan: prefix + ' ' + assetsProgress.userName + afterFix,
                  text: checkStatus == $scope.hasGot ? '查看资产ID' : assetsProgress.progressDescribe,
                  date: assetsProgress.operateTime.substr(0, 10),
                  status: assetsProgress.orderProgress
                });
              }

            }
          }


        } else {

        }
      });
    };

    $scope.readAssetsId = function (event) {
      if (event.status == $scope.hasGot) {
        $scope.idModal.show();
        $scope.queryAssetsIds();
      }
    };




    $scope.checkAssets = function (type) {

      if (type == 1) {
        //通过
        $scope.modal.show();
      } else {
        //拒绝
        $scope.select = {};
        $scope.myPopup = $ionicPopup.show({
          template: '<textarea name="chatInput" style="width:100%;height: 100px;font-size:15px; padding: 7px 8px;background-color:#F3F4F6;border-radius: 8px;margin-top:15px;"placeholder="必填,200字以内" maxlength="200"  ng-model="select.refusedReason" ></textarea>',
          title: '<div> <img src="img/classAppraisal/left.png" /><span>拒绝申请</span><img src="img/classAppraisal/right.png" /></div>',
          scope: $scope,
          cssClass: 'dateSelectAlert',
          buttons: [{
              text: '取消',
              type: 'button-cancel'
            },
            {
              text: '<b>确定</b>',
              type: 'button-xgreen',
              onTap: function (e) {
                if (!$scope.select.refusedReason) {
                  $ionicLoading.show({
                    template: '请填写拒绝申请的理由',
                    duration: 1500
                  });
                  e.preventDefault();
                  return;
                }
                $scope.fixAssetsApplyStatus(4, $scope.select.refusedReason);
                e.preventDefault();
                return;
              }
            }
          ]
        });

        $scope.myPopup.then(function (res) {


        });
      }
    };




    /**
     * modal 审核通过
     */
    var UIPath = '';
    if (Constant.debugMode) UIPath = 'page/';
    $ionicModal.fromTemplateUrl(UIPath + 'home/assets/assetsCheckPass.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.modal = modal;
    });
    $scope.closeModal = function () {
      $scope.modal.hide();

    };

    /**
     * modal 查看资产id
     */
    // var UIPath = '';
    // if (Constant.debugMode) UIPath = 'page/';
    $ionicModal.fromTemplateUrl(UIPath + 'home/assets/assetsIds.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {

      $scope.idModal = modal;
    });

    $scope.closeIdModal = function () {
      $scope.idModal.hide();
    };

    //已申领的资产id 
    $scope.queryAssetsIds = function () {
      Requester.queryAssetsApplyIds($scope.assetsId).then(function (res) {
        if (res.result) {
          $scope.assetsApplyIds = res.data;
        }
      });
    };

    //审核通过
    $scope.applyPass = function () {
      if ($scope.select.count > $scope.assetsDetail.applyAmount || $scope.select.count < 1) {
        toaster.warning({
          title: '审核失败',
          body: "审批数量错误",
          timeout: 3000
        });
        return;
      }
      $scope.fixAssetsApplyStatus(8, $scope.select.content, $scope.select.count);
    };

    //将资产申领标记为已读
    $scope.fixAssetsApplyStatus = function (status, describe, fixApplyCount) {
      //staus : 2:管理员已查;4:拒绝;8:审核通过
      Requester.addAssetsApplyProgress($scope.assetsId, status, fixApplyCount, describe).then(function (res) {
        if (res.result) {
          $scope.getAssetsDetail();
          if (status == 8) {
            //查看详情     
            toaster.success({
              title: "审核成功",
              body: "资产申领审核通过",
              timeout: 3000
            });
            $scope.modal.hide();
          } else if (status == 4) {
            //查看详情
            toaster.success({
              title: "审核成功",
              body: "资产申领审核拒绝",
              timeout: 3000
            });
            $scope.myPopup.close();
          }

        } else {
          toaster.warning({
            title: '审核失败',
            body: res.message,
            timeout: 3000
          });
          $scope.myPopup.close();
        }
      });
    };


  }]);