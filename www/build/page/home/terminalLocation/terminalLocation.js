angular.module('app.controllers')
.controller('terminalLocationCtrl', ['$scope', 'Constant', 'UserPreference', '$state', '$ionicHistory', 'Requester', '$ionicScrollDelegate', function ($scope, Constant, UserPreference, $state, $ionicHistory, Requester, $ionicScrollDelegate) {

    $scope.goBack = function () {
        if ($scope.deep === 0) {
            if ($ionicHistory.backView())
                $ionicHistory.goBack();
            else
                $state.go('tabsController.mainPage');
        } else {
            $scope.deep--;
            $scope.areaName = $scope.history[$scope.deep].areaName;
            $scope.getTerminalsLocation($scope.history[$scope.deep].id);
        }
    };

    $scope.$on("$ionicView.loaded", function (event, data) {
        $scope.areaName = UserPreference.getObject('user').area.areaName;
        $scope.chartHeight = 200;
        $scope.deep = 0;
        $scope.userRole = UserPreference.getObject('user').role;
        if (String($scope.userRole) === Constant.USER_ROLES.PROVINCE_ADMIN) {
            $scope.MAX = 2;
        } else if (String($scope.userRole) === Constant.USER_ROLES.CITY_ADMIN) {
            $scope.MAX = 1;
        } else {
            $scope.MAX = 0;
        }
        $scope.history = [2];
        $scope.history[$scope.deep] = {
            id: undefined,
            areaName: $scope.areaName
        };
        $scope.chart = echarts.init(document.getElementById("bar-chart"));
        $scope.chart.on('click', function (param) {
            if ($scope.deep < $scope.MAX && $scope.areas && $scope.areas.length > param.dataIndex) {
                $scope.deep++;
                var selectedArea = $scope.areas[param.dataIndex];
                $scope.areaName = selectedArea.regionName;
                $scope.getTerminalsLocation(selectedArea.id);
                $scope.history[$scope.deep] = {
                    id: selectedArea.id,
                    areaName: $scope.areaName
                };
            }
        });

        $scope.getTerminalsLocation();
    });

    $scope.getTerminalsLocation = function (id) {
        Requester.getTerminalsLocation(id).then(function (resp) {
            var y = [];
            var x = [];
            $scope.areas = [];
            if (!resp.data) {
                $scope.chartHeight = 0;
                return;
            }
            $scope.chartHeight = resp.data.content.length * 40 + 25;
            for (var i = resp.data.content.length - 1; i >= 0; i--) {
                y.push(i + 1 + '.' + resp.data.content[i].regionName);
                x.push(resp.data.content[i].terminalCount);
                $scope.areas.push(resp.data.content[i]);
            }
            setTimeout(function () {
                var option = {
                    color: ['#3398DB'],
                    grid: {
                        left: '5%',
                        right: '4%',
                        top: '20',
                        bottom: '5',
                        containLabel: true
                    },
                    yAxis: [
                        {
                            type: 'category',
                            data: y,
                            axisLine: {
                                show: false
                            },
                            axisTick: {
                                show: false
                            }
                        }
                    ],
                    xAxis: [
                        {
                            type: 'value',
                            show: false
                        }
                    ],
                    series: [
                        {
                            type: 'bar',
                            smooth: true,
                            data: x,
                            label: {
                                normal: {
                                    show: true,
                                    position: 'inside',
                                    textStyle: {
                                        color: '#fff',
                                        fontSize: 14
                                    },
                                    formatter: '{c}台'
                                }

                            },
                            barWidth: 18,
                            barMinHeight: 50,
                            itemStyle: {
                                emphasis: {
                                    barBorderRadius: 25
                                },
                                normal: {
                                    barBorderRadius: 25,
                                    color: new echarts.graphic.LinearGradient(
                                        0, 0, 1, 0,
                                        [
                                            {offset: 0, color: '#096fcf'},
                                            {offset: 1, color: '#47fbb9'}
                                        ]
                                    )
                                }
                            }
                        }
                    ]
                };
                $scope.chart.setOption(option);
                $scope.chart.resize();
                $ionicScrollDelegate.scrollTop();
            }, 100);
        });
    };

}]);
