angular.module('app.controllers')
    .controller('repairDetailCtrl', ['$scope', '$rootScope', '$state', 'Constant', '$timeout', 'Requester', 'SavePhotoTool', '$stateParams', 'UserPreference', function ($scope, $rootScope, $state, Constant, $timeout, Requester, SavePhotoTool, $stateParams, UserPreference) {

        //收到通知 保存图片
        $rootScope.$on('saveImgUrl', function (event, url) {
            SavePhotoTool.savePhoto(url);
        });

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.isIos = ionic.Platform.isIOS();
            $scope.repairRecordId = $stateParams.repairRecordId;
            $scope.readStatus = $stateParams.readStatus;
            // $scope.picUrls = UserPreference.getArray('RepairPictures');
            // $scope.repairDetail = UserPreference.getObject('RepairDetail');
            // $scope.events = UserPreference.getArray('RepairEvents');
            if ($scope.readStatus) {
                $scope.getRepairDetail();

            } else {
                $scope.updateRepairRecordStatus();

            }
        });

        //即将进入
        $scope.$on("$ionicView.enter", function (event, data) {

        });



        //获取维修记录详情
        $scope.getRepairDetail = function () {
            Requester.getRepairRecordDetail($scope.repairRecordId).then(function (res) {
                if (res.result) {
                    $scope.repairDetail = res.data;
                    $scope.picUrls = [];
                    $scope.events = []; //维修进度
                    $scope.manufacturersContactId = res.data.manufacturersContactId;//售后人员id
                    $scope.manufacturersId = res.data.manufacturersId;//厂家id
                    $scope.isAssociated = res.data.manufacturersContactId;//是否关联了厂家 售后人员id
                    if ($scope.repairDetail) {
                        $scope.repairDetail.tbMaintainImgs.forEach(function (pic) {
                            $scope.picUrls.push({
                                src: pic.url,
                                thumb: pic.thumb
                            });
                        });
                        // UserPreference.setObject('RepairDetail', $scope.repairDetail);
                        // UserPreference.setObject('RepairPictures', $scope.picUrls);
                       console.log($scope.repairDetail); 
                        var count = 0;
                        for (var i = 0; i < $scope.repairDetail.tbMaintainProgresses.length; i++) {
                            var pro = $scope.repairDetail.tbMaintainProgresses[i];
                            var icon;
                            if ($scope.repairDetail.tbMaintainProgresses.length > 2) {
                                if (i == 0) {
                                    icon = 'img/repair/selectRepair.png';
                                    // if(pro.orderProgress==Math.pow(2,30)){
                                    //     $scope.isClose = true; 
                                    //  }
                                } else if (i == $scope.repairDetail.tbMaintainProgresses.length - 1) {
                                    icon = 'img/repair/normalRepair.png';
                                } else if (i == $scope.repairDetail.tbMaintainProgresses.length - 2) {
                                    icon = 'img/repair/normalEmail.png';
                                } else {
                                    icon = 'img/repair/normalRepair.png';
                                }
                            } else {
                                if (i == $scope.repairDetail.tbMaintainProgresses.length - 1) {
                                    icon = 'img/repair/normalRepair.png';

                                } else {
                                    icon = 'img/repair/normalEmail.png';
                                    if (pro.orderProgress == Math.pow(2, 30)) {
                                        icon = 'img/repair/selectRepair.png';
                                        // $scope.isClose = true; 
                                    }
                                }
                            }
                            var manStatus = pro.orderProgress == 1 ? '申请人: ' : '处理人: '; //pro.orderProgress == 8 ? '通知售后:':pro.orderProgress == 16?'售后回复:':'处理人: ';
                            var userPhone = i == $scope.repairDetail.tbMaintainProgresses.length - 1 ? pro.userPhone : '';
                            if (pro.orderProgress == 8 ) {
                               // $scope.manufacturersContactId = pro.manufacturersContactId;
                               // count++;
                            }
                            $scope.events.push({
                                iconImg: icon,
                                badgeIconClass: 'tm-icon',
                                title: pro.progressName,
                                operateMan: manStatus + pro.userName,
                                operatePhone: userPhone,
                                text: pro.progressDescribe,
                                date: pro.operateTime.substr(5, 6),
                                time: pro.operateTime.substr(11, 5)
                            });

                        }
                        for (var k = 0; k < $scope.repairDetail.tbMaintainProgresses.length; k++) {
                            var proc = $scope.repairDetail.tbMaintainProgresses[k];
                            if (proc.orderProgress == Math.pow(2, 30)) {
                                $scope.isClose = false;
                                return;
                            } else {
                                $scope.isClose = true;
                            }
                        }
                        // UserPreference.setObject('RepairEvents', $scope.events);

                    }

                } else {

                }
            });
        };

        //修改报修记录已读状态
        $scope.updateRepairRecordStatus = function () {
            Requester.addRepairProgress($scope.repairRecordId, 2, '').then(function (res) {
                if (res.result) {
                    // console.log('update is readed');
                    $scope.getRepairDetail();
                }
            });
        };


        //改变维修进度
        $scope.fixRepairProgress = function () {
            $state.go('repairProgress', {
                repairRecordId: $scope.repairRecordId,
                manufacturersContactId: $scope.manufacturersContactId,
                manufacturersId : $scope.manufacturersId,
                isAssociated:$scope.isAssociated
            });
        };
    }]);