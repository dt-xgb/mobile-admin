angular.module('app.controllers')
.controller('usageCtrl', ['$scope', 'Constant', 'UserPreference', '$state', '$ionicHistory', 'Requester', function ($scope, Constant, UserPreference, $state, $ionicHistory, Requester) {
    $scope.userRole = UserPreference.getObject('user').role;
    if (String($scope.userRole) === Constant.USER_ROLES.SCHOOL_ADMIN) {
        $scope.schoolAdmin = true;
    }
    $scope.data = {
        date: [],
        data: []
    };
    $scope.refreshData = function () {
        Requester.getUsedTerminalsRecently().then(function (resp) {
            var date = [];
            var data = [];
            if (resp.result) {
                for (var i = 0; i < resp.data.length; i++) {
                    if (resp.data[i].day.length === 10)
                        date.push(resp.data[i].day.substr(5, 5));
                    else
                        date.push(resp.data[i].day);
                    data.push(resp.data[i].count);
                }
                $scope.data = {
                    date: date,
                    data: data
                };
            }
        });
    };
    $scope.refreshData();

    $scope.goBack = function () {
        if ($ionicHistory.backView())
            $ionicHistory.goBack();
        else
            $state.go('tabsController.mainPage');
    };

}]);