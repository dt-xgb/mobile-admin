angular.module('app.controllers')
.controller('rankCtrl', ['$scope', 'Constant', 'UserPreference', '$state', '$ionicHistory', 'Requester', '$stateParams', function ($scope, Constant, UserPreference, $state, $ionicHistory, Requester, $stateParams) {

    $scope.$on("$ionicView.loaded", function (event, data) {
        $scope.unit = '';
        $scope.type = $stateParams.type;
        $scope.userRole = UserPreference.getObject('user').role;
        if (String($scope.userRole) === Constant.USER_ROLES.SCHOOL_ADMIN) {
            $scope.schoolAdmin = true;
        }
        switch ($scope.type) {
            case 'usage':
                $scope.rankTitle = '学校设备使用排行';
                $scope.unit = '%';
                break;
            case 'class':
                $scope.rankTitle = '班级使用统计';
                break;
            case 'teacher':
                $scope.rankTitle = '教师使用统计';
                break;
            case 'time':
                $scope.rankTitle = '学校设备使用时长排行';
                break;
            case 'teacherUseTime':
                $scope.rankTitle = '学校教师使用时长排行';
                break;
        }
        $scope.cities = [];
        $scope.areas = [];
        $scope.data = {};
        $scope.selected = {
            type:0,//0 :设备使用率 1:设备在线率 
            days: 7,
            area: UserPreference.getObject('user').area.id
        };
        $scope.userRole = UserPreference.getObject('user').role;
        if (String($scope.userRole) === Constant.USER_ROLES.PROVINCE_ADMIN) {
            $scope.cityChoosable = true;
            $scope.areaChoosable = true;
            Requester.getArea($scope.selected.area).then(function (resp) {
                $scope.cities = resp;
            });
        } else if (String($scope.userRole) === Constant.USER_ROLES.CITY_ADMIN) {
            $scope.cityChoosable = false;
            $scope.areaChoosable = true;
            Requester.getArea($scope.selected.area).then(function (resp) {
                $scope.areas = resp;
            });
        } else {
            $scope.cityChoosable = false;
            $scope.areaChoosable = false;
        }
        $scope.getData();
    });

    $scope.onSelectedDayChange = function (d) {
        if ($scope.selected.days != d) {
            $scope.selected.days = d;
            $scope.getData();
        }
    };

    $scope.onSelectionChange = function (isCity) {
        if ($scope.cityChoosable && !$scope.selected.city) {
            $scope.selected.area = UserPreference.getObject('user').area.id;
            $scope.areas = [];
        } else if (isCity) {
            Requester.getArea($scope.selected.city).then(function (resp) {
                $scope.areas = resp;
            });
            $scope.selected.area = $scope.selected.city;
        }
        $scope.getData();
    };

    $scope.makeChart = function (y, x) {
        $scope.chartHeight = y.length * 40 + 25;
        $scope.data = {
            y: y,
            x: x,
            unit: $scope.unit
        };
    };

    $scope.getData = function () {
        switch ($scope.type) {
            case 'usage':
                Requester.getTerminalUsageRank($scope.selected.type,$scope.selected.area, $scope.selected.days).then(function (resp) {
                    if (resp.result) {
                        if (resp.data.content.length === 0) {
                            $scope.emptyContent = true;
                        } else {
                            $scope.emptyContent = false;
                            var x = [], y = [];
                            for (var i = resp.data.content.length - 1; i >= 0; i--) {
                                y.push(i + 1 + '.' + resp.data.content[i].schoolName);
                                var rate = resp.data.content[i].rate;
                                x.push(rate.substr(0, rate.length - 1));
                            }
                            $scope.makeChart(y, x);
                        }
                    }
                });
                break;
            case 'class':
                var param;
                if ($scope.selected.days === 30)
                    param = 'lastMonth';
                else
                    param = 'lastWeek';
                Requester.getClassUseRank(param).then(function (resp) {
                    if (resp.result) {
                        if (resp.data.length === 0) {
                            $scope.emptyContent = true;
                        } else {
                            $scope.emptyContent = false;
                            var x = [], y = [];
                            for (var i = resp.data.length - 1; i >= 0; i--) {
                                y.push(i + 1 + '.' + resp.data[i].className);
                                x.push(resp.data[i].time);
                            }
                            $scope.makeChart(y, x);
                        }
                    }
                });
                break;
            case 'teacher':
                if ($scope.selected.days === 30)
                    param = 'lastMonth';
                else
                    param = 'lastWeek';
                Requester.getTeacherUseRank(param).then(function (resp) {
                    if (resp.result) {
                        if (resp.data.length === 0) {
                            $scope.emptyContent = true;
                        } else {
                            $scope.emptyContent = false;
                            var x = [], y = [];
                            for (var i = resp.data.length - 1; i >= 0; i--) {
                                y.push(i + 1 + '.' + resp.data[i].teacherName);
                                x.push(resp.data[i].time);
                            }
                            $scope.makeChart(y, x);
                        }
                    }
                });
                break;
            case 'time':
                Requester.getTerminalUseTimeRank($scope.selected.area, $scope.selected.days).then(function (resp) {
                    if (resp.result) {
                        if (resp.data.content.length === 0) {
                            $scope.emptyContent = true;
                        } else {
                            $scope.emptyContent = false;
                            var x = [], y = [];
                            for (var i = resp.data.content.length - 1; i >= 0; i--) {
                                y.push(i + 1 + '.' + resp.data.content[i].schoolName);
                                x.push(resp.data.content[i].timeonline);
                            }
                            $scope.makeChart(y, x);
                        }
                    }
                });
                break;
            case 'teacherUseTime':
                Requester.getTeacherUseTimeRank($scope.selected.area, $scope.selected.days).then(function (resp) {
                    if (resp.result) {
                        if (resp.data.content.length === 0) {
                            $scope.emptyContent = true;
                        } else {
                            $scope.emptyContent = false;
                            var x = [], y = [];
                            for (var i = resp.data.content.length - 1; i >= 0; i--) {
                                y.push(i + 1 + '.' + resp.data.content[i].name);
                                x.push(resp.data.content[i].time);
                            }
                            $scope.makeChart(y, x);
                        }
                    }
                });
                break;
        }
    };


    $scope.goBack = function () {
        if ($ionicHistory.backView())
            $ionicHistory.goBack();
        else
            $state.go('tabsController.mainPage');
    };

    //设备使用统计类型
    $scope.onSelectedUsedOrOnline = function(type){
        //0:设备使用率 1：设备上课率
        //  $scope.selected.type = type; 
         if ($scope.selected.type != type) {
            $scope.selected.type = type;
            $scope.getData();
        }
    };

}]);
