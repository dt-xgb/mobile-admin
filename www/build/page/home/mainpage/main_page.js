angular.module('app.controllers')
    .controller('mainPageCtrl', ['$q', '$scope', '$ionicModal', '$sce', '$state', 'CameraHelper', 'BroadCast', 'NoticeService', 'Constant', 'UserPreference', 'toaster', 'MESSAGES', '$ionicLoading', '$ionicPopup', '$ionicListDelegate', 'Requester', '$rootScope', 'ionicImageView', function ($q, $scope, $ionicModal, $sce, $state, CameraHelper, BroadCast, NoticeService, Constant, UserPreference, toaster, MESSAGES, $ionicLoading, $ionicPopup, $ionicListDelegate, Requester, $rootScope, ionicImageView) {
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'page/';
        $ionicModal.fromTemplateUrl(UIPath + 'home/mainpage/new_notice.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
        });
        $scope.openModal = function () {
            $scope.modal.show();
        };
        $scope.closeModal = function () {
            $scope.modal.hide();
        };
        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function () {
            $scope.modal.remove();
        });

        $scope.rmStyle = function (str) {
            return $sce.trustAsHtml(str);
        };


        $scope.goDetail = function (notice) {
            if (notice) {
                $state.go('news_detail', {
                    notice: angular.copy(notice),
                    index: notice.id
                });
                //消息标记为已读
                Requester.setNoticeRead(notice.id).then(function (res) {

                }, function () {

                });
            }
        };
        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            console.log('before');
           
            $scope.noReadNum = 0; //未读数
            $scope.totalNum = 0; //总条数
            // Requester.getNoticeNotReadNumber().then(function (res) {
            //     console.log('=======00000======');
            //     if (res.result) {
            //         res.data.forEach(function (notice) {
            //             if (notice.key == '11') {
            //                 $scope.noReadNum = notice.value;
            //             } else if (notice.key == '99992') {
            //                 $scope.totalNum = notice.value;
            //             }
            //         });

            //     }
            // });


        });

        $scope.data = {
            data: [1, 0],
            title: '',
            subtitle: ''
        };

        $scope.$on("$ionicView.enter", function (event, data) {
            var user = UserPreference.getObject('user');
            console.log('=====user====');
            console.log(user);
            if (String(user.role) === Constant.USER_ROLES.SCHOOL_ADMIN) {
                console.log('--check auth');
               // $scope.checkAuthOrAccountIsOverdue();

            }



            $scope.schoolName = UserPreference.get('DefaultSchoolName');
            $scope.noticeType = '11';
            $scope.userId = user.id;
            $scope.page = 1;
            $scope.noticeList = UserPreference.getArray('notice_list_cache' + $scope.noticeType);


            $scope.authBanBanTong = user.AUTH_TERMINAL_MANAGE === 1;
            $scope.authOA = user.AUTH_SCHOOL_OA === 1;
            $scope.authTeachingManage = user.AUTH_EDUCATION_MANAGE === 1;
            $scope.authAssetsManage = user.AUTH_ASSET_MANAGE === 1;
            $scope.authBanPai = user.AUTH_ADTERMINAL_MANAGE === 1;
            $scope.safeManage = user.AUTH_SAFETY_MANAGE === 1; //安全管理
            $scope.babyVideoManage = user.AUTH_KINDERGARTEN_STATUS ===1;//宝宝视频权限
            $scope.covidProvent = user.AUTH_PROTECT_VIRUS ===1; //疫情防控权限
            $scope.listHasMore = true;
           // $scope.dataInit();
            NoticeService.getNoticeList($scope.page, $scope.noticeType);

            // if ($scope.authBanBanTong)
            //     $scope.getDeviceInfo();

            // Requester.getCampusUnread().then(function (resp) {
            //     if (resp.result) {
            //         console.log('======1111111=====');
            //         var count = resp.data.statePendingAuditNum;
            //         if (count > 0) {
            //             $rootScope.$broadcast(BroadCast.BADGE_UPDATE, {
            //                 type: 'campus',
            //                 count: count
            //             });
            //         }
            //     }
            // });
            
            //获取评比列表
           // $scope.getClassVoteTasktList();

            //资产申领未读数
           // $scope.getAssetsUnreadCount();

        });

        $scope.onChartClick = function () {
            if (ionic.Platform.isIOS()) {
                $state.go('devicesOnline');
            } else {
                $scope.getDeviceInfo();
            }
        };

        $scope.getDeviceInfo = function () {
            var now = new Date().getTime() / 1000;
            if (!$scope.lastGet || now - $scope.lastGet > 5) {
                $scope.lastGet = now;
                $scope.data = {
                    data: [1, 0],
                    title: '',
                    subtitle: ''
                };
                setTimeout(function () {
                    Requester.getGeneralInfo().then(function (resp) {
                        if (resp.result) {
                            console.log('======2222222=====');
                            var terminalNum = resp.data.terminalNum;
                            var terminalOnlineNum = resp.data.terminalOnlineNum;
                            var offLineDevice = terminalNum - terminalOnlineNum;
                            if (offLineDevice === 0)
                                offLineDevice = 1;
                            $scope.data = {
                                data: [offLineDevice, terminalOnlineNum],
                                title: '{hh|设备' + '}\n{t1|' + terminalNum + '}',
                                subtitle: '{hh|' + '在线' + '}\n{t2|' + terminalOnlineNum + '}'
                            };

                        }
                    });
                }, 500);
            }
        };

        $scope.deleteItem = function (item, $event) {
            $event.stopPropagation();
            var confirmPopup = $ionicPopup.confirm({
                title: '删除确认',
                template: '确认删除 ' + item.title + ' ?',
                cancelText: '取消',
                okText: '确认',
                okType: 'button-balanced'
            });
            confirmPopup.then(function (res) {
                if (res) {
                    NoticeService.deleteNotice(item.id);
                    $ionicListDelegate.closeOptionButtons();
                }
            });
        };

        $scope.removeHTMLTag = function (str) {
            str = str.replace(/<\/?[^>]*>/g, ''); //去除HTML tag
            str = str.replace(/[ | ]*\n/g, '\n'); //去除行尾空白
            str = str.replace(/\n[\s| | ]*\r/g, '\n'); //去除多余空行
            str = str.replace(/&nbsp;/ig, ''); //去掉&nbsp;
            return str;
        };

        $scope.$on(BroadCast.DELETE_NOTICE_REV, function (a, rst) {
            if (rst && rst.result) {
                var i = 0;
                for (; i < $scope.noticeList.length; i++) {
                    if ($scope.noticeList[i].id == rst.request.id) {
                        switch (rst.request.type) {
                            case BroadCast.DELETE_NOTICE_REV:
                                $scope.noticeList.splice(i, 1);
                                break;
                        }
                        break;
                    }
                }
            } else
                toaster.error({
                    title: MESSAGES.OPERATE_ERROR,
                    body: rst.message
                });
        });

        $scope.dataInit = function () {
            $scope.attachments = [];
            $scope.selected = {
                key: $scope.noticeType
            };
        };

        $scope.loadData = function (reset) {
            if (Constant.debugMode) console.log('load notice restart ' + reset);
            if (reset) {
                $scope.page = 1;
            } else if (NoticeService.listHasMore)
                $scope.page++;
            else
                return;

            NoticeService.getNoticeList($scope.page, $scope.noticeType);
        };


        $scope.$on(BroadCast.NOTICE_LIST_REV, function (a, rst) {
            console.log('=======rst=====');
            console.log(rst);
            if (rst && rst.result) {
                $scope.noticeList = NoticeService.list;
                console.log('notice data has more:' + $scope.listHasMore);
            }
            $scope.listHasMore = NoticeService.listHasMore;
            $scope.$broadcast('scroll.refreshComplete');
            $scope.$broadcast('scroll.infiniteScrollComplete');
        });

        $scope.selectImg = function () {
            CameraHelper.selectMultiImage(function (resp) {
                if (!resp)
                    return;
                if (resp instanceof Array) {
                    Array.prototype.push.apply($scope.attachments, resp);
                    $scope.$apply();
                } else {
                    $scope.attachments.push({
                        data: "data:image/jpeg;base64," + resp
                    });
                }
            }, 9 - $scope.attachments.length);
        };

        $scope.removeImg = function (index, $event) {
            $event.stopPropagation();
            $scope.attachments.splice(index, 1);
        };

        $scope.preViewImages = function (index) {
            var origin = [];
            $scope.attachments.forEach(function (libraryItem) {
                if (libraryItem.data)
                    origin.push(libraryItem.data);
                else
                    origin.push(libraryItem);
            });
            ionicImageView.showViewModal({
                allowSave: false
            }, origin, index);
        };

        function onImageResized(resp) {
            var start = resp.indexOf('base64,');
            $scope.selected.picdatas.push(resp.substr(start + 7));
        }

        $scope.save = function () {
            $scope.selected.picdatas = [];
            var promiseArr = [];
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
            $scope.attachments.forEach(function (libraryItem) {
                if (libraryItem.data) {
                    onImageResized(libraryItem.data);
                } else {
                    promiseArr.push(resizeImage(Constant.CAPTURE_IMAGE_RANGE, libraryItem).then(onImageResized));
                }
            });
            $q.all(promiseArr).then(function () {
                NoticeService.newNotice($scope.selected);
            });
        };

        $scope.$on(BroadCast.NEW_NOTICE, function (a, rst) {
            if (rst && rst.result) {
                toaster.success({
                    title: MESSAGES.REMIND,
                    body: MESSAGES.NEW_NOTICE
                });
                $scope.closeModal();
                $scope.dataInit();
                $scope.loadData(true);
            } else
                toaster.error({
                    title: MESSAGES.REMIND,
                    body: rst.message
                });
            $ionicLoading.hide();
        });

        $scope.$on(BroadCast.CONNECT_ERROR, function (arg, data) {
            console.log('=====rootscope errror===');
            $scope.$broadcast('scroll.refreshComplete');
            $scope.$broadcast('scroll.infiniteScrollComplete');
            $ionicLoading.hide();
        });


        $scope.getBgColor = function (role) {
            var bgColor = '';
            switch (role) {
                case 'province_admin':
                    bgColor = '#FB6D0F';
                    break;

                case 'city_admin':
                    bgColor = '#448aca'; //市
                    break;

                case 'area_admin':
                    bgColor = '#556fb5'; //区
                    break;

                case 'district_admin':
                    bgColor = '#80c269'; //县级
                    break;

                case 'superadmin':
                    bgColor = '#2bcab2'; //小跟班平台
                    break;

                default:
                    bgColor = '';
                    break;
            }
            return bgColor;

        };

        //班级评比
        $scope.classAppraisal = function () {
            $state.go('classAppraisalList');
        };

        $scope.showMarket = function () {
            $state.go('market');
        };

        //获取评比任务列表
        $scope.getClassVoteTasktList = function () {
            Requester.getCurrentSemesterVoteTaskCount().then(function (res) {
                console.log('======333333=====');
                console.log(res);
                console.log('======333333 end=====');
                $scope.voteLength = res.data ? res.data.size : 0;
            });
        };
        //获取资产申领未读数
        $scope.getAssetsUnreadCount = function () {
            Requester.getAssetsUnread().then(function (res) {
                console.log('======444444=====');
                console.log(res);
                if (res.result)
                    $scope.assetsUnreadCount = res.data.size;
            });
        };

        $scope.goToNextPage = function (url) {
            $state.go(url);
        };

        //关闭授权提示框
        $scope.closeAuthAlert = function () {
            $scope.authAlert.close();
        };

        //检查授权到期提醒
        $scope.checkAuthOrAccountIsOverdue = function () {
            var beforeTamp = UserPreference.get('SchoolAuthAlertDateTamp');
            var currentDate = getNowDate();
            var currentTamp = Date.parse(currentDate.replace(/\-/g, "/")) / 1000;
            var interval = 24 * 60 * 60;
            var flag = currentTamp - beforeTamp >= interval;
            if (!beforeTamp ||beforeTamp==='undefined'|| (beforeTamp && flag)) {
                Requester.checkAuthOrAccountIsOverdue().then(function (resp) {
                    if (resp.result) {
                        console.log('======5555555=====');
                        console.log(resp);
                        console.log('======5555555 end=====');
                        $scope.authList = resp.data;
                        UserPreference.set('SchoolAuthAlertDateTamp', currentTamp);
                        //检查授权到期提醒
                        //大于1天且未曾弹出过 则弹出授权提醒
                        if (resp.data) {
                            $scope.authAlert = $ionicPopup.show({
                                 template: '<div style="color:#666;font-size:15px;padding:10px;width:100%;"><div ng-repeat="item in authList">【{{item.authModeName}}】功能权限将于<span style="color:#2bcab2;font-weight:bold;">{{item.expireDate.substr(0,10)}}</span>过期</div> <div>过期后对应模块在APP及网页端均不能在使用，续期请联系<span style="color:#2bcab2;font-weight:bold;">QQ客服(1833025389)</span>或小跟班业务员</div><div class="button" ng-click="closeAuthAlert()" style="height:30px;width:calc(100% - 80px);margin-left:40px;margin-top:15px;background:#2bcab2;color:white;">知道了</div></div>',
                                //  templateUrl: UIPath + 'common/areaAuthalert.html',
                                title: '<div style="color:white !important;">授权即将到期提醒</div><div class="logo" ><img src="img/icon/logo.png" /></div>',
                                scope: $scope,
                                cssClass: 'auth_alert'
                            });
                        }

                    } else {
                        console.log('未过期');
                    }
                });
            }

        };

    }]);
