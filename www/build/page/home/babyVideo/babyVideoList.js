angular.module('app.controllers')
    .controller('babyVideoListCtrl', ['$scope', '$state', 'UserPreference', function ($scope, $state, UserPreference) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            var user = UserPreference.getObject('user');
            $scope.videoList = user.cameraManageList&&user.cameraManageList.length>0?user.cameraManageList:[];
        });

        $scope.onVideClick = function(item){
                console.log(item);
                $state.go('babyVideoDetail', {url: item.pcBroadcastUrl});
        };

    }]);