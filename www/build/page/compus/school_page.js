angular.module('app.controllers')
    .controller('schoolPageCtrl', ['$q', '$scope', '$ionicModal', 'CameraHelper', 'BroadCast', '$timeout', 'UserPreference', 'SchoolService', 'MESSAGES', 'toaster', '$state', 'Constant', '$ionicLoading', '$ionicPopup', '$ionicListDelegate', 'ionicImageView', 'closePopupService', 'Requester', '$rootScope', function ($q, $scope, $ionicModal, CameraHelper, BroadCast,$timeout, UserPreference, SchoolService, MESSAGES, toaster, $state, Constant, $ionicLoading, $ionicPopup, $ionicListDelegate, ionicImageView, closePopupService, Requester, $rootScope) {
        //收到通知 保存图片
        $rootScope.$on('saveImgUrl', function (event, url) {
            var config = {
                allowSave: true,
                album: 'Xgenban'
            };
            $scope.config = angular.extend({}, config, {
                allowSave: true
            });
            if (window.cordova) {
                cordova.plugins.photoLibrary.getAlbums(
                    function (result) {
                        saveImgUrl(url);
                    },
                    function (err) {
                        if (err.startsWith('Permission')) {
                            cordova.plugins.photoLibrary.requestAuthorization(
                                function () {
                                    saveImgUrl(url);
                                },
                                function (err) {
                                    // User denied the access
                                    console.log(err);
                                }, {
                                    read: true,
                                    write: true
                                }
                            );
                        }
                    }
                );

            }
        });

        function saveImgUrl(url) {
            if (window.cordova) {
                cordova.plugins.photoLibrary.saveImage(url + '?ext=.jpg', $scope.config.album, function (libraryItem) {
                    $ionicLoading.show({
                        template: '保存成功',
                        duration: 1500
                    });
                }, function (err) {
                    console.log(err);
                });
            }

        }


        $scope.$on("$ionicView.enter", function (event, data) {
            $scope.getCampusUnread();
            if (!$scope.newsTypeSelectable || $scope.newsTypeSelectable.length === 0) {
                SchoolService.getNewsType();
            }
        });

        if (window.cordova) MobclickAgent.onEvent('app_school_page');
        var UIPath = '';
        if (Constant.debugMode) UIPath = 'page/';
        $ionicModal.fromTemplateUrl(UIPath + 'compus/new_news.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
        });
        $scope.openModal = function () {
            $scope.modal.show();
        };
        $scope.closeModal = function () {
            $scope.modal.hide();
        };
        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function () {
            $scope.modal.remove();
        });


        $scope.canCancel = function (item) {
            if (item.authorId == $scope.userId)
                return false;
            return item.state == Constant.NEWS_STATUS.ADMIN_REVIEW_PASS.key && (!item.role || item.role === 'school_admin');

        };
        $scope.canDelete = function (item) {
            return item.authorId == $scope.userId;
        };

        $scope.openTypeSelectModal = function () {
            $scope.typeChooser = $ionicPopup.show({
                template: '<div class="item" ng-repeat="item in newsTypeQueryable" ng-click="onTypeSelected(item)" style="padding: 10px 20px;border-radius: 0">{{item.name}}</div>',
                title: '风采类型',
                scope: $scope
            });
            closePopupService.register($scope.typeChooser);
        };

        $scope.onTypeSelected = function (item) {
            $scope.queryParam.key = item.key;
            $scope.queryParam.parentKey = item.parentKey;
            $scope.selectedTypeame = item.name;
            $scope.page = 1;
            console.log($scope.queryParam);
            SchoolService.getNewsList($scope.page, $scope.queryParam, $scope.reqId);
            if ($scope.typeChooser)
                $scope.typeChooser.close();
        };


        function getFilterList() {
            $scope.newsTypeList = UserPreference.getArray('news_type');
            $scope.newsTypeSelectable = UserPreference.getArray('news_type_selectable');
            $scope.newsTypeQueryable = angular.copy($scope.newsTypeSelectable);
            $scope.newsTypeQueryable.unshift({
                key: undefined,
                parentKey: undefined,
                name: '全部类别'
            });
        }

        $scope.changeFilter = function (arg) {
            if ($scope.activeTab === arg) {
                return;
            }
            $scope.activeTab = arg;
            $scope.queryParam = {};
            if (arg === 'pending') {
                $scope.queryParam.status = Constant.NEWS_STATUS.ADMIN_REVIEW.key;
                $scope.disableTypeSelect = true;
            } else {
                $scope.queryParam.status = Constant.NEWS_STATUS.ADMIN_REVIEW_PASS.key;
                $scope.disableTypeSelect = false;
            }
            $scope.reqId++;
            $scope.onTypeSelected({
                key: undefined,
                parentKey: undefined,
                name: '全部类别'
            });
        };


        $scope.attachments = [];
        $scope.selected = {};
        $scope.queryParam = {
            status: Constant.NEWS_STATUS.ADMIN_REVIEW_PASS.key
        };
        $scope.activeTab = 'all';
        $scope.selectedTypeame = '全部类别';
        $scope.userRole = UserPreference.getObject('user').role;
        $scope.userId = UserPreference.getObject('user').id;

        $scope.listFilter = {
            ispublish: true
        };
        $scope.page = 1;
        $scope.reqId = 0;
        $scope.newsList = UserPreference.getArray('news_list_cache');

        $scope.listHasMore = SchoolService.listHasMore;
        $scope.bannerList = UserPreference.getArray('news_banner_cache');
        $scope.classes = UserPreference.getArray('class');
        $scope.schoolName = UserPreference.get('DefaultSchoolName');
        getFilterList();

        SchoolService.getNewsType();
        SchoolService.getNewsList(1, $scope.queryParam);
        SchoolService.getBannerList();

        $scope.loadData = function (reset) {
            if (Constant.debugMode) console.log('load news restart ' + reset);
            if (reset) {
                $scope.page = 1;
                SchoolService.getBannerList();
            } else if ($scope.listHasMore)
                $scope.page++;
            else
                return;

            SchoolService.getNewsList($scope.page, $scope.queryParam);
            $scope.getCampusUnread();
        };

        $scope.getDisplayTime = function (time) {
            var now = new Date().Format("yyyy-MM-dd");
            if (now === time.substr(0, 10))
                return time.substr(11, 5);
            else
                return time.substr(5, 11);
        };

        $scope.getNewsType = function (key) {
            for (var i = 0; i < $scope.newsTypeList.length; i++) {
                if (key == $scope.newsTypeList[i].key)
                    return $scope.newsTypeList[i].name;
            }
            return '';
        };

        $scope.delHtmlTag = function (str) {
            return delHtmlTag(str);
        };

        $scope.deleteItem = function (item, $event) {
            $event.stopPropagation();
            var pre = '';
            if (item.top)
                pre = '该风采已置顶。';
            var confirmPopup = $ionicPopup.confirm({
                title: '删除确认',
                template: pre + '确认删除?',
                cancelText: '取消',
                okText: '确认',
                okType: 'button-balanced'
            });
            confirmPopup.then(function (res) {
                if (res) {
                    SchoolService.deleteNews(item.id);
                    $ionicListDelegate.closeOptionButtons();
                }
            });
        };
        $scope.cancelItem = function (item, $event) {
            $event.stopPropagation();
            var pre = '';
            if (item.top)
                pre = '该风采已置顶。';
            var confirmPopup = $ionicPopup.confirm({
                title: '撤回确认',
                template: pre + '撤回后该条消息将被忽略，确认撤回?',
                cancelText: '取消',
                okText: '确认',
                okType: 'button-balanced'
            });
            confirmPopup.then(function (res) {
                if (res) {
                    SchoolService.cancelPublish(item.id);
                    $ionicListDelegate.closeOptionButtons();
                }
            });
        };

        //点赞
        $scope.favorNews = function (item, $event) {
            $event.stopPropagation();
            if (item.isFavor === true) {
                Requester.unfavorCampus(item.id);
                if (item.praiseNum > 0)
                    item.praiseNum--;
            } else {
                Requester.favorCampus(item.id);
                item.praiseNum++;
            }
            item.isFavor = !item.isFavor;
        };


        $scope.$on(BroadCast.NEWS_LIST_REV, function (a, rst) {
            if (rst && rst.result) {
                if (!rst.reqId || (rst.reqId && rst.reqId === $scope.reqId)) {
                    $scope.newsList = SchoolService.list;
                    $scope.newsList.forEach(function (item) {
                        item.picUrls = [];
                        item.imageUrls.forEach(function (url) {
                            $scope.urlItem = {
                                thumb: '',
                                src: ''
                            };
                            $scope.urlItem.thumb = url;
                            $scope.urlItem.src = url;
                            item.picUrls.push($scope.urlItem);
                        });
                    });
                } else {
                    if (Constant.debugMode) console.log('req is deprecated');
                }
            }
            $scope.listHasMore = SchoolService.listHasMore;
            $scope.$broadcast('scroll.refreshComplete');
            $scope.$broadcast('scroll.infiniteScrollComplete');
        });

        function onImageResized(resp) {
            var start = resp.indexOf('base64,');
            $scope.selected.picdatas.push(resp.substr(start + 7));
        }

        $scope.save = function () {
            if (!$scope.selected.key || $scope.selected.key === '') {
                toaster.warning({
                    title: MESSAGES.OPERATE_ERROR,
                    body: MESSAGES.NO_NEWS_TYPE
                });
                return;
            }

            if ($scope.attachments.length === 0) {
                toaster.warning({
                    title: MESSAGES.OPERATE_ERROR,
                    body: MESSAGES.AT_LEAST_IMAGE
                });
                return;
            }
            $scope.selected.picdatas = [];
            var promiseArr = [];
            $ionicLoading.show({
                noBackdrop: true,
                template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
            });
            $scope.attachments.forEach(function (libraryItem) {
                if (libraryItem.data) {
                    onImageResized(libraryItem.data);
                } else {
                    promiseArr.push(resizeImage(Constant.CAPTURE_IMAGE_RANGE, libraryItem).then(onImageResized));
                }
            });
            $q.all(promiseArr).then(function () {
                SchoolService.newNews($scope.selected);
            });
        };

        $scope.$on(BroadCast.NEW_NEWS, function (a, rst) {
            if (rst && rst.result) {
                toaster.success({
                    title: MESSAGES.REMIND,
                    body: MESSAGES.NEW_NEWS_ADMIN
                });
                $scope.closeModal();
                $scope.selected = {};
                $scope.attachments = [];
                $scope.activeTab = 'all';
            } else
                toaster.error({
                    title: MESSAGES.REMIND,
                    body: rst.message
                });
            $ionicLoading.hide();
        });

        $scope.$on(BroadCast.BANNER_LIST_REV, function (a, rst) {
            if (rst && rst.result)
                $scope.bannerList = SchoolService.bannerList;
        });

        $scope.$on(BroadCast.NEWS_TYPE_REV, function (a, rst) {
            if (rst && rst.result) {
                getFilterList();
            }
        });

        $scope.selectImg = function () {
            // CameraHelper.selectMultiImage(function (resp) {
            //     if (!resp)
            //         return;
            //     if (resp instanceof Array) {
            //         Array.prototype.push.apply($scope.attachments, resp);
            //         $scope.$apply();
            //     } else {
            //         $scope.attachments.push({
            //             data: "data:image/jpeg;base64," + resp
            //         });
            //     }
            // }, 9 - $scope.attachments.length);

            if (window.cordova) {
                CameraHelper.selectMultiImage(function (resp) {
                    if (!resp)
                        return;
                    if (resp instanceof Array) {
                        Array.prototype.push.apply($scope.attachments, resp);
                        $scope.$apply();
                    } else {
                        $scope.attachments.push({
                            data: "data:image/jpeg;base64," + resp
                        });
                    }
                }, 9 - $scope.attachments.length);
            } else {
                //web 端
                var input = document.getElementById("capture");
                input.click();
            }
        };

         // input形式打开系统系统相册
         $scope.getFile = function (files) {
            var file = files[0];
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (theFile) {
                $timeout(function () {
                    $scope.testImg = theFile.target.result; //设置一个中间值
                    $scope.attachments.push({
                        data: $scope.testImg
                    });

                }, 100);
            };
        };

        $scope.removeImg = function (index, $event) {
            $event.stopPropagation();
            $scope.attachments.splice(index, 1);
        };

        $scope.$on(BroadCast.CONNECT_ERROR, function (arg, data) {
            $scope.$broadcast('scroll.refreshComplete');
            $ionicLoading.hide();
        });

        $scope.setStick = function (item, $event) {
            $event.stopPropagation();
            if (item.top)
                SchoolService.undoStickNews(item.id);
            else
                SchoolService.stickNews(item.id);
        };

        $scope.getStickText = function (isTop) {
            if (isTop)
                return '取消置顶';
            return '置顶';
        };

        $scope.setIgnore = function (item, $event) {
            $event.stopPropagation();
            var confirmPopup = $ionicPopup.confirm({
                title: '忽略确认',
                template: '确认忽略此风采?',
                cancelText: '取消',
                okText: '确认',
                okType: 'button-balanced'
            });
            confirmPopup.then(function (res) {
                if (res) {
                    SchoolService.ignorePublish(item.id);
                }
            });
        };

        $scope.setPublish = function (item, $event) {
            $event.stopPropagation();
            $scope.publishData = {
                key: item.key,
                id: item.id
            };
            $ionicPopup.show({
                title: '发布确认',
                scope: $scope,
                template: '<p>请选择发布风采的类型：</p> <div style="padding: 1px 15px !important;"> <button class="button button-outline " ng-repeat="btn in newsTypeSelectable" ng-click="publishData.key=btn.key" ng-class="publishData.key==btn.key?\'button-positive\':\'button-stable\'" style="margin: 2px 3px;">{{btn.name}} </button> </div>',
                buttons: [{
                        text: '取消'
                    },
                    {
                        text: '<b>确认</b>',
                        type: 'button-balanced',
                        onTap: function (e) {
                            if ($scope.publishData.key)
                                SchoolService.allowPublish($scope.publishData);
                            else {
                                e.preventDefault();
                                toaster.warning({
                                    title: MESSAGES.NO_NEWS_TYPE,
                                    body: ''
                                });
                            }
                        }
                    }
                ]
            });
        };

        $scope.goDetail = function (item) {
            $state.go('news_detail', {
                post: item,
                index: item.id
            });
        };

        $scope.viewImages = function (urls, index, $event) {
            $event.stopPropagation();
            ionicImageView.showViewModal({
                allowSave: true
            }, urls, index);
        };

        $scope.preViewImages = function (index) {
            var origin = [];
            $scope.attachments.forEach(function (libraryItem) {
                if (libraryItem.data)
                    origin.push(libraryItem.data);
                else
                    origin.push(libraryItem);
            });
            ionicImageView.showViewModal({
                allowSave: false
            }, origin, index);
        };

        $scope.$on(BroadCast.NEWS_STATE_CHANGED, function (a, rst) {
            if (Constant.debugMode) console.log(rst);
            if (rst && rst.result) {
                var i = 0;
                for (; i < $scope.newsList.length; i++) {
                    if ($scope.newsList[i].id == rst.request.id) {
                        switch (rst.request.type) {
                            case BroadCast.UNDO_STICK_RST_REV:
                                $scope.newsList[i].top = false;
                                SchoolService.getBannerList();
                                break;
                            case BroadCast.SET_FOCUS_RST_REV:
                                $scope.newsList[i].top = true;
                                if (rst.data) {
                                    for (var j = 0; j < $scope.newsList.length; j++) {
                                        if ($scope.newsList[j].id == rst.data) {
                                            $scope.newsList[j].top = false;
                                            break;
                                        }
                                    }
                                }
                                SchoolService.getBannerList();
                                break;
                            case BroadCast.IGNORE_RST_REV:
                            case BroadCast.ALLOW_PUBLISH_RST_REV:
                                $scope.newsList.splice(i, 1);
                                break;
                            case BroadCast.DELETE_NEWS_REV:
                            case BroadCast.CANCEL_PUBLISH_REV:
                                $scope.newsList.splice(i, 1);
                                SchoolService.getBannerList();
                                break;
                        }
                        break;
                    }
                }
                $scope.getCampusUnread();
            } else
                toaster.error({
                    title: MESSAGES.OPERATE_ERROR,
                    body: rst.message
                });
        });

        $scope.getCampusUnread = function () {
            Requester.getCampusUnread().then(function (resp) {
                if (resp.result) {
                    var count = resp.data.myCampusViewUnrendNum + resp.data.statePendingAuditNum;
                    $scope.unreadCount = resp.data.myCampusViewUnrendNum;
                    $scope.unreadList = resp.data.newsCampusViewCommentsVoList;
                    $scope.pendingPublishCount = resp.data.statePendingAuditNum;
                    if (count < 0 || !Number.isInteger(count)) {
                        count = 0;
                    }
                    $rootScope.$broadcast(BroadCast.BADGE_UPDATE, {
                        type: 'campus',
                        count: count
                    });
                }
            });
        };
    }]);