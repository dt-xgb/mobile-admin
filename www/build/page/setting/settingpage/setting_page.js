angular.module('app.controllers')
.controller('settingPageCtrl', ['$scope', '$state', '$ionicLoading', 'AuthorizeService', 'SettingService', 'Requester', 'UserPreference', 'BroadCast', 'Constant', 'ngIntroService', 'ChatService', '$ionicHistory', function ($scope, $state, $ionicLoading, AuthorizeService, SettingService, Requester,UserPreference, BroadCast, Constant, ngIntroService, ChatService, $ionicHistory) {

    $scope.$on("$ionicView.beforeEnter", function (event, data) {
        if (window.cordova&&ionic.Platform.isIOS()) {
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if( UserPreference.get('VersionIntroduce')){
         console.log('exit:'+UserPreference.get('VersionIntroduce'));
         $scope.isRead = UserPreference.get('VersionIntroduce')=='UNREAD'?false:true;
        }else{
         console.log('not exit');
         UserPreference.set('VersionIntroduce','UNREAD');
         $scope.isRead = false;
        }
       
     });

    $scope.$on("$ionicView.enter", function (event, data) {
        $scope.user = UserPreference.getObject("user");
        if (!$scope.user.logo || $scope.user.logo.trim() === '')
            $scope.user.logo = Constant.IM_USER_AVATAR;
        $scope.childName = UserPreference.get('DefaultChildName');
        $scope.userAccount = UserPreference.get('username');
        $scope.needCardFee = UserPreference.getBoolean('NeedCardFee');
        //获取意见反馈的条数
        $scope.getFeedbackNotreadNum();
    });

    $scope.user = UserPreference.getObject("user");
    if (!$scope.user.logo || $scope.user.logo.trim() === '')
        $scope.user.logo = Constant.IM_USER_AVATAR;
    $scope.childName = UserPreference.get('DefaultChildName');
    $scope.userAccount = UserPreference.get('username');
    if ($scope.user.logo === Constant.IM_USER_AVATAR && !UserPreference.getBoolean("ShowSettingTip")) {
        var IntroOptions = {
            steps: [
                {
                    element: document.querySelector('#focusItem'),
                    intro: "<strong>点击头像可以修改个人信息和重置密码</strong>"
                }],
            showStepNumbers: false,
            showBullets: false,
            exitOnOverlayClick: true,
            exitOnEsc: true
        };
        ngIntroService.onHintClick(function () {
            ngIntroService.exit();
        });
        ngIntroService.setOptions(IntroOptions);
        ngIntroService.start();
        UserPreference.set("ShowSettingTip", true);
    }
    $scope.isAndroid = ionic.Platform.isAndroid();

    $scope.checkUpdate = function () {
        SettingService.checkUpdate(false);
    };

    $scope.hideTip = function () {
        ngIntroService.exit();
    };

    function logout() {
        $ionicLoading.hide();
        setTimeout(function () {
            $ionicHistory.clearCache();
            $ionicHistory.clearHistory();
        }, 1000);
        var currentVersion = Constant.version;
        var rst = UserPreference.getBoolean(currentVersion);
        var lastUser = UserPreference.get('username', '');
        var tip = UserPreference.getBoolean("ShowSettingTip");
        var readStatue =  UserPreference.get('VersionIntroduce');
        var schoolBeforeTamp = UserPreference.get('SchoolAuthAlertDateTamp');
        var areaBeforeTamp = UserPreference.get('AreaAuthAlertDateTamp');
        var areaAssetsBeforeTamp = UserPreference.get('AreaAssetsAuthAlertDateTamp');
       // var localTeacherList = UserPreference.getArray('TeacherList');

        UserPreference.clear();
        UserPreference.set(currentVersion, rst);
        UserPreference.set("username", lastUser);
        UserPreference.set("ShowSettingTip", tip);
        UserPreference.set('VersionIntroduce',readStatue);
        UserPreference.set('SchoolAuthAlertDateTamp', schoolBeforeTamp);
        UserPreference.set('AreaAuthAlertDateTamp', areaBeforeTamp);
        UserPreference.set('AreaAssetsAuthAlertDateTamp',areaAssetsBeforeTamp);
       // UserPreference.setObject('TeacherList', localTeacherList);
        $state.go('login');
        ChatService.logout();
    }

    $scope.doLogout = function () {
        $ionicLoading.show({
            noBackdrop: true,
            template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
        });
        console.log('to exit ');
        AuthorizeService.logout().finally(logout());
    };
    //获取意见反馈未读的条数
    $scope.getFeedbackNotreadNum = function(){
        $scope.noReadNum = 0;
        Requester.getNoreadNewsNumber().then(function (resp) {
            if (resp.result) {
                console.log('获取反馈未读数成功');
                $scope.noReadNum = resp.data.size;

            }
        });
    };
    
    $scope.clearCache = function () {
        $ionicLoading.show({
            noBackdrop: true,
            template: '<ion-spinner icon="lines" class="spinner-stable"></ion-spinner>'
        });
        setTimeout(function () {
            $ionicLoading.show({
                noBackdrop: true,
                template: '清理完成'
            });
            setTimeout(function () {
                $ionicLoading.hide();
            }, 1000);
        }, 1000);
    };

    $scope.openEditView = function (content) {
        $state.go('edit_profile', {content: content});
    };

    $scope.$on(BroadCast.CONNECT_ERROR, function (arg, data) {
        $ionicLoading.hide();
    });
}]);