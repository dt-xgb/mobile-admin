angular.module('app.routes', [])
    .config(['$stateProvider', '$urlRouterProvider', 'Constant', function ($stateProvider, $urlRouterProvider, Constant) {
        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the app can be in.
        // Each state's controller can be found in controllers.js
        var UIPath = '';
        if (Constant.debugMode) UIPath= 'page/';
        $stateProvider
            .state('tabsController.mainPage', {
                url: '/main_page',
                views: {
                    'main_tab': {
                        templateUrl: UIPath + 'home/mainpage/main_page.html',
                        controller: 'mainPageCtrl'
                    }
                }
            })
            .state('market',{
                url:'/market',
                templateUrl:UIPath + 'home/mainpage/market.html',
                controller:'marketCtrl'
            })
            .state('tabsController.schoolPage', {
                url: '/school_page',
                views: {
                    'school_tab': {
                        templateUrl:  UIPath + 'compus/school_page.html',
                        controller: 'schoolPageCtrl'
                    }
                }
            })
            .state('tabsController.communicatePage', {
                url: '/communicate_page',
                views: {
                    'communicate_tab': {
                        templateUrl: UIPath + 'interactive/communicate_page.html',
                        controller: 'communicatePageCtrl'
                    }
                }
            })
            .state('tabsController.general', {
                url: '/general',
                views: {
                    'general_tab': {
                        templateUrl: UIPath + 'home/general/general.html',
                        controller: 'generalCtrl'
                    }
                }
            })
            .state('tabsController.usage', {
                url: '/usage',
                views: {
                    'usage_tab': {
                        templateUrl: UIPath + 'home/usage/usage.html',
                        controller: 'usageCtrl'
                    }
                }
            })
            .state('tabsController.settingPage', {
                url: '/setting_page',
                views: {
                    'setting_tab': {
                        templateUrl: UIPath + 'setting/settingpage/setting_page.html',
                        controller: 'settingPageCtrl'
                    }
                }
            })
            .state('tabsController.assetsStatistic',{
                url:'/assetsStatistic',
                views: {
                    'assets_tab': {
                        templateUrl: UIPath + 'home/others/assetsStatistic/assetsStatistic.html',
                        controller: 'assetsStatisticCtrl'
                    }
                },
            })
            .state('communicate_detail', {
                url: '/communicate_detail',
                templateUrl: UIPath + 'interactive/communicate_detail.html',
                controller: 'communicateDetailCtrl',
                params: {
                    obj: null
                }
            })
            .state('pdfView', {
                url: '/pdfView',
                templateUrl: UIPath + 'common/pdfView.html',
                controller: 'pdfViewCtrl',
                params: {
                    url: null
                }
            })
            .state('devicesOnline', {
                url: '/devicesOnline',
                templateUrl: UIPath + 'home/devicesOnline/devicesOnline.html',
                controller: 'devicesOnlineCtrl'
            })
            .state('general', {
                url: '/general',
                templateUrl: UIPath + 'home/general/general.html',
                controller: 'generalCtrl'
            })
            .state('usage', {
                url: '/usage',
                templateUrl: UIPath + 'home/usage/usage.html',
                controller: 'usageCtrl'
            })
            .state('terminalLocation', {
                url: '/terminalLocation',
                templateUrl: UIPath + 'home/terminalLocation/terminalLocation.html',
                controller: 'terminalLocationCtrl'
            })
            .state('terminalList', {
                url: '/terminalList',
                templateUrl: UIPath + 'home/terminalList/terminalList.html',
                controller: 'terminalListCtrl'
            })
            .state('terminalUsageInfo', {
                url: '/terminalUsageInfo',
                templateUrl: UIPath + 'home/terminalUsageInfo/terminalUsageInfo.html',
                controller: 'terminalUsageInfoCtrl',
                params: {
                    id: null
                }
            })
            .state('rank', {
                url: '/rank',
                templateUrl: UIPath + 'home/rank/rank.html',
                controller: 'rankCtrl',
                params: {
                    type: null
                }
            })
            .state('contact_profile', {
                url: '/contact_profile',
                templateUrl: UIPath + 'interactive/contact_profile.html',
                controller: 'contactProfileCtrl',
                params: {
                    obj: null,
                    self: false
                }
            })
            .state('tabsController', {
                url: '/tab',
                templateUrl: UIPath + 'tabs/tabsController.html',
                controller: 'tabsCtrl',
                abstract: true
            })
            .state('login', {
                url: '/login',
                templateUrl: UIPath + 'login/login.html',
                controller: 'loginCtrl'
            })
            .state('user_profile', {
                url: '/user_profile',
                templateUrl: UIPath + 'setting/userProfile/user_profile.html',
                controller: 'userProfileCtrl'
            })
            .state('edit_profile', {
                url: '/edit_profile',
                templateUrl: UIPath + 'setting/userProfile/edit_profile.html',
                controller: 'editProfileCtrl',
                params: {
                    content: null
                }
            })
            .state('news_detail', {
                url: '/news_detail',
                templateUrl: UIPath + 'home/newsdetail/news_detail.html',
                controller: 'newsDetailCtrl',
                params: {
                    post: null,
                    notice: null,
                    index: null
                }
            })
            .state('generalAbout', {
                url: '/generalAbout',
                templateUrl: UIPath + 'home/generalAbout/generalAbout.html',
                controller: 'generalAboutCtrl'
            })
            .state('about', {
                url: '/about',
                templateUrl: UIPath + 'setting/aboutpage/about.html',
                controller: 'aboutCtrl'
            })
            .state('suggest', {
                url: '/suggest',
                templateUrl: UIPath + 'setting/suggestpage/suggest.html',
                controller: 'suggestCtrl'
            })
            .state('notification_setting', {
                url: '/notification_setting',
                templateUrl: UIPath + 'setting/notificationpage/notification_setting.html',
                controller: 'notificationSettingCtrl'
            })
            .state('classAppraisalList',{
                url:'/classAppraisalList',
                templateUrl:UIPath+'home/classAppraisal/classAppraisalList.html',
                controller:'classAppraisalListCtrl'
            })
            .state('classAppraisalDetail',{
                url:'/classAppraisalDetail',
                templateUrl:UIPath+'home/classAppraisal/classAppraisalDetail.html',
                controller:'classAppraisalDetailCtrl',
                params:{
                    voteTaskId:null,
                    title:null
                }
            })
            .state('versionIntroduce',{
                url:'/versionIntroduce',
                templateUrl:UIPath +'setting/versionIntroduce/versionIntroduce.html',
                controller:'versionIntroduceCtrl'
            })
            .state('abnormalHandle',{
              url:'/abnormalHandle',
              templateUrl:UIPath+'common/abnormalHandle.html',
              controller:'abnormalHandleCtrl',
              params:{
                  notice:null
              }
            })
            .state('repairRecordList',{
                url:'/repairRecordList',
                templateUrl:UIPath + 'home/equipmentRepair/repairRecordList.html',
                controller:'repairRecordListCtrl'
            })
            .state('repairDetail',{
                url:'/repairDetail',
                templateUrl:UIPath + 'home/equipmentRepair/repairDetail.html',
                controller:'repairDetailCtrl',
                params:{
                    repairRecordId:null,
                    readStatus:null
                }
            })
            .state('repairProgress',{
                url:'/repairProgress',
                templateUrl :UIPath + 'home/equipmentRepair/repairProgress.html',
                controller:'repairProgressCtrl',
                params:{
                    repairRecordId:null,
                    manufacturersContactId:null ,//售后人员id
                    manufacturersId:null,//厂家id 
                    isAssociated:null //表示是否已经关联了厂家
                }
            })
            .state('assetsCheckList',{
                    url:'/assetsCheckList',
                    templateUrl :UIPath + 'home/assets/assetsCheckList.html',
                    controller:'assetsCheckListCtrl'
                })
             .state('assetsCheckDetail',{
                    url:'/assetsCheckDetail',
                    templateUrl :UIPath + 'home/assets/assetsCheckDetail.html',
                    controller:'assetsCheckDetailCtrl',
                    params:{
                        assetsId:null,
                        progressStatus:null
                    }
                })
                .state('approvalList',{
                    url:'/approvalList',
                    templateUrl :UIPath + 'home/approvalAdmin/approvalList.html',
                    controller:'approvalListCtrl'
                })
                .state('approvalDetail',{
                    url:'/approvalDetail',
                    templateUrl:UIPath + 'home/approvalAdmin/approvalDetail.html',
                    controller:'approvalDetailCtrl',
                    params:{
                        approvalId:null,
                        
                    }
                })
               //安全管理 演练和安全隐患
                .state('exerciseNoticeList',{
                    url:'/exerciseNoticeList',
                    templateUrl:UIPath + 'home/others/safetyManage/exerciseNoticeList.html',
                    controller :'exerciseNoticeListCtrl'
                })
                .state('exerciseNoticeDetail',{
                    url:'/exerciseNoticeDetail',
                    templateUrl:UIPath + 'home/others/safetyManage/exerciseNoticeDetail.html',
                    controller :'exerciseNoticeDetailCtrl',
                    params: {
                        itemId:null,
                        title:null
                    }
                })
                .state('exerciseRecord',{
                    url:'/exerciseRecord',
                    templateUrl:UIPath + 'home/others/safetyManage/exerciseRecord.html',
                    controller :'exerciseRecordCtrl',
                    params:{
                        itemId:null,
                        startTime:null,
                        endTime:null
                        
                    }
                })
                .state('addExerciseModal',{
                    url:'/addExerciseModal',
                    templateUrl:UIPath + 'home/others/safetyManage/addExerciseModal.html',
                    controller :'addExerciseModalCtrl',
                    params:{
                        programId:null ,
                        programName:null
                    }
                })
                .state('hidenTroubleList',{
                    url:'/hidenTroubleList',
                    templateUrl:UIPath + 'home/others/safetyManage/hidenTroubleList.html',
                    controller :'hidenTroubleListCtrl'
                })
                .state('hidenTroubleDetail',{
                    url:'/hidenTroubleDetail',
                    templateUrl:UIPath + 'home/others/safetyManage/hidenTroubleDetail.html',
                    controller :'hidenTroubleDetailCtrl',
                    params: {
                        itemId:null
                    }
                })
                .state('hidenTroubleReport',{
                    url:'/hidenTroubleReport',
                    templateUrl:UIPath + 'home/others/safetyManage/hidenTroubleReport.html',
                    controller :'hidenTroubleReportCtrl'
                })    
                .state('assetsScanning',{
                    url:'/assetsScanning',
                    templateUrl:UIPath +'home/assets/assetsScanning.html',
                    controller:'assetsScanningCtrl'
                })
                .state('assetsScanningDetail',{
                    url:'/assetsScanningDetail',
                    templateUrl:UIPath +'home/assets/assetsScanningDetail.html',
                    controller:'assetsScanningDetailCtrl'
                })
                .state('portraitList',{
                    url:'/portraitList',
                    templateUrl:UIPath +'home/others/portrait/portraitList.html',
                    controller:'portraitListCtrl'
                })
                .state('babyVideoList',{
                    url:'/babyVideoList',
                    templateUrl:UIPath + 'home/babyVideo/babyVideoList.html',
                    controller :'babyVideoListCtrl' 
                })
                .state('babyVideoDetail',{
                    url:'/babyVideoDetail',
                    templateUrl:UIPath + 'home/babyVideo/babyVideoDetail.html',
                    controller :'babyVideoDetailCtrl' ,
                    params:{
                        url:''
                    }
                })
                .state('covidDailySummery',{
                    url:'/covidDailySummery',
                    templateUrl:UIPath + 'home/covid/dailySummery.html',
                    controller :'covidDailySummeryCtrl' 
                })
                .state('covidWarningDetail',{
                    url:'/covidWarningDetail',
                    templateUrl:UIPath + 'home/covid/warningDetail.html',
                    controller :'covidWarningDetailCtrl',
                    params: {
                        id: {}
                    }
                })
                .state('userProtocol',{
                   url:'/userProtocol' ,
                   templateUrl :UIPath + 'setting/userprotocol/userProtocol.html',
                   controller:'userProtocolCtrl'
                })
            ;

        $urlRouterProvider.otherwise(function ($injector) {
            var $state = $injector.get("$state");
            $state.go('login');
        });

    }])
    .run(['$rootScope', '$state', 'UserPreference', 'Constant', function ($rootScope, $state, UserPreference, Constant) {
        $rootScope.$on('$stateChangeStart', function (event, next) {
            if (next.url === $state.current.url && next.url !== '/news_detail') {
                if (Constant.debugMode) console.log("same page, go nowhere..");
                event.preventDefault();
            }
        });
    }]);
