/**
 * Created by hewz on 2018/4/2.
 */
angular.module('app.controllers')
    .controller('pdfViewCtrl', ['$scope', '$http', '$stateParams', '$ionicHistory', function ($scope, $http, $stateParams, $ionicHistory) {

        $scope.$on("$ionicView.enter", function (event, data) {
            $scope.url = $stateParams.url;
            if ($scope.url) {
                $http.get($scope.url, {
                    responseType: 'arraybuffer'
                }).then(function (response) {
                    $scope.pdfdata = new Uint8Array(response.data);
                });
            } else
                $ionicHistory.goBack();
        });

    }]);