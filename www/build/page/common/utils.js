/**
 * Created by hewz on 16/8/9.
 */

Date.prototype.Format = function (fmt) {
	var o = {
		"M+": this.getMonth() + 1, //月份
		"d+": this.getDate(), //日
		"h+": this.getHours(), //小时
		"m+": this.getMinutes(), //分
		"s+": this.getSeconds(), //秒
		"q+": Math.floor((this.getMonth() + 3) / 3), //季度
		"S": this.getMilliseconds() //毫秒
	};
	if (/(y+)/.test(fmt))
		fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	for (var k in o)
		if (new RegExp("(" + k + ")").test(fmt))
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
	return fmt;
};

function formatTimeWithoutSecends(timestamp, format) {
	if (!timestamp) {
		return 0;
	}
	var formatTime;
	format = format || 'yyyy-MM-dd hh:mm';
	var date = new Date(timestamp * 1000);
	var o = {
		"M+": date.getMonth() + 1, //月份
		"d+": date.getDate(), //日
		"h+": date.getHours(), //小时
		"m+": date.getMinutes(), //分
		"s+": date.getSeconds() //秒
	};
	if (/(y+)/.test(format)) {
		formatTime = format.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
	} else {
		formatTime = format;
	}
	for (var k in o) {
		if (new RegExp("(" + k + ")").test(formatTime))
			formatTime = formatTime.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
	}
	return formatTime;
}
//将文本转化成html-suggest
function encodeHtml(str) {
	var html = '';
	html += webim.Tool.formatText2Html(str);

	var urls = /(\b(https?|ftp):\/\/[A-Z0-9+&@#\/%?=~_|!:,.;-]*[-A-Z0-9+&@#\/%=~_|])/gim;
	if (html.match(urls)) {
		html = html.replace(urls, "<a href=\"$1\" target=\"_system\">$1</a>");
	}
	return html;
}

function getWeekday(date) {
	if (date && date.length == 19) {
		var d = new Date(date.replace(/\s+/g, 'T').concat('.000+08:00'));
		return date.substr(0, 10) + " " + getWeekdayByIndex(d.getDay());
	}
	return date;
}

function getWeekdayByIndex(dayIndex) {
	switch (String(dayIndex)) {
		case '1':
			return '周一';
		case '2':
			return '周二';
		case '3':
			return '周三';
		case '4':
			return '周四';
		case '5':
			return '周五';
		case '6':
			return '周六';
		case '0':
			return '周日';
		default:
			return '';
	}
}

function delHtmlTag(str) {
	if (str) {
		str = str.replace(/<\/?[^>]*>/g, ''); //去除HTML tag
		str = str.replace(/[ | ]*\n/g, '\n'); //去除行尾空白
		str = str.replace(/\n[\s| | ]*\r/g, '\n'); //去除多余空行
		str = str.replace(/&nbsp;/ig, ''); //去掉&nbsp;
	}
	return str;
}

function convertMsgtoPushStr(msg) {
	var html = "",
		elems, elem, type, content;
	elems = msg.getElems(); //获取消息包含的元素数组
	var count = elems.length;
	for (var i = 0; i < count; i++) {
		elem = elems[i];
		type = elem.getType(); //获取元素类型
		content = elem.getContent(); //获取元素对象
		var eleHtml;
		switch (type) {
			case webim.MSG_ELEMENT_TYPE.TEXT:
				eleHtml = convertTextMsgToHtml(content);
				//转义，防XSS
				html += webim.Tool.formatText2Html(eleHtml);
				break;
			case webim.MSG_ELEMENT_TYPE.FACE:
				html += getFaceIndexStr(content);
				break;
			case webim.MSG_ELEMENT_TYPE.IMAGE:
				html += '[图片]';
				break;
			default:
				html += '[未知消息类型]';
				webim.Log.error('未知消息元素类型');
				break;
		}
		if (html.length > 20)
			break;
	}
	return delHtmlTag(html);
}

function getFaceIndexStr(content) {
	var emotionDataIndexs = [
		"[惊讶]", "[撇嘴]", "[色]", "[发呆]", "[得意]", "[流泪]", "[害羞]", "[闭嘴]",
		"[睡]", "[大哭]", "[尴尬]", "[发怒]", "[调皮]", "[龇牙]", "[微笑]", "[难过]",
		"[酷]", "[冷汗]", "[抓狂]", "[吐]", "[偷笑]", "[可爱]", "[白眼]", "[傲慢]",
		"[饿]", "[困]", "[惊恐]", "[流汗]", "[憨笑]", "[大兵]", "[奋斗]", "[咒骂]",
		"[疑问]", "[嘘]", "[晕]"
	];
	var data = content.getData();
	var index = webim.EmotionDataIndexs[data];
	if (index >= 0 && index < emotionDataIndexs.length)
		return emotionDataIndexs[index];
	else
		return '';
}

function getSuggestEmotions() {
	var EMOJIS =
		"😀 😃 😄 😁 😆 😅 😂 😊 😇 🙂 😉 😌 😭 😍 😘 😗 😙 😚 😋 😜 😝 😛 🤑 🤗 🤓 😎 😏 😒 😞 😔 😟 😕 🙁 ☹️" +
		" 😣 😖 😫 😩 😤 😠 😡 😶 😐 😑 😯 😦 😧 😮 😲 😵 😳 😱 😨 😰 😢 😥 😓 😪 😴 🙄 🤔 😬 🤐 😷 🤒 🤕 😈 👿 👹 👺" +
		" 💩 👻 💀 ☠️ 👽 👾 🤖 🎃 😺 😸 😹 😻 😼 😽 🙀 😿 😾 👐 🙌 👏 🙏 🤝 👍 👎 👊 ✊" +
		" 👋 🤙 💪 🖕 ✍️ 🤳 💅 🖖 💄 💋 👄 👅 👂 👃 👣 👁 👀 🗣 👤 👥 🕶 🌂 ☂️";
	var EmojiArr = EMOJIS.split(" ");
	var groupNum = Math.ceil(EmojiArr.length / 24);
	var items = [];

	for (var i = 0; i < groupNum; i++) {
		items.push(EmojiArr.slice(i * 24, (i + 1) * 24));
	}
	return items;
}

//将文本转化成html-suggest
function encodeHtml(str) {
	var html = '';
	html += webim.Tool.formatText2Html(str);

	var urls = /(\b(https?|ftp):\/\/[A-Z0-9+&@#\/%?=~_|!:,.;-]*[-A-Z0-9+&@#\/%=~_|])/gim;
	if (html.match(urls)) {
		html = html.replace(urls, "<a href=\"$1\" target=\"_system\">$1</a>");
	}
	return html;
}

//把消息转换成Html
function convertMsgtoHtml(msg) {
	var html = "",
		elems, elem, type, content;
	elems = msg.getElems(); //获取消息包含的元素数组
	var count = elems.length;
	for (var i = 0; i < count; i++) {
		elem = elems[i];
		type = elem.getType(); //获取元素类型
		content = elem.getContent(); //获取元素对象
		var eleHtml;
		switch (type) {
			case webim.MSG_ELEMENT_TYPE.TEXT:
				eleHtml = convertTextMsgToHtml(content);
				//转义，防XSS
				html += webim.Tool.formatText2Html(eleHtml);
				break;
			case webim.MSG_ELEMENT_TYPE.FACE:
				html += convertFaceMsgToHtml(content);
				break;
			case webim.MSG_ELEMENT_TYPE.IMAGE:
				if (i <= count - 2) {
					var customMsgElem = elems[i + 1]; //获取保存图片名称的自定义消息elem
					var imgName = customMsgElem.getContent().getData(); //业务可以自定义保存字段，demo中采用data字段保存图片文件名
					html += convertImageMsgToHtml(content, imgName);
					i++; //下标向后移一位
				} else {
					html += convertImageMsgToHtml(content);
				}
				break;
			case webim.MSG_ELEMENT_TYPE.SOUND:
				html += convertSoundMsgToHtml(content);
				break;
			case webim.MSG_ELEMENT_TYPE.FILE:
				html += convertFileMsgToHtml(content);
				break;
			case webim.MSG_ELEMENT_TYPE.LOCATION:
				html += convertLocationMsgToHtml(content);
				break;
			case webim.MSG_ELEMENT_TYPE.CUSTOM:
				eleHtml = convertCustomMsgToHtml(content);
				//转义，防XSS
				html += webim.Tool.formatText2Html(eleHtml);
				break;
			case webim.MSG_ELEMENT_TYPE.GROUP_TIP:
				eleHtml = convertGroupTipMsgToHtml(content);
				//转义，防XSS
				html += webim.Tool.formatText2Html(eleHtml);
				break;
			default:
				webim.Log.error('未知消息元素类型: elemType=' + type);
				break;
		}
	}
	if (type == webim.MSG_ELEMENT_TYPE.TEXT) {
		var urls = /(\b(https?|ftp):\/\/[A-Z0-9+&@#\/%?=~_|!:,.;-]*[-A-Z0-9+&@#\/%=~_|])/gim;
		if (html.match(urls)) {
			html = html.replace(urls, "<a href=\"$1\" target=\"_system\">$1</a>");
		}
	}
	return html;
}

//解析文本消息元素
function convertTextMsgToHtml(content) {
	return content.getText();
}
//解析表情消息元素
function convertFaceMsgToHtml(content) {
	var faceUrl = null;
	var data = content.getData();
	var index = webim.EmotionDataIndexs[data];

	var emotion = webim.Emotions[index];
	if (emotion && emotion[1]) {
		faceUrl = emotion[1];
	}
	if (faceUrl) {
		return "<img src='" + faceUrl + "'/>";
	} else {
		return data;
	}
}
//解析图片消息元素
function convertImageMsgToHtml(content, imageName) {
	var smallImage = content.getImage(webim.IMAGE_TYPE.SMALL); //小图
	var bigImage = content.getImage(webim.IMAGE_TYPE.LARGE); //大图
	var oriImage = content.getImage(webim.IMAGE_TYPE.ORIGIN); //原图
	if (!bigImage) {
		bigImage = smallImage;
	}
	if (!oriImage) {
		oriImage = smallImage;
	}
	var bigUrl = bigImage.getUrl();
	return '<img name="' + imageName + '" src="' + smallImage.getUrl() + '" id="' + content.getImageId() + '" ng-click=\'onImageClick("' + bigUrl + '")\' style="max-width: 100%" ion-img-cache />';
}
//解析语音消息元素
function convertSoundMsgToHtml(content) {
	var second = content.getSecond(); //获取语音时长
	var downUrl = content.getDownUrl();
	if (webim.BROWSER_INFO.type == 'ie' && parseInt(webim.BROWSER_INFO.ver) <= 8) {
		return '[这是一条语音消息]demo暂不支持ie8(含)以下浏览器播放语音,语音URL:' + downUrl;
	}
	return '<audio id="uuid_' + content.uuid + '" src="' + downUrl + '" controls="controls" onplay="onChangePlayAudio(this)" preload="none"></audio>';
}
//解析文件消息元素
function convertFileMsgToHtml(content) {
	var fileSize, unitStr;
	fileSize = content.getSize();
	unitStr = "Byte";
	if (fileSize >= 1024) {
		fileSize = Math.round(fileSize / 1024);
		unitStr = "KB";
	}
	// return '<a href="' + content.getDownUrl() + '" title="点击下载文件" ><i class="glyphicon glyphicon-file">&nbsp;' + content.getName() + '(' + fileSize + unitStr + ')</i></a>';

	return '<a href="javascript:;" onclick=\'webim.onDownFile("' + content.uuid + '")\' title="点击下载文件" ><i class="glyphicon glyphicon-file">&nbsp;' + content.name + '(' + fileSize + unitStr + ')</i></a>';
}
//解析位置消息元素
function convertLocationMsgToHtml(content) {
	return '经度=' + content.getLongitude() + ',纬度=' + content.getLatitude() + ',描述=' + content.getDesc();
}
//解析自定义消息元素
function convertCustomMsgToHtml(content) {
	var data = content.getData(); //自定义数据
	var desc = content.getDesc(); //描述信息
	var ext = content.getExt(); //扩展信息
	return "data=" + data + ", desc=" + desc + ", ext=" + ext;
}
//解析群提示消息元素
function convertGroupTipMsgToHtml(content) {
	var WEB_IM_GROUP_TIP_MAX_USER_COUNT = 5;
	var text = "";
	var maxIndex = WEB_IM_GROUP_TIP_MAX_USER_COUNT - 1;
	var opType, opUserId, userIdList;
	opType = content.getOpType(); //群提示消息类型（操作类型）
	opUserId = content.getOpUserId(); //操作人id
	var service = angular.element(document.querySelector('[ng-app]')).injector().get("ChatService");
	var info = service.getMemberInfoFromMap(webim.SESSION_TYPE.C2C, opUserId);
	var opUserName = info ? info.name : opUserId;
	if (opUserName === 'XgbAdmin')
		opUserName = '管理员';
	var tmp, username;
	switch (opType) {
		case webim.GROUP_TIP_TYPE.JOIN: //加入群
			userIdList = content.getUserIdList();
			for (var m in userIdList) {
				tmp = service.getMemberInfoFromMap(webim.SESSION_TYPE.C2C, userIdList[m]);
				username = tmp ? tmp.name : '';
				if (username !== '')
					text += (username + '、');
				if (userIdList.length > WEB_IM_GROUP_TIP_MAX_USER_COUNT && m == maxIndex) {
					break;
				}
			}
			if (text.length > 0) {
				text = text.substring(0, text.length - 1);
				if (userIdList.length > WEB_IM_GROUP_TIP_MAX_USER_COUNT)
					text += "等共" + userIdList.length + "人";
				text += "加入该群";
			} else
				return '';
			break;
		case webim.GROUP_TIP_TYPE.QUIT: //退出群
			text += opUserName + "离开该群";
			break;
		case webim.GROUP_TIP_TYPE.KICK: //踢出群
			var tem = opUserName + "将";
			userIdList = content.getUserIdList();
			for (var mm in userIdList) {
				tmp = service.getMemberInfoFromMap(webim.SESSION_TYPE.C2C, userIdList[mm]);
				username = tmp ? tmp.name : '';
				if (username !== '')
					text += (username + '、');
				if (userIdList.length > WEB_IM_GROUP_TIP_MAX_USER_COUNT && mm == maxIndex) {
					break;
				}
			}
			if (text.length > 0) {
				text = text.substring(0, text.length - 1);
				if (userIdList.length > WEB_IM_GROUP_TIP_MAX_USER_COUNT)
					text += "等共" + userIdList.length + "人";
				text = tem + text + "移出该群";
			} else
				return '';
			break;
		case webim.GROUP_TIP_TYPE.SET_ADMIN: //设置管理员
			text += opUserName + "将";
			userIdList = content.getUserIdList();
			for (var mmm in userIdList) {
				tmp = service.getMemberInfoFromMap(webim.SESSION_TYPE.C2C, userIdList[mmm]);
				username = tmp ? tmp.name : userIdList[mmm];
				text += (username + '、');
				if (userIdList.length > WEB_IM_GROUP_TIP_MAX_USER_COUNT && mmm == maxIndex) {
					text += "等共" + userIdList.length + "人";
					break;
				}
			}
			text += "设为管理员";
			break;
		case webim.GROUP_TIP_TYPE.CANCEL_ADMIN: //取消管理员
			text += opUserName + "取消";
			userIdList = content.getUserIdList();
			for (var m4 in userIdList) {
				tmp = service.getMemberInfoFromMap(webim.SESSION_TYPE.C2C, userIdList[m4]);
				username = tmp ? tmp.name : userIdList[m4];
				text += (username + '、');
				if (userIdList.length > WEB_IM_GROUP_TIP_MAX_USER_COUNT && m4 == maxIndex) {
					text += "等共" + userIdList.length + "人";
					break;
				}
			}
			text += "的管理员资格";
			break;

		case webim.GROUP_TIP_TYPE.MODIFY_GROUP_INFO: //群资料变更
			text += opUserName + "修改";
			var groupInfoList = content.getGroupInfoList();
			var type, value;
			for (var m5 in groupInfoList) {
				type = groupInfoList[m5].getType();
				value = groupInfoList[m5].getValue();
				switch (type) {
					case webim.GROUP_TIP_MODIFY_GROUP_INFO_TYPE.FACE_URL:
						text += "了群头像";
						break;
					case webim.GROUP_TIP_MODIFY_GROUP_INFO_TYPE.NAME:
						text += "群名称为" + value;
						break;
					case webim.GROUP_TIP_MODIFY_GROUP_INFO_TYPE.OWNER:
						text += "群主为" + value;
						break;
					case webim.GROUP_TIP_MODIFY_GROUP_INFO_TYPE.NOTIFICATION:
						text += "群公告为" + value;
						break;
					case webim.GROUP_TIP_MODIFY_GROUP_INFO_TYPE.INTRODUCTION:
						text += "群简介为" + value;
						break;
					default:
						text += "未知信息为:type=" + type + ",value=" + value;
						break;
				}
			}
			break;

		case webim.GROUP_TIP_TYPE.MODIFY_MEMBER_INFO: //群成员资料变更(禁言时间)
			text += opUserName + "修改了群成员资料:";
			var memberInfoList = content.getMemberInfoList();
			var userId, shutupTime;
			for (var m6 in memberInfoList) {
				userId = memberInfoList[m6].getUserId();
				shutupTime = memberInfoList[m6].getShutupTime();
				text += userId + ": ";
				if (shutupTime !== null && shutupTime !== undefined) {
					if (shutupTime === 0) {
						text += "取消禁言; ";
					} else {
						text += "禁言" + shutupTime + "秒; ";
					}
				} else {
					text += " shutupTime为空";
				}
				if (memberInfoList.length > WEB_IM_GROUP_TIP_MAX_USER_COUNT && m6 == maxIndex) {
					text += "等" + memberInfoList.length + "人";
					break;
				}
			}
			break;
		default:
			text += "未知群提示消息类型：type=" + opType;
			break;
	}
	return text;
}

function convertImgToBase64URL(url, callback, outputFormat) {
	var img = new Image();
	img.crossOrigin = 'Anonymous';
	img.onload = function () {
		var canvas = document.createElement('CANVAS'),
			ctx = canvas.getContext('2d'),
			dataURL;
		canvas.height = this.height;
		canvas.width = this.width;
		ctx.drawImage(this, 0, 0);
		dataURL = canvas.toDataURL(outputFormat);
		callback(dataURL);
		canvas = null;
	};
	img.src = url;
}

function isEmptyObject(e) {
	var t;
	for (t in e)
		return !1;
	return !0;
}

/**
 * 将对象参数序列化
 * @return {string}
 */
function CustomParam(obj) {
	if (!angular.isObject(obj)) {
		return ((obj === null) ? "" : obj.toString());
	}
	var query = '',
		name, value, fullSubName, subName, subValue, innerObj, i;
	for (name in obj) {
		value = obj[name];
		if (value instanceof Array) {
			for (i in value) {
				subValue = value[i];
				innerObj = {};
				innerObj[name] = subValue;
				query += CustomParam(innerObj) + '&';
			}
		} else if (value instanceof Object) {
			for (subName in value) {
				subValue = value[subName];
				fullSubName = name + '[' + subName + ']';
				innerObj = {};
				innerObj[fullSubName] = subValue;
				query += CustomParam(innerObj) + '&';
			}
		} else if (value !== undefined && value !== null)
			query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
	}
	return query.length ? query.substr(0, query.length - 1) : query;
}


function getChatTimeLabel(msgTime) {
	if (msgTime > 0) {
		var now = new Date();
		var startOfDay = new Date(now.getFullYear(), now.getMonth(), now.getDate());
		var timestamp = startOfDay / 1000;
		var strTime = webim.Tool.formatTimeStamp(msgTime);
		return msgTime > timestamp ? strTime.substr(11, 5) : strTime.substr(5, 5);
	}
	return '';
}

function resetHMSM(currentDate) {
	currentDate.setHours(0);
	currentDate.setMinutes(0);
	currentDate.setSeconds(0);
	currentDate.setMilliseconds(0);
	return currentDate;
}

function insertEnter2Txt(str) {
	var newstr = "";
	for (var i = 0; i < str.length; i += 9) {
		var tmp = str.substring(i, i + 9);
		newstr += tmp + '\n';
	}
	return newstr;
}

//定位光标
function setCaretPosition(tObj, sPos) {
	if (tObj.setSelectionRange) {
		setTimeout(function () {
			tObj.setSelectionRange(sPos + 1, sPos + 1);
			tObj.focus();
		}, 10);
	} else if (tObj.createTextRange) {
		var rng = tObj.createTextRange();
		rng.move('character', sPos);
		rng.select();
	}
}

//获取当前时间
function getNowFormatDate() {
	var date = new Date();
	var seperator1 = "-";
	var seperator2 = ":";
	var month = date.getMonth() + 1;
	var strDate = date.getDate();
	if (month >= 1 && month <= 9) {
		month = "0" + month;
	}
	if (strDate >= 0 && strDate <= 9) {
		strDate = "0" + strDate;
	}
	var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate + " " + date.getHours() + seperator2 + date.getMinutes();
	return currentdate;
}

//获取当前时间
function getNowDate(str) {
	var str1;
	var date;
	if (str) {
		str1 = str.replace(/\-/g, "/");	
		date = new Date(str1);
	}else{
		date = new Date();
	}
	var seperator1 = "-";
	var seperator2 = ":";
	var month = date.getMonth() + 1;
	var strDate = date.getDate();
	if (month >= 1 && month <= 9) {
		month = "0" + month;
	}
	if (strDate >= 0 && strDate <= 9) {
		strDate = "0" + strDate;
	}
	var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate + " " + '00' + seperator2 + '00';
	return currentdate;
}

/**
 * Resize Image
 * @param longSideMax
 * @param url
 * @return promise
 */
function resizeImage(longSideMax, url) {
	var $q = angular.injector(['ng']).get('$q');
	var defer = $q.defer();
	var tempImg = new Image();
	tempImg.src = url;
	tempImg.onload = function () {
		// Get image size and aspect ratio.
		var targetWidth = tempImg.width;
		var targetHeight = tempImg.height;
		var aspect = tempImg.width / tempImg.height;

		// Calculate shorter side length, keeping aspect ratio on image.
		// If source image size is less than given longSideMax, then it need to be
		// considered instead.
		if (tempImg.width > tempImg.height) {
			longSideMax = Math.min(tempImg.width, longSideMax);
			targetWidth = longSideMax;
			targetHeight = longSideMax / aspect;
		} else {
			longSideMax = Math.min(tempImg.height, longSideMax);
			targetHeight = longSideMax;
			targetWidth = longSideMax * aspect;
		}

		// Create canvas of required size.
		var canvas = document.createElement('canvas');
		canvas.width = targetWidth;
		canvas.height = targetHeight;

		var ctx = canvas.getContext("2d");
		// Take image from top left corner to bottom right corner and draw the image
		// on canvas to completely fill into.
		ctx.drawImage(this, 0, 0, tempImg.width, tempImg.height, 0, 0, targetWidth, targetHeight);
		defer.resolve(canvas.toDataURL("image/jpeg"));
	};
	return defer.promise;
}


//判断一个数是否为大于0的整数
function judgeIsInteger(obj) {
	var num = Number(obj);
	return typeof num === 'number' && num % 1 === 0 && num > 0 && num <= 100;

}

function isEmojiCharacter(substring) {
	for (var i = 0; i < substring.length; i++) {
		var hs = substring.charCodeAt(i);
		if (0xd800 <= hs && hs <= 0xdbff) {
			if (substring.length > 1) {
				var ls = substring.charCodeAt(i + 1);
				var uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
				if (0x1d000 <= uc && uc <= 0x1f77f) {
					return true;
				}
			}
		} else if (substring.length > 1) {
			var ls1 = substring.charCodeAt(i + 1);
			if (ls1 == 0x20e3) {
				return true;
			}
		} else {
			if (0x2100 <= hs && hs <= 0x27ff) {
				return true;
			} else if (0x2B05 <= hs && hs <= 0x2b07) {
				return true;
			} else if (0x2934 <= hs && hs <= 0x2935) {
				return true;
			} else if (0x3297 <= hs && hs <= 0x3299) {
				return true;
			} else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50) {
				return true;
			}
		}
	}
}

/**
 1. @param str 要计算的字符串
 2. @param fontSize 字符串的字体大小
 3. 根据需要，还可以添加字体控制，如微软雅黑与Times New Roma的字符宽度肯定不一致
 */
function textWidth(str, fontSize) {
	if (!textWidth.txt) {
		var txt = document.createElement('span');
		txt.style.position = 'absolute';
		//  避免遮挡其他元素
		txt.style.zIndex = -10;
		//  千万不可设置为display：none；
		txt.style.visibility = 'hidden';
		//  一定要加载到DOM中才能计算出字符宽度
		document.body.appendChild(txt);
		textWidth.txt = txt;
	}
	//  控制字符的字体大小
	textWidth.txt.style.fontSize = fontSize + 'px';
	//  设置字符内容
	textWidth.txt.textContent = str;
	//  返回计算结果
	return textWidth.txt.offsetWidth;
}

/**
 * 判断文本中是否由数字和字母组成
 */
function checkStringIsIncludeSpecialChar(str) {
	var re = /^[0-9a-zA-Z]*$/; //判断字符串是否为数字和字母组合     
	if (!re.test(str)) {
		return false;
	} else {
		return true;
	}
}

/**
 * 是不是ios设备
 */
function iOSDevice(){
	var isIosDevice ;
	if(window.cordova){
		isIosDevice = ionic.Platform.isIOS();
	}
	
     return isIosDevice;
}